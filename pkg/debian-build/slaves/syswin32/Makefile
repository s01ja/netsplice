SHELL = /bin/bash
INSTALLABLES_DIR=/var/installables
BIN_DIR=/var/tmp/bin
TMP_DIR=/var/tmp
SRC_DIR=/var/src

DEB_NAME=netsplice
APP_NAME=netsplice
VERSION=$(shell cd $(SRC_DIR) && make -s version)
SVERSION=$(shell echo $(VERSION) | sed 's|[^a-zA-Z0-9_\.-]|_|g')
BUILD_DIR := $(TMP_DIR)/$(APP_NAME)-$(SVERSION)

COMPOSE_SRC_ROOT=$(shell cat $(BUILD_DIR)/pkg/debian-build/docker-compose.yml | grep SRC_ROOT | sed 's|.*SRC_ROOT=||')
COMPOSE_SLAVE_ROOT=$(COMPOSE_SRC_ROOT)/var/tmp/win32/$(APP_NAME)-$(SVERSION)
COMPOSE_ROOT=$(COMPOSE_SLAVE_ROOT)/pkg/debian-build/slaves/syswin32

APPS = \
  Backend \
  Gui \
  Net \
  Privileged \
  Unprivileged

APPS_NAME = $(APPS:%=Netsplice%App)
APPS_APP = $(APPS_NAME:%=%.app)

OPENVPN_VERSIONS = $(shell bash $(BUILD_DIR)/pkg/debian-build/build.sh.inc --variable OPENVPN_VERSIONS)
TOR_VERSIONS = $(shell bash $(BUILD_DIR)/pkg/debian-build/build.sh.inc --variable TOR_VERSIONS)

OPENVPN_EXE_NAMES = $(OPENVPN_VERSIONS:%=$(TMP_DIR)/executables/libexec/openvpn-v%/bin/openvpn.exe)
TOR_EXE_NAMES = $(TOR_VERSIONS:%=$(TMP_DIR)/executables/libexec/tor-v%/bin/tor.exe)
DEPENDENCY_EXE_NAMES =$(APPS_NAME:%=$(BIN_DIR)/%.pyinst.log)
INSTALLER_EXE_NAME=$(INSTALLABLES_DIR)/$(PKGNAME)-$(VERSIONMAJOR).$(VERSIONMINOR).$(VERSIONBUILD)-$(VERSIONRELEASE).exe
.PHONY: all clean clean-images package

all: docker-compose.yml $(INSTALLER_EXE_NAME)

docker-compose.yml: docker-compose.yml.in
	cat docker-compose.yml.in \
	  | sed 's|COMPOSE_ROOT|$(COMPOSE_ROOT)|;s|SRC_ROOT|$(COMPOSE_SRC_ROOT)|;s|SLAVE_ROOT|$(COMPOSE_SLAVE_ROOT)|' \
	  > docker-compose.yml

$(BIN_DIR)/NetspliceBackendApp.pyinst.log: docker-compose.yml
	mkdir -p $(BIN_DIR)
	rm -r $(TMP_DIR)/executables/$(APP_NAME) || true
	set -o pipefail; \
	  docker-compose run --rm -e BUILD_NAME=backend dependencies \
	  | tee $@

$(BIN_DIR)/NetspliceGuiApp.pyinst.log: docker-compose.yml $(BIN_DIR)/NetspliceBackendApp.pyinst.log
	mkdir -p $(BIN_DIR)
	set -o pipefail; \
	  docker-compose run --rm -e BUILD_NAME=gui dependencies \
	  | tee $@

$(BIN_DIR)/NetsplicePrivilegedApp.pyinst.log: docker-compose.yml $(BIN_DIR)/NetspliceBackendApp.pyinst.log
	mkdir -p $(BIN_DIR)
	set -o pipefail; \
	  docker-compose run --rm -e BUILD_NAME=privileged dependencies \
	  | tee $@

$(BIN_DIR)/NetspliceUnprivilegedApp.pyinst.log: docker-compose.yml $(BIN_DIR)/NetspliceBackendApp.pyinst.log
	mkdir -p $(BIN_DIR)
	set -o pipefail; \
	  docker-compose run --rm -e BUILD_NAME=unprivileged dependencies \
	  | tee $@

$(BIN_DIR)/NetspliceNetApp.pyinst.log: docker-compose.yml $(BIN_DIR)/NetspliceBackendApp.pyinst.log
	mkdir -p $(BIN_DIR)
	set -o pipefail; \
	  docker-compose run --rm -e BUILD_NAME=network dependencies \
	  | tee $@

#		$(OPENVPN_EXE_NAMES)
$(INSTALLER_EXE_NAME): \
		docker-compose.yml \
		$(OPENVPN_EXE_NAMES) \
		$(TOR_EXE_NAMES) \
		$(DEPENDENCY_EXE_NAMES)
	mkdir -p $(INSTALLABLES_DIR)
	chmod 777 $(INSTALLABLES_DIR)
	docker-compose run --rm installer

$(TMP_DIR)/executables/libexec/openvpn-v%/bin/openvpn.exe:
	docker-compose run --rm -e BUILD_OPENVPN_VERSION=$* openvpn

$(TMP_DIR)/executables/libexec/tor-v%/bin/tor.exe:
	docker-compose run --rm -e BUILD_TOR_VERSION=$* tor

clean-images:
	docker rmi windows_dependencies
	docker rmi windows_installer

clean:
	rm -rf $(INSTALLABLE_DIR)/netsplice-*.exe || true
	rm -rf $(TMP_DIR)/installer || true
	# rm -rf $(TMP_DIR)/executables || true
	rm docker-compose.yml ||  true
