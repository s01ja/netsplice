#!/bin/bash

# Render OpenVPN prepared for installer
# ================================================
#
# Requires:
#  - a Linux host with mingw installed
#  - a rw directory mounted to /var/build
# Returns nonzero exit code when failed.
#
# clone openvpn-build repository
# runs cross-compile build
# - downloads OpenVPN dependencies
# - compiles
# Copy files to executables so they can be installed
# Cleans up (remove read-write copy)
# The location where the OpenVPN binaries are placed.

export VERSION=not_required
set -o pipefail
source /usr/local/bin/build.sh.inc

absolute_executable_path=${TMPDIR}/executables
temporary_build_path=${TMPDIR}/openvpn

# Cleanup the temporary build path for subsequent executes.
function cleanup() {
  rm -r ${temporary_build_path} 2>/dev/null
}

# Build OpenVPN source.
function buildSource() {
  export OPENVPN_VERSION=$1
  OPENVPN_VERSION_ONLY=$(echo ${OPENVPN_VERSION} | sed 's|\([^-]*\)-.*|\1|')
  OPENVPN_SPECIAL_ONLY=$(echo ${OPENVPN_VERSION} | sed 's|[^-]*-\(.*\)|\1|')
  # '
  export OPENVPN_URL="https://build.openvpn.net/downloads/releases/openvpn-${OPENVPN_VERSION_ONLY}.tar.gz"
  pushd ${temporary_build_path}/openvpn-build/generic
  echo "Build Version: ${OPENVPN_VERSION}"
  CHOST=i686-w64-mingw32 \
  CBUILD=i686-pc-linux-gnu \
  OPENVPN_VERSION=${OPENVPN_VERSION_ONLY} \
  SPECIAL_BUILD=${OPENVPN_SPECIAL_ONLY} \
  USE_LIBRESSL=$(echo ${OPENVPN_SPECIAL_ONLY} | grep 'libressl') \
  EXTRA_TARGET_CFLAGS=" " \
  ./build 2>&1 | tee build-${OPENVPN_VERSION}.log \
    || die "build openvpn ${OPENVPN_VERSION} from source failed"
  mkdir -p ${absolute_executable_path}/libexec/openvpn/v${OPENVPN_VERSION}/openvpn \
  && cp -r image/openvpn/* \
    ${absolute_executable_path}/libexec/openvpn/v${OPENVPN_VERSION}/openvpn \
  && cp build-${OPENVPN_VERSION}.log \
    ${absolute_executable_path}/libexec/openvpn/v${OPENVPN_VERSION}/openvpn \
  && cp /usr/lib/gcc/i686-w64-mingw32/5.3-win32/libgcc_s_sjlj-1.dll \
    ${absolute_executable_path}/libexec/openvpn/v${OPENVPN_VERSION}/openvpn/sbin \
  || die "copy of compiled files failed"
  popd
  # add shasums of downloaded / used sources in build log
  sha256sum sources
  # remove files that are not required for distribution
  pushd ${absolute_executable_path}/libexec/openvpn/v${OPENVPN_VERSION}/openvpn/sbin
  rm c_rehash ocspcheck* openssl openssl.exe openvpn-gui.exe openvpnserv.exe
  popd
}

# Fetch tap-windows.exe as defined in the OpenVPN vars.
function fetchTapWindows() {
  pushd ${temporary_build_path}/openvpn-build
  mkdir -p ${absolute_executable_path}/openvpn
  cp tap-windows-*.exe ${absolute_executable_path}/openvpn/tap-windows.exe \
    || die 'tap-windows.exe could not be copied'
  popd
}

# Prepare read-write copy.
function prepareBuildPath() {
  export OPENVPN_VERSION=$1
  OPENVPN_VERSION_ONLY=$(echo ${OPENVPN_VERSION} | sed 's|\([^-]*\)-.*|\1|')
  # '
  cleanup
  mkdir -p ${temporary_build_path}
  pushd ${temporary_build_path}
  cp ${DISTFILESDIR}/openvpn-build.tar.bz2 . || die 'no openvpn build package available'
  tar xjf openvpn-build.tar.bz2 || die 'cannot unpack openvpn-build sources'
  pushd openvpn-build/generic/sources
  mv openvpn-${OPENVPN_VERSION_ONLY}.tar.gz .. \
    && mv openvpn-gui-* .. \
    || die 'cannot cleanup openvpn-gui from distfiles'
  rm openvpn* || true
  mv ../openvpn-${OPENVPN_VERSION_ONLY}.tar.gz . \
    && mv ../openvpn-gui-* . \
    || die 'cannot move relevant build sources'
  popd
  cp ${SOURCEDIR}/pkg/libexec/openvpn/v${OPENVPN_VERSION}/* \
    openvpn-build/generic/patches \
    || echo "building openvpn ${OPENVPN_VERSION} without patches"
  popd
}

# Display failure message and emit non-zero exit code.
function die() {
  echo "die:" $@
  exit 1
}

function main() {
  prepareBuildPath ${BUILD_OPENVPN_VERSION}
  fetchTapWindows
  buildSource ${BUILD_OPENVPN_VERSION}
  cleanup
}

main $@
