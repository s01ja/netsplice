#!/bin/bash

# ci script to build from current package
# executed on debian-build (or on osx for development

cd $(dirname $0)
source ./slave-config

function die() {
  echo $@
  exit 1
}

ssh -p ${slave_port} ${user}@${slave} "bash ${root_ro}/pkg/debian-build/slaves/sysosx/bin/build-slave.sh" \
  || die 'Build Slave Failed'
