Setup packaging host
--------------------

Unfortunately it is not yet possible to build packages on a linux host for the
Darwin platform. The workaround is to setup a clean macOS host and install the
usual XCode as well as a ssh server for ci-execution.

Make sure that XCode is installed with the commandline tools::

    sudo xcode-select --install

Setup PIP
---------

macOS does not come with python-pip installed:::

    sudo easy_install pip

Setup PySide
------------

PySide is not a described dependency because its heavy and error-prone to build.

On macOS it is required to ::

    brew install qt

to get a version of Qt4.8 installed locally.

Now install PySide, as user but not into a virtualenv.:::

    brew install cmake
    sudo pip install pyside virtualenv

When you omit sudo, you will get the following output and have to recompile qt again:::

    running install_lib
    creating /Library/Python/2.7/site-packages/PySide
    error: could not create '/Library/Python/2.7/site-packages/PySide': Permission denied

then fix the installation, because it has broken @rpath and relative library
refs, as root:::

    install_name_tool -change \
      @rpath/libshiboken-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/libshiboken-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/libpyside-python2.7.1.2.dylib

    install_name_tool -change \
      @rpath/libpyside-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/libpyside-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/QtGui.so
    install_name_tool -change \
      @rpath/libpyside-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/libpyside-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/QtCore.so

    install_name_tool -change \
      @rpath/libshiboken-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/libshiboken-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/QtGui.so
    install_name_tool -change \
      @rpath/libshiboken-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/libshiboken-python2.7.1.2.dylib \
      /Library/Python/2.7/site-packages/PySide/QtCore.so

copy pyside-rcc from site-packages to /usr/local/bin
check if qt can build resource-files:::

    export DYLD_PRINT_LIBRARIES=1
    pyside-qic
    pyside-rcc

display no errors

then copy the /Library/Python/2.7/site-packages/PySide/ directory into the
virtual-env's site-packages.

then:::

    pip install -r pkg/requirements.pip
    pip install -r pkg/requirements-pkg.pip

Build OpenVPN
-------------

To build OpenVPN some prerequisites are required. The maintainer has to
make sure that those tools are up-to-date before building the installer.

The following software should be installed:::

    brew install automake autoconf libtool lzo

As autotools are not smart, the libtoolize binary needs to be exposed
with its expected name:::

    ln -s /usr/local/bin/glibtoolize /usr/local/bin/libtoolize

OpenVPN can be build directly from the sources. They are a 10MByte download
and only stable tags should be used:::

    git clone https://github.com/OpenVPN/openvpn.git
    git tag -l
    git checkout v2.3.14 # or later

Each build should assume a freshly cloned repository. This adds build-time:::

    autoreconf -i -v -f
    ./configure LDFLAGS=-L/usr/local/opt/openssl/lib CFLAGS=-I/usr/local/opt/openssl/include --prefix=$(pwd)/bin
    # absolute path! choose something _without_ a username, preferably /var/tmp/netsplice
    make
    make install

The OpenVPN binary should now be copied into the App as CWD/openvpn/openvpn.
