#!/bin/bash

source /usr/local/bin/build.sh.inc

unpack

cd ${BUILDDIR}/doc
export PYTHONPATH=${PYTHONPATH}:${BUILDDIR}/src/
make clean \
  || die 'documentation could not be cleaned'

# make sure autodoc can import the requirements
pip install -r ${BUILDDIR}/pkg/requirements.pip

declare -a packages=('html' 'singlehtml' 'man' 'text')

for package in ${packages[@]};
do
  cd ${BUILDDIR}/doc
  make ${package} \
    || die "${package} documentation could not be created"
  cd ${TMPDIR}/doc/build
  # it is sometimes useful to access the generated output without unpacking
  # in CI it is not
  # mkdir -p ${INSTALLABLESDIR}/${PRODUCT}-${VERSION}-documentation-${package}
  # cp -r ${package}/* ${INSTALLABLESDIR}/${PRODUCT}-${VERSION}-documentation-${package}
  tar --transform "s/^${package}/${PRODUCT}-${VERSION}-documentation-${package}/" \
      -cjf \
      ${INSTALLABLESDIR}/${PRODUCT}-${VERSION}-documentation-${package}.tar.bz2 \
      ${package} \
    || die "${package} package could not be created"
done

cd ${BUILDDIR}/doc
make latexpdf \
    || die "latexpdf documentation could not be created"
  cd ${TMPDIR}/doc/build
  mv latex/${PRODUCT_ENDUSER}.pdf \
    ${INSTALLABLESDIR}/${PRODUCT}-${VERSION}-documentation.pdf \
    || die "pdf package could not be created"
