Name:           netsplice
Version:        VERSION
Release:        RELEASE%{?dist}
Summary:        Netsplice is a multiplatform desktop client for VPN services.

License:        GPLv3

URL:            https://ipredator.se/netsplice
# The libexec binaries break rpm...
AutoReqProv:    no
# Requires:       python-pyside

%description
Netsplice is a multiplatform desktop client for VPN services.

#This is a comment (just as example)

%changelog
* Mon Mar 10 2017 autobuild <build@predator.se> 0.2-1.fc25
- introduce requires to allow libexec binaries

* Thu May 26 2016 autobuild <build@predator.se> 0.1-1.fc25
- initial build
