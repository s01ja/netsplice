#!/bin/bash

source /usr/local/bin/build.sh.inc
# set -x
OPENVPN_RELEASES=https://build.openvpn.net/downloads/releases
TOR_RELEASES=https://www.torproject.org/dist
pushd ${TMPDIR}
#
# OpenVPN
#
git clone ${OPENVPN_BUILD_REPOSITORY}
pushd openvpn-build
cp ${SOURCEDIR}/pkg/debian-build/*.patch .
# 2018-03-01
git checkout 105d4dc1d24acdf49ff6d8abaa6479326bb95911

git apply openvpn-build-0-libressl.patch \
  || die 'libressl patch did not apply'
git apply openvpn-build-1-https-only.patch \
  || die 'https only patch did not apply'
git apply openvpn-build-2-extra-target-cflags.patch \
  || die 'extra target cflags patch did not apply'
git apply openvpn-build-3-openssl-switch.patch \
  || die 'switch-openssl patch did not apply'
git apply openvpn-build-4-disable-pkcs11-for-libressl.patch \
  || die 'disable-pkcs11-libressl patch did not apply'

pushd generic
sed -ibak 's|TAP_WINDOWS_VERSION="${TAP_WINDOWS_VERSION:-9.21.2}"|TAP_WINDOWS_VERSION="${TAP_WINDOWS_VERSION:-9.22.1}"|' build.vars
source build.vars
TAP_WINDOWS_INSTALLER_URL="https://build.openvpn.net/downloads/releases/tap-windows-${TAP_WINDOWS_VERSION}-I601.exe"
PKCS11_HELPER_VERSION="1.22"
PKCS11_HELPER_URL="https://github.com/OpenSC/pkcs11-helper/releases/download/pkcs11-helper-${PKCS11_HELPER_VERSION}/pkcs11-helper-${PKCS11_HELPER_VERSION}.tar.bz2"
mkdir -p sources
pushd sources
for version in ${OPENVPN_VERSIONS}
  do
  version_only=$(echo ${version} | sed 's|\([^-]*\)-.*|\1|')
  OPENVPN_VERSION_URL="${OPENVPN_RELEASES}/openvpn-${version_only}.tar.gz"
  if [ ! -f openvpn-${version_only}.tar.gz ]; then
    wget ${OPENVPN_VERSION_URL} || die "failed to fetch ${OPENVPN_VERSION_URL}"
  fi
done
for url in \
  ${LZO_URL} \
  ${LIBRESSL_URL} \
  ${OPENSSL_URL} \
  ${OPENSSL_LEGACY_URL} \
  ${PKCS11_HELPER_URL} \
  ${TAP_WINDOWS_URL} \
  ${OPENVPN_GUI_URL};
  do
  wget "${url}" || die "failed to fetch ${url}"
done
# Count the files to avoid breaking the build with 'dirty' sources
popd #sources
popd #generic
wget ${TAP_WINDOWS_INSTALLER_URL} \
  || die "failed to fetch ${TAP_WINDOWS_INSTALLER_URL}"
popd #openvpn-build
#
# TOR
#
cp -R /var/tor-build .
pushd tor-build
pushd generic
export OPENSSL_VERSION=${OPENSSL_LEGACY_VERSION}
export OPENSSL_URL=${OPENSSL_LEGACY_URL}
source build.vars
mkdir -p sources
pushd sources
gpg --keyserver pool.sks-keyservers.net --recv-keys \
  ${LIBEVENT_KEY_ID} \
  ${OPENSSL_KEY_ID} \
  ${TOR_KEY_ID_NM} \
  ${TOR_KEY_ID_RD1} \
  ${TOR_KEY_ID_RD2} \
  ${ZLIB_KEY_ID}
gpg -o public_sign_keys.asc --armor --export \
  ${LIBEVENT_KEY_ID} \
  ${OPENSSL_KEY_ID} \
  ${TOR_KEY_ID_NM} \
  ${TOR_KEY_ID_RD1} \
  ${TOR_KEY_ID_RD2} \
  ${ZLIB_KEY_ID}

for version in ${TOR_VERSIONS}
  do
  version_only=$(echo ${version} | sed 's|\([^-]*\)-.*|\1|')
  TOR_VERSION_URL="${TOR_RELEASES}/tor-${version_only}.tar.gz"
  TOR_VERSION_SIG_URL="${TOR_RELEASES}/tor-${version_only}.tar.gz.asc"
  if [ ! -f tor-${version_only}.tar.gz ]; then
    wget ${TOR_VERSION_URL} || die "failed to fetch ${TOR_VERSION_URL}"
    wget ${TOR_VERSION_SIG_URL} || die "failed to fetch ${TOR_VERSION_URL}"
    gpg --verify $(basename ${TOR_VERSION_SIG_URL}) \
      || die "failed to verify signature"
  fi
done

for url in \
  ${ZLIB_URL} \
  ${OPENSSL_URL} \
  ${LIBEVENT_URL};
  do
  wget "${url}" || die "failed to fetch ${url}"
done

for url in \
  ${ZLIB_SIG_URL} \
  ${OPENSSL_SIG_URL} \
  ${LIBEVENT_SIG_URL};
  do
  wget "${url}" || die "failed to fetch ${url}"
  gpg --verify $(basename ${url}) || die "failed to verify signature ${url}"
done
popd #sources
popd #generic
popd #tor-build
tar cjf openvpn-build.tar.bz2 openvpn-build
tar cjf tor-build.tar.bz2 tor-build
popd #TMPDIR
cp ${TMPDIR}/openvpn-build.tar.bz2 ${DISTFILESDIR}
cp ${TMPDIR}/tor-build.tar.bz2 ${DISTFILESDIR}
cp /var/packages/*.tar.gz ${DISTFILESDIR}
