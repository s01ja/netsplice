#!/usr/bin/env bash

# rebuild docker images from current dockerfiles

if [ "$1" == "pull" ]; then
  FROMS=$( find . | grep Dockerfile | xargs cat | grep 'FROM' | sort -u | sed 's|FROM ||')
  for t in ${FROMS[@]}; do docker pull $t; done
fi
docker ps -a | grep 'Exited' | awk '{print $1}' | xargs docker rm
docker images | grep '<none>' | awk '{print $3}' | xargs docker rmi
pushd debian-build
make docker-compose.yml
docker-compose build
rm docker-compose.yml
popd

pushd debian-build/slaves/syswin32
make docker-compose.yml
docker-compose build
rm docker-compose.yml
popd

docker ps -a | grep 'Exited' | awk '{print $1}' | xargs docker rm
docker images | grep '<none>' | awk '{print $3}' | xargs docker rmi
