#!/bin/bash
#
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Restore a previously written /etc/resolv.conf

# Using this script may break modern dns resolver features. Read the
# documentation of your distribution to learn if this is the correct
# method to set the DNS servers.
cd "$(dirname "$0")" || exit_fatal "Cannot change directory to $0."
source .subroutines.inc.sh

declare RESOLVCONF=${RESOLVCONF:-/etc/resolv.conf}
declare ACCOUNT_ID=${netsplice_account_id}
declare RESOLVCONF_BACKUP="${RESOLVCONF}.${ACCOUNT_ID}"

if [ ! -f "${RESOLVCONF_BACKUP}" ]; then
    error_fatal "No backup for ${ACCOUNT_ID}, not restoring"
fi
run_cmd_foe "${ERROR_CODE_DISCONNECT}" \
    rm "${RESOLVCONF}"
run_cmd_foe "${ERROR_CODE_DISCONNECT}" \
    mv "${RESOLVCONF_BACKUP}" "${RESOLVCONF}"
