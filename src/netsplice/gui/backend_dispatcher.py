# -*- coding: utf-8 -*-
# backend_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Backend event dispatcher.

Beware, this dispatcher exists in a thread different from the Qt Event-loop.
Therefore it must not be used with callbacks but with signals. When you need
a GUI notification pass a signal to be emitted on success & failure. Use the
backend signals if possible.
'''

import time
import sys

from PySide import QtCore
from tornado import ioloop, gen, httpclient
from socket import error as socket_error

from netsplice.config import backend as config_backend
from netsplice.config import gui as config
import netsplice.gui.dispatcher_endpoints as endpoints
from netsplice.gui import backend_dispatcher_qt_replacement
from netsplice.gui.account_edit.model.request.account_create import (
    account_create as account_create_model
)
from netsplice.gui.account_edit.model.request.account_update import (
    account_update as account_update_model
)
from netsplice.gui.model.request.chain_create import (
    chain_create as chain_create_model
)
from netsplice.gui.model.request.chain_update import (
    chain_update as chain_update_model
)
from netsplice.gui.model.request.group_create import (
    group_create as group_create_model
)
from netsplice.gui.model.request.group_update import (
    group_update as group_update_model
)
from netsplice.gui.model.request.grouped_account_create import (
    grouped_account_create as grouped_account_create_model
)
from netsplice.gui.model.request.grouped_account_update import (
    grouped_account_update as grouped_account_update_model
)
from netsplice.gui.model.request.account_id import (
    account_id as account_id_model
)
from netsplice.gui.model.request.group_id import (
    group_id as group_id_model
)
from netsplice.gui.model.request.preference_value import (
    preference_value as preference_value_model
)
from netsplice.gui.model.response.group_id import (
    group_id as group_id_response_model
)
from netsplice.gui.model.response.gui import (
    gui as gui_model
)
from netsplice.gui.model.response.preferences import (
    preferences as preferences_model
)
from netsplice.model.credential import (
    credential as credential_model
)
from netsplice.model.log_item import (
    log_item as log_item_model
)
from netsplice.model.process import (
    process as process_model
)
from netsplice.util.errors import (
    NotInitializedError, NotActiveError, NotFoundError
)
from netsplice.util.ipc.service import service
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.ipc.shared_secret import shared_secret
from netsplice.util.model.errors import ValidationError
from netsplice.util.process.dispatcher import (
    dispatcher as process_dispatcher
)
from netsplice.util.process.errors import (
    ProcessError
)
from netsplice.util import get_logger, get_logger_handler

logger = get_logger()

# XXX review for skipped response models

if sys.platform.startswith('darwin'):
    # Python only threads in gui.
    # Due to bugs in PySide osx may crash at random locations. Use this option
    # to use a threading implementation that mimics the emit/Signal code.
    Signal = backend_dispatcher_qt_replacement.Signal
    QObject = backend_dispatcher_qt_replacement.QObject
    QThread = backend_dispatcher_qt_replacement.QThread
    QMutex = backend_dispatcher_qt_replacement.QMutex
    QCoreApplication = backend_dispatcher_qt_replacement.QCoreApplication
else:
    Signal = QtCore.Signal
    QObject = QtCore.QObject
    QThread = QtCore.QThread
    QMutex = QtCore.QMutex
    QCoreApplication = QtCore.QCoreApplication


class backend_thread(QThread):
    '''
    Backend Thread that is responsible to create the backend dispatcher and
    make it available to the main-thread. The Backend Thread continuously
    executes QCoreApplication.processEvents to ensure that emit'ed signals are
    received in this thread. The dispatcher QObject is created when the thread
    has started.
    '''

    def __init__(self, dispatcher_class, host, port):
        QThread.__init__(self, None)
        self._active = False
        self.backend_dispatcher_class = dispatcher_class
        self.backend_dispatcher_host = host
        self.backend_dispatcher_port = port
        self.backend_dispatcher = None
        self.mutex = QMutex()

    def _schedule_event_loop(self):
        '''
        Let tornado call the qt-event-loop.
        '''
        ioloop.IOLoop.current().call_later(
            config.LOOP_TIMER_QT_EVENTS, self._event_loop)

    def _event_loop(self):
        '''
        The Backend Thread continuously executes
        QCoreApplication.processEvents to ensure that emit'ed signals are
        received in this thread.
        '''
        QCoreApplication.processEvents()
        self._schedule_event_loop()

    def get_dispatcher(self):
        '''
        Returns the dispatcher for the main-thread.
        Should be called after start()
        '''
        self.mutex.lock()
        while self.backend_dispatcher is None:
            # Thread not yet running
            # wait until the dispatcher is available
            self.mutex.unlock()
            time.sleep(config.LOOP_TIMER_NOOP)
            self.mutex.lock()
        self.mutex.unlock()
        return self.backend_dispatcher

    def run(self):
        '''
        Backend Thread main. Schedules qt event loop and enters the tornado
        event loop.
        '''
        # force IOLoop to this thread (windows wants this)
        # otherwise the ioloop will not execute the dispatched
        # requests
        thread_event_noop = lambda: ioloop.IOLoop.current().call_later(
            config.LOOP_TIMER_NOOP, lambda: thread_event_noop())
        thread_event_noop()
        self._schedule_event_loop()

        backend_dispatcher = self.backend_dispatcher_class(
            self.backend_dispatcher_host, self.backend_dispatcher_port,
            None, None)
        self.mutex.lock()
        self.backend_dispatcher = backend_dispatcher
        self.mutex.unlock()
        self._active = True
        self.backend_dispatcher.exec_()
        self._active = False

    def wait(self):
        '''
        Wait.

        Wait that the backend_dispatcher ioloop stops.
        '''
        while self._active:
            time.sleep(config.LOOP_TIMER_NOOP)


class backend_dispatcher(QObject, service, process_dispatcher):
    '''
    Backend dispatcher to be used by the main thread. All functions of this
    class are private and must not be called from the main thread. Only
    signals may be emitted that Qt promises to be thread safe. Most signals
    receive a done_signal and a failed_signal in order to handle the async
    requests.
    '''

    model_changed = Signal(basestring)
    preferences_changed = Signal(basestring)
    preferences_plugins_changed = Signal(basestring)
    available = Signal()
    not_available = Signal()
    process_gone = Signal()
    heartbeat_connected = Signal()
    heartbeat_error = Signal()
    heartbeat_warning = Signal()
    plugins_changed = Signal()

    # General Signals
    check_available = Signal()
    events_get = Signal()
    model_get = Signal()

    logs_delete = Signal(Signal, Signal)
    logs_get = Signal(int, Signal, Signal)
    logs_set = Signal(basestring)

    preferences_get = Signal()
    preferences_events_get = Signal()
    preferences_complex_set = Signal(
        basestring, basestring, basestring, Signal, Signal)
    preferences_plugin_get = Signal(
        basestring, basestring, Signal, Signal)
    preferences_plugin_set = Signal(
        basestring, basestring, basestring, Signal, Signal)
    preferences_plugin_collection_instance_get = Signal(
        basestring, basestring, basestring, Signal, Signal)
    preferences_plugin_collection_instance_set = Signal(
        basestring, basestring, basestring, object, Signal, Signal)
    preferences_value_set = Signal(
        basestring, basestring, basestring, Signal, Signal)
    shutdown = Signal()
    startup_progress = Signal(basestring)

    # Signals to receive work from gui
    # This is the API for the gui

    account_connect = Signal(basestring, Signal, Signal)
    account_create = Signal(
        basestring, basestring,  # name, type
        basestring, basestring,  # conf, import_conf
        bool, bool, bool,  # enabled, default_route, autostart
        Signal, Signal)
    account_delete = Signal(basestring, Signal, Signal)
    account_disconnect = Signal(basestring, Signal, Signal)
    account_get_for_edit = Signal(basestring, Signal, Signal)
    account_reconnect = Signal(basestring, Signal, Signal)
    account_reset = Signal(basestring, Signal, Signal)
    account_update = Signal(
        basestring, basestring, basestring,  # id, name, type
        basestring, basestring,  # conf, import_conf
        bool, bool, bool,  # enabled, default_route, autostart
        Signal, Signal)
    credential_cancel_request = Signal(Signal, Signal)
    credential_send = Signal(
        basestring, basestring, basestring,  # account_id, username, password
        basestring,  # store_password_type
        Signal, Signal)

    chain_connect = Signal(
        basestring,  # account_id
        Signal, Signal)
    chain_create = Signal(
        basestring, basestring,  # parent_account_id, account_id
        int,  # weight
        Signal, Signal)
    chain_update = Signal(
        basestring, basestring,  # parent_account_id, account_id
        int, basestring, basestring,  # weight, connect_mode, failure_mode
        bool,  # collapsed
        Signal, Signal)

    grouped_account_create = Signal(
        basestring, basestring,  # account_id, group_id
        int,  # weight
        Signal, Signal)
    grouped_account_delete = Signal(basestring, Signal, Signal)
    grouped_account_update = Signal(
        basestring, basestring,  # account_id, group_id
        int,  # weight
        Signal, Signal)
    group_connect = Signal(
        basestring,  # group_id
        Signal, Signal)
    group_create = Signal(
        basestring, int, basestring,  # name, weight, parent_group_id
        Signal, Signal)
    group_delete = Signal(basestring, Signal, Signal)
    group_disconnect = Signal(
        basestring,  # group_id
        Signal, Signal)
    group_get = Signal(basestring, Signal, Signal)
    group_reconnect = Signal(
        basestring,  # group_id
        Signal, Signal)
    group_update = Signal(
        basestring, basestring,  # id, name
        int, bool, basestring,  # weight, collapsed, parent
        Signal, Signal)
    groups_get = Signal(Signal, Signal)

    def __init__(self, host, port, host_owner, port_owner):
        QObject.__init__(self)
        service.__init__(
            self, host, port, host_owner, port_owner,
            'backend', 'gui')
        self.process_model = process_model()
        process_dispatcher.__init__(
            self, 'NetspliceBackendApp', {
                '--host': host,
                '--port': port
            },
            model=self.process_model,
            unique=True)
        try:
            self.start_subprocess()
        except ProcessError:
            self.service_shutdown()
        self.last_etag = ''
        self.wait_for_service = True
        self._startup_message = ''

        # Ensure only one schedule running

        self._check_available_scheduled = False
        self._exit_scheduled = False
        self._events_get_scheduled = False
        self._logs_set_scheduled = False
        self._preferences_events_get_scheduled = False

        self.plugins_changed.connect(self._plugins_changed)

        # Connect the Signals with private member methods.

        self.check_available.connect(self._check_available)
        self.events_get.connect(self._events_get)
        self.model_get.connect(self._model_get)
        self.logs_delete.connect(self._logs_delete)
        self.logs_get.connect(self._logs_get)
        self.logs_set.connect(self._logs_set)

        self.account_connect.connect(self._account_connect)
        self.account_create.connect(self._account_create)
        self.account_delete.connect(self._account_delete)
        self.account_disconnect.connect(self._account_disconnect)
        self.account_reconnect.connect(self._account_reconnect)
        self.account_reset.connect(self._account_reset)
        self.account_update.connect(self._account_update)
        self.account_get_for_edit.connect(self._account_get_for_edit)

        self.credential_cancel_request.connect(self._credential_cancel_request)
        self.credential_send.connect(self._credential_send)

        self.group_get.connect(self._group_get)
        self.group_connect.connect(self._group_connect)
        self.group_create.connect(self._group_create)
        self.group_delete.connect(self._group_delete)
        self.group_disconnect.connect(self._group_disconnect)
        self.group_reconnect.connect(self._group_reconnect)
        self.group_update.connect(self._group_update)
        self.chain_connect.connect(self._chain_connect)
        self.chain_create.connect(self._chain_create)
        self.chain_update.connect(self._chain_update)
        self.grouped_account_create.connect(self._grouped_account_create)
        self.grouped_account_delete.connect(self._grouped_account_delete)
        self.grouped_account_update.connect(self._grouped_account_update)
        self.groups_get.connect(self._groups_get)

        self.preferences_get.connect(self._preferences_get)
        self.preferences_events_get.connect(self._preferences_events_get)
        self.preferences_complex_set.connect(self._preferences_complex_set)
        self.preferences_plugin_get.connect(self._preferences_plugin_get)
        self.preferences_plugin_set.connect(self._preferences_plugin_set)
        self.preferences_plugin_collection_instance_get.connect(
            self._preferences_plugin_collection_instance_get)
        self.preferences_plugin_collection_instance_set.connect(
            self._preferences_plugin_collection_instance_set)
        self.preferences_value_set.connect(self._preferences_value_set)

        self.shutdown.connect(self._shutdown)

    @gen.coroutine
    def _account_connect(self, account_id, done_signal, failed_signal):
        '''
        Connect the account with the given account_id.
        '''
        request_model = account_id_model()
        try:
            request_model.id.set(account_id)
            options = {
                "account_id": request_model.id.get()
            }
            response = yield self.post(
                endpoints.BACKEND_GUI_ACCOUNTS_CONNECT % options,
                '{}')
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _account_create(self, name, type, configuration,
                        import_configuration, enabled, default_route,
                        autostart,
                        done_signal, failed_signal):
        '''
        Create account with given properties.
        '''
        request_model = account_create_model()

        try:
            request_model.name.set(name)
            request_model.type.set(type)
            request_model.configuration.set(configuration)
            request_model.import_configuration.set(import_configuration)
            request_model.enabled.set(enabled)
            request_model.default_route.set(default_route)
            request_model.autostart.set(autostart)

            response = yield self.post(
                endpoints.BACKEND_PREFERENCES_ACCOUNTS,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _account_delete(self, account_id, done_signal, failed_signal):
        '''
        Delete the account with the given account_id.
        '''
        request_model = account_id_model()
        try:
            request_model.id.set(account_id)
            options = {
                "account_id": request_model.id.get()
            }
            response = yield self.delete(
                endpoints.BACKEND_PREFERENCES_ACCOUNTS_ITEM % options)
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _account_disconnect(self, account_id, done_signal, failed_signal):
        '''
        Disconnect the account with the given account_id.
        '''
        request_model = account_id_model()
        try:
            request_model.id.set(account_id)
            options = {
                "account_id": request_model.id.get()
            }
            response = yield self.post(
                endpoints.BACKEND_GUI_ACCOUNTS_DISCONNECT % options,
                '{}')
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _account_get_for_edit(self, account_id, done_signal, failed_signal):
        '''
        Get the account information with details like password for editing.
        '''
        request_model = account_id_model()

        try:
            request_model.id.set(account_id)
            options = {
                "account_id": request_model.id.get()
            }
            response = yield self.get(
                endpoints.BACKEND_PREFERENCES_ACCOUNTS_ITEM % options, None)

            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _account_reconnect(self, account_id, done_signal, failed_signal):
        '''
        Reconnect the account with the given account_id.
        '''
        request_model = account_id_model()
        try:
            request_model.id.set(account_id)
            options = {
                "account_id": request_model.id.get()
            }
            response = yield self.post(
                endpoints.BACKEND_GUI_ACCOUNTS_RECONNECT % options,
                '{}')
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _account_reset(self, account_id, done_signal, failed_signal):
        '''
        Resets the account with the given account_id.
        '''
        request_model = account_id_model()
        try:
            request_model.id.set(account_id)
            options = {
                "account_id": request_model.id.get()
            }
            response = yield self.post(
                endpoints.BACKEND_GUI_ACCOUNTS_RESET % options,
                '{}')
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _account_update(self, account_id, name, type,
                        configuration, import_configuration,
                        enabled, default_route, autostart,
                        done_signal, failed_signal):
        '''
        Update the account with the given account_id with the given values.
        '''

        request_model = account_update_model()
        try:
            request_model.id.set(account_id)
            request_model.name.set(name)
            request_model.type.set(type)
            request_model.configuration.set(configuration)
            request_model.import_configuration.set(import_configuration)
            request_model.enabled.set(enabled)
            request_model.default_route.set(default_route)
            request_model.autostart.set(autostart)
            response = yield self.put(
                endpoints.BACKEND_PREFERENCES_ACCOUNTS,
                request_model.to_json())

            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _credential_cancel_request(self, done_signal, failed_signal):
        try:
            response = yield self.delete(endpoints.BACKEND_GUI_CREDENTIAL)

            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _credential_send(
            self, account_id, username, password,
            store_password_type,
            done_signal, failed_signal):
        request_model = credential_model()
        try:
            request_model.account_id.set(account_id)
            request_model.username.set(username)
            request_model.password.set(password)
            request_model.store_password.set(store_password_type)
            response = yield self.post(
                endpoints.BACKEND_GUI_CREDENTIAL,
                request_model.to_json())

            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _check_available(self):
        '''
        Check if the backend service is available with the ipc-status
        controller. The function will signal the state and continue doing so
        reschedule until the backend is available.
        '''
        self._check_available_scheduled = False
        try:
            self.get_subprocess().poll()
        except NotInitializedError:
            self.not_available.emit()
            self.heartbeat_error.emit()
            self.process_gone.emit()
            return
        if self.get_subprocess().returncode is not None:
            if self.get_subprocess().returncode > 0:
                self.not_available.emit()
                self.heartbeat_error.emit()
                self.process_gone.emit()
                return

        try:
            # ensure that a updated secret is loaded
            if not self._active and self._check_log_ready_message():
                shared_secret.load_from_file()
                self._active = True
                self.set_available()
            elif not self._active and self._check_log_startup_messages():
                self.startup_progress.emit(self._startup_message)
                raise NotActiveError()
            elif not self._active:
                raise NotActiveError()
            yield self.get(endpoints.BACKEND_STATUS, None)
            self.available.emit()
            self.heartbeat_connected.emit()
        except ServerConnectionError:
            self.not_available.emit()
            self.heartbeat_error.emit()
            self._schedule_check_available()
        except NotActiveError:
            self.not_available.emit()
            self._schedule_check_available()
        except Exception:
            # Service is not available.
            self.not_available.emit()

            self._schedule_check_available()

    def _check_log_ready_message(self):
        '''
        Check log ready message.

        Check the log for a ready message from the backend process
        and return True when the message is found.
        '''
        logger_handler = get_logger_handler()
        found = logger_handler.check_log_content(
            config_backend.BACKEND_READY_MESSAGE)
        return found

    def _check_log_startup_messages(self):
        '''
        Check log startup message.

        Check the log for backend startup messages. When a message is found
        it is stored in the self._startup_message and can be emitted to the
        main window.
        '''
        logger_handler = get_logger_handler()
        startup_messages = []
        logs = logger_handler.get_logs()
        for log_item in logs:
            if isinstance(log_item, (dict,)):
                message = log_item['message']
            else:
                message = log_item.message.get()
            if config_backend.BACKEND_STARTUP_MESSAGE not in message:
                continue
            startup_messages.append(message)
        if len(startup_messages) is 0:
            return False
        if startup_messages[-1] == self._startup_message:
            return False
        self._startup_message = startup_messages[-1]
        return True

    @gen.coroutine
    def _chain_connect(
            self, account_id,
            done_signal, failed_signal):
        '''
        Connect chain.

        Connect chain with its children.
        '''
        request_model = account_id_model()

        try:
            request_model.id.set(account_id)
            options = {
                'account_id': request_model.id.get()
            }
            response = yield self.post(
                endpoints.BACKEND_GUI_CHAIN_CONNECT % options, '{}')
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _chain_create(
            self, parent_account_id, account_id, weight,
            done_signal, failed_signal):
        '''
        Create a new group entry.
        '''
        request_model = chain_create_model()

        try:
            request_model.id.set(account_id)
            request_model.parent_id.set(parent_account_id)
            request_model.weight.set(weight)
            response = yield self.post(
                endpoints.BACKEND_PREFERENCES_CHAINS,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _chain_update(
            self,
            parent_account_id, account_id, weight, connect_mode, failure_mode,
            collapsed, done_signal, failed_signal):
        '''
        Update the given group.
        '''
        request_model = chain_update_model()
        try:
            request_model.id.set(account_id)
            if parent_account_id != '':
                request_model.parent_id.set(parent_account_id)
            request_model.weight.set(weight)
            request_model.connect_mode.set(connect_mode)
            request_model.failure_mode.set(failure_mode)
            request_model.collapsed.set(collapsed)
            options = {
                'chain_id': request_model.id.get()
            }
            response = yield self.put(
                endpoints.BACKEND_PREFERENCES_CHAIN_ITEM % options,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    def _emit_failure(self, failed_signal, errors):
        '''
        Convenience function to make error handling less verbose.
        '''
        if isinstance(errors, (ServerConnectionError,)):
            message = str(errors)
            failed_signal.emit(message)
            return
        if errors is None:
            # There are shutdown situations that will raise errors
            # without a response object.
            # Silently swallow the error.
            return
        message = str(errors)
        logger.error(message)
        if isinstance(errors, (httpclient.HTTPError,)):
            if errors.response is None:
                # There are shutdown situations that will raise errors
                # without a response object.
                # Silently swallow the error.
                return

            headers = errors.response.headers.get_all()
            message = ''
            for (header_name, header_value) in sorted(headers):
                if header_name != 'X-Errordetail':
                    continue
                message += header_value
            message = message.replace('\\n', '\n')
        failed_signal.emit(message)

    @gen.coroutine
    def _events_get(self):
        '''
        Long-Polling call to backend that will return every time the backend-
        model changes.
        '''
        self._events_get_scheduled = False
        try:
            response = yield self.get(endpoints.BACKEND_GUI_EVENTS, None)
            '''
            Long-Polling Event Handler. Handles when backend has events and
            reschedules the next get, so future events are received as well
            '''
            if response.code == 200:
                # TODO: evaluate type of event
                # model-> what changed
                # action -> trigger
                self.heartbeat_connected.emit()
                yield self._model_get()
            else:
                self.heartbeat_warning.emit()
        except httpclient.HTTPError, http_error:
            if http_error.code == 599:
                # Stream Closed, Timeout
                self.not_available.emit()
                self.heartbeat_error.emit()
                self._schedule_check_available()
                return
        except ServerConnectionError:
            self.not_available.emit()
            self.heartbeat_error.emit()
            return
        except Exception:
            # ensure that regardless of backend errors the event are requested
            # again and again
            self.heartbeat_error.emit()
            pass

        # reschedule call to function
        self._schedule_events_get()

    @gen.coroutine
    def _group_connect(
            self, group_id,
            done_signal, failed_signal):
        '''
        Connect all accounts of given group_id.
        '''
        request_model = group_id_model()
        try:
            request_model.id.set(group_id)
            options = {
                'group_id': request_model.id.get()
            }
            response = yield self.post(
                endpoints.BACKEND_GUI_GROUPS_CONNECT % options,
                '{}')
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _group_create(self, group_name, group_weight, parent_group_id,
                      done_signal, failed_signal):
        '''
        Create a new group entry.
        '''
        request_model = group_create_model()
        response_model = group_id_response_model()

        try:
            request_model.name.set(group_name)
            request_model.weight.set(group_weight)
            request_model.parent.set(parent_group_id)
            response = yield self.post(
                endpoints.BACKEND_PREFERENCES_GROUPS,
                request_model.to_json())
            response_model.from_json(response.body)
            done_signal.emit(response_model.to_json())
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _group_delete(self, group_id, done_signal, failed_signal):
        '''
        Delete a existing group entry.
        '''
        request_model = group_id_model()
        try:
            request_model.id.set(group_id)
            options = {
                "group_id": request_model.id.get()
            }
            response = yield self.delete(
                endpoints.BACKEND_PREFERENCES_GROUPS_ITEM % options)
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _group_disconnect(
            self, group_id,
            done_signal, failed_signal):
        '''
        Disconnect all accounts of given group_id.
        '''
        request_model = group_id_model()
        try:
            request_model.id.set(group_id)
            options = {
                'group_id': request_model.id.get()
            }
            response = yield self.post(
                endpoints.BACKEND_GUI_GROUPS_DISCONNECT % options,
                '{}')
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _group_get(self, group_id, done_signal, failed_signal):
        '''
        Get details for the given group_id.
        '''
        request_model = group_id_model()
        try:
            request_model.id.set(group_id)
            options = {
                "group_id": request_model.id.get()
            }
            response = yield self.get(
                endpoints.BACKEND_PREFERENCES_GROUPS_ITEM % options, None)
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _group_reconnect(
            self, group_id,
            done_signal, failed_signal):
        '''
        Disconnect all accounts of given group_id.
        '''
        request_model = group_id_model()
        try:
            request_model.id.set(group_id)
            options = {
                'group_id': request_model.id.get()
            }
            response = yield self.post(
                endpoints.BACKEND_GUI_GROUPS_RECONNECT % options,
                '{}')
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _group_update(
            self,
            group_id, group_name, group_weight, collapsed,
            parent_group_id,
            done_signal, failed_signal):
        '''
        Update the given group.
        '''
        request_model = group_update_model()
        try:
            request_model.id.set(group_id)
            request_model.name.set(group_name)
            request_model.weight.set(group_weight)
            request_model.collapsed.set(collapsed)
            request_model.parent.set(parent_group_id)
            response = yield self.put(
                endpoints.BACKEND_PREFERENCES_GROUPS,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _grouped_account_create(
            self, account_id, group_id, weight,
            done_signal, failed_signal):
        '''
        Create a new group entry.
        '''
        request_model = grouped_account_create_model()

        try:
            request_model.id.set(account_id)
            request_model.group_id.set(group_id)
            request_model.weight.set(weight)
            response = yield self.post(
                endpoints.BACKEND_PREFERENCES_GROUPED_ACCOUNTS,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _grouped_account_delete(self, account_id, done_signal, failed_signal):
        '''
        Delete an existing grouped_account entry.
        '''
        request_model = account_id_model()
        try:
            request_model.id.set(account_id)
            options = {
                "account_id": request_model.id.get()
            }
            response = yield self.delete(
                endpoints.BACKEND_PREFERENCES_GROUPED_ACCOUNTS_ITEM % options)
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _grouped_account_update(
            self,
            account_id, group_id, weight,
            done_signal, failed_signal):
        '''
        Update the given group.
        '''
        request_model = grouped_account_update_model()
        try:
            request_model.id.set(account_id)
            request_model.group_id.set(group_id)
            request_model.weight.set(weight)
            response = yield self.put(
                endpoints.BACKEND_PREFERENCES_GROUPED_ACCOUNTS,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _groups_get(self, done_signal, failed_signal):
        '''
        Get the list of available groups
        '''
        try:
            response = yield self.get(
                endpoints.BACKEND_PREFERENCES_GROUPS, None)
            done_signal.emit(response.body)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _logs_delete(self, done_signal, failed_signal):
        '''
        Delete Logs.

        Remove logs from backend.
        '''
        try:
            yield self.delete(
                endpoints.BACKEND_GUI_LOGS)
            done_signal.emit()
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)
            self.heartbeat_error.emit()

    @gen.coroutine
    def _logs_get(self, offset, done_signal, failed_signal):
        '''
        Gets logs from backend.
        '''
        try:
            options = {
                'offset': offset
            }
            response = yield self.get(
                endpoints.BACKEND_GUI_LOGS_OFFSET % options,
                None)
            done_signal.emit(response.body)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)
            self.heartbeat_error.emit()

    @gen.coroutine
    def _logs_set(self, log_item_json):
        '''
        Sends log_item to backend. This function does not process the response
        failures get swallowed.
        '''
        self._logs_set_scheduled = False
        if self._shutting_down:
            try:
                log_item = log_item_model()
                log_item.from_json(log_item_json)
                logger_handler = get_logger_handler()
                logger_handler.append(log_item, marshalable=False)
                self.process_gone.emit()
            except ValidationError:
                pass
            return
        try:
            yield self.post(
                endpoints.BACKEND_GUI_LOGS, log_item_json)
        except httpclient.HTTPError, errors:
            pass
        except ServerConnectionError:
            self._schedule_logs_set(log_item_json)
        except socket_error:
            # backend not yet available, logs are stored for later
            self._schedule_logs_set(log_item_json)

    @gen.coroutine
    def _model_get(self):
        '''
        Gets model from backend and notifies gui.
        '''
        try:
            response = yield self.get(endpoints.BACKEND_GUI_MODEL, None)
            model = gui_model()
            model.from_json(response.body)
            self.model_changed.emit(model.to_json())
            self.heartbeat_connected.emit()
        except ServerConnectionError:
            self.heartbeat_error.emit()

    @gen.coroutine
    def _plugins_changed(self):
        '''
        Plugins Changed.

        GUI thread signaled that the list of dispatcher-plugins has changed.
        Re-iterate the plugin list and create instances for plugins where no
        dispatcher exists yet.
        '''
        for plugin_item in self.application.get_plugin_dispatchers():
            try:
                # make it impossible to overwrite core properties
                plugin_dispatcher = self.__dict__[plugin_item['name']]
                continue
            except KeyError:
                pass
            self.__dict__[plugin_item['name']] = plugin_item['class'](self)

    @gen.coroutine
    def _preferences_get(self):
        '''
        Gets the preferences from backend and notifies gui.
        '''
        try:
            response = yield self.get(endpoints.BACKEND_PREFERENCES, None)
            model = preferences_model()
            response_plugins = self.application.get_model().preferences.\
                response.plugins
            for plugin_item in response_plugins:
                model.plugins.register_plugin(
                    plugin_item.name.get(), plugin_item)
            model.from_json(response.body)
            for plugin in model.plugins:
                plugin_name = plugin.name.get()
                model_plugin = model.plugins.find_by_name(plugin_name)
                plugin_response_model = model.plugins.find_by_name(plugin_name)
                options = {
                    'plugin_name': plugin_name
                }
                plugin_response = yield self.get(
                    endpoints.BACKEND_PREFERENCES_PLUGIN_ALL % options, None)
                plugin_response_model.from_json(plugin_response.body)
                model_plugin.from_json(plugin_response_model.to_json())
            self.preferences_changed.emit(model.to_json())
            self.preferences_plugins_changed.emit(model.plugins.to_json())
            self.heartbeat_connected.emit()
        except ServerConnectionError:
            self.heartbeat_error.emit()

    @gen.coroutine
    def _preferences_events_get(self):
        '''
        Long-Polling call to backend that will return every time the backend-
        preferences-model changes.
        '''
        self._preferences_events_get_scheduled = False
        try:
            response = yield self.get(
                endpoints.BACKEND_PREFERENCES_EVENTS, None)
            '''
            Long-Polling Event Handler. Handles when backend has events and
            reschedules the next get, so future events are received as well
            '''
            if response.code == 200:
                # TODO: evaluate type of event
                # model-> what changed
                # action -> trigger
                self.heartbeat_connected.emit()
                yield self._preferences_get()
            else:
                self.heartbeat_warning.emit()
        except httpclient.HTTPError, http_error:
            if http_error.code == 599:
                # Stream Closed
                self.not_available.emit()
                self.heartbeat_error.emit()
                self._schedule_check_available()
                return
        except ServerConnectionError:
            self.heartbeat_error.emit()
            return
        except Exception:
            # ensure that regardless of backend errors the event are requested
            # again and again
            self.heartbeat_error.emit()
            pass

        # reschedule call to function
        self._schedule_preferences_events_get()

    @gen.coroutine
    def _preferences_complex_set(
            self, collection_name, field_name, value,
            done_signal, failed_signal):
        '''
        Set a specific preferene value.
        '''
        options = {
            'collection_name': collection_name,
            'field_name': field_name
        }
        try:
            request_model = preference_value_model()
            request_model.value.set(value)

            response = yield self.put(
                endpoints.BACKEND_PREFERENCES_COMPLEX % options,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _preferences_plugin_get(
            self, plugin_name,
            done_signal, failed_signal):
        '''
        Get all preference values for the plugin.
        '''
        options = {
            'plugin_name': plugin_name
        }
        model = preferences_model()
        model.register_plugins(
            self.application.get_model().preferences.response.plugins)
        try:
            plugin_model = model.plugins.find_by_name(plugin_name)

            response = yield self.get(
                endpoints.BACKEND_PREFERENCES_PLUGIN_ALL % options,
                None)

            plugin_model.from_json(response.body)
            done_signal.emit(plugin_model.to_json())
        except NotFoundError, errors:
            self._emit_failure(failed_signal, errors)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _preferences_plugin_collection_instance_get(
            self, plugin_name, collection_name, instance_id,
            done_signal, failed_signal):
        '''
        Get all preference values for the plugin.
        '''
        options = {
            'plugin_name': plugin_name,
            'collection_name': collection_name,
            'instance_id': instance_id,
        }
        response_model = preferences_model()
        try:
            collection_model = self.application.get_model().preferences.\
                __dict__[collection_name]
            response_collection_model = response_model.\
                __dict__[collection_name]
            response_collection_model.register_plugins(
                self.application.get_model().preferences.
                __dict__[collection_name].response.plugins)
            plugin_model = response_collection_model.plugins.find_by_name(
                plugin_name)

            response = yield self.get(
                endpoints.BACKEND_PREFERENCES_PLUGIN_COLLECTION_INSTANCE
                % options,
                None)

            plugin_model.from_json(response.body)
            plugin_model.name.set(plugin_name)
            done_signal.emit(plugin_model.to_json())
        except NotFoundError, errors:
            self._emit_failure(failed_signal, errors)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _preferences_plugin_set(
            self, plugin_name, field_name, value,
            done_signal, failed_signal):
        '''
        Set a specific preference value.
        '''
        options = {
            'field_name': field_name,
            'plugin_name': plugin_name
        }
        try:
            request_model = preference_value_model()
            request_model.value.set(value)

            response = yield self.put(
                endpoints.BACKEND_PREFERENCES_PLUGIN_ITEM % options,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _preferences_plugin_collection_instance_set(
            self, plugin_name, collection_name, instance_id, model,
            done_signal, failed_signal):
        '''
        Set a specific preference model.
        '''
        options = {
            'instance_id': instance_id,
            'collection_name': collection_name,
            'plugin_name': plugin_name
        }
        try:
            response = yield self.put(
                endpoints.BACKEND_PREFERENCES_PLUGIN_COLLECTION_INSTANCE
                % options,
                model.to_json())
            done_signal.emit(response.body)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _preferences_value_set(
            self, collection_name, field_name, value,
            done_signal, failed_signal):
        '''
        Set a specific preferene value.
        '''
        options = {
            'collection_name': collection_name,
            'field_name': field_name
        }
        try:
            request_model = preference_value_model()
            request_model.value.set(value)

            response = yield self.put(
                endpoints.BACKEND_PREFERENCES_VALUE % options,
                request_model.to_json())
            done_signal.emit(response.body)
        except ValidationError, errors:
            self._emit_failure(failed_signal, errors)
        except httpclient.HTTPError, errors:
            self._emit_failure(failed_signal, errors)
        except ServerConnectionError, errors:
            self._emit_failure(failed_signal, errors)

    def _schedule_check_available(self):
        '''
        Schedule check_available without producing an endless stack.
        '''
        if self._check_available_scheduled:
            return
        self._check_available_scheduled = True
        ioloop.IOLoop.current().call_later(
            config.LOOP_TIMER_AVAILABLE, self._check_available)

    def _schedule_exit(self):
        '''
        Allow events to be processed before the thread is stopped.
        '''
        if self._exit_scheduled:
            return
        self._exit_scheduled = True
        ioloop.IOLoop.current().call_later(
            config.SHUTDOWN_TIMEOUT, lambda: ioloop.IOLoop.current().stop())

    def _schedule_events_get(self):
        '''
        Schedule get_events call without producing a endless stack.
        '''
        if self._events_get_scheduled:
            return
        self._events_get_scheduled = True
        ioloop.IOLoop.current().call_later(
            config.LOOP_TIMER_EVENTS, self._events_get)

    def _schedule_logs_set(self, log_item_json):
        '''
        Schedule _logs_set call when backend is not yet available but gui
        wants to set logs.
        '''
        if self._logs_set_scheduled:
            return
        self._logs_set_scheduled = True
        ioloop.IOLoop.current().call_later(
            config.LOOP_TIMER_LOGS_SET, lambda: self._logs_set(log_item_json))

    def _schedule_preferences_events_get(self):
        '''
        Schedule get_events call without producing a endless stack.
        '''
        if self._preferences_events_get_scheduled:
            return
        self._preferences_events_get_scheduled = True
        ioloop.IOLoop.current().call_later(
            config.LOOP_TIMER_EVENTS, self._preferences_events_get)

    @gen.coroutine
    def _shutdown(self):
        '''
        Request the backend to shutdown.
        '''
        try:
            yield self.post(
                endpoints.BACKEND_GUI_ACTIONS_SHUTDOWN, '{}')
            self._shutting_down = True
        except:
            # Backend is already gone
            pass

        self._schedule_exit()

    @gen.coroutine
    def service_shutdown(self):
        '''
        Service Shutdown.

        The dispatcher encountered an unrecoverable error and needs to shut
        down. The gui will be notified so it can display information to the
        user. Stop processing gen.coroutines in this thread to prevent crashes
        during shutdown and endless useless get's.
        '''
        self._shutting_down = True
        self.not_available.emit()
        self.heartbeat_error.emit()
        self.process_gone.emit()
        ioloop.IOLoop.current().call_later(
            config.SHUTDOWN_TIMEOUT, lambda: ioloop.IOLoop.current().stop())
