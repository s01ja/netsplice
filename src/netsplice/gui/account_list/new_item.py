# -*- coding: utf-8 -*-
# new_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Widget to accept external drops (eg config files)
'''

from PySide import QtCore, QtGui
import os

from netsplice.config import backend as config_backend
from netsplice.config import gui as config_gui
from netsplice.config import about as config_about
from netsplice.util import get_logger

logger = get_logger()


class new_item(QtGui.QWidget):
    '''
    Custom Widget for accepting external drops.
    '''
    preferences_changed = QtCore.Signal(int)

    def __init__(self, parent, mainwindow=None, group_item=None):
        QtGui.QWidget.__init__(self, parent)
        self.mainwindow = mainwindow
        self.backend = mainwindow.backend
        self.model = group_item
        self.parent_scrollarea = parent.parent().parent()
        self.ui = QtGui.QVBoxLayout(self)
        self.label = QtGui.QLabel(self)
        self.ui.addWidget(self.label)
        self.ui.addItem(QtGui.QSpacerItem(
            40, 20,
            QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding))
        self.setAcceptDrops(True)
        self.in_drag_mode = False
        self.drop_after = False
        self.drop_before = False
        self.drop_into = False
        # XXX TODO: new_item.ui
        self.label.setText(self.tr(config_gui.NO_CONNECTIONS_CONFIGURED))
        self.label.setWordWrap(True)
        self.label.setCursor(QtCore.Qt.PointingHandCursor)

    def dragEnterEvent(self, event):
        '''
        Drag enter event.

        Event when something is dragged on the widget.

        Arguments:
            event (Qt.QEvent): Event that contains details about the drop
        '''
        logger.debug(str(event.mimeData().formats()))
        if event.mimeData().hasFormat("text/uri-list"):
            self.in_drag_mode = True
            event.acceptProposedAction()
            self.update()
        if event.mimeData().hasFormat("text/plain"):
            self.in_drag_mode = True
            event.acceptProposedAction()
            self.update()

    def dragLeaveEvent(self, event):
        '''
        Drag Leave Event.

        The user moved the dragable out of the widget.

        Arguments:
            event (Qt.QEvent): Event that contains details.
        '''
        self.in_drag_mode = False
        self.update()

    def dropEvent(self, event):
        '''
        Drop Event.

        Event that occurs when the user decided to drop the current drag on
        the widget.

        Arguments:
            event (Qt.QEvent): Event that contains details.
        '''
        self.in_drag_mode = False
        self.update()
        logger.debug(event.mimeData().text())
        event.acceptProposedAction()
        if event.mimeData().hasFormat("text/uri-list"):
            urls = event.mimeData().urls()
            for url in urls:
                if not os.path.exists(url):
                    logger.debug('cannot open %s' % (url.toString(),))
                    continue
                # ?? classify -> preparse?
                with open(url, 'r') as file_handle:
                    text = file_handle.read(config_backend.MAX_CONFIG_SIZE)
                    self.mainwindow.create_account_from_text.emit(text)
                break

        if event.mimeData().hasFormat('text/plain'):
            self.mainwindow.create_account_from_text.emit(
                event.mimeData().text())

    def mousePressEvent(self, event):
        '''
        Mouse press event.

        Event that occurs when the user clicks on the widget.
        Create a new account using the mainwindow.

        Arguments:
            event (Qt.QEvent): Event with details.
        '''
        if event.button() == QtCore.Qt.LeftButton:
            self.mainwindow.ui.action_create_account_activity.triggered.emit()

    def paintEvent(self, paint_event):
        '''
        Paint Event.

        The widgets needs paint (due to update call). Draw a drop indicator
        when the widget is in_drag_mode.

        Arguments:
            paint_event (Qt.QEvent): Event with details.
        '''
        if not self.in_drag_mode:
            return
        painter = QtGui.QPainter(self)
        all_widget_rect = QtCore.QRect()
        all_widget_rect.setHeight(self.geometry().height())
        all_widget_rect.setWidth(self.geometry().width())
        drop_target_color = QtGui.QColor(
            config_gui.DROP_INDICATOR_COLOR_R,
            config_gui.DROP_INDICATOR_COLOR_G,
            config_gui.DROP_INDICATOR_COLOR_B,
            config_gui.DROP_INDICATOR_COLOR_A)
        drop_target_brush = QtGui.QBrush(
            drop_target_color, QtCore.Qt.SolidPattern)
        drop_target_pen = QtGui.QPen(QtCore.Qt.NoPen)
        painter.setPen(drop_target_pen)
        painter.setBrush(drop_target_brush)
        painter.drawRect(all_widget_rect)
