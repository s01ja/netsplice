# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.gui.model.connection_list import (
    connection_list as connection_list_model
)
from netsplice.gui.model.event_list import (
    event_list as event_list_model
)
from netsplice.model.validator.min import min as min_validator


class gui(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.connections = connection_list_model()

        self.failed_connections = connection_list_model()

        self.events = event_list_model()

        self.log_index = field(
            default=0,
            validators=[min_validator(0)])
