# -*- coding: utf-8 -*-
# grouped_account_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for grouped accounts.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.gui.model.response.grouped_account_item import (
    grouped_account_item as grouped_account_item_model
)


class grouped_account_list(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, grouped_account_item_model)
