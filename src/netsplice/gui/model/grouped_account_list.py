# -*- coding: utf-8 -*-
# grouped_account_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for grouped accounts.
'''

from netsplice.util.errors import NotFoundError
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.gui.model.grouped_account_item import (
    grouped_account_item as grouped_account_item_model
)


class grouped_account_list(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, grouped_account_item_model)

    def add_account(self, group_id, account_id):
        new_grouped_account = self.item_model_class()
        new_grouped_account.id.set(account_id)
        new_grouped_account.group_id.set(group_id)
        self.append(new_grouped_account)

    def find_by_id(self, account_id):
        for grouped_account in self:
            if grouped_account.id.get() == account_id:
                return grouped_account
        raise NotFoundError(
            'No grouped_account with account %s found.' % (account_id,))

    def find_by_group_id(self, group_id):
        accounts = grouped_account_list()
        for grouped_account in self:
            if grouped_account.group_id.get() == group_id:
                accounts.append(grouped_account)
        if len(accounts) is 0:
            raise NotFoundError(
                'No grouped_account with group %s found.' % (group_id,))
        return accounts
