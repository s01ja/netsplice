# -*- coding: utf-8 -*-
# group_create.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for creating groups.
'''

from netsplice.gui.model.validator.group_name import (
    group_name as group_name_validator
)
from netsplice.gui.model.validator.weight import (
    weight as weight_validator
)
from netsplice.model.validator.group_id import (
    group_id as group_id_validator
)
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class group_create(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.name = field(
            required=True,
            validators=[group_name_validator()])

        self.parent = field(
            required=True,
            validators=[group_id_validator()])

        self.weight = field(
            required=True,
            validators=[weight_validator()])
