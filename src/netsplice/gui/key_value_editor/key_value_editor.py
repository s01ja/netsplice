# -*- coding: utf-8 -*-
# keyvalue_editor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Key Value Editor.

Account editor as listview operating on the contents of the config.
'''

from PySide import QtGui, QtCore
from netsplice.gui.key_value_editor.key_value_table import key_value_table
from netsplice.model.configuration_list import (
    configuration_list as configuration_list_model
)


class key_value_editor(QtGui.QWidget):
    '''
    Key Value Editor Widget.

    Custom Editor with keywords as column 0 and value as column 1. Additional
    Values may be set using the values property of the key_value_item.
    Items may have a key and value documentation that is exposed as tooltip
    and identified by the items key.
    '''

    model_changed = QtCore.Signal(object)
    values_changed = QtCore.Signal(object)
    value_checked = QtCore.Signal(object)

    # Toolbar options
    INSERT = 1
    EDIT = 2
    FILTER = 4
    REMOVE = 8
    RESTORE = 16

    # Toolbar action flags

    COLLECTION = 1
    SELECTED = 2

    def __init__(self, parent=None):
        '''
        Initialize Widget.

        Initialize QWidget and member.
        '''
        QtGui.QWidget.__init__(self, parent)

        self.ui = QtGui.QVBoxLayout(self)
        self.ui.setContentsMargins(0, 0, 0, 0)
        self.table = key_value_table(self)
        self.toolbar = None
        self.edit_action = None
        self.filter_input = None
        self.insert_action = None
        self.remove_action = None
        self.restore_action = None
        self.clear_filter_action = None
        self.ui.addWidget(self.table)

        self._custom_row_insert = False
        self._key_documentation = dict()
        self._insert_callback = None
        self._insert_template = list()
        self._toolbar_actions = list()
        self._value_documentation = dict()
        self.table.set_header([self.tr('Option'), self.tr('Value')])
        self.table.row_selected.connect(self._selection_changed)

        self.model_changed.connect(self._model_changed)
        self.table.values_changed.connect(self._commit_change)
        self.table.value_checked.connect(self._value_checked)

    def _clear_filter(self):
        '''
        Clear Filter.

        Clear the user input. Triggers that the table is re-filtered.
        '''
        self.filter_input.setText('')

    def _filter(self):
        '''
        Update Filter.

        Sets the user input to the table filter.
        '''
        filter_text = self.filter_input.text()
        self.table.set_filter([0, 3], filter_text)

    def _insert_row(self):
        '''
        Insert Row.

        Insert a new row in the table.
        '''
        if self._custom_row_insert:
            self._insert_callback()
        else:
            self.table.insert_row(self._insert_template)

    def _commit_change(self):
        '''
        Commit Change.

        Emit that the values of the table have changed.
        '''
        self.values_changed.emit(self.get_values())

    def _edit_row(self):
        '''
        '''
        self.table.edit_row.emit()

    def _model_changed(self, model):
        '''
        Model Changed.

        Set the values for the table from the given model.
        '''
        self.set_values(model)

    def _remove_row(self):
        '''
        Remove Row.

        Remove the selected row from the table.
        '''
        self.table.remove_selected_row()

    def _restore_row(self):
        '''
        Restore Row.

        Restore the selected row to the table defaults.
        '''
        self.table.restore_selected_row()

    def _setup_toolbar(self, action_flags=0):
        '''
        Setup Toolbar.

        Add Actions and Widgets to the toolbar.
        '''
        if self.toolbar is None:
            self.toolbar = QtGui.QToolBar(self)
            self.ui.insertWidget(0, self.toolbar)
            if action_flags & self.INSERT:
                self.add_toolbar_action(
                    self.COLLECTION, self.tr('Insert'), self._insert_row)
            if action_flags & self.REMOVE:
                self.add_toolbar_action(
                    self.SELECTED, self.tr('Remove'), self._remove_row)
            if action_flags & self.EDIT:
                self.add_toolbar_action(
                    self.SELECTED, self.tr('Edit'), self._edit_row)
            if action_flags & self.RESTORE:
                self.add_toolbar_action(
                    self.SELECTED, self.tr('Restore'), self._restore_row)
            if action_flags & self.FILTER:
                self.clear_filter_action = QtGui.QAction(
                    self.tr('Clear'), self)
                self.filter_input = QtGui.QLineEdit(self)
                self.clear_filter_action.triggered.connect(self._clear_filter)
                self.filter_input.textChanged.connect(self._filter)
                self.toolbar.addWidget(self.filter_input)
                self.toolbar.addAction(self.clear_filter_action)

    def _value_checked(self):
        '''
        Value Checked.

        Notify that a value has been checked in the selected row.
        '''
        self.value_checked.emit(self.get_selected_values())

    def add_item(self, item):
        '''
        Add Item.

        Add the given named item to the table and apply the documentation for
        the key.
        '''
        key = item.key.get()
        key_tooltip = None
        values_tooltip = None
        if key in self._key_documentation:
            key_tooltip = self._key_documentation[key]
        if key in self._value_documentation:
            values_tooltip = self._value_documentation[key]
        values = []
        for optional_value in item.values:
            values.append(optional_value)
        self.table.add_row(
            key, values,
            key_tooltip, values_tooltip)

    def add_toolbar_action(self, selection_state, label, callback):
        '''
        Add Toolbar Action.

        Add the given action that is enabled based on the selected item with
        the given label.
        '''
        toolbar_action = QtGui.QAction(self)
        toolbar_action.setText(label)
        toolbar_action.triggered.connect(lambda: callback())
        self.toolbar.addAction(toolbar_action)
        toolbar_action.selection_state = selection_state
        if selection_state & self.COLLECTION:
            toolbar_action.setEnabled(True)
        else:
            toolbar_action.setEnabled(False)
        self._toolbar_actions.append(toolbar_action)

    def _selection_changed(self, row):
        '''
        Selection Changed.

        The selection in the table has changed to the given row. Update the
        toolbar based on the selected row.
        '''
        for toolbar_action in self._toolbar_actions:
            if toolbar_action.selection_state & self.COLLECTION:
                continue
            toolbar_action.setEnabled(row is not -1)

    def disable_editor_column(self, column):
        '''
        Disable Editor Column.

        Disable that any editor is created for the given column.

        Arguments:
            column -- model index column.
        '''
        self.table.disable_editor_column(column)

    def enable_editor_column(self, column):
        '''
        Enable Editor Column.

        Re-enables the given column so it can be edited again.

        Arguments:
            column -- model index column.
        '''
        self.table.enable_editor_column(column)

    def enable_toolbar(self, action_flags):
        '''
        Enable Toolbar.

        Setup and show the toolbar for the table.
        '''
        self._setup_toolbar(action_flags)

    def get_selected_values(self):
        '''
        Get Selected Values.

        Compile a configuration_list from the selected table values.
        '''
        values = configuration_list_model()
        for (row, row_item) in enumerate(self.table.get_selected_values()):
            new_item = values.item_model_class(row)

            new_item.key.set(row_item['key'])

            for (index, value) in enumerate(row_item['values']):
                new_value_item = new_item.values.item_model_class()
                new_value_item.name.set(value.name.get())
                new_value_item.value.set(value.value.get())
                new_item.values.append(new_value_item)
            values.append(new_item)
        return values

    def get_values(self):
        '''
        Get Values.

        Compile a configuration_list from the table values.
        '''
        values = configuration_list_model()
        for (row, row_item) in enumerate(self.table.get_values()):
            new_item = values.item_model_class(row)

            new_item.key.set(row_item['key'])

            for (index, value) in enumerate(row_item['values']):
                new_value_item = new_item.values.item_model_class()
                new_value_item.name.set(value.name.get())
                new_value_item.value.set(value.value.get())
                new_item.values.append(new_value_item)
            values.append(new_item)
        return values

    def in_edit_mode(self):
        '''
        In Edit Mode.

        Return if the table is currently in edit mode.
        '''
        return self.table.in_edit_mode()

    def set_default_values(self, key_value_list):
        '''
        Set Default Values.

        Set values that are considered default and are used for the 'status'
        value.
        Iterate the key_value_list and compile the default values for config
        with hundred of comments.
        '''
        key_map = {}
        for item in key_value_list:
            if item.key.get() == '' and len(item.values) == 0:
                continue
            try:
                key_map[item.key.get()].append(item.values)
            except KeyError:
                key_map[item.key.get()] = [item.values]

        for key, values_list in key_map.items():
            self.table.set_default_values(key, values_list)

        self.table.set_inserted_from_defaults()
        self.table.set_changed_from_defaults()

    def set_header(self, label_list):
        '''
        Set Header.

        Set the given (already translated) strings as the header for the table.
        '''
        self.table.set_header(label_list)

    def set_inserted(self, inserted):
        '''
        Set inserted.

        Set the complete table to behave as if values were inserted.

        Arguments:
            inserted -- True/False
        '''
        self.table.set_inserted(inserted)

    def set_key_documentation(self, documentation):
        '''
        Set Key Documentation.

        Set the given documentation map that is used when rows are added. The
        documentation is used for the key (first) column of the table.
        '''
        self._key_documentation = documentation
        self.table.key_documentation = documentation

    def set_insert_template(self, template):
        '''
        Set Insert Template.

        Set a List of Dicts that will be used for newly inserted rows.
        '''
        self._insert_template = template

    def set_row_edit_callback(self, callback_function):
        '''
        Set Row Editor.

        Set the function that is used to modify the selected row(s).
        '''
        self.table.set_row_edit_callback(callback_function)

    def set_row_insert_callback(self, callback_function):
        '''
        Set Row Insert Callback.

        Override the default cell editor with a editor that creates a new row.
        '''
        if callback_function is None:
            self._custom_row_insert = False
            self._insert_callback = None
        else:
            self._custom_row_insert = True
            self._insert_callback = callback_function

    def set_status_column(self, column, state_map):
        '''
        Set State Column.

        Set the column that is updated by the table based on the state of the
        row with the texts from the state_map.
        '''
        self.table.set_status_column(column, state_map)

    def set_value_column(self, column):
        '''
        Set Value Column.

        Set the column that is used for values and for comparing to default
        values.
        '''
        self.table.set_value_column(column)

    def set_value_documentation(self, documentation):
        '''
        Set Value Documentation.

        Set the given documentation map that is used when rows are added. The
        documentation is used for all columns except the first.
        '''
        self._value_documentation = documentation
        self.table.value_documentation = documentation

    def set_values(self, key_value_list):
        '''
        Set Values.

        Set the given key_value_list to the table. Clear all current value
        first. Try to restore the current selected key.
        '''
        selected_key = self.table.get_selected_key()
        self.table.clear()
        self.table.setRowCount(0)
        self.table.reset_header()
        for item in key_value_list:
            if item.key.get() == '' and len(item.values) == 0:
                continue
            self.add_item(item)

        self.table.resizeColumnsToContents()
        self.table.set_selected_key(selected_key)

    def update_selected_values(self, new_values):
        '''
        Update Selected Values.

        Change the table selection with the given values
        '''
        update_values = []
        for value in new_values:
            update_value = {
                'key': value.key.get(),
                'values': value.values
            }
            update_values.append(update_value)
        self.table.update_selected_values(update_values)
