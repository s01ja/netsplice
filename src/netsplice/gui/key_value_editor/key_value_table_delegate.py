# -*- coding: utf-8 -*-
# key_value_table_delegate.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Key Value Table Item Delegate.

ItemView delegate to display combo boxes for the known documentation keywords.
'''
from PySide.QtGui import QItemDelegate, QComboBox
from PySide.QtCore import Qt


class key_value_table_delegate(QItemDelegate):
    def __init__(self, parent):
        QItemDelegate.__init__(self, parent)
        self.table = parent

    def createEditor(self, parent, option, index):
        '''
        Create Editor.

        Overload that creates a QWidget that is placed and handled by the other
        methods in this class. The parent widget and style option are used to
        control how the editor widget appears.

        Arguments:
            parent -- Widget(ItemView).
            option -- Style information.
            index -- ModelIndex to edit.
        '''
        if not self.table.can_edit(index):
            return None
        if not self.table.has_documentation(index):
            return QItemDelegate.createEditor(self, parent, option, index)
        widget = QComboBox(parent)
        widget.setEditable(True)
        return widget

    def setEditorData(self, editor, index):
        '''
        Set Editor Data.

        Set the data to be displayed and edited by the editor from the data
        model item specified by the model index.


        Arguments:
            editor -- Widget that will now be used for edit.
            index -- ModelIndex to edit.
        '''
        if not isinstance(editor, (QComboBox,)):
            return QItemDelegate.setEditorData(self, editor, index)
        value = index.model().data(index, Qt.EditRole)
        has_key = False
        docs = dict()
        if index.column() == 0:
            docs = self.table.key_documentation
        elif index.column() == self.table._value_column:
            docs = self.table.value_documentation
        for key, description in docs.iteritems():
            editor.addItem(key, key)
            editor.setItemData(
                editor.findData(key),
                description,
                Qt.ToolTipRole)
            if key == value:
                has_key = True
        if not has_key:
            editor.addItem(value, value)
        editor.setCurrentIndex(editor.findText(value))

    def setModelData(self, editor, model, index):
        '''
        Set Model Data.

        Get data from the editor widget and store it in the specified model
        at the item index.

        Arguments:
            editor -- Widget that will now be used for edit.
            model -- Model of the Item View
            index -- ModelIndex to update.
        '''
        if not isinstance(editor, (QComboBox,)):
            return QItemDelegate.setModelData(self, editor, model, index)
        value = editor.currentText()
        model.setData(index, value, Qt.EditRole)

    def updateEditorGeometry(self, editor, option, index):
        '''
        Update Editor Geometry

        Updates the editor for the item specified by index according to the
        style option given.

        Arguments:
            editor -- [description]
            option -- [description]
            index -- [description]
        '''
        editor.setGeometry(option.rect);
