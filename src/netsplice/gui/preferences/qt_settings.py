# -*- coding: utf-8 -*-
# qt_settings.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Settings for gui application.

Supports early-stage settings for the gui that get synced with the
preferences.
'''

from netsplice.config import constants
from netsplice.config import backend as config_backend
from netsplice.config import gui as config_gui

from PySide.QtCore import QSettings

GEOMETRY_MAINWINDOW = 'GEOMETRY_MAINWINDOW'


class qt_settings(object):
    '''
    Settings object.

    Loads the settings from ini/plist/conf and syncs the config values,
    defines fallback/default. Handles the load and store of values.
    '''

    def __init__(self):
        '''
        Initialize object.

        Sets and loads defaults.
        '''
        self._settings = QSettings(constants.NS_NAME, constants.NS_NAME)
        self.load()

    def get_mainwindow_geometry(self):
        '''
        Get Mainwindow Geometry.

        Return None or the last geometry of the mainwindow.
        '''
        return self._settings.value(GEOMETRY_MAINWINDOW)

    def load(self):
        '''
        Load Startup Setting.

        Loads the last configured theme from the qt settings. This prevents
        that the gui application is unconfigured on startup until the backend
        preferences are loaded.
        '''
        for config_name in config_gui.SETTING_PREFERENCES:
            value = self._settings.value(config_name)
            if value is None:
                # keep the compiled-in value
                continue
            # map strings to bool (PySide:#459)
            if value == 'true':
                value = True
            if value == 'false':
                value = False
            config_gui.__dict__[config_name] = value

    def store(self):
        '''
        Store Startup Setting.

        Store the setting in the registry/plist/conf that is managed by
        QT. This allows the startup to load the correct values without waiting
        for the backend.
        '''
        for config_name in config_gui.SETTING_PREFERENCES:
            self._settings.setValue(
                config_name, config_gui.__dict__[config_name])
        self._settings.sync()

    def store_mainwindow_geometry(self, geometry):
        '''
        Store Mainwindow Geometry.

        Store the geometry of the mainwindow to the settings so it can be
        restored at the same location on the desktop.
        '''
        self._settings.setValue(GEOMETRY_MAINWINDOW, geometry)
        self._settings.sync()

    def update_backend(self, backend_model):
        '''
        Update Backend.

        Update backend settings.
        '''
        config_backend.STORE_PASSWORD_DEFAULT = \
            backend_model.store_password_default.get()

    def update_gui(self, ui_model):
        '''
        Update Gui.

        Update the config_gui variables from the ui-model.
        '''
        config_gui.CLOSE_TO_SYSTRAY = ui_model.close_to_systray.get()
        config_gui.START_DRAG_DISTANCE = ui_model.start_drag_distance.get()
        config_gui.SHOW_LOG_ON_CONNECTION_FAILURE = \
            ui_model.show_log_on_connection_failure.get()
        config_gui.SHOW_WELCOME_ON_START = ui_model.show_welcome_on_start.get()

        config_gui.SHORTCUT_HELP = ui_model.shortcut_help.get()
        config_gui.SHORTCUT_CONNECT_ALL = ui_model.shortcut_connect_all.get()
        config_gui.SHORTCUT_CREATE_CONNECTION = \
            ui_model.shortcut_create_connection.get()
        config_gui.SHORTCUT_SELECTED_CONNECT = \
            ui_model.shortcut_selected_connect.get()
        config_gui.SHORTCUT_SELECTED_DISCONNECT = \
            ui_model.shortcut_selected_disconnect.get()
        config_gui.SHORTCUT_SELECTED_EDIT = \
            ui_model.shortcut_selected_edit.get()
        config_gui.SHORTCUT_SELECTED_RENAME = \
            ui_model.shortcut_selected_rename.get()
        config_gui.SHORTCUT_SHOW_LOGS = \
            ui_model.shortcut_show_logs.get()
        config_gui.SHORTCUT_SHOW_PREFERENCES = \
            ui_model.shortcut_show_preferences.get()
        config_gui.SHORTCUT_QUIT = \
            ui_model.shortcut_quit.get()
        config_gui.SYSTRAY_ANIMATE_ICON = \
            ui_model.systray_animate_icon.get()
        config_gui.SYSTRAY_SHOW_NOTIFICATIONS = \
            ui_model.systray_show_notifications.get()

        config_gui.START_HIDDEN = ui_model.start_hidden.get()

        config_gui.THEME = ui_model.theme.get()
