# -*- coding: utf-8 -*-
# systray.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Custom system tray manager.
'''

import sys
from PySide.QtCore import QTimer
from PySide.QtGui import QSystemTrayIcon, QIcon, QPixmap
from netsplice.config import gui as config_gui
from netsplice.gui.systray import config
from netsplice.gui.systray import messages
from netsplice.gui.systray.menu import menu

WAIT_FOR_ACCOUNTS_TIMEOUT = 300


class systray(QSystemTrayIcon):
    '''
    Custom system tray that changes its icon based on the connection state
    and notifies when the state changes.
    '''

    def __init__(self, parent=None, dispatcher=None):
        QSystemTrayIcon.__init__(self, parent)
        self.backend = dispatcher
        self.backend_available = False
        self.mainwindow = parent
        self.connected = False
        self.connecting = False
        self.connecting_index = -1
        self._wait_for_accounts_timer = None

        self.ICON_TRAY = QIcon()
        self.ICON_TRAY_CONNECTED = QIcon()
        self.ICON_TRAY_CONNECTING = []
        self.ICON_TRAY_CONNECTING.append(QIcon())
        self.ICON_TRAY_CONNECTING.append(QIcon())
        self.ICON_TRAY_CONNECTING.append(QIcon())
        self.ICON_TRAY_BACKEND_NOT_AVAILABLE = QIcon()
        self._load_icons()

        self.menu = menu(self, self.backend)

        self.setContextMenu(self.menu)
        self._update_icon()
        self.show()
        self.activated.connect(self._tray_activated)
        self.backend.application.event_loop.connections_changed.connect(
            self._backend_connections_changed)
        self.backend.application.event_loop.accounts_changed.connect(
            self._backend_accounts_changed)

        self._services = {}
        self._accounts = None
        self._recent_connecting_accounts = []
        self._recent_connected_accounts = []

    def _animate_connecting_icon(self):
        '''
        Animate connecting icon.

        When the connecting state is False, stop the animation, otherwise
        rotate through the config.ICON_TRAY_CONNECTING icons with a timer
        scheduling the next icon.
        '''
        if not self.connecting:
            # End animation by checking the connecting state.
            # Causes next update_connecting_icon() to start animation
            self.connecting_index = -1
            return

        self.setIcon(self.ICON_TRAY_CONNECTING[self.connecting_index])
        self.connecting_index += 1
        if self.connecting_index >= len(self.ICON_TRAY_CONNECTING):
            self.connecting_index = 0
        animate = lambda: self._animate_connecting_icon()
        QTimer.singleShot(
            config.SYSTRAY_ANIMATION_DELAY, animate)

    def _change_tooltip(self, connections):
        '''
        Change Tooltip.

        Change the tooltip to display the accounts that are currently
        active.

        Arguments:
            connections (list): list of backend connections.
        '''
        tooltip = self.tr('Active Connections')
        for connection_item in connections:
            name = connection_item.get_account_name(self._accounts)
            state = connection_item.state.get()
            tooltip += '\n%s %s: %s' % (self.tr('Account'), name, state)
        self.set_service_tooltip(tooltip)

    def _change_tray_icon(self, connections):
        '''
        Change Tray Icon.

        Change the icon in the system tray to reflect the current backend
        activity.

        Arguments:
            connections (list): list of backend connections.
        '''
        connecting = False
        connected = False
        for connection_item in connections:
            if connection_item.is_connecting():
                connecting = True
                continue
            if connection_item.is_connected():
                connected = True
                continue
        connecting_with_connected = (
            self.connecting and self.connected and
            self.connecting != connecting)
        if self.connecting != connecting:
            self.set_connecting(connecting)
        if self.connected != connected or connecting_with_connected:
            self.set_connected(connected)

    def _backend_connections_changed(
            self, connection_list, failed_connection_list):
        '''
        Backend Connections Changed.

        Handler for the backend connections.
        Display messages, update tray icon and tooltip.

        Arguments:
            connection_list (list): list of backend connections.
        '''
        self._show_notification_messages(
            connection_list, failed_connection_list)
        self._change_tray_icon(connection_list)
        self._change_tooltip(connection_list)

    def _backend_accounts_changed(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Backend Accounts Changed.

        Handler for the backend account change event.

        Arguments:
            accounts_model (list): All configured accounts
            groups_model (list): All configured groups
            grouped_accounts_model (list): \
                All configured accounts with a group parent (unchained)
            chains_model (list): All configured accounts with \
                their chain relation.
        '''
        self._accounts = accounts_model

    def _load_icons(self):
        '''
        Load Icons.

        Load icons that are used for the system tray. This loader is called
        once on initialize so the icons are reused.
        '''
        self.ICON_TRAY.addPixmap(
            QPixmap(config.RES_ICON_TRAY),
            QIcon.Normal, QIcon.Off)
        self.ICON_TRAY_CONNECTED.addPixmap(
            QPixmap(config.RES_ICON_TRAY_CONNECTED),
            QIcon.Normal, QIcon.Off)
        self.ICON_TRAY_CONNECTING[0].addPixmap(
            QPixmap(config.RES_ICON_TRAY_CONNECTING_0),
            QIcon.Normal, QIcon.Off)
        self.ICON_TRAY_CONNECTING[1].addPixmap(
            QPixmap(config.RES_ICON_TRAY_CONNECTING_1),
            QIcon.Normal, QIcon.Off)
        self.ICON_TRAY_CONNECTING[2].addPixmap(
            QPixmap(config.RES_ICON_TRAY_CONNECTING_2),
            QIcon.Normal, QIcon.Off)
        self.ICON_TRAY_BACKEND_NOT_AVAILABLE.addPixmap(
            QPixmap(config.RES_ICON_TRAY_BACKEND_NOT_AVAILABLE),
            QIcon.Normal, QIcon.Off)

    def _show_notification_messages(self, connections, failed_connection_list):
        '''
        Show notification messages.

        Evaluate which connections have changed and collect their
        account-names.
        Evaluate which state-change has happened.
        Display notification message based on the changed state.
        '''
        if config_gui.SYSTRAY_SHOW_NOTIFICATIONS is False:
            return

        if self._accounts is None:
            self._wait_for_accounts_timer = QTimer(self)
            self._wait_for_accounts_timer.timeout.connect(
                lambda: self._show_notification_messages(
                    connections, failed_connection_list))
            self._wait_for_accounts_timer.setInterval(
                WAIT_FOR_ACCOUNTS_TIMEOUT)
            self._wait_for_accounts_timer.setSingleShot(True)
            self._wait_for_accounts_timer.start()
            return

        connected_account_names = []
        connecting_account_names = []
        disconnected_account_names = []
        failed_account_names = []
        show_connected_account_names = []
        show_connecting_account_names = []
        for connection_item in connections:
            if connection_item.is_connecting():
                connecting_account_names.append(
                    connection_item.get_account_name(self._accounts)
                )
                continue
            if connection_item.is_connected():
                connected_account_names.append(
                    connection_item.get_account_name(self._accounts)
                )
                continue

        # Evaluate changed state.
        for account_name in self._recent_connected_accounts:
            if account_name not in connected_account_names:
                disconnected_account_names.append(account_name)
        for account_name in connected_account_names:
            if account_name not in self._recent_connected_accounts:
                show_connected_account_names.append(account_name)
        for account_name in self._recent_connecting_accounts:
            if account_name not in connecting_account_names:
                if account_name not in connected_account_names:
                    failed_account_names.append(account_name)
        for account_name in connecting_account_names:
            if account_name not in self._recent_connecting_accounts:
                show_connecting_account_names.append(account_name)
        self._recent_connected_accounts = connected_account_names
        self._recent_connecting_accounts = connecting_account_names

        # Display messages.
        if len(disconnected_account_names):
            self.show_disconnected_message(disconnected_account_names)
        if len(failed_account_names):
            self.show_failed_message(failed_account_names)
        if len(show_connecting_account_names):
            self.show_connecting_message(show_connecting_account_names)
        if len(show_connected_account_names):
            self.show_connected_message(show_connected_account_names)

    def _tray_activated(self, reason=None):
        '''
        TRIGGERS:
            self.activated

        :param reason: The reason why the tray got activated.
        :type reason: int

        Display the context menu from the tray icon.
        '''
        if reason is QSystemTrayIcon.Trigger:
            if not sys.platform.startswith('darwin'):
                if not self.mainwindow.isVisible():
                    self.show_main_window()
                else:
                    self.hide_main_window()
                return
        context_menu = self.contextMenu()

        if not sys.platform.startswith('darwin'):
            # For some reason, context_menu.show()
            # is failing in a way beyond my understanding.
            # (not working the first time it's clicked).
            # this works however.
            context_menu.exec_(self.geometry().center())

    def _update_icon(self):
        '''
        Update Icon.

        Update the tray icon according to the current state.
        '''
        if not self.backend_available:
            self.setIcon(self.ICON_TRAY_BACKEND_NOT_AVAILABLE)
            return
        if self.connecting:
            if config_gui.SYSTRAY_ANIMATE_ICON:
                self.set_connecting_icon()
            return
        if self.connected:
            self.setIcon(self.ICON_TRAY_CONNECTED)
            return
        self.setIcon(self.ICON_TRAY)

    def create_account(self):
        '''
        Create Account.

        Handler for the create account menu action.
        '''
        self.mainwindow.create_account_activity()

    def hide_main_window(self):
        '''
        Hide Main Window.

        Handler for the hide main window menu action.
        '''
        self.mainwindow.ensure_invisible()

    def quit(self):
        '''
        Quit.

        Handler for the quit menu action.
        '''
        self.mainwindow.quit()

    def set_backend_available(self, backend_available):
        '''
        Set Backend Available.

        Notify the user that the backend is available and store the state so
        future calls to this function only notify when the state has changed.
        '''
        if self.backend_available != backend_available:
            if backend_available is False:
                self.show_named_message(
                    messages.BACKEND_NOT_AVAILABLE,
                    icon=QSystemTrayIcon.Critical)
        self.backend_available = backend_available
        self._update_icon()

    def set_connected(self, connected, account_names=[]):
        '''
        Set Connected.

        Set the internal connected state and update the icon.
        '''
        self.connected = connected
        self._update_icon()

    def set_connecting(self, connecting, account_names=[]):
        '''
        Set Connecting.

        Set the internal connecting state and update the icon.
        '''
        self.connecting = connecting
        self._update_icon()

    def set_connecting_icon(self):
        '''
        Set Connecting Icon.

        Set the initial connecting_index and start the connecting animation.
        '''
        if self.connecting and self.connecting_index == -1:
            self.connecting_index = 0
            self._animate_connecting_icon()

    def set_service_tooltip(self, tooltip):
        '''
        Sets the service tooltip.

        :param service: service name identifier.
        :type service: unicode
        :param tooltip: tooltip to display for that service.
        :type tooltip: unicode
        '''

        self.setToolTip(tooltip)

    def show_connected_message(self, account_names):
        '''
        Show Connected Message.

        Show a message that the given account_names are now connected.

        Arguments:
            account_names (string): Names of accounts.
        '''
        self.show_named_message(messages.CONNECTED, {
            'account_name': ', '.join(account_names)
        })

    def show_connecting_message(self, account_names):
        '''
        Show Connecting Message.

        Show a message that the given account_names are now connecting.

        Arguments:
            account_names (string): Names of accounts.
        '''
        self.show_named_message(messages.CONNECTING, {
            'account_name': ', '.join(account_names)
        })

    def show_disconnected_message(self, account_names):
        '''
        Show Disconnected Message.

        Show a message that the given account_names are now disconnected.

        Arguments:
            account_names (string): Names of accounts.
        '''
        self.show_named_message(messages.DISCONNECTED, {
            'account_name': ', '.join(account_names)
        }, icon=QSystemTrayIcon.Warning)

    def show_failed_message(self, account_names):
        '''
        Show Failed Message.

        Show a message that the given account_names have failed.

        Arguments:
            account_names (string): Names of accounts.
        '''
        self.show_named_message(messages.FAILED, {
            'account_name': ', '.join(account_names)
        }, icon=QSystemTrayIcon.Critical)

    def show_main_window(self):
        '''
        Show Main Window.

        Handler for the show main window menu action.
        '''
        self.mainwindow.ensure_visible()

    def show_named_message(
            self, message, options=dict(),
            icon=QSystemTrayIcon.Information, timeout=10000):
        '''
        Show Named Message.

        Display a message with options, a icon and a timeout. This method
        is used for displaying any message (failed/connected/disconnected).

        Arguments:
            message (dict): Message with a title and a body. The body may \
                contain named placeholders that are replaced with the options \
                argument.

        Keyword Arguments:
            options (dict): Dictionary with named arguments to be replaced \
                in the message body (default: {dict()})
            icon (QIcon): Icon that may be used for the message. \
                (default: {QSystemTrayIcon.Information})
            timeout (number): [description] (default: {10000})
        '''
        self.showMessage(
            self.tr(message['title']),
            self.tr(message['body']) % options,
            icon,
            timeout)

    def show_quit_message(self):
        '''
        Show Quit Message.

        Display a message that the application is going to quit.
        '''
        self.show_named_message(messages.QUIT)
