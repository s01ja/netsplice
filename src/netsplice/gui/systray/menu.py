# -*- coding: utf-8 -*-
# menu.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Custom system tray menu.
'''

from PySide.QtCore import Signal
from PySide.QtGui import QAction, QMenu

from netsplice.config import gui as config_gui
from netsplice.gui import get_systray_menu_items
from netsplice.gui.model.account_list_item import (
    account_list_item as account_list_item_model
)
from netsplice.gui.model.group_list import (
    group_list as group_list_model
)
from netsplice.gui.model.chain_list import (
    chain_list as chain_list_model
)
from netsplice.gui.model.grouped_account_list import (
    grouped_account_list as grouped_account_list_model
)
from netsplice.gui.model.connection_list import (
    connection_list as connection_list_model
)
from netsplice.util.errors import NotFoundError


class menu(QMenu):
    '''
    Custom system tray menu that handles the account items based on their
    state.
    '''
    backend_done = Signal(basestring)
    backend_failed = Signal(basestring)

    def __init__(self, parent=None, dispatcher=None):
        QMenu.__init__(self, parent.parent())
        self.systray = parent
        self.backend = dispatcher

        self.connections = connection_list_model()

        self.account_menus = list()
        self.chain_menus = list()
        self.group_menus = list()
        self.account_state = list()
        self.group_state = list()
        self.chain_state = list()

        self.accounts = account_list_item_model()
        self.chains = chain_list_model()
        self.groups = group_list_model()
        self.grouped_accounts = grouped_account_list_model()

        self._action_show = QAction(self.tr(
            "Show Main Window"), self)
        self._action_show.triggered.connect(self._show_main_window)
        self._action_hide = QAction(self.tr(
            "Hide Main Window"), self)
        self._action_hide.triggered.connect(self._hide_main_window)

        self._action_create_account = QAction(self.tr(
            "Create Connection"), self)
        self._action_create_account.triggered.connect(self._create_account)

        self._action_quit = QAction(self.tr("Quit"), self)
        self._action_quit.triggered.connect(self._quit)

        self.addAction(self._action_show)
        self.addAction(self._action_hide)
        self.addSeparator()
        self._add_plugin_menu_items()

        self.addAction(self._action_create_account)
        self._action_account_separator = self.addSeparator()
        self.addAction(self._action_quit)
        self.backend.application.event_loop.connections_changed.connect(
            self._backend_connections_changed)
        self.backend.application.event_loop.accounts_changed.connect(
            self._backend_accounts_changed)

    def _add_accounts(
            self, grouped_accounts_model, accounts_model):
        '''
        Add new accounts.

        Iterate the accounts_model and create an account item for
        each item that can not be found in the current account list.
        '''
        group_map = {}
        for grouped_account in grouped_accounts_model:
            try:
                group_map[grouped_account.group_id.get()].append(
                    grouped_account)
            except KeyError:
                group_map[grouped_account.group_id.get()] = [
                    grouped_account
                ]

        for group_id, group_items in group_map.items():
            sorted_accounts = sorted(
                group_items, key=self._get_group_weight)

            for account_item in sorted_accounts:
                if len(self.chains.get_children(account_item.id.get())) > 0:
                    self._create_chain_item(None, account_item.id.get())
                else:
                    self._create_account_item(account_item.id.get())

    def _add_groups(
            self, parent_group_menu, parent_item, groups_model):
        '''
        Add new groups.

        Add the actions with the preferences groups.
        '''
        groups = groups_model.find_children(parent_item.id.get())
        sorted_groups = sorted(
            groups, key=self._get_group_weight)
        for group_item in sorted_groups:
            group_menu = None
            for active_group_menu in self.group_menus:
                if active_group_menu['id'] == group_item.id.get():
                    group_menu = active_group_menu['menu']
                    break
            if group_menu is not None:
                continue
            group_menu = self._create_group_menu(
                parent_group_menu, group_item)
            self._add_groups(
                group_menu, group_item, groups_model)

    def _add_plugin_menu_items(self):
        '''
        Add Plugin Menu Items.

        Add the items that are registered to the systray menu.
        '''
        plugin_menu_items = get_systray_menu_items()
        if len(plugin_menu_items) > 0:
            for menu_item in plugin_menu_items:
                menu_action = QAction(menu_item['label'], self)
                menu_action.triggered.connect(menu_item['callback'])
                self.addAction(menu_action)
                if menu_item['separator']:
                    self.addSeparator()
            self.addSeparator()

    def _backend_accounts_changed(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Slot for handling that the preferences have changed.
        '''
        updated = False
        updated |= self._update_account_state(grouped_accounts_model)
        updated |= self._update_group_state(groups_model)
        updated |= self._update_chain_state(chains_model)
        if updated:
            # avoid frequent recreation as the menu acts weird when
            # actions are removed or created while it is visible.
            self.accounts = accounts_model
            self.chains = chains_model
            self.groups = groups_model
            self.grouped_accounts = grouped_accounts_model
            self._recreate_model_menu(
                accounts_model, groups_model, grouped_accounts_model,
                chains_model)

    def _backend_connections_changed(
            self, connection_list, failed_connection_list):
        '''
        Slot for handling that the backend model has changed.
        '''
        self._update_context_connections(connection_list)

    def _create_account(self):
        '''
        Create Account.

        Handler to forward the menu action to the systray class.
        '''
        self.systray.create_account()

    def _create_account_item(self, account_id):
        '''
        Create account item.

        Create an account (menu) item in the correct group submenu.
        '''
        account_item = self.accounts.find_by_id(account_id)
        chain_item = self.chains.find_by_id(account_id)
        account_action = QAction(account_item.name.get(), self)
        account_action.triggered.connect(
            lambda: self._toggle_account(account_item)
        )
        account_action.setData(account_item)
        account_action.setEnabled(False)
        account_action.setChecked(False)
        account_action.setCheckable(True)
        account_action.setVisible(True)
        self._update_account_item_state(account_action)
        account_parent = self
        try:
            grouped_account_item = self.grouped_accounts.find_by_id(
                account_id)
            for group_menu in self.group_menus:
                if group_menu['id'] == grouped_account_item.group_id.get():
                    account_parent = group_menu['menu']
                    break
        except NotFoundError:
            pass

        for chain_menu in self.chain_menus:
            if chain_menu['id'] == account_id:
                account_parent = chain_menu['menu']
                break
        if chain_item.parent_id.get() is not None and account_parent is self:
            for chain_menu in self.chain_menus:
                if chain_menu['id'] == chain_item.parent_id.get():
                    account_parent = chain_menu['menu']
                    break
        if account_parent is self:
            before_action = self._action_account_separator
            account_parent.insertAction(before_action, account_action)
        else:
            account_parent.addAction(account_action)
        self.account_menus.append({
            'id': account_id,
            'action': account_action,
            'parent_menu': account_parent})

    def _create_chain_item(self, parent_menu, chain_id):
        '''
        Create group menu.

        Create a group menu item.
        '''
        chain_item = self.chains.find_by_id(chain_id)
        account_item = self.accounts.find_by_id(chain_id)
        chain_children = self.chains.get_children(chain_id)
        label_text = account_item.name.get()
        if len(chain_children) > 0:
            chain_menu = QMenu(label_text, self)
            chain_menu.setEnabled(True)
            chain_menu_action = None
            if parent_menu is None:
                grouped_account = self.grouped_accounts.find_by_id(chain_id)
                for group_menu in self.group_menus:
                    if group_menu['id'] == grouped_account.group_id.get():
                        parent_menu = group_menu['menu']
                        break
            if parent_menu is None:
                parent_menu = self
                before_action = self._action_account_separator
                chain_menu_action = parent_menu.insertMenu(
                    before_action, chain_menu)
            else:
                chain_menu_action = parent_menu.addMenu(chain_menu)
            chain_menu_action.setData(chain_item)

            self.chain_menus.append({
                'id': chain_item.id.get(),
                'parent': chain_item.parent_id.get(),
                'menu': chain_menu,
                'action': chain_menu_action,
                'parent_menu': parent_menu})
            self._create_account_item(chain_id)
            for child in chain_children:
                self._create_chain_item(chain_menu, child.id.get())
        else:
            self._create_account_item(chain_id)

    def _create_group_menu(self, parent_group_menu, group_item):
        '''
        Create group menu.

        Create a group menu item.
        '''
        label_text = group_item.name.get()
        if group_item.id.get() == self.groups.find_global().id.get():
            label_text = self.tr(config_gui.GLOBAL_GROUP_NAME)
        group_menu_action = None
        group_menu = QMenu(label_text, self)
        group_menu.setEnabled(True)

        if parent_group_menu is self:
            before_action = self._action_account_separator
            group_menu_action = parent_group_menu.insertMenu(
                before_action, group_menu)
        else:
            group_menu_action = parent_group_menu.addMenu(group_menu)
        group_menu_action.setData(group_item)

        self.group_menus.append({
            'id': group_item.id.get(),
            'parent': group_item.parent.get(),
            'menu': group_menu,
            'action': group_menu_action,
            'parent_menu': parent_group_menu})
        return group_menu

    def _find_account_action_by_id(self, account_id):
        '''
        Find account_action by id.

        Find the given account_id in any of the group menus or the root menu.
        '''
        for menu_item in self.account_menus:
            if menu_item['id'] != account_id:
                continue
            return menu_item['action']
        return None

    def _get_account_item_data(self, action):
        '''
        Get Account item data.

        Return the account_list_item_model for the given action or
        None if the action is not related to a model.
        '''
        if action.data() is None:
            return None
        account_item = action.data()
        if not isinstance(account_item, (account_list_item_model,)):
            return None
        return account_item

    def _get_account_weight(self, account_item):
        '''
        Get account weight.

        Return the weight for the item used for sorting.
        '''
        return account_item.weight.get()

    def _get_chain_weight(self, chain_item):
        '''
        Get chain weight.

        Return the weight for the item used for sorting.
        '''
        return chain_item.weight.get()

    def _get_group_weight(self, group_item):
        '''
        Get group weight.

        Return the weight for the item used for sorting.
        '''
        return group_item.weight.get()

    def _hide_main_window(self):
        '''
        Hide main window.

        Request the systray to hide the main window.
        '''
        self.systray.hide_main_window()

    def _recreate_model_menu(
            self, accounts_model, groups_model,
            grouped_accounts_model, chains_model):
        '''
        Recreate model menu.

        Remove accounts and groups and build the menu based on the models.
        '''
        self._remove_accounts()
        self._remove_chains()
        self._remove_groups()
        root_item = groups_model.find_global()
        self._add_groups(self, root_item, groups_model)
        self._add_accounts(grouped_accounts_model, accounts_model)
        self._update_context_accounts(accounts_model)

    def _remove_accounts(self):
        '''
        Remove accounts.

        Remove all accounts that have a menu item.
        '''
        for menu_item in self.account_menus:
            menu_item['parent_menu'].removeAction(menu_item['action'])
        self.account_menus = []

    def _remove_chains(self):
        '''
        Remove chains.

        Remove all chains that have a menu item.
        '''
        for menu_item in self.chain_menus:
            menu_item['parent_menu'].removeAction(menu_item['action'])
        self.chain_menus = []

    def _remove_groups(self):
        '''
        Remove Groups.

        Remove all groups that have a menu item.
        '''
        for menu_item in self.group_menus:
            menu_item['parent_menu'].removeAction(menu_item['action'])
        self.group_menus = []

    def _show_main_window(self):
        '''
        Show Main Window.

        Handler to forward the menu action to the systray class.
        '''
        self.systray.show_main_window()

    def _toggle_account(self, account_item_model):
        connected = False
        connecting = False
        try:
            connection_model = self.connections.find_by_account_id(
                account_item_model.id.get())
            connected = connection_model.is_connected()
            connecting = connection_model.is_connecting()
        except NotFoundError:
            pass

        if connecting or connected:
            self.backend.account_disconnect.emit(
                account_item_model.id.get(),
                self.backend_done, self.backend_failed)
        else:
            self.backend.account_connect.emit(
                account_item_model.id.get(),
                self.backend_done,
                self.backend_failed)

    def _update_account_state(self, grouped_accounts):
        '''
        Update account state.

        Create a state-hash that can be compared between
        model changes. Store the hash when the state has changed
        and return True. Return False when no change was detected.
        '''
        new_state = []
        for grouped_account in grouped_accounts:
            state_line = (
                '%s%d%s%s'
                % (
                    grouped_account.name.get(),
                    grouped_account.weight.get(),
                    grouped_account.id.get(),
                    grouped_account.group_id.get()))
            new_state.append(state_line)
        if new_state == self.account_state:
            return False
        self.account_state = new_state
        return True

    def _update_account_item_state(self, account_action):
        '''
        Update account_item state.

        Update the state/label of the account_action based on the
        connection state.
        '''
        account_item_model = self._get_account_item_data(account_action)
        if account_item_model is None:
            account_action.setEnabled(False)
            return
        enabled = True
        label_text = ''
        connected = False
        connecting = False
        enabled = account_item_model.enabled.get()

        try:
            connection_model = self.connections.find_by_account_id(
                account_item_model.id.get())
            connected = connection_model.is_connected()
            connecting = connection_model.is_connecting()
        except NotFoundError:
            pass

        if connected:
            label_text = self.tr('Disconnect')
            label_text += ' '
            label_text += account_item_model.name.get()
            # XXX account_item_model.connect_time.get()
            # label_text += '\t'  # right-align time
            # label_text += '365 days ago'
            account_action.setChecked(True)
        else:
            label_text = self.tr('Connect')
            label_text += ' '
            label_text += account_item_model.name.get()
            account_action.setChecked(False)

        if connecting:
            label_text = self.tr('Connecting')
            label_text += ' '
            label_text += account_item_model.name.get()
            label_text += '\t'
            # XXX account_item_model.connect_time.get()
            # label_text += '\t'  # right-align time
            # label_text += '30 seconds ago'

        if not enabled:
            label_text = self.tr('Disabled')
            label_text += ' '
            label_text += account_item_model.name.get()

        account_action.setEnabled(enabled)
        account_action.setText(label_text)

    def _update_context_accounts(self, accounts_model):
        '''
        Update context accounts.

        Sync the actions with the preferences accounts
        '''
        if len(accounts_model) is 0:
            self._action_create_account.setVisible(True)
        else:
            self._action_create_account.setVisible(False)

    def _update_context_connections(self, connection_list_model_instance):
        '''
        Update context actions.

        Update the actions based on their connected state.
        '''
        self.connections = connection_list_model_instance
        for menu_item in self.account_menus:
            self._update_account_item_state(menu_item['action'])

    def _update_chain_state(self, chains_model):
        '''
        Update group state.

        Create a state-hash that can be compared between
        model changes. Store the hash when the state has changed
        and return True. Return False when no change was detected.
        '''
        new_state = []
        for chain in chains_model:
            state_line = (
                '%s%d%s'
                % (
                    chain.id.get(),
                    chain.weight.get(),
                    chain.parent_id.get()))
            new_state.append(state_line)
        if new_state == self.chain_state:
            return False
        self.chain_state = new_state
        return True

    def _update_group_state(self, groups_model):
        '''
        Update group state.

        Create a state-hash that can be compared between
        model changes. Store the hash when the state has changed
        and return True. Return False when no change was detected.
        '''
        new_state = []
        for group in groups_model:
            state_line = (
                '%s%d%s%s'
                % (
                    group.name.get(),
                    group.weight.get(),
                    group.id.get(),
                    group.parent.get()))
            new_state.append(state_line)
        if new_state == self.group_state:
            return False
        self.group_state = new_state
        return True

    def _quit(self):
        '''
        Quit.

        Handler to forward the menu action to the systray class.
        '''
        self.systray.quit()
