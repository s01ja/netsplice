# -*- coding: utf-8 -*-
# messages.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Named Messages.
All the Strings are passed to qt::tr for translation
'''

BACKEND_NOT_AVAILABLE = dict(
    title='Backend Not Available',
    body=r'The backend is not available. Please restart the application.'
)
CONNECTING = dict(
    title=r'Connecting',
    body=r'%(account_name)s: connecting.'
)
CONNECTED = dict(
    title=r'Connected',
    body=r'%(account_name)s: connected.'
)
DISCONNECTED = dict(
    title=r'Disconnected',
    body=r'%(account_name)s: disconnected.'
)
FAILED = dict(
    title=r'Failed',
    body=r'%(account_name)s: failed to connect.'
)
QUIT = dict(
    title=r'Quitting',
    body=r'Netsplice is quitting, please wait.'
)
