# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
init file for netsplice.gui
'''
from PySide.QtGui import QPixmap
import sys
from netsplice.config import gui as config
from netsplice.util import get_logger

logger = get_logger()

this = sys.modules[__name__]

this.account_type_pixmap_resource_names = {}
this.account_type_pixmaps = {}
this.menu_items = {}
this.systray_menu_items = []


def register_account_type_pixmap(account_type, pixmap_resource_name):
    '''
    Register account type pixmap.

    Register a pixmap for the given account_type.
    '''
    this.account_type_pixmap_resource_names[account_type] = \
        pixmap_resource_name


def register_menu_item(
        menu_name, name=None, label=None, callback=None, shortcut=None,
        separator=False, before=None):
    '''
    Register a main menu item.
    '''
    menu_instance = {
        'before': before,
        'name': name,
        'label': label,
        'callback': callback,
        'separator': separator,
        'shortcut': shortcut,
    }
    try:
        this.menu_items[menu_name].append(menu_instance)
    except KeyError:
        this.menu_items[menu_name] = [menu_instance]


def register_systray_menu_item(
        name=None, label=None, callback=None,
        separator=False):
    '''
    Register a systray menu item.
    '''
    menu_instance = {
        'name': name,
        'label': label,
        'callback': callback,
        'separator': separator,
    }
    this.systray_menu_items.append(menu_instance)


def get_account_type_pixmap(account_type):
    '''
    Get Account Type Pixmap.

    Create a pixmap or use a cached instance
    '''
    try:
        return this.account_type_pixmaps[account_type]
    except KeyError:
        pass
    try:
        this.account_type_pixmaps[account_type] = QPixmap(
            this.account_type_pixmap_resource_names[account_type])
    except KeyError:
        logger.warn('No pixmap registered for %s' % (account_type,))
        this.account_type_pixmaps[account_type] = QPixmap()
    return this.account_type_pixmaps[account_type]


def get_menu_items(menu_name):
    '''
    Get Menu Item.

    Return the menu_items for the given menu_name.
    '''
    return this.menu_items[menu_name]


def get_systray_menu_items():
    '''
    Get Systray Menu Item.

    Return the menu_items for the systray.
    '''
    return this.systray_menu_items


register_account_type_pixmap('Raw', config.RES_ICON_TYPE_RAW)
