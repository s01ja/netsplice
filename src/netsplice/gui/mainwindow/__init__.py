# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
mainwindow init function.
'''

import os
import signal
import sys

from functools import partial

from netsplice.gui.mainwindow.mainwindow import mainwindow
from netsplice.gui.mainwindow.model import model as module_model

this = sys.modules[__name__]
this.window = None

name = "mainwindow"

endpoints = []

model = module_model()


def get_window():
    return this.window


def signal_handler(window, pid, signum, frame):
    '''
    Signal handler that quits the running app cleanly.

    :param window: A window with a `quit` callable.
    :type window: MainWindow
    :param pid: Process id of the main process.
    :type pid: int
    :param signum: Number of the signal received (e.g. SIGINT -> 2).
    :type signum: int
    :param frame: Current stack frame.
    :type frame: frame or None
    '''
    my_pid = os.getpid()

    if pid == my_pid:
        if window is not None:
            window.quit()


def show(application):
    this.window = mainwindow(application)

    # Setup signal handlers
    my_pid = os.getpid()

    signal_handler_partial = partial(signal_handler, this.window, my_pid)
    signal.signal(signal.SIGINT, signal_handler_partial)
    signal.signal(signal.SIGTERM, signal_handler_partial)
