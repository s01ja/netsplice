# -*- coding: utf-8 -*-
# logviewer.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
History log window.
This dialog allows the user to display the current log entries and filter them
by loglevels and text. The window may be configured to have a account_id for
a filter so failures for specific accounts can be specified from other
application locations.
'''
from PySide.QtCore import (
    Qt, QModelIndex, Signal, QPoint, QTimer
)
from PySide.QtGui import (
    QFileDialog
)
from logviewer_ui import Ui_LogViewer
from netsplice.gui.logviewer.model.log_gui_model import log_gui_model
from netsplice.gui.widgets.dialog import dialog
from netsplice.model.log_list import log_list as log_list_model
from netsplice.util import get_logger, take_logs, log_report
from netsplice.config import log as config_log
from netsplice.config import gui as config_gui

logger = get_logger()


class logviewer(dialog):
    '''
    Logviewer for backend logs.

    Dialog that displays a history of the logged messages in the app.

    '''

    _paste_ok = Signal(object)
    _paste_error = Signal(object)
    _backend_logs_deleted_signal = Signal()
    _backend_logs_failed_signal = Signal(object)
    _backend_logs_received_signal = Signal(object)

    def __init__(self, parent, dispatcher):
        '''
        Initialize the widget.

        :param parent: The widget that owns the logviewer (usualy mainwindow)
        :param dispatcher: backend dispatcher for model change events
        '''
        dialog.__init__(self, parent)

        # Load UI
        self.ui = Ui_LogViewer()
        self.ui.setupUi(self)
        self.backend = dispatcher
        self.level_toggle_buttons = {
            'DEBUG': self.ui.action_toggle_debug,
            'INFO': self.ui.action_toggle_info,
            'WARNING': self.ui.action_toggle_warning,
            'ERROR': self.ui.action_toggle_error,
            'CRITICAL': self.ui.action_toggle_critical
        }

        # Setup Model
        self.log_model = log_gui_model(self)

        self.ui.log_item_list.setModel(self.log_model)
        self.ui.log_item_list.setTextElideMode(Qt.ElideNone)

        # Private Attributes
        self.accounts = None
        self._backend_is_gone = False
        self._account_filter = None
        self._connected = False
        self._extra_filter = None
        self._scroll_to_end_timer = None
        self._update_date_timer = None
        self._request_in_progress = False
        self._last_hidden_rows = 0

        # Initial calls that requests values from backend.
        self._filter_changed()
        self._schedule_update_date()
        self._get_logs(0)
        self.backend.preferences_get.emit()

    def _apply_filter(self):
        '''
        Apply the current filters to the listview.

        Set rows visible or hidden based on the currently configured filters.
        Tests each row and aggregates and displays statistic values.
        '''
        root_index = QModelIndex()
        row_count = self.log_model.rowCount(root_index)
        hidden_rows = 0
        for row in range(0, row_count):
            hidden = self.log_model.is_hidden(row)
            self.ui.log_item_list.setRowHidden(row, root_index, hidden)
            if hidden:
                hidden_rows += 1

        # Update status information
        self.ui.stat_row_count.setText(
            self.tr('%(row_count)d Items')
            % {'row_count': row_count})
        self.ui.stat_hidden_rows.setText(
            self.tr('%(hidden_rows)d hidden by %(filter)s')
            % {
                'hidden_rows': hidden_rows,
                'filter': self.log_model.get_filter()
            })
        if hidden_rows > 0:
            self.ui.stat_hidden_rows.show()
            self.ui.action_clear_hidden.show()
            if self._last_hidden_rows < hidden_rows:
                self._hidden_ui_hint()
            self._last_hidden_rows = hidden_rows
        else:
            self.ui.stat_hidden_rows.hide()
            self.ui.action_clear_hidden.hide()
        self.ui.stat_account_name.setText('')
        if isinstance(self._extra_filter, (dict,)):
            if 'account_id' in self._extra_filter:
                self._show_account_filter(
                    self._extra_filter['account_id'])
        else:
            self.ui.stat_account_name.setText('')

    def _backend_failure(self, message):
        '''
        Backend failure handler.

        The backend signaled a failure receiving the latest log entries.
        Do nothing, the next request will work or we are shutting down anyway.
        '''
        self._request_in_progress = False

    def _backend_gone(self):
        '''
        Backend gone.

        Take the local (stdout) logs and put them into the logview.
        '''
        self._backend_is_gone = True
        self.set_filter({'level': 'ERROR,CRITICAL'})
        log_items = log_list_model()
        for log_item in take_logs():
            if isinstance(log_item, (dict,)):
                log_items.append_from_dict(log_item)
            else:
                log_items.append(log_item)
        self._load_new_items(log_items)

    def _backend_logs_received(self, model_json):
        '''
        Handle the requested log entries.

        Loads all received entries. The requested entries should only be new
        items.
        '''
        logs = log_list_model()
        logs.from_json(model_json)
        self._load_new_items(logs)
        self._request_in_progress = False

    def _backend_model_changed(self, model):
        '''
        Handle the change of the backend model.

        Extract the log_index that indicates the last log_index in the backend
        model. When the last known log_index is smaller, request the backend to
        send the log with that offset.
        '''
        if len(self.log_model.log_items) is 0:
            self._get_logs(0)
            return

        last_log_item_index = self.log_model.log_items[-1].index.get()
        if model.log_index.get() > last_log_item_index:
            self._get_logs(last_log_item_index + 1)

    def _clear_log(self):
        '''
        Clear Log.

        Clear all log items in gui and backend.
        '''
        self.backend.logs_delete.emit(
            self._backend_logs_deleted_signal,
            self._backend_logs_failed_signal)

    def _close_gone_gui(self):
        '''
        Close gone Gui.

        Close the application when the backend is gone.
        '''
        if not self._backend_is_gone:
            return
        self.parent().quit()

    def _close_window(self):
        '''
        Close Window.

        Close the logview window.
        '''
        self.hide()

    def _connections_create(self):
        '''
        Make GUI Connections.

        Connect signals with member functions.
        '''
        if self._connected:
            return
        self.ui.action_save.clicked.connect(self._save_log_to_file)
        for level, ui_element in self.level_toggle_buttons.items():
            ui_element.toggled.connect(self._filter_changed)

        self.ui.filter.textEdited.connect(self._filter_by_text)
        self.ui.case_sensitive.stateChanged.connect(self._filter_changed)
        self.ui.action_clear_filter.clicked.connect(self.reset_filter)
        self.ui.action_clear_hidden.clicked.connect(self.reset_filter)
        self.ui.action_close.clicked.connect(self._close_window)
        self.ui.action_clear_log.clicked.connect(self._clear_log)
        self._backend_logs_deleted_signal.connect(self._logs_deleted)
        self._backend_logs_failed_signal.connect(self._backend_failure)
        self._backend_logs_received_signal.connect(
            self._backend_logs_received)

        self.backend.application.event_loop.gui_model_changed.connect(
            self._backend_model_changed)
        self.backend.application.event_loop.preferences_changed.connect(
            self._preferences_changed)
        self.backend.process_gone.connect(self._backend_gone)
        self._preferences_changed(
            self.backend.application.get_model().preferences)
        self._connected = True

    def _connections_disconnect(self):
        '''
        Disconnect GUI Connections.

        Disconnect signals from member functions.
        '''
        if not self._connected:
            return
        self.ui.action_save.clicked.disconnect(self._save_log_to_file)
        for level, ui_element in self.level_toggle_buttons.items():
            ui_element.toggled.disconnect(self._filter_changed)

        self.ui.filter.textEdited.disconnect(self._filter_by_text)
        self.ui.case_sensitive.stateChanged.disconnect(self._filter_changed)
        self.ui.action_clear_filter.clicked.disconnect(self.reset_filter)
        self.ui.action_clear_hidden.clicked.disconnect(self.reset_filter)
        self.ui.action_close.clicked.disconnect(self._close_window)
        self.ui.action_clear_log.clicked.disconnect(self._clear_log)
        self._backend_logs_deleted_signal.disconnect(self._logs_deleted)
        self._backend_logs_failed_signal.disconnect(self._backend_failure)
        self._backend_logs_received_signal.disconnect(
            self._backend_logs_received)

        self.backend.application.event_loop.gui_model_changed.disconnect(
            self._backend_model_changed)
        self.backend.application.event_loop.preferences_changed.disconnect(
            self._preferences_changed)
        self.backend.process_gone.disconnect(self._backend_gone)
        self._connected = False

    def _filter_by_text(self, text):
        '''
        Set the text to use for filtering logs in the log window.

        :param text: the text to compare with the logs when filtering.
        :type text: str
        '''
        self.log_model.set_text_filter(
            text, self.ui.case_sensitive.isChecked())
        if self.ui.filter.text() != '':
            self.ui.action_clear_filter.setDisabled(False)
        self._apply_filter()

    def _filter_changed(self):
        '''
        Set the log_model filters from the GUI values.

        Enable or disable the action_clear_filter button.
        '''
        level_filter = {}
        filter_active = False
        for level, ui_element in self.level_toggle_buttons.items():
            level_filter[level] = ui_element.isChecked()
            filter_active |= not ui_element.isChecked()
            # force recalculation of stylesheet with the [checked="true"]
            # property selectors.
            ui_element.setStyleSheet('/* /')
        self.log_model.set_level_filter(level_filter)
        if self.ui.filter.text() != '':
            filter_active = True
        if self.ui.case_sensitive.isChecked():
            filter_active = True
        if self._extra_filter != {}:
            filter_active = True
        self.log_model.set_text_filter(
            self.ui.filter.text(),
            self.ui.case_sensitive.isChecked())
        self.log_model.set_extra_filter(self._extra_filter)
        if filter_active:
            self.ui.action_clear_filter.setDisabled(False)
        self._apply_filter()

    def _get_logs(self, index):
        '''
        Get Logs.

        Request Backend to send logs that have at least the given index.
        '''
        if self._request_in_progress:
            return
        self._request_in_progress = True
        self.backend.logs_get.emit(
            index,
            self._backend_logs_received_signal,
            self._backend_logs_failed_signal)

    def _hidden_ui_hint(self):
        '''
        Hidden ui hint.

        Highlight the hidden filter so the user notifies that new items
        may be available but hidden.
        '''
        self.ui.stat_hidden_rows.setStyleSheet(
            config_gui.LABEL_HIGHLIGHT_STYLESHEET)
        QTimer.singleShot(
            config_gui.LABEL_HIGHLIGHT_TIME, self._hidden_ui_hint_remove)

    def _hidden_ui_hint_remove(self):
        '''
        Remove Hidden ui hint.

        Remove a previously set highlight stylesheet.
        '''
        self.ui.stat_hidden_rows.setStyleSheet('')

    def _load_new_items(self, new_log_items):
        '''
        Load the previous logged messages in the widget.

        Schedules a scroll_to_end when the scrollbar is currently at the end
        and ensures that only the maximum amount of items is displayed.
        '''
        if len(new_log_items) is 0:
            return

        scroll = self.ui.log_item_list.verticalScrollBar()
        if scroll.value() == scroll.maximum():
            self._schedule_scroll_to_end()

        self.log_model.beginInsertRows(
            QModelIndex(),
            self.log_model.rowCount(QModelIndex()),
            len(new_log_items))
        for log_item_instance in new_log_items:
            self.log_model.add_log_item(log_item_instance)
            self._last_index = log_item_instance.index.get()
        self.log_model.endInsertRows()

        max_log_items = config_log.MAX_LOGVIEWER_ITEMS
        if len(self.log_model.log_items) > max_log_items:
            self.log_model.beginRemoveRows(
                QModelIndex(),
                0,
                len(self.log_model.log_items) - max_log_items)
            while len(self.log_model.log_items) > max_log_items:
                del self.log_model.log_items[0]
            self.log_model.endRemoveRows()
        self._apply_filter()

    def _logs_deleted(self):
        '''
        Handle Logs Deleted.

        When the backend has deleted all logs, the items in the GUI model are
        removed.
        '''
        self.log_model.beginRemoveRows(
            QModelIndex(),
            0,
            len(self.log_model.log_items))
        del self.log_model.log_items[:]
        self.log_model.endRemoveRows()
        self.reset_filter()

    def _preferences_changed(self, model):
        '''
        Handle Preference model changed.

        To store the accounts for displaying account filter.
        '''
        config_log.MAX_LOGVIEWER_ITEMS = model.ui.max_logviewer_items.get()
        config_log.TEXT_LINES_PER_ITEM = \
            model.ui.logviewer_text_lines_per_item.get()
        config_log.LOGVIEWER_DATE = model.ui.logviewer_date.get()
        self.accounts = model.accounts

        self.log_model.reset_default_size()

    def _save_log_to_file(self):
        '''
        Safe the current filtered log as text to file.

        Let the user save the current log to a file. Builds a string of
        log entries from the model, opens a save-as dialog and writes the file.
        '''
        log_text = self.log_model.to_plain_text(
            text_format=config_log.LINE_FORMAT_NOCONTEXT,
            wrap_column=config_log.REPORT_WRAP_COLUMN,
            highlight_time=config_log.REPORT_BLOCK_TIME)
        history = log_report(log_text)

        file_name, filter_selection = QFileDialog.getSaveFileName(
            self, self.tr("Save As"),
            options=QFileDialog.DontUseNativeDialog)

        if not file_name:
            logger.warn('Log not saved!')
            return
        try:
            with open(file_name, 'w') as output:
                output.write(history)
        except IOError as e:
            logger.error("Error saving log file: %r" % (e, ))

    def _schedule_scroll_to_end(self):
        '''
        Schedule that the listview is scrolled to the end.

        Start a timer when no timer is active so the scroll happens after all
        items are added.
        '''
        if self._scroll_to_end_timer is not None:
            return
        self._scroll_to_end_timer = QTimer(self)
        self._scroll_to_end_timer.timeout.connect(self.scroll_to_end)
        self._scroll_to_end_timer.setInterval(config_log.SCROLL_TO_END_TIMER)
        self._scroll_to_end_timer.setSingleShot(True)
        self._scroll_to_end_timer.start()

    def _schedule_update_date(self):
        '''
        Schedule that the listview date is updated.

        Start a timer when no timer is active and the config is timeago
        '''
        if self._update_date_timer is not None:
            return
        if config_log.LOGVIEWER_DATE != 'timeago':
            return
        self._update_date_timer = QTimer(self)
        self._update_date_timer.timeout.connect(self._update_date)
        self._update_date_timer.setInterval(config_log.UPDATE_TIMER)
        self._update_date_timer.start()

    def _update_date(self):
        '''
        Update date.

        Update the date by forcing the list to redraw and therefore reget
        the DisplayRole
        '''
        list_height = self.ui.log_item_list.height()
        last_item = None
        for y in range(0, list_height, 10):
            item = self.ui.log_item_list.indexAt(QPoint(1, y))
            if item == last_item or item is None:
                continue
            last_item = item
            self.ui.log_item_list.update(item)

        self._schedule_update_date()

    def closeEvent(self, event):
        '''
        Widget Close event.

        Disconnect connections to events that should be off when not visible.
        '''
        self._connections_disconnect()
        self._close_gone_gui()

    def hideEvent(self, event):
        '''
        Widget Hide event.

        Disconnect connections to events that should be off when not visible.
        '''
        self._connections_disconnect()
        self._close_gone_gui()

    def showEvent(self, event):
        '''
        Widget Show event.

        Create connections to events that should be off when not visible.
        '''
        self._connections_create()

    def scroll_to_end(self):
        '''
        Scroll the listview to the end.

        Set the verticalScrollBar to its maximum value.
        '''
        self._scroll_to_end_timer = None
        scroll = self.ui.log_item_list.verticalScrollBar()
        scroll.setValue(scroll.maximum())

    def _show_account_filter(self, account_id):
        '''
        Display account name for given account id.

        Evaluate the account name for the given account_id and display the
        name in the status.
        '''
        if self.accounts is None:
            self.ui.stat_account_name.setText('')
            return
        try:
            account = self.accounts.find_by_id(account_id)
            self.ui.stat_account_name.setText(account.name.get())
        except:
            pass

    def reset_filter(self):
        '''
        Reset the filters and the UI Filter configuration.

        Reset all private filter values (extra).
        Reset all ui elements.
        Call filter changed so the reseted values are applied.
        '''
        self._extra_filter = {}
        self.ui.filter.setText('')
        self.ui.case_sensitive.setChecked(False)
        for level, ui_element in self.level_toggle_buttons.items():
            ui_element.setChecked(True)
        self.ui.action_clear_filter.setDisabled(True)
        self._schedule_scroll_to_end()
        self._filter_changed()

    def set_filter(self, filter=None):
        '''
        Set a filter to the current log view.

        The filter is provided using a dict with the following structure::

            {
                'extra': {...},  # extra keys used for filtering
                'text': 'string',  # string that will be displayed
                'case_sensitive': True,  # Boolean toggles case sensitivity
                'level': 'DEBUG,INFO'  # logger levels to be displayed
            }

        When keys are not in the dict, they will not change the filter. The
        filter will change the state of the UI. When filter is None or not a
        dict, all filters will be reset.
        '''
        if not isinstance(filter, (dict,)):
            self.reset_filter()
            return
        if 'extra' in filter.keys():
            if isinstance(filter['extra'], (dict,)):
                self._extra_filter = filter['extra']
            else:
                self._extra_filter = {}
        if 'text' in filter.keys():
            self.ui.filter.setText(filter['text'])
        if 'case_sensitive' in filter.keys():
            self.ui.case_sensitive.setChecked(filter['case_sensitive'])
        if 'level' in filter.keys():
            for level, ui_element in self.level_toggle_buttons.items():
                if level in filter['level']:
                    ui_element.setChecked(True)
                else:
                    ui_element.setChecked(False)
        self.ui.action_clear_filter.setDisabled(False)
        self._filter_changed()
