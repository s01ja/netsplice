# -*- coding: utf-8 -*-
# test_log_gui_model.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for the log list view.
'''

import mock

from PySide.QtGui import QWidget
from .log_gui_model import log_gui_model

def test_wrap_a_returns_wrapped():
    l = log_gui_model(QWidget())
    result = l.wrap_line(
        'Message from the management interface.'
        '   (Wed Feb 28 01:44:01 2018 MANAGEMENT: CMD \'\')',
        72)
    expect = (
        'Message from the management interface. \\\n'
        '    (Wed Feb 28 01:44:01 2018 MANAGEMENT: CMD \'\')\n')
    print(result, expect)
    assert(result == expect)


def test_wrap_b_returns_wrapped():
    l = log_gui_model(QWidget())
    result = l.wrap_line(
        'OpenVPN was shut down. This is a normal message unless the connection'
        ' is in a connected state. (Connection is no longer active.)',
        72)
    expect = (
        'OpenVPN was shut down. This is a normal message unless the connection'
        ' is \\\n    in a connected state. \\\n    (Connection is no longer'
        ' active.)\n')
    print(result, expect)
    assert(result == expect)


def test_wrap_c_returns_wrapped():
    l = log_gui_model(QWidget())
    result = l.wrap_line(
        'This debug message indicates that the privileged backend has started'
        ' to execute the configuration. (SUCCESS: hold release succeeded)',
        72)
    expect = (
        'This debug message indicates that the privileged backend has started'
        ' to \\\n    execute the configuration. \\\n'
        '    (SUCCESS: hold release succeeded)\n')
    print(result, expect)
    assert(result == expect)
