# -*- coding: utf-8 -*-
# backend_dispatcher_qt_replacement.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Python only threads in gui.

Due to bugs in PySide osx may crash at random locations. Use this option
to use a threading implementation that mimics the emit/Signal code.
'''

import threading

from netsplice.config import gui as config
from netsplice.util import get_logger

logger = get_logger()


class Signal(object):
    '''
    Signal Mock Implementation

    Mock the QtCore.Signal interface to allow programming like in the GUI
    but implement a different thread-sync method to avoid PySide Issue #151
    '''
    def __init__(self, *args):
        '''
        Initialize.

        Initialize supported arguments and prepare members.
        '''
        self._emit_args = args
        self._descriptor_object = None
        self._descriptor_thread = None
        self._callbacks = []

    def __get__(self, obj, objtype):
        '''
        Get.

        Descriptor object handling.
        Store the thread where the Signal is declared and add a emit_queue
        and a lock to the thread object.
        '''
        self._descriptor_object = obj
        if self._descriptor_thread is None:
            self._descriptor_thread = threading.current_thread()
        if 'emit_queue_lock' not in self._descriptor_thread.__dict__:
            self._descriptor_thread.emit_queue = []
            self._descriptor_thread.emit_queue_lock = threading.RLock()
        return self

    def connect(self, callback_function):
        '''
        Connect.

        Implementation that collects all callbacks that need to be called
        on emit.
        '''
        self._callbacks.append(callback_function)

    def disconnect(self, callback_function):
        '''
        Disconnect.

        Implementation that removes the given callback_function from the list
        of callbacks if it exists.
        '''
        try:
            self._callbacks.remove(callback_function)
        except ValueError:
            pass

    def emit(self, *args):
        '''
        Emit.

        Implementation that raises a Type error if the arguments do not match
        the Signal arguments and queue the callback in the appropriate thread.
        This implementation is simpler than the QtCore.Signal.emit as it
        assumes that all callbacks have the same signature.
        '''
        if len(args) != len(self._emit_args):
            raise TypeError(
                '%d arguments of types %s expected'
                % (len(self._emit_args), str(self._emit_args)))
        for callback in self._callbacks:
            try:
                self._descriptor_thread.emit_queue_lock.acquire()
                self._descriptor_thread.emit_queue.append((callback, args))
                self._descriptor_thread.emit_queue_lock.release()
            except AttributeError:
                logger.error('Descriptor thread has no emit-queue')
                # XXX Handle?


class QCoreApplication(object):
    def __init__(self):
        pass

    @staticmethod
    def processEvents():
        '''
        Process Emits.

        Process Emitted callbacks from other threads.
        '''
        t = threading.current_thread()
        try:
            t.emit_queue_lock.acquire()
            for (callback, args) in t.emit_queue:
                try:
                    callback(*args)
                except Exception, errors:
                    logger.error('Callback Error: %s' % (str(errors),))
            t.emit_queue = []
        except AttributeError:
            logger.error('processEvents before qt_backend_bridge started')
            pass
        # always unlock, even if the emit_queue
        # was causing an attribute error.
        try:
            t.emit_queue_lock.release()
        except AttributeError:
            logger.error('release of emit_queue_lock not possible')
            pass


class QObject(object):
    '''
    QObject.

    QObject Mock to satisfy the original and preferred Qt Signal
    implementation.
    '''
    def __init__(self):
        pass

    def qt_backend_bridge(self, QTimer):
        '''
        Interface for mainwindow.

        Requires the QTimer class as parameter to avoid importing Qt
        in this module.
        '''
        self._emit_queue_timer = QTimer()
        self._emit_queue_timer.timeout.connect(self.check_emit_queue)
        self._emit_queue_timer.start(config.EMIT_QUEUE_TIMER)

    def check_emit_queue(self):
        '''
        Check Emit Queue.

        Check and execute all emits that have been queued in another thread.
        This
        '''
        QCoreApplication.processEvents()


class QThread(threading.Thread):
    '''
    QThread.

    QThread Mock to satisfy the original and preferred Qt Thread
    implementation.
    '''
    def __init__(self, opt):
        threading.Thread.__init__(self)


class QMutex(object):
    '''
    QMutex.

    QMutex Mock to satisfy the original and preferred Qt Mutex implementation.
    '''
    def __init__(self):
        self._lock = threading.Lock()

    def lock(self):
        self._lock.acquire()

    def unlock(self):
        self._lock.release()
