# -*- coding: utf-8 -*-
# ordered_string_item_list.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Item list that allows the user to add and remove items and order them using
drag and drop.
'''
import json

from PySide.QtCore import Qt, QSize, Signal
from PySide.QtGui import (
    QAction, QAbstractItemView, QListView, QHBoxLayout, QPushButton,
    QSizePolicy, QStringListModel, QToolBar, QVBoxLayout, QWidget
)


class _string_list_model(QStringListModel):
    '''
    String List Model.

    Private model for overriding flags of items.
    '''
    def __init__(self, parent=None):
        QStringListModel.__init__(self, parent)

    def flags(self, model_index):
        '''
        Flags.

        Flags for model index. Disallows drops on items, only allow on the
        (invalid) root item. This results in a list that can move the items.
        '''
        index_flags = QStringListModel.flags(self, model_index)
        if model_index.isValid():
            # The valid items shall not receive drops, otherwise
            # the drop will move the item content 'into' the target item
            index_flags = index_flags ^ Qt.ItemIsDropEnabled
        return index_flags


class ordered_string_item_list(QWidget):
    '''
    Custom Widget with a listview and buttons to add, remove and order
    items.
    '''

    textChanged = Signal(object)

    # Toolbar Action Flags
    NEED_PREV = 1
    NEED_NEXT = 2
    NEED_COUNT = 4
    NEED_NOT_EMPTY = 8

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.toolbar = None
        self._toolbar_actions = []

        self._init_ui()

    def _activated(self, model_index):
        '''
        Activated.

        An item in the list is activated.
        '''
        self._update_actions()

    def _add_toolbar_action(self, selection_state, label, callback):
        '''
        Add Toolbar Action.

        Add the given action that is enabled based on the selected item with
        the given label.
        '''
        toolbar_action = QAction(self)
        toolbar_action.setText(label)
        toolbar_action.triggered.connect(lambda: callback())
        self.toolbar.addAction(toolbar_action)
        toolbar_action.selection_state = selection_state
        self._toolbar_actions.append(toolbar_action)

    def _changed(
            self, model_index=None,
            opt_start=0, opt_end=0, opt_dparent=None, opt_drow=None):
        '''
        Changed.

        An item in the list is activated.
        This is a proxy for any changes in the model and any changes that
        are triggered by the widget.
        '''
        self.textChanged.emit(self.to_json())
        self._update_actions()

    def _down(self):
        '''
        Down.

        Move item down. Select the moved item.
        '''
        current = self.list_view.currentIndex()
        if current.row() > self.model.rowCount() - 2:
            return
        selected_row = current.row()
        items = self.to_list()
        item = items[selected_row]
        del items[selected_row]
        items.insert(selected_row + 1, item)
        self.model.setStringList(items)
        new_index = self.model.index(selected_row + 1)
        self.list_view.setCurrentIndex(new_index)
        self._changed()
        self._update_actions()

    def _init_ui(self):
        '''
        Initialize UI.

        Define layouts and widgets and connect them to member functions
        '''
        self.layout = QVBoxLayout(self)
        self.list_view = QListView(self)
        self.list_view.setMinimumSize(QSize(200, 90))
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.layout.addWidget(self.list_view)

        self.model = _string_list_model(self)
        self.model.dataChanged.connect(self._changed)
        self.model.rowsInserted.connect(self._changed)
        self.model.rowsMoved.connect(self._changed)
        self.model.rowsRemoved.connect(self._changed)

        self.list_view.clicked.connect(self._activated)
        self.list_view.activated.connect(self._activated)
        self.list_view.setModel(self.model)
        self.list_view.setSelectionMode(QAbstractItemView.SingleSelection)
        self.list_view.setDragEnabled(True)
        self.list_view.viewport().setAcceptDrops(True)
        self.list_view.setDropIndicatorShown(True)
        self.list_view.setEditTriggers(
            QAbstractItemView.AnyKeyPressed | QAbstractItemView.DoubleClicked)
        self.list_view.setDragDropMode(
            QAbstractItemView.InternalMove)
        self.list_view.setDefaultDropAction(
            Qt.MoveAction)
        self._setup_toolbar()
        self._update_actions()

    def _insert(self):
        '''
        Insert.

        Insert a new item in the list.
        When the list is empty, insert as 0.
        Make the item the current item.
        '''
        current = self.list_view.currentIndex()
        selected_row = 0
        if current.isValid():
            selected_row = current.row()
        self.model.insertRows(selected_row, 1)
        new_index = self.model.index(selected_row)
        self.list_view.setCurrentIndex(new_index)
        self._update_actions()

    def _remove(self):
        '''
        Remove.

        Remove an item from the list.
        Select the item at the same row or the one before the removed item.
        '''
        current = self.list_view.currentIndex()
        selected_row = current.row()
        self.model.removeRows(current.row(), 1)
        if self.model.rowCount() <= selected_row:
            selected_row -= 1
        new_index = self.model.index(selected_row)
        self.list_view.setCurrentIndex(new_index)

    def _setup_toolbar(self):
        '''
        Setup Toolbar.

        Add Actions and Widgets to the toolbar.
        '''
        if self.toolbar is None:
            self.toolbar = QToolBar(self)
            self.layout.insertWidget(0, self.toolbar)
            self._add_toolbar_action(
                self.NEED_NOT_EMPTY, self.tr('Insert'), self._insert)
            self._add_toolbar_action(
                self.NEED_COUNT, self.tr('Remove'), self._remove)
            self._add_toolbar_action(
                self.NEED_PREV, self.tr('Up'), self._up)
            self._add_toolbar_action(
                self.NEED_NEXT, self.tr('Down'), self._down)

    def _up(self):
        '''
        Up.

        Move item up. Select the moved item.
        '''
        current = self.list_view.currentIndex()
        if current.row() < 1:
            return
        selected_row = current.row()
        items = self.to_list()
        item = items[selected_row]
        del items[selected_row]
        items.insert(selected_row - 1, item)
        self.model.setStringList(items)
        new_index = self.model.index(selected_row - 1)
        self.list_view.setCurrentIndex(new_index)
        self._changed()
        self._update_actions()

    def _update_actions(self):
        '''
        Update Actions.

        Update the Widget actions depending on the selected item.
        Always enable Insert.
        Enable Up/Down only if current is supporting it.
        Enable Remove only if model is not empty.
        '''
        for toolbar_action in self._toolbar_actions:
            toolbar_action.setEnabled(False)

        current = self.list_view.currentIndex()
        has_empty = False
        current_list = self.to_list()
        for item in current_list:
            if item == '':
                has_empty = True

        for toolbar_action in self._toolbar_actions:
            if toolbar_action.selection_state is None:
                toolbar_action.setEnabled(True)
                continue

            if toolbar_action.selection_state & self.NEED_NOT_EMPTY:
                if has_empty is False:
                    toolbar_action.setEnabled(True)
                continue

            if not current.isValid():
                continue

            if toolbar_action.selection_state & self.NEED_PREV:
                if current.row() > 0:
                    toolbar_action.setEnabled(True)
                continue
            if toolbar_action.selection_state & self.NEED_NEXT:
                if current.row() < self.model.rowCount() - 1:
                    toolbar_action.setEnabled(True)
                continue
            if toolbar_action.selection_state & self.NEED_COUNT:
                if self.model.rowCount() > 0:
                    toolbar_action.setEnabled(True)

    def to_json(self):
        '''
        To JSON.

        Return the current model as json string.
        '''
        item_list = self.to_list()
        return json.dumps(item_list)

    def to_list(self):
        '''
        To List.

        Return the current model as list.
        '''
        item_list = list()
        for item in self.model.stringList():
            item_list.append(item)
        return item_list

    def from_json(self, json_string):
        '''
        From JSON.

        Fill the model with the items from the JSON. The JSON needs to be an
        array of strings::

            ["A", "B", "C"]
        '''
        item_list = json.loads(json_string)
        self.from_list(item_list)

    def from_list(self, item_list):
        '''
        From List.

        Fill the model with the items in the list.
        '''
        self.model.setStringList(item_list)

    def setText(self, json_string):
        '''
        Set Text.

        Implement QLineEdit API for seamless replacement.
        '''
        self.from_json(json_string)

    def text(self):
        '''
        Text.

        Implement QLineEdit API for seamless replacement.
        '''
        return self.to_json()
