# -*- coding: utf-8 -*-
# syntax_class.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Syntax Class for Syntax Highlighter
'''

from PySide import QtGui, QtCore


class syntax_class(object):
    def __init__(self, class_name, font, color, background_color=None):
        self.name = class_name
        self.font = font
        self.color = color
        self.background_color = background_color
        self.initialized = False
        self._text_format = QtGui.QTextCharFormat()

    def get_text_format(self):
        if self.initialized:
            return self._text_format
        self._text_format.setFont(self.font)
        self._text_format.setForeground(self.color)
        if self.background_color is not None:
            self._text_format.setBackground(self.background_color)
        self.initialized = True
        return self._text_format
