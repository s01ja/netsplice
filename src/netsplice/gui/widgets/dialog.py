# -*- coding: utf-8 -*-
# dialog.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Common Dialog.

Common class for all dialogs that are displayed.
'''
import sys
from PySide.QtGui import QDialog, QApplication, QMainWindow
from PySide.QtCore import Signal


class dialog(QDialog):
    '''
    Dialog.

    Common Dialog methods shared by all dialogs.
    '''
    model_changed = Signal(object)
    values_changed = Signal(object)

    def __init__(self, parent=None):
        '''
        Initialize Dialog.

        Initialize QDialog and members.
        '''
        QDialog.__init__(self, parent)
        self._minimal = False
        self.reset_position()

    def get_parent_mainwindow(self):
        '''
        Get Parent Mainwindow.

        Iterate parents to find a QMainWindow widget. Return None if no
        QMainWindow is in parents.
        '''
        parent = self.parent()
        while parent is not None:
            if isinstance(parent, (QMainWindow,)):
                break
            parent = parent.parent()
        return parent

    def get_parent_dialog(self):
        '''
        Get Parent dialog.

        Iterate parents to find a dialog widget. Return None if no dialog is
        in parents.
        '''
        parent = self.parent()
        while parent is not None:
            if isinstance(parent, (dialog,)):
                break
            parent = parent.parent()
        return parent

    def reset_position(self):
        '''
        Reset Position.

        Show window left or right of the mainwindow, depending on which side
        of the desktop the mainwindow is located.
        '''
        offset = 0
        dialog_rect = self.geometry()
        parent_dialog = self.get_parent_dialog()
        parent_mainwindow = self.get_parent_mainwindow()
        if parent_dialog:
            dialog_rect.moveCenter(parent_dialog.geometry().center())
            self.setGeometry(dialog_rect)
            return
        parent_rect = parent_mainwindow.geometry()
        if sys.platform.startswith('win32'):
            # additional offset for windows
            # otherwise the dialog is placed on the parent
            offset = 10

        app = QApplication.instance()
        width = app.desktop().width()
        height = app.desktop().height()
        if not self._minimal:
            parent_ratio_size = parent_rect.width() * 2.5
            if parent_ratio_size > width:
                parent_ratio_size = width
            dialog_rect.setWidth(parent_ratio_size)
        if parent_rect.left() < width / 2:
            dialog_rect.moveLeft(
                parent_rect.left() + parent_rect.width() + offset)
        else:
            dialog_rect.moveLeft(
                parent_rect.left() - (dialog_rect.width() + offset))
        if dialog_rect.top() + dialog_rect.height() < height:
            dialog_rect.moveTop(parent_rect.top())
        self.setGeometry(dialog_rect)

    def show(self):
        '''
        Show Dialog.

        Displays the window of the Dialog and ensures its size and position.
        '''
        self.reset_position()
        super(QDialog, self).show()

    def set_minimal(self):
        '''
        Set Minimal Size.

        Avoid expanding the window width based on the screen size.
        '''
        self._minimal = True
