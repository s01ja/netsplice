# -*- coding: utf-8 -*-
# list_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Custom list item.

Used as baseclass for account_list_item and account_group_item.
'''

from PySide.QtCore import (
    Property, Qt, Signal, QTimer
)
from PySide.QtGui import (
    QApplication, QDrag, QPainter, QScrollArea,
    QStyle, QStyleOptionFrame, QWidget
)
import netsplice.config.gui as config


class list_item(QWidget):
    '''
    Custom Widget Listing Drag and Dropable Items.

    Implements the base functionality for drag&drop and allows to
    customize the stylesheet for active items.
    Using the item in a (vertical) scroll area will provide navigation in
    the view while dragging.
    '''

    selected = Signal(object)

    def __init__(self, parent):
        '''
        Initialize Widget.

        Set members and defaults.
        '''
        QWidget.__init__(self, parent)
        # avoid right-side spacing
        self.setContentsMargins(-7, 0, -7, 0)

        self.setAcceptDrops(True)
        self.drag_start_position = None
        self.in_drag_mode = False
        self.is_destroyed = False
        self.can_drag = True

        self._active = False

        self._scroll_area = None
        self._scroll_timer_up = QTimer()
        self._scroll_timer_down = QTimer()
        self._scroll_timer_up.setInterval(config.DRAG_SCROLL_TIMER)
        self._scroll_timer_down.setInterval(config.DRAG_SCROLL_TIMER)
        self._scroll_timer_up.timeout.connect(
            lambda: self._scroll(-1 * config.DRAG_SCROLL_DISTANCE))
        self._scroll_timer_down.timeout.connect(
            lambda: self._scroll(config.DRAG_SCROLL_DISTANCE))

    def _end_scroll(self):
        '''
        End Scroll.

        End automatic scroll timeouts.
        '''
        self._scroll_timer_up.stop()
        self._scroll_timer_down.stop()

    def _locate_scroll_area(self):
        '''
        Locate Scroll Area.

        Traverse the parents until a scroll-area is found that can be
        manipulated by the timers.
        '''
        parent_widget = self.parent()
        while parent_widget:
            if isinstance(parent_widget, (QScrollArea,)):
                break
            parent_widget = parent_widget.parent()

        self._scroll_area = parent_widget

    def _scroll(self, value):
        '''
        Scroll.

        Timer callback. Change the vertical scrollbar by the given value.
        '''
        if self._scroll_area is None:
            return
        c = self._scroll_area.verticalScrollBar().value()
        self._scroll_area.verticalScrollBar().setValue(c + value)

    def _start_scroll_down(self):
        '''
        Start Scroll.

        Start the timer that issues a scroll down if possible.
        '''
        self._end_scroll()
        self._scroll_timer_down.start()

    def _start_scroll_timer(self, event):
        '''
        Start Scroll Timer.

        Evaluate the event position to find one of three actions: up, down or
        stop scrolling.
        '''
        if self._scroll_area is None:
            return
        top = self.mapTo(self._scroll_area, event.pos()).y()
        height = self._scroll_area.geometry().height()
        zone = height * config.DRAG_SCROLL_ZONE_SIZE
        if top < zone:
            self._start_scroll_up()
        elif top > height - zone:
            self._start_scroll_down()
        else:
            self._end_scroll()

    def _start_scroll_up(self):
        '''
        Start Scroll.

        Start the timer that issues a scroll up if possible.
        '''
        self._end_scroll()
        self._scroll_timer_up.start()

    def get_active(self):
        '''
        Get Active.

        Returns if the item has been selected (eg clicked on).
        '''
        return self._active

    # Widget virtual implementations
    def closeEvent(self, event):
        '''
        Close Event.

        This event occurs when the widget is deleted.

        Arguments:
            event (Qt.QEvent): Event details.
        '''
        self.is_destroyed = True

    def dragEnterEvent(self, event):
        '''
        Drag Enter Event.

        Handle that anything has been dragged over the item. Ensure that
        the indicator is drawn.
        '''
        if not self.accept_mime_data(event.mimeData()):
            event.accept()
            return
        self.in_drag_mode = True
        self.update()
        event.acceptProposedAction()

    def dragLeaveEvent(self, event):
        '''
        Drag Leave Event.

        Stop handling a drag event and ensure that indicators are reset.
        '''
        self.in_drag_mode = False
        self.drag_end()
        self.update()
        self._end_scroll()

    def dragMoveEvent(self, event):
        '''
        Drag Move Event.

        Ensure that the indicators are drawn and give items the
        possibility to reject the drop.
        '''
        self.drag_move(event.pos())
        self.update()
        event.acceptProposedAction()

        self._locate_scroll_area()
        self._start_scroll_timer(event)

    def dropEvent(self, event):
        '''
        Drop Event.

        Handle a completed drop and ensure that any indicators are reset.
        '''
        self.in_drag_mode = False
        self.drop_mime_data(event.mimeData())
        self.update()
        event.acceptProposedAction()
        self._end_scroll()

    def mousePressEvent(self, event):
        '''
        Mouse Press Event.

        Initialize a possible drag action and emit a selected event.
        '''
        if event.button() == Qt.LeftButton:
            self.drag_start_position = event.pos()
            # context menu on label does not work if all buttons emit select.
            self.setFocus(Qt.FocusReason.MouseFocusReason)
            self.selected.emit(self)

    def mouseReleaseEvent(self, event):
        '''
        Mouse Release Event.

        Resets the position used for evaluating if a drag action started.
        '''
        self.drag_start_position = None

    def mouseMoveEvent(self, event):
        '''
        Mouse Move Event.

        Handles move events and starts drag if a drag_start_position is set
        with the manhattanLength.
        '''
        if self.drag_start_position is None:
            return
        if not self.can_drag:
            return
        manhattan = (event.pos() - self.drag_start_position).manhattanLength()
        start_drag_distance = config.START_DRAG_DISTANCE
        if start_drag_distance is -1:
            start_drag_distance = QApplication.startDragDistance()
        if manhattan < start_drag_distance:
            return
        drag = QDrag(self)

        drag.setMimeData(self.get_mime_data())
        # drag.setPixmap()
        drag.exec_()

    def paintEvent(self, paint_event):
        '''
        Paint Event.

        Ensures the Highlight of the active state and when in drag mode
        calls painter for drop indicators.
        '''
        painter = QPainter(self)
        opt = QStyleOptionFrame()
        opt.initFrom(self)
        self.style().drawPrimitive(QStyle.PE_Widget, opt, painter, self)
        if self.in_drag_mode:
            self.paint_drop_regions(painter)

    # Public Interface that should or must be implemented by inheritants

    def accept_mime_data(self, mime_data):
        '''
        Accept Mime Data.

        Return True when the given mime_data has the correct format and
        may be evaluated its contents.

        Arguments:
            mime_data (QtCore.QMimeData): Mime data of element to be dropped.

        Returns:
            bool -- True when the mime data can be accepted.
        '''
        return False

    def drag_end(self):
        '''
        Drag End.

        Cleanup any state that was setup during dragging.
        '''
        pass

    def drag_move(self, position):
        '''
        Drag Move.

        Evaluate how the drag would be handled when it is dropped at the given
        position. Used to prepare the painter.

        Arguments:
            position (QtCore.QPoint): Current point of the drag.
        '''
        pass

    def drop_mime_data(self, mime_data):
        '''
        Drop Mime Data.

        Handle the dropped mime_data after drop has finished.

        Arguments:
            mime_data (QtCore.QMimeData): Mime data of the drop

        Returns:
            bool: True if the drop was handled with success.
        '''
        return False

    def get_mime_data(self):
        '''
        Get Mime Data.

        Return QtCore.QMimeData() that contains a dropable handle.
        '''
        raise NotImplementedError()

    def paint_drop_regions(self, painter):
        '''
        Paint drop regions based on the status of the widget.

        Optional that allows drawing indicators.

        Arguments:
            painter (QtGui.QPainter): Painter of the widget.
        '''
        pass

    # Public interface that may be called from other widgets
    def set_active(self, state):
        '''
        Set Active.

        Sets the list_item in active state and ensures that a property
        based stylesheet is reconsidered.
        '''
        if self.is_destroyed:
            return
        self._active = state
        # force recalculation of stylesheet with the [active="true"] Property
        # selectors.
        self.setStyleSheet('/* /')
        self.update()
        self.setFocus(Qt.FocusReason.MouseFocusReason)
        return self

    # Properties that allow styling of the custom drawn widget states

    # Property that indicates if the list_item is active
    # Setting the property forces a recalculation of the stylesheet
    # list_item[active="true"] > QLabel { color: white; }
    # list_item[active="false"] > QLabel { color: black; }
    active = Property(
        bool, get_active, set_active)
