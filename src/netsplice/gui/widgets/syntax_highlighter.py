# -*- coding: utf-8 -*-
# syntax_highlighter.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor syntax highlighter.
'''

from PySide.QtCore import Qt
from PySide.QtGui import QBrush, QSyntaxHighlighter

from netsplice.config import gui as config
from netsplice.gui.widgets.syntax_class import syntax_class
from netsplice.gui.widgets.syntax_element import syntax_element


class syntax_highlighter(QSyntaxHighlighter):
    '''
    Custom syntax highlighter
    '''
    def __init__(self, editor):
        QSyntaxHighlighter.__init__(self, editor.document())
        self.editor = editor
        self.classes = {}
        self.elements = {}
        self.setup_default()

    def setup_default(self):
        self.elements['default'] = []
        font = self.editor.currentFont()
        self.classes['keyword'] = syntax_class(
            'keyword', font, QBrush(
                config.SYNTAX_KEYWORD_COLOR, Qt.SolidPattern))
        self.classes['comment'] = syntax_class(
            'comment', font, QBrush(
                config.SYNTAX_COMMENT_COLOR, Qt.SolidPattern))
        self.classes['file'] = syntax_class(
            'file', font, QBrush(
                config.SYNTAX_FILE_COLOR, Qt.SolidPattern))

    def add_keyword(self, highlighter_name, class_name, keyword):
        '''
        Add keyword to be highlighted with given class.
        '''
        self.add_block(highlighter_name, class_name, keyword, None)

    def add_block(self, highlighter_name, class_name,
                  start_pattern, end_pattern):
        '''
        Add block to be highlightd with given class.
        '''
        if class_name not in self.classes:
            return
        syntax_class = self.classes[class_name]
        self.elements[highlighter_name].append(syntax_element(
            syntax_class, start_pattern, end_pattern))

    def add_format_range(self, syntax_class_instance, start_index, length):
        '''
        Apply format on given text position.
        '''
        text_format = syntax_class_instance.get_text_format()
        self.setFormat(start_index, length, text_format)

    def find_element_pattern(self, text, start_index, pattern):
        '''
        Match the given pattern on the text and return the match index.
        If the pattern is not matched it returns -1.
        '''
        if start_index < 0:
            return -1
        match_index = pattern.indexIn(text, start_index)
        return match_index

    def find_elements(self, text, start_index, element_list):
        '''
        Evaluates all syntax_elements in the element_list for the given
        text and applys the format if matched. Handles Block opening.
        '''
        result_index = start_index
        for element in element_list:
            element_start_index = start_index
            while element_start_index >= 0:
                element_start_index = self.find_element_pattern(
                    text, element_start_index, element.rx_start)
                if element_start_index < 0:
                    break
                length = len(element.rx_start.cap(0))
                end_index = element_start_index + length
                if element.is_block:
                    end_index = self.find_element_pattern(
                        text, end_index, element.rx_end)
                    if end_index < 0:
                        length = len(text) - element_start_index
                        self.setCurrentBlockState(
                            self.get_syntax_element_index(element))
                if length == 0:
                    break
                self.add_format_range(
                    element.syntax_class, element_start_index, length)
                element_start_index = end_index
        return result_index

    def find_highlighters(self, text, start_index):
        '''
        Process highlighters in correct order. Ensures that the 'default'
        highlighter is executed after all other highlighters have been
        executed.
        '''
        result_index = start_index
        for (highlighter_name, elements) in self.elements.items():
            if highlighter_name == 'default':
                continue
            result_index = self.find_elements(text, result_index, elements)
        result_index = self.find_elements(
            text, result_index, self.elements['default'])
        return result_index

    def find_end_block(self, text, start_index, syntax_element_instance):
        '''
        Find End Block of a block started in find_elements. Only accepts
        the closing pattern to end the block otherwise applies the started
        blockformat for all characters.
        '''
        matched_length = 0
        result_index = self.find_element_pattern(
            text, start_index, syntax_element_instance.rx_end)
        if result_index >= 0:
            result_index += len(syntax_element_instance.rx_end.cap(0))
            matched_length = result_index
        else:
            matched_length = len(text)
            self.setCurrentBlockState(self.previousBlockState())
        self.add_format_range(
            syntax_element_instance.syntax_class, 0, matched_length)
        return result_index

    def get_syntax_element_index(self, syntax_element_instance):
        '''
        Used for setCurrentBlockState (a integer) that allows block formats.
        Returns the index of the given syntax_element_instance.
        '''
        element_index = 0
        for (highlighter_name, elements) in self.elements.items():
            for element in elements:
                if element is syntax_element_instance:
                    return element_index
                element_index += 1
        raise NotFoundError()

    def get_syntax_element_by_index(self, index):
        '''
        Used for previousBLockState() that allows block formats.
        Returns the element that matches the index. The index has to be
        created with the get_syntax_element_index function.
        '''
        element_index = 0
        for (highlighter_name, elements) in self.elements.items():
            for element in elements:
                if element_index == index:
                    return element
                element_index += 1
        return syntax_element()

    def highlightBlock(self, text):
        '''
        Qt Overload
        function frequently! called by the QTextEditor whenever it may have
        changed and wants to update the highlight.
        Usualy the function is called for each line from top to bottom. Then
        only for lines that are edited.
        '''
        start_position = 0
        self.setCurrentBlockState(0)
        if self.previousBlockState() > 0:
            # in block
            previous_element = self.get_syntax_element_by_index(
                self.previousBlockState())
            start_position = self.find_end_block(
                text, start_position, previous_element)
        if start_position >= 0:
            self.find_highlighters(text, start_position)
