# -*- coding: utf-8 -*-
# account_type_editor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Account editor for configurations.

Skeleton implementation only.
'''

from PySide.QtGui import QWidget
from PySide.QtCore import Signal
from netsplice.gui.account_edit.abstract_account_type_editor import (
    abstract_account_type_editor
)
from netsplice.gui.account_edit.raw.account_type_editor_ui import (
    Ui_AccountTypeEditor
)


class account_type_editor(QWidget, abstract_account_type_editor):
    '''
    RAW Account Type Editor for Accounts.

    Skeleton implementation only.
    '''

    config_changed = Signal()

    def __init__(self, parent=None, dispatcher=None):
        '''
        Initialize Module.

        Initialize bases and load ui.
        '''
        QWidget.__init__(self, parent)
        abstract_account_type_editor.__init__(self, 'Raw', dispatcher)
        self.ui = Ui_AccountTypeEditor()
        self.ui.setupUi(self)

    def can_check(self):
        '''
        Evaluate form completeness.

        Checks values in widget are complete to check the account.
        '''
        return False

    def can_commit(self):
        '''
        Evaluate form completeness for create or update.

        Checks values in widget are complete to create or update the account.
        '''
        return True

    def create(
            self, account_name, enabled, default_route, autostart, group_id,
            create_done_signal, create_failed_signal):
        '''
        Create a new account.

        Create a new account in the given group with the given name and the
        type values.
        '''
        configuration = self.ui.plain_config_edit.toPlainText()
        import_configuration = configuration

        self.backend.account_create.emit(
            account_name,
            self.type,
            configuration,
            import_configuration,
            enabled,
            default_route,
            autostart,
            create_done_signal,
            create_failed_signal)

    def commit(
            self, account_id, account_name, enabled, default_route, autostart,
            commit_done_signal, commit_failed_signal):
        '''
        Update existing account.

        Update an existing account with the given name and the type values.
        '''
        configuration = self.ui.plain_config_edit.toPlainText()
        import_configuration = configuration

        self.backend.account_update.emit(
            account_id,
            account_name,
            self.type,
            configuration,
            import_configuration,
            enabled,
            default_route,
            autostart,
            commit_done_signal,
            commit_failed_signal)

    def set_model(self, account_model):
        '''
        Set Model.

        Set Model for the widget.
        '''
        if account_model is not None:
            self.ui.plain_config_edit.setPlainText(
                account_model.configuration.get())
