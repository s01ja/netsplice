# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Init file for Netsplice.

Initializes version and app info.
'''


__version__ = 'unknown'

try:
    from netsplice._version import get_versions
    __version__ = get_versions()['version']
    del get_versions
except ImportError:
    # running on a tree that has not run
    # the setup.py setver
    print('[warn] except getting version')
    pass
