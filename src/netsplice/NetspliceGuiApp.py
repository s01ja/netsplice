#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
App that displays the GUI.
'''
import atexit
import multiprocessing
import os
import sys

import netsplice.gui.about as about_module
import netsplice.gui.account_edit as account_edit_module
import netsplice.gui.account_list as account_list_module
import netsplice.gui.credential as credential_module
import netsplice.gui.group_edit as group_edit_module
import netsplice.gui.help as help_module
import netsplice.gui.mainwindow as mainwindow_module
import netsplice.gui.logviewer as logviewer_module
import netsplice.gui.preferences as preferences_module
import netsplice.gui.statusbar as statusbar_module
import netsplice.gui.systray as systray_module

from PySide.QtCore import Qt, QLocale, QTranslator, QTimer
from PySide.QtGui import QApplication

from netsplice import __version__ as VERSION
from netsplice.util import get_logger

from netsplice.config import flags
from netsplice.config import constants
from netsplice.config import log as log_config
from netsplice.config import gui as config
from netsplice.gui.backend_dispatcher import backend_dispatcher
from netsplice.gui.backend_dispatcher import backend_thread
from netsplice.gui.event_loop import event_loop
from netsplice.gui.model import model as gui_model
from netsplice.plugins import get_plugins
from netsplice.util import commandline
from netsplice.util.ipc.application import application

logger = get_logger()


def run_gui():
    '''
    Run the GUI for the application.

    :param options: A dict of options parsed from the command line.
    :type options: dict
    :param flags_dict: A dict containing the flag values set on app start.
    :type flags_dict: dict
    '''

    # We force the style if on KDE so that it doesn't load all the KDE
    # libs, which causes a compatibility issue in some systems.
    # For more info, see issue #3194.
    if os.environ.get('KDE_SESSION_UID') is not None:
        sys.argv.append('-style')
        sys.argv.append('Cleanlooks')
    qApp = QApplication(sys.argv)

    locale = QLocale.system().name()  # en_US, es_AR, ar_SA, etc
    locale_short = locale[:2]  # en, es, ar, etc
    rtl_languages = ('ar', )  # Right now tested on 'arabic' only.

    systemQtTranslator = QTranslator(qApp)
    if systemQtTranslator.load('qt_%s' % locale, ':/translations'):
        qApp.installTranslator(systemQtTranslator)

    netspliceQtTranslator = QTranslator(qApp)
    if netspliceQtTranslator.load('%s.qm' % locale_short, ':/translations'):
        qApp.installTranslator(netspliceQtTranslator)

    if locale_short in rtl_languages:
        qApp.setLayoutDirection(Qt.LayoutDirection.RightToLeft)

    # Needed for initializing qsettings it will write
    # .config/OrganizationName/ApplicationName.conf top level app
    # settings in a platform independent way.
    qApp.setOrganizationName(constants.NS_NAME)
    qApp.setApplicationName(constants.NS_NAME)
    qApp.setOrganizationDomain(constants.NS_DOMAIN)

    # HACK:
    # We need to do some 'python work' once in a while, otherwise, no python
    # code will be called and the Qt event loop will prevent the signal
    # handlers for SIGINT/SIGTERM to be called.
    # See: http://stackoverflow.com/a/4939113/687989
    qt_loop_timer = QTimer()
    qt_loop_timer.setInterval(config.LOOP_TIMER)
    qt_loop_timer.timeout.connect(lambda: None)

    options = commandline.get_options()

    flags.DEBUG = options.debug
    if options.port is not None:
        constants.PORT_BACKEND = options.port
    logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    logger.info('Netsplice version %s' % VERSION)
    logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    if log_config.WRITE_STDERR:
        logger.warn(
            'Connection refused errors are normal during startup.'
            ' Change the config.log.WRITE_STDERR option to hide messages')

    backend_host = constants.LOCALHOST
    backend_port = constants.PORT_BACKEND
    logger.debug('Start backend thread')
    backend = backend_thread(backend_dispatcher, backend_host, backend_port)
    backend.start()
    dispatcher = backend.get_dispatcher()

    logger.debug('Configure application glue')
    app = application()
    app.set_dispatcher(dispatcher)
    app.set_model(gui_model())
    app.set_event_loop(event_loop())
    dispatcher.model_changed.connect(app.event_loop.process_events)
    dispatcher.preferences_changed.connect(
        app.event_loop.process_preferences_events)
    dispatcher.preferences_plugins_changed.connect(
        app.event_loop.process_preferences_plugins_events)

    logger.debug('Add Modules')
    app.add_module(about_module)
    app.add_module(account_edit_module)
    app.add_module(account_list_module)
    app.add_module(credential_module)
    app.add_module(group_edit_module)
    app.add_module(help_module)
    app.add_module(mainwindow_module)
    app.add_module(preferences_module)
    app.add_module(logviewer_module)
    app.add_module(statusbar_module)
    app.add_module(systray_module)

    for plugin in get_plugins():
        app.add_plugin(plugin)
    app.register_plugin_components('util')
    app.register_plugin_components('gui')

    logger.debug('Show mainwindow')

    app.get_module('mainwindow').show(app)

    logger.debug('Pass control to Qt event handler')
    app.event_loop.start()

    exit_code = qApp.exec_()

    # Wait for the backend-thread.
    # Trigger a shutdown regardless if it was already triggered.
    dispatcher.shutdown.emit()
    backend.wait()
    sys.exit(exit_code)


if __name__ == '__main__':
    multiprocessing.freeze_support()
    run_gui()
