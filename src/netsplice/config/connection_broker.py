# -*- coding: utf-8 -*-
# connection_broker.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Constants for the connection_broker state machine.
'''

# States
ARBORTED = 'backend.connection_broker.state.aborted'
ACTIVE = 'backend.connection_broker.state.active'
INACTIVE = 'backend.connection_broker.state.inactive'

STATES = [
    ARBORTED,
    ACTIVE,
    INACTIVE
]

# Triggers
RESUME = 'backend.connection_broker.trigger.resume'
CANCEL = 'backend.connection_broker.trigger.cancel'
DONE = 'backend.connection_broker.trigger.done'
