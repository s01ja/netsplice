# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Variables defined here are local to the model component of the client.
'''


# The backend-GUI model will wait at most this long before it emits a heartbeat
# regardless if there are any changes in the model.
HEARTBEAT_SECONDS = 5  # in s

# Model Version
# Increase whenever a model migration is required. Persistent items  will
# store that version and request an upgrade on load.
VERSION = 1
