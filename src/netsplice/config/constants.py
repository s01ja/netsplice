# -*- coding: utf-8 -*-
# constants.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Variables defined here are shared between the modules.

Only use global variables when no safer alternative is available and the
variables need to span all modules / processes.
'''


# Naming constants
NS_NAME = 'Netsplice'
NS_DOMAIN = 'ipredator.se'

# Default host definitions
LOCALHOST = '127.0.0.1'

# Default port definitions
# This option may be controlled by the --port option to the NetspliceGuiApp
PORT_BACKEND = 10000
PORT_NET = PORT_BACKEND + 1
PORT_PRIV = PORT_BACKEND + 2
PORT_UNPRIV = PORT_BACKEND + 3
