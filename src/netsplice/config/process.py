# -*- coding: utf-8 -*-
# process.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configuration for executables that are executed as subprocesses.
'''
import sys

# Graphical Sudo Application that is used to elevate a specific task.
# Only relevant for linux, should be handled by the installer for end-users.
# None: only unprivileged actions will work, privileged parts of the
#       application have to be privileged by other means (eg start as
#       service).
# ['ktsuss', '-u', 'root']: more complex example of passing the elevation to
#                           the OS. The current user needs to be in the wheel
#                           group
GRAPHICAL_SUDO_APPLICATION = ['pkexec']

# Execute Source
# During development it is inconvenient to build search for executables where
# they might be installed on production systems. Therefore a developer may
# set this to True, but never commit this line
#
# When set to True, Netsplice*App binaries ale located in src/netsplice and
# will be executed with python ${Name}.py and openvpn binaries are expected in
# var/build/openvpn
#
# When set to False Netsplice*App binaries are located in the same directory
# as the caller (${PREFIX}/bin/NetspliceGuiApp will start
# ${PREFIX}/lib/NetspliceBackendApp) and openvpn binaries are expected in
# ${PREFIX}/lib/libexec/openvpn/openvpn-${version}/sbin/openvpn
execute_source = not getattr(sys, 'frozen', False)

# Default prefix for third party executables bundled with the installation
LIBEXEC = 'libexec'

# Developer prefix for third party executables that have been compiled for the
# current platform.
LIBEXEC_BUILD = 'var/build'


# Names of binaries that have a python counterpart and will be run by python if
# execute_source is true
SUBAPP_NAMES = [
    'NetspliceBackendApp',
    'NetspliceNetApp',
    'NetsplicePrivilegedApp',
    'NetspliceUnprivilegedApp',
]

# Locations where to search for binaries when they are not found in the
# LIBEXEC directory.
LIBEXEC_BINARY_SYSTEM_LOCATIONS = {
    'openssl': ['/usr/bin']
}

# When a unique process is requested, we will wait this long after SIGTERM
# before sending SIGKILL
KILL_TIMEOUT = 2

# When the privileged backend kills any other privileged instances, this
# Wait-time in seconds should be added to ensure that the ports are free again.
# Affects the NetsplicePrivilegedApp
WAIT_AFTER_KILL_OTHER = 3
