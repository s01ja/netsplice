# -*- coding: utf-8 -*-
# test_openvpn.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Tests for Parser OpenVPN
'''

import mock
import os
import sys
from .openvpn import openvpn
from netsplice.plugins.openvpn.config import openvpn as config_openvpn
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.model.configuration_list import (
    configuration_list as configuration_list_model
)
from netsplice.util.errors import (
    InvalidUsageError
)


def get_instance():
    instance = openvpn()
    return instance


def test_analyze_with_empty_current_line_returns_false():
    instance = get_instance()
    instance.current_line = None
    result = instance.analyze()
    # analyze should not do anything with empty current
    print(result)
    assert(result is False)


def test_analyze_with_simple_current_line_returns_true():
    instance = get_instance()
    instance.current_line = 'testvalue'
    result = instance.analyze()
    # analyze should not do anything with empty current
    print(result)
    assert(result is True)


def test_analyze_with_current_line_block_start_returns_false():
    instance = get_instance()
    instance.current_line = '<testvalue>'
    result = instance.analyze()
    assert(result is False)


def test_analyze_with_current_line_bad_block_start_returns_false():
    instance = get_instance()
    instance.current_line = '<testvalue'
    result = instance.analyze()
    assert(result is False)


def test_analyze_with_current_line_bad_block_end_returns_false():
    instance = get_instance()
    instance.option_name = 'testvalue'
    instance.in_block = True
    instance.current_line = '</testvalue'
    result = instance.analyze()
    assert(result is False)


def test_analyze_with_current_line_in_block_end_returns_false():
    instance = get_instance()
    instance.option_name = 'testvalue'
    instance.option_value = ''
    instance.in_block = True
    instance.current_line = 'something'
    result = instance.analyze()
    assert(result is False)


def test_analyze_with_current_line_in_block_end_returns_true():
    instance = get_instance()
    instance.option_name = 'testvalue'
    instance.option_value = ''
    instance.in_block = True
    instance.current_line = '</testvalue>'
    result = instance.analyze()
    assert(result is True)


def test_analyze_with_current_line_in_block_noend_returns_false():
    instance = get_instance()
    instance.option_name = 'testvalue'
    instance.option_value = ''
    instance.in_block = True
    instance.current_line = '<block_content>'
    result = instance.analyze()
    assert(result is False)


def test_analyze_with_empty_current_line_in_block_noend_returns_false():
    instance = get_instance()
    instance.option_name = 'testvalue'
    instance.option_value = 'something'
    instance.in_block = True
    instance.current_line = 'current line'
    result = instance.analyze()
    assert(result is False)
    print(instance.option_value)
    assert(instance.option_value == 'something\ncurrent line')


def test_analyze_with_comment_clears_option_and_returns_true():
    instance = get_instance()
    instance.option_name = 'testvalue'
    instance.option_value = 'something'
    instance.current_line = '# current line'
    result = instance.analyze()
    assert(result is True)
    print(instance.option_value)
    assert(instance.option_name == '#')
    assert(instance.option_type == config_openvpn.TYPE_COMMENT)
    assert(instance.option_value == 'current line')


def test_analyze_with_comment_unspaced_clears_option_and_returns_true():
    instance = get_instance()
    instance.option_name = 'testvalue'
    instance.option_value = 'something'
    instance.current_line = '#current line'
    result = instance.analyze()
    assert(result is True)
    print(instance.option_value)
    assert(instance.option_name == '#')
    assert(instance.option_type == config_openvpn.TYPE_COMMENT)
    assert(instance.option_value == 'current line')


def test_analyze_with_comment_empty_clears_option_and_returns_true():
    instance = get_instance()
    instance.option_name = 'testvalue'
    instance.option_value = 'something'
    instance.current_line = '#'
    result = instance.analyze()
    assert(result is True)
    print(instance.option_value)
    assert(instance.option_name == '#')
    assert(instance.option_type == config_openvpn.TYPE_COMMENT)
    assert(instance.option_value == '')


def test_analyze_with_current_line_block_start_sets_correct_key():
    instance = get_instance()
    instance.current_line = '<testvalue>'
    instance.analyze()
    result = instance.option_name

    assert(result == 'testvalue')


def test_commit_adds_element_with_values():
    instance = get_instance()
    instance.option_name = 'testname'
    instance.option_value = 'testvalue'
    instance.commit()
    assert(instance.elements[0].key.get() == 'testname')
    assert(instance.elements[0].get_value('value') == 'testvalue')


def test_commit_resets_values():
    instance = get_instance()
    instance.option_name = 'testname'
    instance.option_value = 'testvalue'
    instance.commit()
    assert(instance.option_name is None)
    assert(instance.option_value is None)


def test_get_insert_template_returns_empty_configuration_item():
    instance = get_instance()
    result = instance.get_insert_template()
    assert(isinstance(result, (configuration_item_model,)))
    assert(result.get_key() == '')
    assert(result.get_value('status') is None)
    assert(result.get_value('type') == config_openvpn.TYPE_STRING)
    assert(result.get_value('value') == '')
    assert(result.get_value('block') is False)


def test_get_text_produces_blocks():
    instance = get_instance()
    instance.option_name = 'testname'
    instance.option_value = 'testvalue\ntestvalue\ntestvalue'
    instance.option_block = True
    instance.commit()

    result = instance.get_text()
    expected = '<testname>\ntestvalue\ntestvalue\ntestvalue\n</testname>'
    print(expected, result)
    assert(expected == result)


def test_get_text_with_singleline_block_produces_blocks():
    instance = get_instance()
    instance.option_name = 'testname'
    instance.option_value = 'testvalue'
    instance.option_block = True
    instance.commit()

    result = instance.get_text()
    assert(result == '<testname>\ntestvalue\n</testname>')


def test_get_text_with_empty_block_produces_blocks():
    instance = get_instance()
    instance.option_name = 'testname'
    instance.option_value = ''
    instance.option_block = True
    instance.commit()

    result = instance.get_text()
    assert(result == '<testname>\n\n</testname>')

def test_get_text_with_keyvalue_produces_lines():
    instance = get_instance()
    instance.option_name = 'testname'
    instance.option_value = 'testvalue1'
    instance.commit()

    result = instance.get_text()
    print(result)
    assert(result == 'testname testvalue1')
    instance.option_name = 'testname'
    instance.option_value = 'testvalue2'
    instance.commit()

    result = instance.get_text()
    print(result)
    assert(result == 'testname testvalue1\ntestname testvalue2')


def test_get_text_with_keyonly_produces_lines():
    instance = get_instance()
    instance.option_name = 'testname'
    instance.option_value = ''
    instance.commit()

    result = instance.get_text()
    print(result)
    assert(result == 'testname')
    instance.option_name = 'testname'
    instance.option_value = ''
    instance.commit()

    result = instance.get_text()
    print(result)
    assert(result == 'testname\ntestname')


def test_get_text_with_none_value_no_value_text():
    instance = get_instance()
    instance.option_name = 'testname'
    instance.option_value = None
    instance.commit()

    result = instance.get_text()
    print(result)
    assert(result == 'testname')


def test_get_type_with_known_returns_correct():
    instance = get_instance()
    result = instance.get_type('#')
    print(result)
    assert(result == config_openvpn.TYPE_COMMENT)


def test_get_type_with_unknown_returns_option():
    instance = get_instance()
    result = instance.get_type('unknown_openvpn_keyword')
    print(result)
    assert(result == config_openvpn.TYPE_OPTION)
