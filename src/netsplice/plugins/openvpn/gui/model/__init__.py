# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from plugin import plugin as plugin_model
from plugin_collection import plugin_collection as plugin_collection_model
from request.plugin_collection import (
    plugin_collection as request_plugin_collection_model
)
from response.plugin import plugin as response_plugin_model
from response.plugin_collection import (
    plugin_collection as response_plugin_collection_model
)
from netsplice.plugins.openvpn.config import PLUGIN_NAME


def register(app):
    '''
    Register.

    Register the preference models in the gui model to receive the weak
    typed plugin models with marshaled model.
    '''
    model = app.get_model().preferences
    model.plugins.register_plugin(
        PLUGIN_NAME, plugin_model(model.plugins))
    model.response.plugins.register_plugin(
        PLUGIN_NAME, response_plugin_model())

    model.accounts.plugins.register_collection_plugin(
        PLUGIN_NAME, plugin_collection_model(model.accounts))
    model.accounts.request.plugins.register_collection_plugin(
        PLUGIN_NAME, request_plugin_collection_model())
    model.accounts.response.plugins.register_collection_plugin(
        PLUGIN_NAME, response_plugin_collection_model())
