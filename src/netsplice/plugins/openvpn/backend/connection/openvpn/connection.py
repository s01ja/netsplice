# -*- coding: utf-8 -*-
# connections.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
OpenVPN connection dispatcher.

Defines dispatcher functions that are called by controllers with the openvpn
privileged backend.
'''
import re
from tornado import gen, ioloop
from netsplice.config import flags
from netsplice.config import connection as config_connection
from netsplice.plugins.openvpn.config import backend as config
from netsplice.plugins.openvpn.config import openvpn as config_openvpn
from netsplice.model.credential import (
    credential as credential_model
)
from netsplice.backend.connection.connection_plugin import (
    connection_plugin as connection_base_dispatcher
)
from netsplice.backend.connection.credential import (
    credential as credential_base_dispatcher
)
from netsplice.backend.connection.model.status_list_item import (
    status_list_item as status_list_item_model
)
from netsplice.util import get_logger
from netsplice.util.errors import (
    NotFoundError, NoCredentialsProvidedError
)
from netsplice.util.model.errors import ValidationError
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.parser import factory as get_config_parser


logger = get_logger()


class connection(connection_base_dispatcher, credential_base_dispatcher):
    '''
    OpenVPN Connection implementation.

    Provides implementations for the connections dispatcher that allow the
    software to handle openVPN connections.
    '''

    def __init__(self, application, connection):
        '''
        Initialize module.

        Initialize the base class.
        '''
        connection_base_dispatcher.__init__(
            self, 'OpenVPN', application, connection)
        credential_base_dispatcher.__init__(self, application, connection)
        self._removing = False
        self._removed = False

    @gen.coroutine
    def _remove_privileged_process(self):
        '''
        Remove Privileged Process.

        Remove the privileged process. Only remove when the connection is not
        already in removing state. This happens when multiple events scheduled
        a remove. When the status of the connection is still active, it is
        disconnected first. This happens when a setup connection is removed
        from the broker.

        Decorators:
            gen.coroutine
        '''
        if self._removed or self._removing:
            return
        if self.connection.model.privileged_id.get() is None:
            return
        self._removing = True
        try:
            status = yield self.get_status()
            if status and status.active.get():
                yield self.privileged.openvpn.disconnect(
                    self.connection.model.privileged_id.get())
            yield self.privileged.openvpn.remove(
                self.connection.model.privileged_id.get())
            self._removed = True
        except ServerConnectionError as errors:
            logger.warn(
                'Failed to remove %s in the privileged subprocess: %s'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
        self._removing = False

    def _schedule_remove(self):
        '''
        Schedule Remove.

        Schedule the async removal of the privileged process.
        '''
        ioloop.IOLoop.current().call_later(
            0, self._remove_privileged_process)

    def apply_preference_overrides(self, named_config_items):
        '''
        Apply overrides from preferences.

        Iterate all configured overrides and apply them on the given items.
        Overrides that have a None value will filter the given item with the
        same name from the item list.
        '''
        preferences_model = self.application.get_module('preferences').model
        overrides = preferences_model.overrides
        for override in overrides:
            if override.type.get() != self.type:
                continue
            try:
                self.apply_preference_override(
                    override.name.get(), override.value.get(),
                    named_config_items)
            except NotFoundError:
                pass

    def apply_preference_override(self, name, value, named_config_items):
        '''
        Apply single preference override.

        Check if the given override is in the named_config_items and change the
        value.

        Arguments:
            name ([type]): name of the attribute to replace.
            value ([type]): value to replace
            named_config_items (list): parsed OpenVPN config.

        Raises:
            NotFoundError -- When the named_config_item has no key with the
                given name.
        '''
        named_value = named_config_items.find_by_key(name)
        if value is None:
            logger.info(
                'Override for %s caused to remove the line'
                % (name,),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            named_config_items.remove(named_value)
        else:
            logger.info(
                'Override for %s with %s'
                % (name, value),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            named_value.values.find_by_name('value').value.set(value)

    @gen.coroutine
    def abort(self):
        '''
        Abort the process.

        Free all resources that were allocated.
        '''
        logger.warn('Aborting %s.' % (self.connection.account.name.get(),))
        self._removing = True
        try:
            if self.connection.model.privileged_id.get() is not None:
                status = yield self.get_status()
                if status and status.active.get():
                    yield self.privileged.openvpn.disconnect(
                        self.connection.model.privileged_id.get())
                if status:
                    yield self.privileged.openvpn.remove(
                        self.connection.model.privileged_id.get())
            self._removed = True
        except ServerConnectionError as errors:
            logger.error(
                'Failed to remove %s in the privileged subprocess: %s'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
        self._removing = False

    @gen.coroutine
    def connect(self):
        '''
        Connect a setup connection.

        Instruct openvpn to initiate a previously setup connection.
        This is a event handler, it is called _after_ the connect-event
        was triggered from an account.
        '''
        # connect_finish is triggered in self.check_log_item

        del self.connection.model.errors[:]
        self.connection.model.reset_environment()
        self.connection.model.reset_routes()
        self.connection.model.commit()
        try:
            yield self.privileged.openvpn.connect(
                self.connection.model.privileged_id.get())
        except ServerConnectionError as errors:
            logger.critical(
                'Cannot connect %s in privileged backend: %s.'
                % (self.connection.account.name.get(), str(errors),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            self.disconnect_abort()
            raise gen.Return(False)
        self.schedule_get_status()
        raise gen.Return(True)

    @gen.coroutine
    def disconnect(self):
        '''
        Disconnect an active connection.

        Instruct openvpn to shutdown a previously setup connection only when
        the connection is currently active. Remove the connection from the
        privileged backend afterwards.
        '''
        # disconnect finish is triggered from check_log_item
        if self.connection.in_disconnecting_process():
            logger.warn(
                'Already disconnecting %s.'
                % (self.connection.account.name.get(),),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            raise gen.Return(False)
        if self.connection.state == config_connection.DISCONNECTING_FAILURE:
            # created, setup, never got the hold release
            self.connection.model.failed.set(True)
            self.disconnect_failure_process()
            yield self._remove_privileged_process()
            self.disconnect_failure_finish()
            raise gen.Return(True)

        yield self._remove_privileged_process()
        raise gen.Return(True)

    @gen.coroutine
    def get_status(self):
        '''
        Get the latest status of the connection.

        Result connection contains connection statistics and an active bit.
        '''
        if self._removed is True:
            raise gen.Return(None)
        try:
            result = yield self.privileged.openvpn.connection_status(
                self.connection.model.privileged_id.get())
            if result.active.get() is False:
                # Mark the connection as failed, get_status should not be
                # happening with inactive connections unless the connection
                # is disconnecting.
                if self.connection.in_disconnecting_process():
                    if self.connection.model.failed.get() is False:
                        self.connection.model.failed.set(True)
                        self.disconnect_failure_init()
                raise gen.Return(result)

            # gui.model.connection_list_item
            try:
                gui_model = self.application.get_module('gui').model
                gui_connection = gui_model.connections.find_by_id(
                    self.connection.id.get())
                if gui_connection.update_connection_statistic(result):
                    gui_model.commit()
            except NotFoundError:
                pass
            raise gen.Return(result)
        except ServerConnectionError as errors:
            logger.critical(
                'Cannot get status for %s from privileged backend: %s.'
                % (self.connection.account.name.get(), str(errors)),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            raise gen.Return(None)

    def parse_environment_values(self, message):
        '''
        Parse Environment Values.

        Parse the message for UPDOWN:ENV, into a dictionary.
        '''
        lines = message.splitlines(False)
        for line in lines:
            if not line.startswith('>UPDOWN:ENV'):
                continue
            if line.startswith('>UPDOWN:ENV,END'):
                continue
            line_value = line.replace('>UPDOWN:ENV,', '')
            key_value = line_value.split('=')
            key = key_value[0]
            del key_value[0]
            value = '='.join(key_value)
            self.connection.model.environment.add_value(
                'openvpn_%s' % (key,), value)
            if key.startswith('ifconfig_') or key == 'redirect_gateway':
                # This values are processed by the gui to display
                # the connection information.
                # This should be changed to a model field of the connection
                self.connection.model.environment.add_value(
                    'connection_%s' % (key,), value)

    def parse_route(self, message):
        '''
        Parse Route.

        Parse the message for /bin/route and store it in an object list.
        '''
        lines = message.splitlines(False)
        for line in lines:
            match = re.search(
                '/bin/route add -net ([^ ]*) netmask ([^ ]*) gw ([^ ]*)',
                line)
            if match is None:
                continue
            try:
                net = match.group(1)
                netmask = match.group(2)
                gateway = match.group(3)
                self.connection.model.routes.add(
                    net=net, netmask=netmask, gateway=gateway)
            except IndexError:
                continue

    @gen.coroutine
    def reconnect(self):
        '''
        Reconnect a previously connected connection.

        Changes the model state and calls the privileged dispatcher to
        reconnect.
        '''
        # reconnected is triggered in self.check_log_item
        del self.connection.model.errors[:]
        self.connection.model.commit()
        active = False
        try:
            status = yield self.get_status()
            if status is not None and status.active.get() is True:
                active = True
        except ServerConnectionError as errors:
            pass
        try:
            if active:
                yield self.privileged.openvpn.reconnect(
                    self.connection.model.privileged_id.get())
            else:
                yield self.privileged.openvpn.connect(
                    self.connection.model.privileged_id.get())
        except ServerConnectionError as errors:
            logger.critical(
                'Cannot reconnect %s in privileged backend: %s.'
                % (self.connection.account.name.get(), str(errors)),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            self.disconnect_abort()
            raise gen.Return(False)
        self.schedule_get_status()
        raise gen.Return(True)

    @gen.coroutine
    def setup(self, connection_model_instance, account_model_instance):
        '''
        Setup a connection with the values of the account.

        Creates the connection_id and returns a connection model instance.
        '''
        credential = credential_model()
        credential.username.set('')
        credential.password.set('')

        plain_config = account_model_instance.configuration.get()
        openvpn_config = account_model_instance.openvpn
        openvpn_version = openvpn_config.openvpn_version.get()
        openvpn_nice = openvpn_config.nice.get()

        openvpn_config_parser = get_config_parser(self.type)
        openvpn_config_parser.set_text(plain_config)
        openvpn_config = openvpn_config_parser.get_config()

        self.apply_preference_overrides(openvpn_config)

        try:
            available_openvpn_versions = yield self.application. \
                get_unprivileged().openvpn.get_version_list()
            found = False
            for available_version in available_openvpn_versions:
                if available_version.version.get() == openvpn_version:
                    found = True
                    break
            if not found:
                logger.warn(
                    'Using Default openvpn version (%s) as configured version'
                    ' (%s) is not available.'
                    % (config.DEFAULT_OPENVPN_VERSION, openvpn_version),
                    extra={
                        'connection_id': self.connection.id.get(),
                        'account_id': self.connection.account.id.get()
                    })
                openvpn_version = config.DEFAULT_OPENVPN_VERSION
        except ServerConnectionError as errors:
            logger.warn(
                'Cannot request available openvpn versions: %s'
                % (str(errors)),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })

        try:
            openvpn_config.find_by_key('auth-user-pass')
            self.credential_type = self.TYPE_USERNAME_PASSWORD
            credential = yield self.get_credential(
                account_model_instance.id.get())
        except NotFoundError:
            pass
        except NoCredentialsProvidedError:
            self.gui_model.credentials.clean()
            self.setup_cancel()
            raise gen.Return(False)

        try:
            openvpn_config.find_by_key('askpass')
            self.credential_type = self.TYPE_PRIVATE_KEY_PASSWORD
            credential = yield self.get_credential(
                account_model_instance.id.get())
        except NotFoundError:
            pass
        except NoCredentialsProvidedError:
            self.gui_model.credentials.clean()
            self.setup_cancel()
            raise gen.Return(False)

        openvpn_config_parser.set_config(openvpn_config)
        plain_config = openvpn_config_parser.get_text()

        try:
            connection_id = yield self.privileged.openvpn.setup_connection(
                plain_config,
                credential.username.get(),
                credential.password.get(),
                openvpn_nice,
                openvpn_version)
            self.log_connection_id = connection_id.id.get()
            connection_model_instance.privileged_id.set(
                connection_id.id.get())
        except ValidationError as errors:
            logger.error(
                'Cannot setup connection for %s in privileged backend, because'
                ' the passed values are invalid. Please check the account'
                ' configuration and retry: %s'
                % (self.connection.account.name.get(), str(errors)),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            self.setup_cancel()
            raise gen.Return(False)
        except ServerConnectionError as errors:
            logger.critical(
                'Cannot setup connection for %s in privileged backend: %s.'
                % (self.connection.account.name.get(), str(errors)),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
            self.setup_cancel()
            raise gen.Return(False)
        self.gui_model.credentials.clean()
        if self.connection.state != config_connection.ABORTED:
            self.setup_finish()
        raise gen.Return(True)

    def check_log_item(self, log_item):
        '''
        Update connection status from the given message.

        Modifies the connection model and adds messages to the status and error
        lists. Interprets certain messages to be a status-change that needs
        further processing.
        '''
        message = log_item.message.get()
        # Log message from Streamlog (stdout of privileged process)
        # without the date and origin infos.
        log_message = message[25:]
        # The message is a 'conclusion' of the process. It is usually the last
        # (fatal) or a success indicator.
        # state_changed
        status_changed = False
        status_initial = self.connection.state
        # The Message was not handled
        handled = False

        error_log_item = status_list_item_model()
        error_log_item.state.set('failure')
        error_log_item.message.set(log_message)
        status_item = status_list_item_model()
        status_item.message.set(message)
        status_item.state.set('success')
        for openvpn_log, details in config_openvpn.LOG_MESSAGES.items():
            if not (message.startswith(openvpn_log) or
                    log_message.startswith(openvpn_log)):
                continue
            handled = True
            if 'exceptions' in details.keys():
                exception_details = None
                for in_string, exception in details['exceptions'].items():
                    if not (in_string in message or
                            in_string in log_message):
                        continue
                    exception_details = exception
                    break
                if exception_details:
                    details = exception_details
            actions = details['actions']
            if config_openvpn.LOG_CHANGE_STATE in actions:
                status_changed = True

            if config_openvpn.LOG_CONNECTED in actions:
                self.parse_environment_values(message)
                if self.connection.state == \
                        config_connection.RECONNECTING_PROCESS:
                    self.reconnect_finish()
                elif self.connection.state == \
                        config_connection.RECONNECTING:
                    self.reconnect_process()
                    self.reconnect_finish()
                elif self.connection.state == \
                        config_connection.CONNECTING_PROCESS:
                    self.connect_finish()
                elif self.connection.state == \
                        config_connection.CONNECTING:
                    self.connect_process()
                    self.connect_finish()
                elif self.connection.state == \
                        config_connection.CONNECTED:
                    # default: UPDOWN:UP
                    # additional: Initialization Sequence Completed
                    #    when reconnecting
                    status_changed = False
                else:
                    logger.error(
                        'Invalid LOG_CONNECTED for %s in state %s'
                        % (message, self.connection.state),
                        extra={
                            'connection_id': self.connection.id.get(),
                            'account_id': self.connection.account.id.get()
                        })
                self.gui_model.credentials.mark_ok(
                    self.connection.account.id.get())
            if config_openvpn.LOG_DISCONNECT in actions:
                self.parse_environment_values(message)
                if self.connection.in_connected():
                    # external trigger of disconnect
                    self.disconnect_init()
            if config_openvpn.LOG_DISCONNECTED in actions:
                if self.connection.state == \
                        config_connection.DISCONNECTING:
                    self.disconnect_process()
                    self.disconnect_finish()
                    self._schedule_remove()
                elif self.connection.state == \
                        config_connection.DISCONNECTING_FAILURE:
                    self.disconnect_failure_process()
                    self.disconnect_failure_finish()
                    self._schedule_remove()
                elif self.connection.state == \
                        config_connection.DISCONNECTING_PROCESS:
                    self.disconnect_finish()
                    self._schedule_remove()
                elif self.connection.state == \
                        config_connection.DISCONNECTING_FAILURE_PROCESS:
                    self.disconnect_failure_finish()
                    self._schedule_remove()
                elif self.connection.state == \
                        config_connection.DISCONNECTED:
                    self._schedule_remove()
                elif self.connection.state == \
                        config_connection.DISCONNECTED_FAILURE:
                    self._schedule_remove()
                elif self.connection.state == \
                        config_connection.RECONNECTING_PROCESS:
                    pass
                elif self.connection.state == \
                        config_connection.ABORTED:
                    pass
                else:
                    self.disconnect_init()
                    self.disconnect_process()
                    self.disconnect_finish()
                    self._schedule_remove()
            if config_openvpn.LOG_CONNECTING in actions:
                if self.connection.state == \
                        config_connection.CONNECTING:
                    self.connect_process()
                elif self.connection.state == \
                        config_connection.RECONNECTING:
                    self.reconnect_process()
                elif self.connection.state == \
                        config_connection.DISCONNECTING:
                    self.reconnect_init()
                    self.reconnect_process()
                elif self.connection.state == \
                        config_connection.CONNECTING_PROCESS:
                    pass
                elif self.connection.state == \
                        config_connection.RECONNECTING_PROCESS:
                    pass
                else:
                    logger.error(
                        'Invalid LOG_CONNECTING for %s in state %s'
                        % (message, self.connection.state),
                        extra={
                            'connection_id': self.connection.id.get(),
                            'account_id': self.connection.account.id.get()
                        })
                # avoid re-entering credentials when they were never checked.
                self.gui_model.credentials.mark_ok(
                    self.connection.account.id.get())
            if config_openvpn.LOG_RECONNECT in actions:
                if self.connection.state == \
                        config_connection.CONNECTED or \
                        self.connection.state == \
                        config_connection.RECONNECTED:
                    self.reconnect_init()
                    self.reconnect_process()
                elif self.connection.state == \
                        config_connection.RECONNECTING:
                    self.reconnect_process()
                elif self.connection.state == \
                        config_connection.CONNECTING_PROCESS:
                    # needs a hold_release
                    pass
                elif self.connection.state == \
                        config_connection.RECONNECTING_PROCESS:
                    pass
                else:
                    logger.error(
                        'Invalid LOG_RECONNECT for %s in state %s'
                        % (message, self.connection.state),
                        extra={
                            'connection_id': self.connection.id.get(),
                            'account_id': self.connection.account.id.get()
                        })

            if config_openvpn.LOG_ERROR in actions:
                self.connection.model.failed.set(True)
                if self.connection.state == \
                        config_connection.DISCONNECTED:
                    pass
                elif self.connection.state == \
                        config_connection.DISCONNECTED_FAILURE:
                    pass
                elif self.connection.state == \
                        config_connection.ABORTED:
                    pass
                else:
                    self.disconnect_failure_init()
                    self.disconnect_failure_process()
                    self.disconnect_failure_finish()
                self._schedule_remove()
                status_item.state.set('failure')
                self.connection.model.errors.append(status_item)
                log_item.level.set('error')
                status_changed = True

            if config_openvpn.LOG_ERROR_CONNECTED in actions:
                # LOGERROR when in a connected/ing state
                if self.connection.state == \
                        config_connection.CONNECTED or \
                        self.connection.state == \
                        config_connection.RECONNECTED or \
                        self.connection.state == \
                        config_connection.CONNECTING or \
                        self.connection.state == \
                        config_connection.CONNECTING_PROCESS or \
                        self.connection.state == \
                        config_connection.RECONNECTING or \
                        self.connection.state == \
                        config_connection.RECONNECTING_PROCESS:
                    self.connection.model.failed.set(True)
                    self.disconnect_failure_init()
                    self.disconnect_failure_process()
                    self.disconnect_failure_finish()
                    self._schedule_remove()
                    status_item.state.set('failure')
                    self.connection.model.errors.append(status_item)
                    log_item.level.set('error')
                    status_changed = True
                else:
                    log_item.level.set('info')

            if config_openvpn.LOG_ABORT in actions:
                self.connection.model.failed.set(True)
                self.disconnect_abort()
                self._schedule_remove()
                status_item.state.set('failure')
                self.connection.model.errors.append(status_item)
                log_item.level.set('error')
                status_changed = True
            if config_openvpn.LOG_INFO in actions:
                log_item.level.set('info')
            if config_openvpn.LOG_ROUTE in actions:
                self.parse_route(message)
            if config_openvpn.LOG_DEBUG in actions:
                log_item.level.set('debug')
            if config_openvpn.LOG_WARNING in actions:
                log_item.level.set('warning')
                status_item.state.set('warning')
            if config_openvpn.LOG_PROGRESS in actions:
                self.connection.model.progress.append(status_item)
            if config_openvpn.LOG_RESET_CREDENTIAL in actions:
                self.gui_model.credentials.mark_wrong(
                    self.connection.account.id.get())
            log_item.message.set(
                '%s (%s)'
                % (details['help'], log_item.message.get()))

        if flags.VERBOSE > 1 and not handled:
            logger.info(
                'Not handled: %s' % (message,),
                extra={
                    'connection_id': self.connection.id.get(),
                    'account_id': self.connection.account.id.get()
                })
        if status_initial != self.connection.state:
            status_changed = True
        return status_changed
