# -*- coding: utf-8 -*-
# openvpn.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from tornado import gen


class openvpn(object):
    '''
    '''
    def __init__(self, application):
        '''
        '''
        self.application = application

    @gen.coroutine
    def get_version_detail(self, version):
        '''
        Get Version Detail.

        Get detail for an installed version.
        '''
        dispatcher = self.application.get_unprivileged().openvpn
        version_detail = yield dispatcher.get_version_detail(version)
        raise gen.Return(version_detail)

    @gen.coroutine
    def get_version_help(self, version):
        '''
        Get Version Help.

        Get the --help information for a version.
        '''
        dispatcher = self.application.get_unprivileged().openvpn
        version_help = yield dispatcher.get_version_help(version)
        raise gen.Return(version_help)

    @gen.coroutine
    def get_version_list(self):
        '''
        Get Version List.

        List versions for the available executables.
        '''
        dispatcher = self.application.get_unprivileged().openvpn
        versions = yield dispatcher.get_version_list()
        raise gen.Return(versions)
