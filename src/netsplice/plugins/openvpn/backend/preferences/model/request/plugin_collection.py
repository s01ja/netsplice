# -*- coding: utf-8 -*-
# plugin_collection.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Account.

Response model for plugin collection (backend->gui).
'''

from netsplice.config import backend as config_backend
from netsplice.util.model.field import field

from netsplice.model.plugin_collection_item import (
    plugin_collection_item as plugin_collection_item_model
)
from netsplice.model.validator.max import max as max_validator
from netsplice.model.validator.min import min as min_validator
from netsplice.model.validator.version import version as version_validator
from netsplice.plugins.openvpn.config import backend as config


class plugin_collection(plugin_collection_item_model):
    def __init__(self):
        plugin_collection_item_model.__init__(self, None)

        self.openvpn_version = field(
            default=config.DEFAULT_OPENVPN_VERSION,
            validators=[version_validator()])

        self.nice = field(
            default=config.DEFAULT_NICE,
            validators=[min_validator(-20), max_validator(19)])
