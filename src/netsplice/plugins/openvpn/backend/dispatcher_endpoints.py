# -*- coding: utf-8 -*-
# dispatcher_endpoints.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Endpoints used by dispatcher class(es).
'''


#
# Endpoints used by this dispatcher
#
OPENVPN_CONN_CONNECT = (
    r'module/plugin/openvpn/connections/%(connection_id)s/connect')
OPENVPN_CONN_DISCONNECT = (
    r'module/plugin/openvpn/connections/%(connection_id)s/disconnect')
OPENVPN_CONN_LIST = (
    r'module/plugin/openvpn/connections')
OPENVPN_CONN_RECONNECT = (
    r'module/plugin/openvpn/connections/%(connection_id)s/reconnect')
OPENVPN_CONN_REMOVE = (
    r'module/plugin/openvpn/connections/%(connection_id)s')
OPENVPN_CONN_SETUP = (
    r'module/plugin/openvpn/connections')
OPENVPN_CONN_STATUS = (
    r'module/plugin/openvpn/connections/%(connection_id)s')
OPENVPN_UNPRIV_EXECUTABLES = (
    r'module/plugin/openvpn/executables')
OPENVPN_UNPRIV_HELP = (
    r'module/plugin/openvpn/versions/%(version)s/help')
OPENVPN_UNPRIV_VERSION = (
    r'module/plugin/openvpn/versions/%(version)s')
