# -*- coding: utf-8 -*-
# openvpn.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
OpenVPN Process (unprivileged commands).
'''

import os
import sys

from .model.version_list import version_list as version_list_model
from .model.version_detail import version_detail as version_detail_model
from .model.version_help import version_help as version_help_model
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.util import get_logger
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.location import (
    factory as location_factory
)
from netsplice.util.process.errors import ProcessError

logger = get_logger()

VERSION_SYSTEM_PROVIDED = 'v0.0.0-system-provided'


class openvpn(object):

    BINARY_NAME = 'openvpn'

    def __init__(self):
        '''
        '''
        self.process = None

    def _execute(self, version, parameter_name, expected_error_code=None):
        '''
        Execute.

        Execute the given parameter and return the execution result.
        '''
        result = ''
        try:
            binary_name = self.BINARY_NAME
            binary_prefix = os.path.join('openvpn', 'sbin')
            if version is not None:
                # openvpn/v1.2.3-patch/openvpn/sbin/openvpn
                binary_prefix = os.path.join(
                    'openvpn',
                    version,
                    'openvpn',
                    'sbin')

            self.process = process_dispatcher(
                binary_name, [parameter_name], None)
            self.process.set_prefix(binary_prefix)
            self.process.set_expected_error_code(expected_error_code)

            # Ensure that LD_LIBRARY_PATH is not exported to the
            # subprocess.
            environment_remove_ld_library = named_value_list_model()
            environment_remove_ld_library.add_value('LD_LIBRARY_PATH', None)
            self.process.set_environment(environment_remove_ld_library)

            result = self.process.start_subprocess_sync()
        except ProcessError as errors:
            logger.error(str(errors))
            raise
        return result

    def _get_executable_path(self, version):
        '''
        Get Executable Path.

        This string function returns the path of the executable in the
        filesystem. This duplicates the implementation in the process
        dispatcher for the purpose of displaying the information to the user.
        '''
        binary_prefix = os.path.join('openvpn', 'sbin')
        if version is not None:
            # openvpn/v1.2.3-patch/openvpn/sbin/openvpn
            binary_prefix = os.path.join(
                'openvpn',
                version,
                'openvpn',
                'sbin')

        location = location_factory(sys.platform)
        location.set_name(self.BINARY_NAME)
        search_paths = location.get_binary_search_paths()
        for path in search_paths:
            versions_path = os.path.join(path, self.BINARY_NAME)
            executable_path = os.path.join(path, location.get_binary_name())
            if not os.path.exists(versions_path):
                continue
            if version is None and os.path.isfile(executable_path):
                return versions_path
            versions = os.listdir(versions_path)
            for dir_version in versions:
                if dir_version != version:
                    continue
                executable_path = os.path.join(
                    versions_path,
                    dir_version,
                    'openvpn', 'sbin', location.get_binary_name())
                if not os.path.exists(executable_path):
                    continue
                return executable_path
        logger.warn('No executable path found for %s' % (str(version),))
        return ''

    def get_version_list(self):
        '''
        Get Version List.

        Create a list of available openvpn executables in the binary search
        paths.
        Only versions that contain a openvpn/sbin/openvpn file will be listed.
        '''
        version_list = version_list_model()
        location = location_factory(sys.platform)
        location.set_name(self.BINARY_NAME)
        search_paths = location.get_binary_search_paths()
        for path in search_paths:
            versions_path = os.path.join(path, self.BINARY_NAME)
            executable_path = os.path.join(path, location.get_binary_name())
            if not os.path.exists(versions_path):
                continue
            if os.path.isfile(executable_path):
                version_item = version_list.item_model_class()
                version_item.version.set(VERSION_SYSTEM_PROVIDED)
                version_list.append(version_item)
                continue
            versions = os.listdir(versions_path)
            for version in versions:
                executable_path = os.path.join(
                    versions_path,
                    version,
                    'openvpn', 'sbin', location.get_binary_name())
                if not os.path.exists(executable_path):
                    continue
                version_item = version_list.item_model_class()
                version_item.version.set(version)
                version_list.append(version_item)
        return version_list

    def get_version_detail(self, version):
        '''
        Get Version Detail.

        Request given openvpn version about its --version text
        '''
        version_detail = version_detail_model()
        version_detail.version.set(version)
        if version == VERSION_SYSTEM_PROVIDED:
            version = None
        detail = self._get_executable_path(version)
        detail += '\n'
        detail += self._execute(version, '--version', expected_error_code=1)
        version_detail.detail.set(detail)
        return version_detail

    def get_version_help(self, version):
        '''
        Get Version Help.

        Request given openvpn version about its --help and convert the output
        to a version_help_model.
        '''
        version_help = version_help_model()
        help_text = self._execute(version, '--help', expected_error_code=1)
        help_item = None
        help_item_text = ''
        for line in help_text.split('\n'):
            if line.startswith('--'):
                # commit last help item
                if help_item is not None:
                    help_item.help.set(help_item_text)
                    version_help.append(help_item)
                # start new help_item
                item_parts = line.split(':')
                if len(item_parts) is 0:
                    continue
                option_parts = item_parts[0].split(' ')
                if len(option_parts) is 0:
                    continue
                help_item = version_help.item_model_class()
                help_item_text = ''
                option = option_parts[0].strip().replace('--', '')
                del option_parts[0]
                del item_parts[0]
                help_item_text = ':'.join(item_parts).strip()
                parameter = ' '.join(option_parts).strip()

                help_item.option.set(option)
                help_item.parameter.set(parameter)
            elif line == '' and help_item is not None:
                # 'section' in help separating client/server/tun etc
                # commit the last item and wait for the next ^--
                help_item.help.set(help_item_text)
                version_help.append(help_item)
                help_item = None
            elif help_item is not None:
                # continuation of a help_item, strip any blanks and append
                # to the help text
                help_item_text += ' %s' % (line.strip(),)

        if help_item is not None:
            # commit the last help_item
            help_item.help.set(help_item_text)
            version_help.append(help_item)

        return version_help
