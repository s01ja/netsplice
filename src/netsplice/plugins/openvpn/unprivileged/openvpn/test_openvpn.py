# -*- coding: utf-8 -*-
# test_openvpn.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Tests for Unprivileged OpenVPN dispatcher.
'''

import mock
import os
import sys
from StringIO import StringIO
from openvpn import openvpn
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.model.configuration_list import (
    configuration_list as configuration_list_model
)
from netsplice.util.errors import (
    InvalidUsageError
)
from netsplice.util.process.errors import ProcessError


def get_instance():
    instance = openvpn()
    return instance


@mock.patch(
    'netsplice.util.process.dispatcher.dispatcher.start_subprocess_sync')
def test__execute_returns_process_result(mock_start_subprocess_sync):
    instance = get_instance()
    mock_start_subprocess_sync.return_value = 'process_output'
    result = instance._execute('version', 'parameter')
    mock_start_subprocess_sync.assert_called()
    assert(result == 'process_output')


@mock.patch(
    'netsplice.util.process.dispatcher.dispatcher.start_subprocess_sync')
@mock.patch('netsplice.util.process.dispatcher.dispatcher.set_prefix')
def test__execute_without_version_sets_correct_prefix(
        mock_set_prefix, mock_start_subprocess_sync):
    instance = get_instance()
    mock_start_subprocess_sync.return_value = 'process_output'
    result = instance._execute(version=None, parameter_name='parameter')
    mock_set_prefix.assert_called_with('openvpn/sbin')
    mock_start_subprocess_sync.assert_called()
    assert(result == 'process_output')


@mock.patch(
    'netsplice.util.process.dispatcher.dispatcher.start_subprocess_sync')
@mock.patch('netsplice.util.process.dispatcher.dispatcher.set_prefix')
def test__execute_with_version_sets_correct_prefix(
        mock_set_prefix, mock_start_subprocess_sync):
    instance = get_instance()
    mock_start_subprocess_sync.return_value = 'process_output'
    result = instance._execute(version='version', parameter_name='parameter')
    mock_set_prefix.assert_called_with('openvpn/version/openvpn/sbin')
    mock_start_subprocess_sync.assert_called()
    assert(result == 'process_output')


@mock.patch(
    'netsplice.util.process.dispatcher.dispatcher.start_subprocess_sync')
def test__execute_with_error_raises(mock_start_subprocess_sync):
    instance = get_instance()
    mock_start_subprocess_sync.side_effect = ProcessError('test')
    try:
        result = instance._execute(
            version='version', parameter_name='parameter')
        assert(False)  # expect ProcessError exception to raise.
    except ProcessError:
        assert(True)
    mock_start_subprocess_sync.assert_called()


@mock.patch(
    'netsplice.util.process.location.location.'
    'location.get_binary_search_paths')
def test_get_version_list_no_search_paths_empty_list(
        mock_get_binary_search_paths):
    instance = get_instance()
    mock_get_binary_search_paths.return_value = []
    result = instance.get_version_list()
    assert(result == [])


@mock.patch(
    'netsplice.util.process.location.location.'
    'location.get_binary_search_paths')
def test_get_version_list_nonexistent_search_paths_empty_list(
        mock_get_binary_search_paths):
    instance = get_instance()
    mock_get_binary_search_paths.return_value = ['/t/h/i/s/should/not/exist']
    result = instance.get_version_list()
    assert(result == [])


@mock.patch(
    'netsplice.util.process.location.location.'
    'location.get_binary_search_paths')
@mock.patch('os.path.isfile')
@mock.patch('os.path.exists')
def test_get_version_list_sys_executable_virtual_version(
        mock_exists, mock_is_file,
        mock_get_binary_search_paths):
    instance = get_instance()
    mock_get_binary_search_paths.return_value = ['/bin']
    instance.BINARY_NAME = 'true'
    mock_is_file.return_value = True
    mock_exists.return_value = True
    result = instance.get_version_list()
    assert(len(result) is 1)
    assert('system-provided' in result[0].version.get())


@mock.patch(
    'netsplice.util.process.location.location.'
    'location.get_binary_search_paths')
@mock.patch('os.path.isfile')
@mock.patch('os.listdir')
@mock.patch('os.path.exists')
def test_get_version_list_only_existing_executables_as_versions(
        mock_exists, mock_listdir, mock_is_file, mock_get_binary_search_paths):
    instance = get_instance()
    mock_get_binary_search_paths.return_value = ['/bin']
    instance.BINARY_NAME = 'true'

    mock_is_file.return_value = False
    mock_exists.return_value = True
    mock_listdir.return_value = ['v1', 'v2', 'v3']
    result = instance.get_version_list()
    print result.to_json()
    assert(len(result) is 3)


@mock.patch(
    'netsplice.plugins.openvpn.unprivileged.openvpn.openvpn.'
    'openvpn._execute')
def test_get_version_detail_returns_output(
        mock_execute):
    instance = get_instance()
    mock_execute.return_value = '--version output'
    result = instance.get_version_detail('v1')
    print result.to_json()
    assert('"detail": "\\n--version output"' in result.to_json())


@mock.patch(
    'netsplice.plugins.openvpn.unprivileged.openvpn.openvpn.'
    'openvpn._execute')
def test_get_version_help_parses_help_output(
        mock_execute):
    instance = get_instance()
    mock_execute.return_value = '--option parameter: help line1\nhelp line2'
    result = instance.get_version_help('v1')
    print result.to_json()
    assert('"option": "option"' in result.to_json())
    assert('"parameter": "parameter"' in result.to_json())
    assert('"help": "help line1 help line2"' in result.to_json())
