# -*- coding: utf-8 -*-
# ipc.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

# Buffer Size for receiving management messages
# Small buffer will cause the messages to be split into multiple
# recv calls and require the process_message to combine them correctly
# 1024: Default
# 16: Debug the process_message code
MAX_RECV = 1024

# Parameter to OpenVPN
# Cache the most recent n lines of log file history for usage by the management
# channel.
# 0: Default of OpenVPN may fill up the clients memory
# 100: should be enough for production.
MAX_MANAGEMENT_LOG_CACHE = 100

# Number of manageable connections. Should not be limited but based on the OS
# there are limits on the number of TCP-connections and or TUN devices.
MAX_PRIVILEGED_CONNECTIONS = 16

# Time to wait after disconnect before the connection is set to
# inactive. Usually the management interface shuts down before this
# but the timer avoids endless disconnects.
OPENVPN_DISCONNECT_WAIT = 1

# Frequency of openvpn status polling in seconds.
OPENVPN_STATUS_SLEEP = 0.5

# Frequency of hold release messages in seconds
# relevant for reconnect due to connectivity loss.
OPENVPN_HOLD_SLEEP = 1

# Frequency of syncing process model (stderr/stdout) with the actual model
OPENVPN_MODEL_SYNC = 0.1
