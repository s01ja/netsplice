# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2013 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for a reverse port forward.

'''
ACCOUNT_LABEL = 'Reverse Port Forward'

DESCRIPTION = '''
<p><b>Reverse Port Forward</b> Pass remote requests to a local system.</p>

<p>Specifies that connections to the given TCP port on the remote (server) host
are to be forwarded to the given host and port on the local side. This works by
allocating a socket to listen to a TCP port on the remote side. Whenever a
connection is made to this port, the connection is forwarded over the secure
channel, and a connection is made to <i>host</i> port <i>hostport</i> from the
local machine.  IPv6 addresses can be specified by enclosing the address in
square brackets.</p>

<p>Allows to access the service HOST:HOSTPORT (HOST is part of your local
 network) as if it was hosted on PORT on the REMOTE_HOST.</p>

<p><b>Warning</b> This exposes a local service to remote users. Make sure it is
secure to do so!</p>
'''

DEFAULT_CONFIG = '''
-R *:PORT:HOST:HOSTPORT
-C
-N
-T
-l USERNAME
host REMOTE_HOST
'''
