# -*- coding: utf-8 -*-
# plugin_collection.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.config import backend as config_backend
from netsplice.util import get_logger
from netsplice.util.model.field import field

from netsplice.model.plugin_collection_item import (
    plugin_collection_item as plugin_collection_item_model
)
from netsplice.model.validator.max import max as max_validator
from netsplice.model.validator.min import min as min_validator
from netsplice.model.validator.none import none as none_validator
from netsplice.model.validator.ip_address import (
    ip_address as ip_address_validator
)
from netsplice.model.validator.hostname import (
    hostname as hostname_validator
)
from netsplice.plugins.ping.config import backend as config

logger = get_logger()


class plugin_collection(plugin_collection_item_model):
    def __init__(self, owner=None):
        plugin_collection_item_model.__init__(self, owner)

        self.period = field(
            default=0,
            validators=[min_validator(0), max_validator(999)])

        self.remote = field(
            default=0,
            validators=[
                none_validator(
                    exp_or=[
                        ip_address_validator(),
                        hostname_validator()
                    ]
                )
            ])
