# -*- coding: utf-8 -*-
# event_plugin.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event plugin for backend app.

'''

from tornado import gen

import netsplice.backend.event.types as event_types
from netsplice.config import connection as config_connection
from netsplice.util import get_logger
from netsplice.util.errors import NotFoundError
from netsplice.util.model.errors import ValidationError

logger = get_logger()


class event_plugin(object):
    '''
    Event loop for the backend.

    Requires modules registered and waits for model-change on that modules
    to process events.
    '''

    def __init__(self, application):
        '''
        Initialize members.

        Requires the user to set application and add modules.
        '''
        self.application = application
        self._active_connections = list()

    @gen.coroutine
    def _start(self, connection_id):
        '''
        Start.

        Start the ping. Get the account config and request the network backend
        to start the ping with the values. For ENV: remotes the environment of
        the connection is evaluated.
        '''
        if connection_id in self._active_connections:
            return

        connection_module = self.application.get_module('connection')

        broker = connection_module.broker
        connection = broker.find_connection_by_connection_id(connection_id)

        remote = connection.account.ping.remote.get()
        period = connection.account.ping.period.get()
        if remote is None:
            return
        if period is 0:
            return
        if remote.startswith('ENV:'):
            env_name = remote[4:]
            try:
                remote = connection.model.environment.get_value(env_name)
            except NotFoundError:
                logger.error(
                    'The ping ENV: value was not found.'
                    ' Available values are: %s'
                    % (connection.model.environment.to_json(),))
                raise
        try:
            yield self.application.get_network().ping.start(
                connection_id, remote, period)
            self._active_connections.append(connection_id)
        except ValidationError as errors:
            logger.error(
                'The options (remote: "{remote}", period: {period}) for the'
                ' network backend are incorrect. Review your settings. No'
                ' ping was started. The error was: {error}'.format(
                    remote=remote,
                    period=period,
                    error=str(errors)))

    @gen.coroutine
    def _stop(self, connection_id):
        '''
        Stop.

        Stop the ping for the given connection_id if it exists.
        '''
        if connection_id not in self._active_connections:
            return
        self._active_connections.remove(connection_id)
        yield self.application.get_network().ping.stop(connection_id)

    @gen.coroutine
    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()
        event_data = event_model_instance.data.get()

        if event_type == event_types.NOTIFY:
            if event_name in config_connection.UP:
                yield self._start(event_data)
            if event_name in config_connection.DOWN:
                yield self._stop(event_data)
