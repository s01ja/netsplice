# -*- coding: utf-8 -*-
# module_controller.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import (NotUniqueError, NotFoundError)
from .model.request.setup import setup as setup_model
from .ping import ping as ping_process


class module_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def post(self):
        '''
        Create a connection ping.

        Create a connection object in the ping module and start the ping
        process. When a connection with the given id already exists, a 403 is
        returned.
        request model: {period, remote, connection_id}
        '''
        request_model = setup_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            model = self.application.get_module('ping').model
            connections = model.connections
            try:
                connections.find_by_id(request_model.connection_id.get())
                raise NotUniqueError(
                    '%s already has a active ping'
                    % (request_model.connection_id.get(),))
            except NotFoundError:
                pass
            if request_model.remote.get() is None:
                raise ValidationError('None is not a valid remote ip address.')
            connections.add_connection(
                request_model.connection_id.get(),
                request_model.remote.get(),
                request_model.period.get())
            connection = connections.find_by_id(
                request_model.connection_id.get())
            connection.process = ping_process(connection)
            connection.process.start()
            self.set_status(201)
        except ValidationError as errors:
            self.set_error_code(2337, errors)
            self.set_status(400)
        except NotUniqueError as errors:
            self.set_error_code(2338, errors)
            self.set_status(403)
        self.finish()
