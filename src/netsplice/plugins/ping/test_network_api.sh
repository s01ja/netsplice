#!/bin/bash
#
# Test network api
# execute curl commands on the backend api and evaluates its output. Used
# for quick integration test during development.
# set -x
echo 'this assumes that you disabled HTTPS and HMAC authentication'
echo 'and the network_backend is running on port 10000 on localhost'
if -z ${PORT}; then
    PORT=10000
fi

function die(){
    echo "$@"
    section "FAILURE"
    exit 1
}
function section(){
    printf '\n'
    printf -v _hr "%*s" $(tput cols) && echo ${_hr// /-}
    printf '[ %s ]\n' "$@"
}
function tcurl(){
    code=$1
    shift
    content_filter=$1
    shift
    method=$1
    if [ ${method} == "GET" ]; then
        method=""
    else
        method="-X ${method}"
    fi
    shift
    url=$1
    section "curl: ${code} ${url}"
    shift
    if [ -z "$1" ]; then
        data=""
    else
        data="--data '"$@"'"
    fi
    export tcurl_result=/tmp/test.result
    export tcurl_script=/tmp/test.script
    echo "curl -sS -v ${url} ${method} ${data} > \$1 2> \$2" > ${tcurl_script}
    bash ${tcurl_script} ${tcurl_result} ${tcurl_result}.2

    rescode=$(cat ${tcurl_result}.2 | grep "HTTP/1.1 "| sed "s|< HTTP/1.1 \([0-9]*\).*|\1|")
    cat ${tcurl_result}.2 | grep "HTTP/1.1 ${code}" > /dev/null \
      && echo "[OK: code: ${code}]" \
      || die "bad response code ${code} vs ${rescode} for ${method} ${url}"

    result_output=$(cat ${tcurl_result})
    if [ "${result_output}" == "" ]; then
      if [ "${content_filter}" == "" ]; then
        echo "[OK: content: '${content_filter}']"
      else
        die "bad response, expecting '${content_filter}' in '${result_output}'"
      fi
    else
      cat ${tcurl_result} | grep "${content_filter}" > /dev/null \
        && echo "[OK: content: '${content_filter}']" \
        || die "bad response, expecting '${content_filter}' in '${result_output}'"
    fi

    export TCURL_RESULT=${result}
}

# preparation:
# * remove preferences

# tor integration
function test_ping(){
    section "module"
    CONNECTION_ID="00000000-0000-0000-0000-000000000001"
    BAD_CONNECTION_ID="00000000-0000-0000-0000-000000000002"
    tcurl 201 "" POST http://localhost:${PORT}/module/plugin/ping/ \
        "{ \
        \"connection_id\": \"${CONNECTION_ID}\", \
        \"frequency\":3, \
        \"remote\": \"8.8.8.8\"
        }"
    # 403: already exists
    tcurl 403 "" POST http://localhost:${PORT}/module/plugin/ping/ \
        "{ \
        \"connection_id\": \"${CONNECTION_ID}\", \
        \"frequency\":3, \
        \"remote\": \"8.8.8.8\"
        }"
    tcurl 200 "" GET http://localhost:${PORT}/module/plugin/ping/connections/${CONNECTION_ID}
    tcurl 404 "" GET http://localhost:${PORT}/module/plugin/ping/connections/${BAD_CONNECTION_ID}
    tcurl 201 "" DELETE http://localhost:${PORT}/module/plugin/ping/connections/${CONNECTION_ID}
    tcurl 404 "" DELETE http://localhost:${PORT}/module/plugin/ping/connections/${BAD_CONNECTION_ID}
}


echo "Hallo developer,
This script creates and deletes pings in the network backend, it does not
modify any configuration and is therefore save to run.
"
test_ping
