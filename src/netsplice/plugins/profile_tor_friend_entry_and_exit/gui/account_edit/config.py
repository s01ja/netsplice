# -*- coding: utf-8 -*-
# config.py
# Copyright (C) 2013 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

ACCOUNT_LABEL = 'Friends'

TRUSTED_NODES = [
    '$1685874B7B91E5C6184CF7CC5ADF8D983F150AD5',
    '$BC630CBBB518BE7E9F4E09712AB0269E9DC7D626',
    '$131B60B9AFE6AEA60042132D648798534ABEA07E',
    '$7661E748639A0A31FFC49380AC19BFD53AD2A8AE'
]

DESCRIPTION = '''
<p>
  Only use a limited set of entry and exit nodes.
</p>
<b>BEWARE</b> using a limited set of Exit and Entry nodes can degrade privacy.
'''

DEFAULT_CONFIG = ('''
SOCKSPort localhost:9055
ExitNodes {exit_nodes}
EntryNodes {entry_nodes}
StrictNodes 1
''').format(
    exit_nodes=','.join(TRUSTED_NODES),
    entry_nodes=','.join(TRUSTED_NODES))
