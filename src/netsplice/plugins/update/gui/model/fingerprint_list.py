# -*- coding: utf-8 -*-
# fingerprint_list.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a list of sha256 fingerprints.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from .fingerprint_item import fingerprint_item as fingerprint_item_model


class fingerprint_list(marshalable_list):

    def __init__(self):
        marshalable_list.__init__(self, fingerprint_item_model)

    def get_list(self):
        '''
        Get List.

        Return a string list from the fingerprints.
        '''
        fingerprints = []
        for fingerprint in self:
            fingerprints.append(fingerprint.fingerprint.get())

    def set_list(self, fingerprint_string_list):
        '''
        Set List.

        Fill the list with values from the given string_list

        Arguments:
            fingerprint_string_list (list): Fingerprints to be added.
        '''
        for fingerprint in fingerprint_string_list:
            new_item = self.item_model_class()
            new_item.fingerprint.set(fingerprint)
            self.append(new_item)
