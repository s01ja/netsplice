# -*- coding: utf-8 -*-
# url_list.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a list of urls.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.util.errors import NotFoundError
from .url_item import url_item as url_item_model


class url_list(marshalable_list):

    def __init__(self):
        marshalable_list.__init__(self, url_item_model)

    def find_by_os(self, os):
        '''
        Find By OS.

        Return the url_item with the os matching the given os.
        '''
        for url in self:
            if url.os.get() == os:
                return url
        raise NotFoundError('No url for OS %s found.')
