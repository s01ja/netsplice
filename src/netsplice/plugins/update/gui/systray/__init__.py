# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.gui import register_systray_menu_item
from netsplice.plugins.update.gui.update.update import update as update_dialog
from netsplice.util.errors import NotFoundError


def show_update_dialog(application):
    '''
    Show Update Dialog.

    Show the dialog for updating the application.
    '''
    mainwindow = application.get_module('mainwindow').get_window()
    try:
        mainwindow.get_dialog('update').show()
    except NotFoundError:
        mainwindow.add_dialog(
            'update', update_dialog(mainwindow, application.get_dispatcher()))
        mainwindow.get_dialog('update').show()


def register(app):
    '''
    Register.

    Register the a menu item.
    '''
    register_systray_menu_item(
        name='check_updates',
        label='Check for Updates',
        callback=lambda: show_update_dialog(app))
