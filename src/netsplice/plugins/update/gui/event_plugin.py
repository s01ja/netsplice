# -*- coding: utf-8 -*-
# event_plugin.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event plugin for gui app.

'''

import netsplice.backend.event.types as event_types
from netsplice.plugins.update.config import events as update_event_names
from netsplice.plugins.update.config.gui import (
    MESSAGE_NEW_VERSION, MESSAGE_AGE
)
from netsplice.util import get_logger

logger = get_logger()


class event_plugin(object):
    '''
    Event loop for the gui.

    Requires modules registered and waits for model-change on that modules
    to process events.
    '''

    def __init__(self, application):
        '''
        Initialize members.

        Requires the user to set application and add modules.
        '''
        self.application = application

    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()
        event_data = event_model_instance.data.get()

        if event_type == event_types.NOTIFY:
            if event_name == update_event_names.UPDATE_NOTIFY_VERSION:
                mainwindow = self.application.get_module('mainwindow')
                systray = mainwindow.window._systray
                systray.show_named_message(
                    MESSAGE_NEW_VERSION, {'NEW_VERSION': str(event_data)})
            if event_name == update_event_names.UPDATE_NOTIFY_AGE:
                mainwindow = self.application.get_module('mainwindow')
                systray = mainwindow.window._systray
                systray.show_named_message(
                    MESSAGE_AGE, {'AGE': str(event_data)})
