# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.ipc.route import get_module_route

name = "plugin_update_gui"

endpoints = get_module_route(
    'netsplice.plugins.update.backend.gui',
    [
        (r'/module/plugin/update/check_now', 'check_now'),
        (r'/module/plugin/update/version_info', 'version_info'),
        (r'/module/plugin/update/download', 'download'),
        (r'/module/plugin/update/download/(?P<download_id>[^\/]+)',
            'download')
    ])
