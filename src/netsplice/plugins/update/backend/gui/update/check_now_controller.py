# -*- coding: utf-8 -*-
# check_now_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from tornado import gen, ioloop, httpclient

from netsplice import __version__ as VERSION
from netsplice.plugins.update.backend.gui.update.model.response.\
    version_detail import version_detail as version_detail_response_model

from netsplice.util.errors import NotFoundError
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.ipc.errors import ServerConnectionError


class check_now_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    @gen.coroutine
    def post(self):
        '''
        Trigger event in backend.

        .. :code-block:: none

            request model: {}
            response_model: {
                "last": unix_timestamp_int,
                "lastest_known_version": "version.string",
                "changelog": "changelog\nwith\nnewlines",
                "update_url_list": [
                    {
                        "os": "os_name",
                        "signature": "base64 encoded openssl signature",
                        "sha1sum": "sha1 checksum",
                        "sha256sum": "sha256 checksum",
                        "url": "url for the remote file",
                    }
                ]
            }
        '''
        try:
            latest = yield self.application.get_network().update.latest()
            preferences_model = self.application.get_module(
                'preferences').model
            model = preferences_model.plugins.find_by_name('update')
            model.last.set(int(ioloop.IOLoop.current().time()))
            model.latest_known_version.set(latest.version.get())
            model.changelog.set(latest.changelog.get())
            model.update_url_list.from_json(latest.update_url_list.to_json())
            model.commit()

            response_model = version_detail_response_model()
            response_model.version.set(VERSION)
            preferences_model = self.application.get_module(
                'preferences').model
            model = preferences_model.plugins.find_by_name('update')
            response_model.latest_known_version.set(
                model.latest_known_version.get())
            response_model.last.set(model.last.get())
            response_model.changelog.set(latest.changelog.get())
            response_model.update_url_list.from_json(
                latest.update_url_list.to_json())
            self.write(response_model.to_json())
        except ValidationError as errors:
            self.set_error_code(2300, str(errors))
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2301, str(errors))
            self.set_status(404)
        except ServerConnectionError as errors:
            self.set_error_code(2302, str(errors))
            self.set_status(502)
        except httpclient.HTTPError as errors:
            self.set_error_code(2303, str(errors))
            if errors.response is not None:
                headers = errors.response.headers.get_all()
                message = ''
                for (header_name, header_value) in sorted(headers):
                    if header_name != 'X-Errordetail':
                        continue
                    message += header_value
                message = message.replace('\\n', '\n')
                self.set_header('X-Errordetail', message)
            else:
                self.set_header('X-Errordetail', str(errors))
            self.set_status(502)
        self.finish()
