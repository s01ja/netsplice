# -*- coding: utf-8 -*-
# version_info_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from tornado import gen

from netsplice import __version__ as VERSION
from netsplice.plugins.update.backend.gui.update.model.response.\
    version_detail import version_detail as version_detail_response_model
from netsplice.util.errors import NotFoundError
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util import get_logger

logger = get_logger()
MSG_CHANGELOG = 'Check for updates to read changelog.'


class version_info_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    @gen.coroutine
    def get(self):
        '''
        The info of the currently known latest version without a network
        request.

        .. :code-block:: none

            request model: {}
            response_model: {
                "last": unix_timestamp_int,
                "lastest_known_version": "version.string",
                "changelog": "changelog\nwith\nnewlines",
                "update_url_list": [
                    {
                        "os": "os_name",
                        "signature": "base64 encoded openssl signature",
                        "sha1sum": "sha1 checksum",
                        "sha256sum": "sha256 checksum",
                        "url": "url for the remote file",
                    }
                ]
            }
        '''
        try:
            response_model = version_detail_response_model()
            response_model.version.set(VERSION)
            preferences_model = self.application.get_module(
                'preferences').model
            model = preferences_model.plugins.find_by_name('update')
            response_model.latest_known_version.set(
                model.latest_known_version.get())
            response_model.last.set(model.last.get())
            response_model.changelog.set(model.changelog.get())
            response_model.update_url_list.from_json(
                model.update_url_list.to_json())
            self.write(response_model.to_json())
        except ValidationError as errors:
            self.set_error_code(2312, str(errors))
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2313, str(errors))
            self.set_status(404)
        self.finish()
