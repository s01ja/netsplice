# -*- coding: utf-8 -*-
# update.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for requesting updates.
'''

from netsplice.util.model.marshalable import marshalable
from netsplice.util.model.field import field
from netsplice.model.validator.version import version as version_validator
from netsplice.model.validator.plaintext import (
    plaintext as plaintext_validator
)
from netsplice.model.validator.date import (
    date as date_validator
)
from netsplice.model.validator.none import (
    none as none_validator
)

from .url_list import url_list


class update(marshalable):
    '''
    Marshalable model that contains a response from the remote update.json
    '''
    def __init__(self):
        marshalable.__init__(self)

        self.version = field(
            required=True,
            validators=[version_validator()])

        self.date = field(
            required=True,
            validators=[date_validator()])

        self.changelog = field(
            required=True,
            validators=[none_validator(exp_or=[plaintext_validator()])])

        self.update_url_list = url_list()
