# -*- coding: utf-8 -*-
# log_item.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a url.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.uri import (
    uri as uri_validator
)
from netsplice.model.validator.signature import (
    signature as signature_validator
)
from netsplice.model.validator.sha1sum import (
    sha1sum as sha1sum_validator
)
from netsplice.model.validator.sha256sum import (
    sha256sum as sha256sum_validator
)


class url_item(marshalable):

    def __init__(self, log_instance=None):
        marshalable.__init__(self)
        self.url = field(
            required=True,
            validators=[uri_validator()])

        self.os = field(
            required=True,
            validators=[])

        self.signature = field(
            required=True,
            validators=[signature_validator()])

        self.sha1sum = field(
            required=True,
            validators=[sha1sum_validator()])

        self.sha256sum = field(
            required=True,
            validators=[sha256sum_validator()])
