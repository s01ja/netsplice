# -*- coding: utf-8 -*-
# update.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for requesting updates.
'''

from netsplice.util.model.marshalable import marshalable
from netsplice.util.model.field import field
from netsplice.model.validator.uri import uri as uri_validator
from netsplice.model.validator.none import (
    none as none_validator
)
from netsplice.model.validator.sign_key import (
    sign_key as sign_key_validator
)
from .fingerprint_list import fingerprint_list


class update(marshalable):
    '''
    Marshalable model that contains the url to request for updates.
    '''
    def __init__(self):
        marshalable.__init__(self)

        self.update_url = field(
            required=True,
            validators=[uri_validator()])

        self.update_signature_url = field(
            required=True,
            validators=[uri_validator()])

        self.fingerprints = fingerprint_list()

        self.sign_key = field(
            required=False,
            validators=[none_validator(exp_or=[sign_key_validator()])])
