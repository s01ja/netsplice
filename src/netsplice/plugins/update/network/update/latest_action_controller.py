# -*- coding: utf-8 -*-
# latest_action_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from tornado import gen
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import (NotFoundError)
from netsplice.util.ipc.errors import ServerConnectionError
from .update import update
from .model.response.update import (
    update as update_response_model
)
from .model.request.update import (
    update as update_request_model
)


class latest_action_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)
        self.update = update(self.application)

    @gen.coroutine
    def post(self):
        '''
        The "check" action requests a remote service about the current
        available version.
        '''
        try:
            request_model = update_request_model()
            request_model.from_json(self.request.body.decode('utf-8'))
            response_model = update_response_model()
            latest = yield self.update.latest(
                request_model.update_url.get(),
                request_model.update_signature_url.get(),
                request_model.fingerprints.get_list(),
                request_model.sign_key.get())
            response_model.from_json(latest.to_json())
            self.write(response_model.to_json())
        except ValidationError as errors:
            self.set_error_code(2314, str(errors))
            self.set_status(400)
        except ValueError as errors:
            self.set_error_code(2315, str(errors))
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2316, str(errors))
            self.set_status(404)
        except ServerConnectionError as errors:
            self.set_error_code(2317, str(errors))
            self.set_status(502)
        self.finish()
