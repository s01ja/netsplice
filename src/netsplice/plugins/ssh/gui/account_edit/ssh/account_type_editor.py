# -*- coding: utf-8 -*-
# account_type_editor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.

Edit account settings for Ssh connections.
'''

import re
import textwrap
from PySide.QtGui import QWidget, QFileDialog
from PySide.QtCore import Qt, Signal
from netsplice.plugins.ssh.config import ACCOUNT_TYPE
from netsplice.plugins.ssh.config.gui import DEFAULT_ACCOUNT
from netsplice.plugins.ssh.config import ssh as config_ssh
from netsplice.gui.key_value_editor import key_value_editor as key_value_editor
from netsplice.gui.account_edit.abstract_account_type_editor import (
    abstract_account_type_editor
)
from .account_type_editor_ui import Ui_AccountTypeEditor

from netsplice.util.parser import factory as get_config_parser
from netsplice.util.model.errors import ValidationError
from netsplice.util import get_logger

logger = get_logger()

STACK_ADVANCED = 0
STACK_EDITOR_KEY_VALUE = 0
STACK_EDITOR_PLAINTEXT = 1
STACK_EDITOR_PLAINTEXT_IMPORT = 2
STACK_HINT_ENABLED = 0
STACK_HINT_DISABLED = 1


class account_type_editor(QWidget, abstract_account_type_editor):
    '''
    Ssh Account Type Editor.

    Custom Editor with syntax highlighter and keyword tooltip.
    '''

    config_changed = Signal()

    def __init__(self, parent=None, dispatcher=None):
        '''
        Initialize Module.

        Initialize widget and abstract base, Setup UI.
        '''
        QWidget.__init__(self, parent)
        abstract_account_type_editor.__init__(self, ACCOUNT_TYPE, dispatcher)
        self.ui = Ui_AccountTypeEditor()
        self.ui.setupUi(self)
        self.ui.editor_layout.setContentsMargins(0, 10, 0, 0)
        self.ui.advanced_layout.setContentsMargins(0, 0, 0, 0)
        self.ui.advanced_button_layout.setContentsMargins(10, 0, 0, 0)
        self.setFocusPolicy(Qt.TabFocus)
        self.model = None
        self.editor_model = None
        self.DEFAULT_ACCOUNT = DEFAULT_ACCOUNT
        self.ui.plain_user_config_edit.setup_keywords(
            self._get_keywords())
        self.ui.plain_user_config_edit.setup_blocks(
            self._get_blocks())
        self.ui.plain_user_config_edit.syntax_highlighter.add_keyword(
            'default', 'comment', '#.*$')
        self.ui.plain_user_config_edit.syntax_highlighter.add_keyword(
            'default', 'comment', ';.*$')
        self.ui.plain_user_config_import_edit.setup_keywords(
            self._get_keywords())
        self.ui.plain_user_config_import_edit.setup_blocks(
            self._get_blocks())
        self.ui.plain_user_config_import_edit.syntax_highlighter.add_keyword(
            'default', 'comment', '#.*$')
        self.ui.plain_user_config_import_edit.syntax_highlighter.add_keyword(
            'default', 'comment', ';.*$')
        self.ui.button_key_value_editor.clicked.connect(self._key_value_editor)
        self.ui.button_plaintext_editor.clicked.connect(self._plaintext_editor)
        self.ui.button_plaintext_import_editor.clicked.connect(
            self._plaintext_import_editor)
        self.ui.button_reset.clicked.connect(self._reset)

        self.ui.plain_user_config_edit.textChanged.connect(
            self._config_changed)
        self.ui.plain_user_config_import_edit.textChanged.connect(
            self._config_changed)
        self.ui.key_value_user_config_edit.values_changed.connect(
            self._key_value_changed)

        self.ui.key_value_user_config_edit.set_key_documentation(
            self._get_keywords())
        self.ui.key_value_user_config_edit.set_value_documentation(
            self._get_keywords())
        self.ui.key_value_user_config_edit.set_header([
            self.tr('Option'),
            self.tr('Status'),
            self.tr('Type'),
            self.tr('Value')
        ])
        self.ui.key_value_user_config_edit.set_value_column(3)
        self.ui.key_value_user_config_edit.set_status_column(1, {
            'default': self.tr('Imported'),
            'changed': self.tr('User Set'),
            'removed': self.tr('Removed'),
            'inserted': self.tr('Inserted')})
        ui_config = get_config_parser(self.type)
        self.ui.key_value_user_config_edit.set_insert_template(
            ui_config.get_insert_template())
        toolbar_flags = key_value_editor.INSERT | key_value_editor.REMOVE | \
            key_value_editor.RESTORE | key_value_editor.FILTER
        self.ui.key_value_user_config_edit.enable_toolbar(toolbar_flags)
        self._key_value_editor()

    def _config_changed(self):
        '''
        Config Changed.

        Emit signal for owning widget that the config has changed.
        '''
        self.config_changed.emit()
        configuration = self._get_configuration()
        import_configuration = self._get_import_configuration()
        if configuration == import_configuration:
            self.ui.button_reset.setEnabled(False)
        else:
            self.ui.button_reset.setEnabled(True)
        try:
            if self.editor_model is not None:
                self.editor_model.configuration.set(configuration)
                self.editor_model.import_configuration.set(
                    import_configuration)
        except ValidationError:
            pass

    def _fix_restore_block_key(self, config_values):
        '''
        Fix Blocks.

        Uses the <.*> pattern to find blocks and correct the key accordingly.
        '''
        for item in config_values:
            key_name = item.key.get()
            key_match = re.match('^<([^>]*)>$', key_name)
            if key_match:
                item.key.set(key_match.group(1))
                item.values.find_by_name('block').value.set(True)
            else:
                item.values.find_by_name('block').value.set(False)
        return config_values

    def _fix_block_key(self, config_values):
        '''
        Fix Block Key.

        Wrap keys in <> when the block value is set True.
        '''
        for item in config_values:
            if item.values.find_by_name('block').value.get():
                item.key.set('<%s>' % (item.key.get(),))
        return config_values

    def _get_blocks(self):
        '''
        Get Blocks.

        Get Keywords that qualify for blocks.
        '''
        blocks = []
        for keyword, definition in config_ssh.KEYWORDS.items():
            if definition['block']:
                blocks.append(keyword)
        return blocks

    def _get_configuration(self):
        '''
        Get Configuration.

        Returns the string for the current visible configuration.
        '''
        plain_edit_active = self.ui.plain_user_config_edit.isVisible()
        plain_edit_import_active = \
            self.ui.plain_user_config_import_edit.isVisible()
        ui_config = get_config_parser(self.type)

        if plain_edit_active or plain_edit_import_active:
            ui_config.set_text(self.ui.plain_user_config_edit.toPlainText())
        else:
            ui_config.set_config(self._fix_restore_block_key(
                self.ui.key_value_user_config_edit.get_values()))
        return ui_config.get_text()

    def _get_import_configuration(self):
        '''
        Get Import Configuration.

        Returns the string for the current active import configuration.
        '''
        ui_config = get_config_parser(self.type)

        ui_config.set_text(self.ui.plain_user_config_import_edit.toPlainText())
        return ui_config.get_text()

    def _get_keywords(self):
        '''
        Get Keywords.

        Get Keyword list with documentation string.
        '''
        keywords = {}
        for keyword, definition in config_ssh.KEYWORDS.items():
            help_text = self.tr(definition['help'])
            wrapped_text = '\n'.join(textwrap.wrap(help_text))
            keywords[keyword] = wrapped_text
            if definition['block']:
                keywords['<%s>' % (keyword,)] = wrapped_text
        return keywords

    def _key_value_changed(self):
        '''
        Value in key_value_editor changed.

        Process to plaintext.
        '''
        ui_config = get_config_parser(self.type)
        editable_values = self._fix_restore_block_key(
            self.ui.key_value_user_config_edit.get_values())
        ui_config.set_config(editable_values)
        self.ui.plain_user_config_edit.setText(
            ui_config.get_text())
        self._config_changed()

    def _key_value_editor(self):
        '''
        Display Key Value Editor.

        Display the key-value editor in the advanced stack. Set the config to
        the associated controls.
        '''
        self.ui.editor_stack.setCurrentIndex(STACK_EDITOR_KEY_VALUE)
        ui_import_config = get_config_parser(self.type)
        ui_import_config.set_text(
            self.ui.plain_user_config_import_edit.toPlainText())
        ui_config = get_config_parser(self.type)
        ui_config.set_text(self.ui.plain_user_config_edit.toPlainText())
        user_config = self._fix_block_key(ui_config.get_config())
        import_config = self._fix_block_key(ui_import_config.get_config())
        self.ui.key_value_user_config_edit.set_values(
            user_config)
        self.ui.key_value_user_config_edit.set_default_values(
            import_config)
        # TODO: override config
        # self.ui.key_value_user_config_edit.set_override_values(
        #     ui_import_config.get_config())

        self.ui.button_key_value_editor.hide()
        self.ui.button_plaintext_editor.show()
        self.ui.button_plaintext_import_editor.hide()
        self.ui.button_reset.show()

    def _import_config(self, config_text):
        '''
        Set Config Text.

        Set the configuration ui elements from the given config_text.
        '''
        if self.editor_model:
            self.editor_model.import_configuration.set(config_text)
            self.editor_model.configuration.set(config_text)
        self.ui.plain_user_config_edit.setPlainText(config_text)
        self.ui.plain_user_config_import_edit.setPlainText(config_text)
        ui_config = get_config_parser(self.type)
        ui_config.set_text(config_text)
        import_config = self._fix_block_key(ui_config.get_config())
        self.ui.key_value_user_config_edit.set_values(
            import_config)
        self.ui.key_value_user_config_edit.set_default_values(
            import_config)
        self._config_changed()

    def _plaintext_editor(self):
        '''
        Plaintext Editor.

        Raise stack with the plaintext editor in the advanced stack.
        '''
        self.ui.editor_stack.setCurrentIndex(STACK_EDITOR_PLAINTEXT)
        self.ui.button_key_value_editor.show()
        self.ui.button_plaintext_editor.hide()
        self.ui.button_plaintext_import_editor.show()
        self.ui.button_reset.show()

    def _plaintext_import_editor(self):
        '''
        Plaintext Import Editor.

        Raise stack with the plaintext editor for the imported config
        in the advanced stack.
        '''
        self.ui.editor_stack.setCurrentIndex(STACK_EDITOR_PLAINTEXT_IMPORT)
        self.ui.button_key_value_editor.show()
        self.ui.button_plaintext_editor.show()
        self.ui.button_plaintext_import_editor.hide()
        self.ui.button_reset.hide()

    def _reset(self):
        '''
        Reset.

        Reset all values to the imported configuration.
        '''
        import_configuration = self._get_import_configuration()
        self._import_config(import_configuration)

    def can_check(self):
        '''
        Evaluate form completeness.

        Checks values in widget are complete to check the account.
        '''
        return self.ui.plain_user_config_edit.toPlainText() != ''

    def can_commit(self):
        '''
        Evaluate form completeness for create or update.

        Checks values in widget are complete to create or update the account.
        '''
        return self.ui.plain_user_config_edit.toPlainText() != ''

    def check(self, check_started_signal, check_failed_signal):
        '''
        Check account values.

        Check the type values for errors.
        '''
        configuration = self._get_configuration()
        self.backend.account_check.emit(
            self.type,
            configuration,
            check_started_signal,
            check_failed_signal)

    def create(
            self, account_name, enabled, default_route, autostart, group_id,
            create_done_signal, create_failed_signal):
        '''
        Create a new account.

        Create a new account in the given group with the given name and the
        type values.
        '''
        configuration = self._get_configuration()
        import_configuration = self._get_import_configuration()

        self.backend.account_create.emit(
            account_name,
            self.type,
            configuration,
            import_configuration,
            enabled,
            default_route,
            autostart,
            create_done_signal,
            create_failed_signal)

    def apply(
            self, account_id, account_name, enabled, default_route, autostart,
            update_done_signal, update_failed_signal):
        '''
        Update existing account.

        Update a existing account with the configured values.
        '''
        configuration = self._get_configuration()
        import_configuration = self._get_import_configuration()
        self.backend.account_update.emit(
            account_id,
            account_name,
            self.type,
            configuration,
            import_configuration,
            enabled,
            default_route,
            autostart,
            update_done_signal,
            update_failed_signal)
        self.model.configuration.set(configuration)
        self.model.import_configuration.set(import_configuration)

    def config_values_changed(self):
        '''
        Config Values Changed.

        Evaluate if the config is different from the loaded values.
        '''
        if self.model is None:
            return True
        changed = False
        configuration = self._get_configuration()
        import_configuration = self._get_import_configuration()
        changed |= self.model.configuration.get() != configuration
        changed |= (
            self.model.import_configuration.get() != import_configuration)
        return changed

    def is_account_profile(self, profile):
        '''
        Is Account Profile.

        Return True when the current (import) configuration matches the
        profile default.
        '''
        profile_config_text = profile['default']
        if profile_config_text is None:
            profile_config_text = self.DEFAULT_ACCOUNT
        profile_config = get_config_parser(self.type)

        profile_config.set_text(profile_config_text)

        import_configuration = self._get_import_configuration()
        profile_config_text = profile_config.get_text()

        return import_configuration == profile_config_text

    def reset_form(self):
        '''
        Reset Form.

        Reset Form values.
        '''
        self.ui.plain_user_config_edit.setPlainText(self.DEFAULT_ACCOUNT)
        self.ui.plain_user_config_import_edit.setPlainText(
            self.DEFAULT_ACCOUNT)

        ui_config = get_config_parser(self.type)
        ui_config.set_text(self.DEFAULT_ACCOUNT)
        default_config = self._fix_block_key(ui_config.get_config())
        self.ui.key_value_user_config_edit.set_values(
            default_config)
        self.ui.key_value_user_config_edit.set_default_values(
            default_config)
        self._config_changed()

    def set_account_profile(self, profile):
        '''
        Set Profile.

        Set the config values from the profile values.
        '''
        default = profile['default']
        if default is None:
            default = self.DEFAULT_ACCOUNT
        self.ui.plain_user_config_edit.setPlainText(default)
        self.ui.plain_user_config_import_edit.setPlainText(default)

        ui_config = get_config_parser(self.type)
        ui_config.set_text(default)
        default_config = self._fix_block_key(ui_config.get_config())
        self.ui.key_value_user_config_edit.set_values(
            default_config)
        self.ui.key_value_user_config_edit.set_default_values(
            default_config)
        self._config_changed()

    def set_model(self, account_model):
        '''
        Set model.

        Set model from given account model and update the ui elements.
        '''
        if account_model is None:
            self.reset_form()
            return
        try:
            self.editor_model = type(account_model)()
            self.model = type(account_model)()
            self.editor_model.configuration.set(
                account_model.configuration.get())
            self.editor_model.import_configuration.set(
                account_model.import_configuration.get())
            self.model.configuration.set(
                account_model.configuration.get())
            self.model.import_configuration.set(
                account_model.import_configuration.get())
        except ValidationError:
            self.reset_form()
            return

        configuration = self.editor_model.configuration.get()
        import_configuration = self.editor_model.import_configuration.get()

        if configuration is None:
            configuration = ''
        if import_configuration is None:
            import_configuration = ''

        self.ui.plain_user_config_edit.setPlainText(
            configuration)
        self.ui.plain_user_config_import_edit.setPlainText(
            import_configuration)
        ui_config = get_config_parser(self.type)
        ui_config.set_text(configuration)
        ui_import_config = get_config_parser(self.type)
        ui_import_config.set_text(import_configuration)
        user_config = self._fix_block_key(ui_config.get_config())
        import_config = self._fix_block_key(ui_import_config.get_config())
        self.ui.key_value_user_config_edit.set_values(
            user_config)
        self.ui.key_value_user_config_edit.set_default_values(
            import_config)
        self._config_changed()
