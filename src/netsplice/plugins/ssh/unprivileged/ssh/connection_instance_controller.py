# -*- coding: utf-8 -*-
# connection_instance_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.errors import (
    ActiveError,
    NotFoundError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from .model.request.connection_id import (
    connection_id as connection_id_model
)
from .model.response.connection_list_item import (
    connection_list_item as connection_list_item_model
)


class connection_instance_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self, connection_id):
        '''
        The "list-connections" action lists information about currently setup
        connections. For each connection, the response includes the connection
        id and whether or not it is currently active.

        request model: {}
        response_model: {id: '',active: false}
        '''
        request_model = connection_id_model()
        response_model = connection_list_item_model()
        model = self.application.get_module('ssh').model
        try:
            request_model.id.set(connection_id)
            connection = model.connections.find_by_id(request_model.id.get())
            response_model.from_json(connection.to_json())
            self.write(response_model.to_json())
        except ValidationError:
            self.set_status(400)
        except NotFoundError:
            self.set_status(404)
        self.finish()

    def delete(self, connection_id):
        '''
        The "delete" action requests the privileged application to remove a
        previously setup connection. Delete only works for inactive
        connections.

        request model: {id: ''}
        response model: {id: ''}
        '''
        request_model = connection_id_model()
        try:
            request_model.id.set(connection_id)
            model = self.application.get_module('ssh').model
            connections = model.connections
            connections.delete_connection(request_model.id.get())
            self.write(request_model.to_json())
        except ValidationError:
            self.set_status(400)
        except ActiveError:
            self.set_status(403)
        except NotFoundError:
            self.set_status(404)
        except Exception:
            self.set_status(400)
        self.finish()
