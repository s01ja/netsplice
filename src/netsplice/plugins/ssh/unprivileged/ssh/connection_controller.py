# -*- coding: utf-8 -*-
# connection_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.errors import (
    InsufficientStorageError,
    InvalidConfigError,
    ActiveError,
    NotFoundError
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from .model.request.connection_id import (
    connection_id as connection_id_model
)
from .model.request.setup import (
    setup as setup_model
)
from .model.response.connection_list import (
    connection_list as connection_list_model
)
from .ssh import ssh


class connection_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        The "list-connections" action lists information about currently setup
        connections. For each connection, the response includes the connection
        id and whether or not it is currently active.

        request model: {}
        response_model: [{id: '',active: false}]
        '''
        response_model = connection_list_model()
        model = self.application.get_module('ssh').model
        connections = model.connections
        for connection in connections:
            response_model.add_connection(connection)
        self.write(response_model.to_json())
        self.finish()

    def put(self):
        '''
        The "setup" action requests the privileged application to setup a
        connection with the required parameters. The privileged application
        stores the details for the connection in memory and allows future
        requests to connect or remove the configuration by name.

        request model: {configuration: '', username: '', password: ''}
        response model: {id: ''}
        '''
        request_model = setup_model()
        response_model = connection_id_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            model = self.application.get_module('ssh').model
            new_connection = model.create_connection('ssh', request_model)
            new_connection.process = ssh(model, new_connection)
            new_connection.process.setup()
            model.add_connection(new_connection)
            response_model.id.set(new_connection.id.get())
            self.write(response_model.to_json())
        except ValidationError:
            import traceback
            traceback.print_exc()
            self.set_status(400)
        except InsufficientStorageError:
            self.set_status(507)
        except InvalidConfigError:
            self.set_status(400)
        self.finish()
