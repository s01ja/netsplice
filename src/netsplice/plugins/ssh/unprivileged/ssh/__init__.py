# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

import os
import sys

from netsplice.util.ipc.route import get_module_route
from . import config
from .model import model as module_model

name = "ssh"

endpoints = get_module_route(
    'netsplice.plugins.ssh.unprivileged',
    [
        (r'/module/plugin/ssh/connections', 'connection'),
        (r'/module/plugin/ssh/connections/'
            '(?P<connection_id>[^\/]+)',
            'connection_instance'),
        (r'/module/plugin/ssh/connections/'
            '(?P<connection_id>[^\/]+)/connect',
            'connect_action'),
        (r'/module/plugin/ssh/connections/'
            '(?P<connection_id>[^\/]+)/disconnect',
            'disconnect_action'),
        (r'/module/plugin/ssh/connections/'
            '(?P<connection_id>[^\/]+)/reconnect',
            'reconnect_action'),
    ])

model = module_model()

# use packaged ssh executable if exists
executable_path = os.path.dirname(sys.executable)
pkg_executable_path = os.path.join(
    executable_path, config.SSH_BINARY_PKG_PATH)
if os.path.exists(pkg_executable_path):
    config.SSH_BINARY_PATH = pkg_executable_path
pkg_executable_path = os.path.join(
    executable_path, config.SSH_BINARY_EXE_PATH)
if os.path.exists(pkg_executable_path):
    config.SSH_BINARY_PATH = pkg_executable_path
