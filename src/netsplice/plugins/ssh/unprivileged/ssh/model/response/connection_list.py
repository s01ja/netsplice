# -*- coding: utf-8 -*-
# connection_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing connections. This is a response model that will be filled
temporary for JSON responses.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from .connection_list_item import connection_list_item


class connection_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    connection_list_item_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, connection_list_item)

    def add_connection(self, connection_model_instance):
        '''
        Add the given (module)connection_model_instance to the connection_list
        by creating a new instance, populate it with values and append it to
        the collection.
        '''
        connection = self.item_model_class()
        connection.id.set(connection_model_instance.id.get())
        connection.type.set(connection_model_instance.type.get())
        connection.active.set(connection_model_instance.active.get())
        connection.read_tap_bytes.set(
            connection_model_instance.read_tap_bytes.get())
        connection.write_tap_bytes.set(
            connection_model_instance.write_tap_bytes.get())
        connection.read_udp_bytes.set(
            connection_model_instance.read_udp_bytes.get())
        connection.write_udp_bytes.set(
            connection_model_instance.write_udp_bytes.get())
        connection.auth_read_bytes.set(
            connection_model_instance.auth_read_bytes.get())
        connection.pre_compress_bytes.set(
            connection_model_instance.pre_compress_bytes.get())
        connection.post_compress_bytes.set(
            connection_model_instance.post_compress_bytes.get())
        connection.pre_decompress_bytes.set(
            connection_model_instance.pre_decompress_bytes.get())
        connection.post_decompress_bytes.set(
            connection_model_instance.post_decompress_bytes.get())
        self.append(connection)
