# -*- coding: utf-8 -*-
# connect_action_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import (ActiveError, NotFoundError)
from .model.request.connection_id import (
    connection_id as connection_id_model
)


class connect_action_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def post(self, connection_id):
        '''
        The "connect" action requests the privileged application to start a
        previously setup connection.

        request model: {id: ''} (for argument validity)
        response model: {id: ''}
        '''
        request_model = connection_id_model()
        try:
            request_model.id.set(connection_id)
            model = self.application.get_module('ssh').model
            connections = model.connections
            connection = connections.find_by_id(request_model.id.get())
            if connection.active.get() is True:
                raise ActiveError()
            connection.process.connect()
            self.write(request_model.to_json())
        except ValidationError:
            self.set_status(400)
        except ActiveError:
            self.set_status(403)
        except NotFoundError:
            self.set_status(404)
        self.finish()
