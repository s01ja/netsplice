# -*- coding: utf-8 -*-
# event_plugin.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event plugin for backend app.

'''

from tornado import gen

from .event import names as names
import netsplice.backend.event.names as event_names
import netsplice.backend.event.origins as origins
import netsplice.backend.event.types as types
from netsplice.util import get_logger

logger = get_logger()


class event_plugin(object):
    '''
    Event loop for the backend.

    Requires modules registered and waits for model-change on that modules
    to process events.
    '''

    def __init__(self, application):
        '''
        Initialize members.

        Requires the user to set application and add modules.
        '''
        self.application = application

    @gen.coroutine
    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()
        event_origin = event_model_instance.origin.get()

        if event_type == types.MODEL_CHANGED:
            if event_origin == origins.UNPRIVILEGED:
                if event_name == names.SSH_MODEL:
                    yield self.unpriv_model_changed()
        if event_type == types.NOTIFY:
            if event_origin == origins.UNPRIVILEGED:
                if event_name == event_names.LOG_CHANGED:
                    yield self.log_model_changed()

    @gen.coroutine
    def unpriv_model_changed(self):
        '''
        The Model in the privileged backend has changed.

        Request all connections and update the priv-model and then instruct the
        connections dispatcher to update any connections that are affected by
        this.
        '''
        unpriv_backend = self.application.get_unprivileged()
        gui_module = self.application.get_module('gui')
        yield unpriv_backend.ssh.list_connections()
        yield gui_module.connections_dispatcher.update(
            self.application)

    @gen.coroutine
    def log_model_changed(self):
        '''
        The Model in the privileged backend has changed.

        Request all connections and update the priv-model and then instruct the
        connections dispatcher to update any connections that are affected by
        this.
        '''
        gui_module = self.application.get_module('gui')
        yield gui_module.connections_dispatcher.update(
            self.application)
