# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
from netsplice.backend.connection.connection_plugin import register_plugin
from netsplice.backend.event import register_origin as register_event_origin
from netsplice.plugins.ssh.config import events
from .ssh.connection import connection as connection_dispatcher


def register(app):
    '''
    Register.

    Register the dispatcher to the backend_connections.
    '''
    register_event_origin(connection_dispatcher, events.OUTGOING)
    register_plugin('ssh', connection_dispatcher)
