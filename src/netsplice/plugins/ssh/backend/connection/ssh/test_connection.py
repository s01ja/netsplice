# -*- coding: utf-8 -*-
# test_connection.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection.

Checks connection base functionality.
'''

import mock
import tornado
import sys
from tornado import gen, ioloop
from tornado.testing import AsyncTestCase

import netsplice.backend.connection as connection_module
import netsplice.backend.event as event_module
import netsplice.backend.gui as gui_module
import netsplice.backend.log as log_module
import netsplice.backend.preferences as preferences_module


from netsplice.config import flags
from netsplice.config import connection as config_connection
from netsplice.config import plugins as config_plugins
from netsplice.plugins.ssh.config import ssh as config_ssh

from netsplice.backend.connection.connection import (
    connection as backend_connection
)
from netsplice.backend.connection.connection_broker import (
    connection_broker as backend_connection_broker
)
from netsplice.backend.connection.chain import (
    chain as connection_chain
)
from .connection import connection as plugin_connection

from netsplice.util.errors import NotInitializedError
from netsplice.backend.gui.model.connection_list_item import (
    connection_list_item as connection_item_model
)
from netsplice.backend.event_loop import event_loop
from netsplice.backend.gui.model import model as module_model
from netsplice.backend.log.model import model as log_model
from netsplice.backend.preferences.model.override_item import (
    override_item as override_item_model
)
from netsplice.backend.preferences.model.account_item import (
    account_item as account_item_model
)
from netsplice.config import backend as backend_config
from netsplice.model.log_item import log_item as log_item_model
from netsplice.model.credential import credential as credential_item_model
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.model.configuration_list import (
    configuration_list as configuration_list_model
)
from netsplice.model.named_value_item import (
    named_value_item as named_value_item_model
)
from netsplice.backend.unprivileged.model.response.connection_status import (
    connection_status as connection_status_model
)
from netsplice.util.ipc.application import application as application
from netsplice.plugins import get_plugins

CONNECTION_ID = 'c115aa5a-d1df-4ee8-9ede-86898effe163'
FIRST_ACCOUNT_ID = 'a115aa5a-d1df-4ee8-9ede-86898effe163'


class mock_connection_item(object):
    def __init__(self):
        self.schedule_get_status_called = False
        self.get_status_called = False

    def schedule_get_status(self):
        self.schedule_get_status_called = True

    @gen.coroutine
    def get_status(self):
        self.get_status_called = True
        status = connection_status_model()
        status.active.set(True)
        raise gen.Return(status)


class mock_unprivileged_ssh(object):
    def __init__(self):
        self.application = None
        self.reset()

    def reset(self):
        self.connect_yielded = False
        self.disconnect_yielded = False
        self.reconnect_yielded = False
        self.remove_yielded = False
        self.setup_connection_yielded = False
        self.setup_connection_return_value = connection_item_model()

    @gen.coroutine
    def connect(self, id):
        self.connect_yielded = True
        raise gen.Return('')

    @gen.coroutine
    def disconnect(self, id):
        self.disconnect_yielded = True
        raise gen.Return('')

    @gen.coroutine
    def reconnect(self, id):
        self.reconnect_yielded = True
        raise gen.Return('')

    @gen.coroutine
    def remove(self, id):
        self.remove_yielded = True
        raise gen.Return('')

    @gen.coroutine
    def setup_connection(
            self, plain_config, username, password, version):
        self.setup_connection_yielded = True
        raise gen.Return(self.setup_connection_return_value)


class mock_unprivileged(object):
    def __init__(self):
        self.application = None
        self.reset()
        self.ssh = mock_unprivileged_ssh()
        self.server_role = ''

    def reset(self):
        pass


class mock_connection_overrides(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.get_credential_yielded = False
        self.get_credential_return_value = credential_item_model()

    @gen.coroutine
    def get_credential(self, account_id):
        self.get_credential_yielded = True
        raise gen.Return(self.get_credential_return_value)

this = sys.modules[__name__]
this.test_application = None


def get_test_application():
    if this.test_application is not None:
        return this.test_application
    app = application()
    app.add_module(connection_module)
    app.add_module(log_module)
    app.add_module(event_module)
    app.add_module(preferences_module)
    app.add_module(gui_module)
    app.set_unprivileged(mock_unprivileged())
    connection_module.register_module_events(event_module)
    orig_active_plugins = config_plugins.ACTIVE_PLUGINS
    config_plugins.ACTIVE_PLUGINS = ['netsplice.plugins.ssh']
    for plugin in get_plugins():
        app.add_plugin(plugin)
    app.set_event_loop(event_loop())
    app.register_plugin_components('util')
    app.register_plugin_components('backend')
    config_plugins.ACTIVE_PLUGINS = orig_active_plugins
    # restore what plugin overrides
    app.get_unprivileged().ssh = mock_unprivileged_ssh()
    this.test_application = app
    return this.test_application


def get_test_object(mock_overrides=[]):
    app = get_test_application()
    app.set_unprivileged(mock_unprivileged())
    preferences_model = app.get_module('preferences').model
    preferences_model.overrides = mock_overrides
    test_account = account_item_model(None)
    test_account.id.set(FIRST_ACCOUNT_ID)
    test_account.configuration.set(' ')
    bcb = app.get_module('connection').broker
    bcb.application = app
    bc = backend_connection(bcb, test_account)
    ci = connection_item_model()
    ch = connection_chain([test_account])
    bc.model = ci
    c = plugin_connection(app, bc)
    app.get_module('connection').broker.connections[FIRST_ACCOUNT_ID] = (
        bc, ch)
    return c


class ConnectionTests(AsyncTestCase):
    '''
    Connection Tests.

    Checks the backend connection base class for its pure-virtual functions
    and its functional elements.
    '''

    @tornado.testing.gen_test
    def test_apply_preference_overrides_empty_stays_empty(self):
        c = get_test_object()
        l = configuration_list_model()
        assert(len(l) == 0)
        c.apply_preference_overrides(l)
        assert(len(l) == 0)

    def test_apply_preference_overrides_no_overriddes_stays_same(self):
        c = get_test_object()
        l = configuration_list_model()
        i = configuration_item_model(0)
        i.key.set('should_stay_same')
        vi0 = named_value_item_model()
        vi1 = named_value_item_model()
        vi2 = named_value_item_model()
        vi0.value.set('ignored')
        vi1.value.set('ignored')
        vi2.name.set('value')
        vi2.value.set('should_stay_same')
        i.values.append(vi0)
        i.values.append(vi1)
        i.values.append(vi2)

        l.append(i)
        assert(len(l) == 1)
        c.apply_preference_overrides(l)
        assert(len(l) == 1)
        print 'i.key', i.key.get()
        print 'vi2.value', vi2.value.get()
        assert(i.key.get() == 'should_stay_same')
        assert(vi2.value.get() == 'should_stay_same')

    def test_apply_preference_overrides_override_modifies(self):
        mock_overrides = []
        override_item = override_item_model(None)
        override_item.type.set('ssh')
        override_item.name.set('should_stay_same')  # eg auth-user-pass
        override_item.value.set('override_value')
        mock_overrides.append(override_item)
        c = get_test_object(mock_overrides=mock_overrides)

        l = configuration_list_model()
        i = configuration_item_model(0)
        i.key.set('should_stay_same')
        vi0 = named_value_item_model()
        vi1 = named_value_item_model()
        vi2 = named_value_item_model()
        vi0.value.set('ignored')
        vi1.value.set('ignored')
        vi2.name.set('value')
        vi2.value.set('should_change')  # /etc/ssh/some_password_file
        i.values.append(vi0)
        i.values.append(vi1)
        i.values.append(vi2)

        l.append(i)
        assert(len(l) == 1)
        c.apply_preference_overrides(l)
        assert(len(l) == 1)
        print i.key.get()
        assert(i.key.get() == 'should_stay_same')
        print vi2.value.get()
        assert(vi2.value.get() == 'override_value')

    def test_apply_preference_overrides_other_type_not_modified(self):
        mock_overrides = []
        override_item = override_item_model(None)
        override_item.type.set('NOssh')
        override_item.name.set('should_stay_same')  # eg auth-user-pass
        override_item.value.set('override_value')
        mock_overrides.append(override_item)
        c = get_test_object(mock_overrides=mock_overrides)

        l = configuration_list_model()
        i = configuration_item_model(0)
        i.key.set('should_stay_same')
        vi0 = named_value_item_model()
        vi1 = named_value_item_model()
        vi2 = named_value_item_model()
        vi0.value.set('ignored')
        vi1.value.set('ignored')
        vi2.name.set('value')
        vi2.value.set('should_stay_same')  # /etc/ssh/some_password_file
        i.values.append(vi0)
        i.values.append(vi1)
        i.values.append(vi2)

        l.append(i)
        assert(len(l) == 1)
        c.apply_preference_overrides(l)
        assert(len(l) == 1)
        print i.key.get()
        assert(i.key.get() == 'should_stay_same')
        print vi2.value.get()
        assert(vi2.value.get() == 'should_stay_same')

    def test_apply_preference_overrides_not_found_not_modified(self):
        mock_overrides = []
        override_item = override_item_model(None)
        override_item.type.set('ssh')
        override_item.name.set('should_stay_same')  # eg auth-user-pass
        override_item.value.set('override_value')
        mock_overrides.append(override_item)
        c = get_test_object(mock_overrides=mock_overrides)

        l = configuration_list_model()
        i = configuration_item_model(0)
        i.key.set('will_not_be_found')
        vi0 = named_value_item_model()
        vi1 = named_value_item_model()
        vi2 = named_value_item_model()
        vi0.value.set('ignored')
        vi1.value.set('ignored')
        vi2.name.set('value')
        vi2.value.set('should_stay_same')  # /etc/ssh/some_password_file
        i.values.append(vi0)
        i.values.append(vi1)
        i.values.append(vi2)

        l.append(i)
        assert(len(l) == 1)
        c.apply_preference_overrides(l)
        assert(len(l) == 1)
        print i.key.get()
        assert(i.key.get() == 'will_not_be_found')
        print vi2.value.get()
        assert(vi2.value.get() == 'should_stay_same')

    def test_apply_preference_overrides_override_clears(self):
        mock_overrides = []
        override_item = override_item_model(None)
        override_item.type.set('ssh')
        override_item.name.set('should_stay_same')  # auth-user-pass
        override_item.value.set('')  # empty string -> credential
        mock_overrides.append(override_item)
        c = get_test_object(mock_overrides=mock_overrides)
        l = configuration_list_model()
        i = configuration_item_model(0)
        i.key.set('should_stay_same')
        vi0 = named_value_item_model()
        vi1 = named_value_item_model()
        vi2 = named_value_item_model()
        vi0.value.set('ignored')
        vi1.value.set('ignored')
        vi2.name.set('value')
        vi2.value.set('should_change')  # /etc/ssh/some_password_file
        i.values.append(vi0)
        i.values.append(vi1)
        i.values.append(vi2)

        l.append(i)
        assert(len(l) == 1)
        c.apply_preference_overrides(l)
        assert(len(l) == 1)
        print i.key.get()
        assert(i.key.get() == 'should_stay_same')
        print vi2.value.get()
        assert(vi2.value.get() == '')

    def test_apply_preference_overrides_override_removes(self):
        mock_overrides = []
        override_item = override_item_model(None)
        override_item.type.set('ssh')
        override_item.name.set('should_be_removed')
        override_item.value.set(None)
        mock_overrides.append(override_item)
        c = get_test_object(mock_overrides=mock_overrides)
        l = configuration_list_model()
        i = configuration_item_model(0)
        i.key.set('should_be_removed')  # auth-user-pass
        vi0 = named_value_item_model()
        vi1 = named_value_item_model()
        vi2 = named_value_item_model()
        vi0.value.set('ignored')
        vi1.value.set('ignored')
        vi2.value.set('should_be_removed')  # '/etc/ssh/password_file
        i.values.append(vi0)
        i.values.append(vi1)
        i.values.append(vi2)

        l.append(i)
        assert(len(l) == 1)
        c.apply_preference_overrides(l)

        assert(len(l) == 0)

    @tornado.testing.gen_test
    def test_connect_calls_unprivileged_ssh_connect(self):
        c = get_test_object()
        yield c.connect()
        assert(c.unprivileged.ssh.connect_yielded)

    @tornado.testing.gen_test
    def test_connect_schedules_status_gets(self):
        c = get_test_object()
        ci = mock_connection_item()
        c.schedule_get_status = ci.schedule_get_status
        yield c.connect()
        assert(ci.schedule_get_status_called)

    @tornado.testing.gen_test
    def test_disconnect_changes_model_state(self):
        c = get_test_object()
        ci = mock_connection_item()
        c.get_status = ci.get_status
        c.connection.state = config_connection.DISCONNECTING
        yield c.disconnect()
        assert(c.connection.state == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_disconnect_calls_unprivileged_disconnect(self):
        c = get_test_object()
        ci = mock_connection_item()
        c.get_status = ci.get_status
        c.connection.state = config_connection.DISCONNECTING
        yield c.disconnect()
        assert(c.connection.state == config_connection.DISCONNECTED)
        assert(c.application.unprivileged.ssh.disconnect_yielded)

    @tornado.testing.gen_test
    def test_disconnect_calls_unprivileged_remove(self):
        c = get_test_object()
        ci = mock_connection_item()
        c.get_status = ci.get_status
        c.connection.state = config_connection.DISCONNECTING
        yield c.disconnect()
        assert(c.connection.state == config_connection.DISCONNECTED)
        assert(c.application.unprivileged.ssh.remove_yielded)

    @tornado.testing.gen_test
    def test_reconnect_calls_unprivileged_ssh_reconnect(self):
        c = get_test_object()
        yield c.reconnect()
        assert(c.unprivileged.ssh.reconnect_yielded)

    @tornado.testing.gen_test
    def test_reconnect_schedules_status_gets(self):
        c = get_test_object()
        ci = mock_connection_item()
        c.schedule_get_status = ci.schedule_get_status
        yield c.reconnect()
        assert(ci.schedule_get_status_called)

    @tornado.testing.gen_test
    @mock.patch(
        'netsplice.plugins.ssh.backend.connection.ssh.connection.connection.'
        'apply_preference_overrides')
    def test_setup_calls_apply_overrides(
            self, mock_apply_preference_overrides):
        c = get_test_object()
        ci = connection_item_model()
        ai = account_item_model(None)
        ai.configuration.set('#sample\n#config\nremote localhost 1234\n')
        c.unprivileged = mock_unprivileged()
        c.unprivileged.ssh.setup_connection_return_value.id.set(
            CONNECTION_ID)
        c.setup(ci, ai)
        mock_apply_preference_overrides.assert_called()

    @tornado.testing.gen_test
    @mock.patch(
        'netsplice.plugins.ssh.backend.connection.ssh.connection.connection.'
        'apply_preference_overrides')
    def test_setup_calls_setup_connection(
            self, mock_apply_preference_overrides):
        c = get_test_object()
        ci = connection_item_model()
        ai = account_item_model(None)
        c.unprivileged = mock_unprivileged()
        c.unprivileged.ssh.setup_connection_return_value.id.set(
            CONNECTION_ID)

        ai.configuration.set('#sample\n#config\nremote localhost 1234\n')
        c.connection.state = config_connection.SETTING_UP
        yield c.setup(ci, ai)
        assert(c.unprivileged.ssh.setup_connection_yielded)

    @tornado.testing.gen_test
    @mock.patch(
        'netsplice.plugins.ssh.backend.connection.ssh.connection.connection.'
        'apply_preference_overrides')
    def test_setup_sets_connection_id(self, mock_apply_preference_overrides):
        c = get_test_object()
        ci = connection_item_model()
        ai = account_item_model(None)
        c.unprivileged = mock_unprivileged()
        c.unprivileged.ssh.setup_connection_return_value.id.set(
            CONNECTION_ID)

        ai.configuration.set('#sample\n#config\nremote localhost 1234\n')
        c.connection.state = config_connection.SETTING_UP
        yield c.setup(ci, ai)
        assert(ci.unprivileged_id.get() == CONNECTION_ID)
        assert(c.log_connection_id == CONNECTION_ID)

    @tornado.testing.gen_test
    @mock.patch(
        'netsplice.plugins.ssh.backend.connection.ssh.connection.connection.'
        'apply_preference_overrides')
    @mock.patch(
        'netsplice.model.credential_list.credential_list.clean')
    def test_setup_calls_clean_credentials(
            self, mock_clean_credential, mock_apply_preference_overrides):
        c = get_test_object()
        ci = connection_item_model()
        ai = account_item_model(None)
        c.unprivileged = mock_unprivileged()
        c.unprivileged.ssh.setup_connection_return_value.id.set(
            CONNECTION_ID)
        ai.configuration.set('#sample\n#config\nremote localhost 1234\n')
        c.gui_model.credentials.clean = mock_clean_credential
        c.connection.state = config_connection.SETTING_UP
        yield c.setup(ci, ai)
        mock_clean_credential.assert_called()

    @tornado.testing.gen_test
    def test_setup_user_pass_sets_credential_type(self):
        c = get_test_object()
        cm = mock_connection_overrides()
        ci = connection_item_model()
        ai = account_item_model(None)
        c.unprivileged = mock_unprivileged()
        c.unprivileged.ssh.setup_connection_return_value.id.set(
            CONNECTION_ID)
        c.get_credential = cm.get_credential
        ai.configuration.set(
            '#sample\n#config\nremote localhost 1234\n'
            'auth-user-pass\n')
        c.credential_type = -1
        c.connection.state = config_connection.SETTING_UP
        yield c.setup(ci, ai)
        assert(c.credential_type == c.TYPE_USERNAME_PASSWORD)

    @tornado.testing.gen_test
    def test_setup_user_pass_calls_get_credential(self):
        c = get_test_object()
        cm = mock_connection_overrides()
        ci = connection_item_model()
        ai = account_item_model(None)
        c.unprivileged = mock_unprivileged()
        c.unprivileged.ssh.setup_connection_return_value.id.set(
            CONNECTION_ID)
        c.get_credential = cm.get_credential
        ai.configuration.set(
            '#sample\n#config\nremote localhost 1234\n'
            'auth-user-pass\n')
        c.credential_type = -1
        c.connection.state = config_connection.SETTING_UP
        yield c.setup(ci, ai)
        assert(cm.get_credential_yielded)

    @tornado.testing.gen_test
    def test_setup_ask_pass_sets_credential_type(self):
        c = get_test_object()
        cm = mock_connection_overrides()
        ci = connection_item_model()
        ai = account_item_model(None)
        c.unprivileged = mock_unprivileged()
        c.unprivileged.ssh.setup_connection_return_value.id.set(
            CONNECTION_ID)
        c.get_credential = cm.get_credential
        ai.configuration.set(
            '#sample\n#config\nremote localhost 1234\n'
            'askpass\n')
        c.credential_type = -1
        c.connection.state = config_connection.SETTING_UP
        yield c.setup(ci, ai)
        assert(c.credential_type == c.TYPE_PRIVATE_KEY_PASSWORD)

    @tornado.testing.gen_test
    def test_setup_ask_pass_calls_get_credential(self):
        c = get_test_object()
        cm = mock_connection_overrides()
        ci = connection_item_model()
        ai = account_item_model(None)
        c.unprivileged = mock_unprivileged()
        c.unprivileged.ssh.setup_connection_return_value.id.set(
            CONNECTION_ID)
        c.get_credential = cm.get_credential
        ai.configuration.set(
            '#sample\n#config\nremote localhost 1234\n'
            'askpass\n')
        c.credential_type = -1
        c.connection.state = config_connection.SETTING_UP
        yield c.setup(ci, ai)
        assert(cm.get_credential_yielded)

    @tornado.testing.gen_test
    def test_check_log_item_any_message_returns_unchanged(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {}
        lm.message.set('ZZZNoSuchMessageYYY')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(result is False)

    @tornado.testing.gen_test
    def test_check_log_item_any_message_returns_unchanged_logs(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {}
        lm.message.set('ZZZNoSuchMessageYYY')
        flags.VERBOSE = 2
        result = c.check_log_item(lm)
        flags.VERBOSE = 0
        config_ssh.LOG_MESSAGES = lconf
        assert(result is False)

    @tornado.testing.gen_test
    def test_check_log_item_change_state_returns_changed(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_CHANGE_STATE],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(result is True)

    @tornado.testing.gen_test
    def test_update_connection_log_connected_state_changes_ci(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        tconf = {
            'MATCHME': {
                'actions': [config_ssh.LOG_CONNECTED],
                'help': ''
            }
        }
        lm.message.set('MATCHME')

        config_ssh.LOG_MESSAGES = tconf
        c.connection.state = config_connection.CONNECTING
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.CONNECTED)

        config_ssh.LOG_MESSAGES = tconf
        c.connection.state = config_connection.CONNECTING_PROCESS
        lm.message.set('MATCHME')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.CONNECTED)

        config_ssh.LOG_MESSAGES = tconf
        c.connection.state = config_connection.RECONNECTING
        lm.message.set('MATCHME')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.RECONNECTED)

        config_ssh.LOG_MESSAGES = tconf
        c.connection.state = config_connection.RECONNECTING_PROCESS
        lm.message.set('MATCHME')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.RECONNECTED)

    @tornado.testing.gen_test
    def test_update_connection_log_disconnected_state_changes_ci(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        tconf = {
            'MATCHME': {
                'actions': [config_ssh.LOG_DISCONNECTED],
                'help': ''
            }
        }
        config_ssh.LOG_MESSAGES = tconf
        lm.message.set('MATCHME')
        c.connection.state = config_connection.CONNECTING
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.DISCONNECTED)

    @tornado.testing.gen_test
    def test_update_connection_log_disconnected_connecting_state_changes_ci(
            self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        tconf = {
            'MATCHME': {
                'actions': [config_ssh.LOG_DISCONNECTED],
                'help': ''
            }
        }
        config_ssh.LOG_MESSAGES = tconf
        lm.message.set('MATCHME')
        c.connection.state = config_connection.CONNECTING
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.DISCONNECTED)

        config_ssh.LOG_MESSAGES = tconf
        lm.message.set('MATCHME')
        c.connection.state = config_connection.DISCONNECTING_FAILURE
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.DISCONNECTED_FAILURE)

    @tornado.testing.gen_test
    def test_update_connection_log_connecting_state_changes_ci(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        tconf = {
            'MATCHME': {
                'actions': [config_ssh.LOG_CONNECTING],
                'help': ''
            }
        }
        config_ssh.LOG_MESSAGES = tconf
        lm.message.set('MATCHME')
        c.connection.state = config_connection.RECONNECTING
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.RECONNECTING_PROCESS)

        config_ssh.LOG_MESSAGES = tconf
        c.connection.state = config_connection.CONNECTING
        lm.message.set('MATCHME')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.CONNECTING_PROCESS)

    @tornado.testing.gen_test
    def test_update_connection_log_error_state_changes_ci(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_ERROR],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        c.connection.state = config_connection.CONNECTING
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(c.connection.state == config_connection.DISCONNECTING_FAILURE)

    @tornado.testing.gen_test
    def test_update_connection_log_error_state_returns_changed(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_ERROR],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        c.connection.state = config_connection.CONNECTING
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(result is True)

    @tornado.testing.gen_test
    def test_update_connection_log_error_state_adds_error(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        tconf = {
            'MATCHME': {
                'actions': [config_ssh.LOG_ERROR],
                'help': ''
            }
        }
        config_ssh.LOG_MESSAGES = tconf
        lm.message.set('MATCHME')
        assert(len(c.connection.model.errors) is 0)

        c.connection.state = config_connection.CONNECTING
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(len(c.connection.model.errors) is 1)

    @tornado.testing.gen_test
    def test_update_connection_log_error_state_sets_lm_level(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_ERROR],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        lm.level.set('info')
        c.connection.state = config_connection.CONNECTING
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(lm.level.get() == 'error')

    @tornado.testing.gen_test
    def test_update_connection_log_info_state_sets_lm_level(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_INFO],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        lm.level.set('error')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(lm.level.get() == 'info')

    @tornado.testing.gen_test
    def test_update_connection_log_debug_state_sets_lm_level(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_DEBUG],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        lm.level.set('error')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(lm.level.get() == 'debug')

    @tornado.testing.gen_test
    def test_update_connection_log_warning_state_sets_lm_level(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_WARNING],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        lm.level.set('error')
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(lm.level.get() == 'warning')

    @tornado.testing.gen_test
    def test_update_connection_log_status_state_adds_progress(self):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_PROGRESS],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        c.connection.state = config_connection.CONNECTING
        assert(len(c.connection.model.progress) is 0)
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        assert(len(c.connection.model.progress) is 1)

    @tornado.testing.gen_test
    @mock.patch('netsplice.model.credential_list.credential_list.mark_wrong')
    def test_update_connection_log_reset_cred_calls_mark_wrong(
            self, mock_mark_wrong):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_RESET_CREDENTIAL],
                'help': ''
            }
        }
        lm.message.set('MATCHME')
        c.gui_model.credentials.mark_wrong = mock_mark_wrong
        result = c.check_log_item(lm)
        config_ssh.LOG_MESSAGES = lconf
        mock_mark_wrong.assert_called()

    @tornado.testing.gen_test
    @mock.patch('netsplice.model.credential_list.credential_list.mark_wrong')
    def test_update_connection_exception_overrides(
            self, mock_mark_wrong):
        c = get_test_object()
        lm = log_item_model()
        lconf = config_ssh.LOG_MESSAGES
        config_ssh.LOG_MESSAGES = {
            'MATCHME': {
                'actions': [config_ssh.LOG_RESET_CREDENTIAL],
                'help': '',
                'exceptions': {
                    'YES': {
                        'actions': [config_ssh.LOG_CHANGE_STATE],
                        'help': ''
                    },
                    'NO': {
                        'actions': [config_ssh.LOG_DEBUG],
                        'help': ''
                    }
                }
            }
        }
        lm.message.set('MATCHME')
        lm.level.set('error')
        result = c.check_log_item(lm)
        assert(result is False)  # LOG_RESET_CREDENTIAL does not change state
        assert(lm.level.get() == 'error')  # unchanged level
        print '----'

        lm.message.set('MATCHME YES')
        lm.level.set('error')
        result = c.check_log_item(lm)
        print lm
        assert(result is True)  # exception YES should be active
        assert(lm.level.get() == 'error')  # unchanged level

        lm.message.set('MATCHME NO')
        lm.level.set('error')
        result = c.check_log_item(lm)
        print lm
        assert(result is False)  # exception NO should be active
        assert(lm.level.get() == 'debug')  # unchanged level

        config_ssh.LOG_MESSAGES = lconf
