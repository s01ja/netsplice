# -*- coding: utf-8 -*-
# ssh.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
SSH Configuration Parser.

Parser that evaluates each line as a key-value, assuming that the first word
in the line is a key and the rest of the line is the value.
'''
import re
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.util.parser.line import line as line_parser
from netsplice.plugins.ssh.config import ssh as config_ssh


class ssh(line_parser):
    '''
    Line Parser.

    Use the parser_base implementation for set_config, set_text.
    '''

    def __init__(self):
        '''
        Initialize.

        Initialize members and base.
        '''
        line_parser.__init__(self)
        self.option_name = None
        self.option_type = None
        self.option_value = None
        self.option_block = False
        self.in_block = False

    def analyze(self):
        '''
        Analyze.

        Store the current line to the current element values when the line
        is not empty.
        Handle blocks that start and end in angle-brackets (xml-like)
        Set option_name, option_type and option_value from the current line.
        '''
        if self.current_line is None:
            return False

        if self.current_line.startswith('<'):
            block_start = re.match('^<([^>]*)>.*$', self.current_line)

            if not block_start:
                # skip line
                return False

            if self.in_block:
                block_end = re.match(
                    r'^.*</%s>.*$' % (self.option_name,), self.current_line)
                if block_end:
                    self.in_block = False
                    return True
                # closing tag not yet found
                return False

            # opening tag
            self.option_name = block_start.group(1)
            self.option_type = self.get_type(self.option_name)
            self.option_value = ''
            self.option_block = True
            self.in_block = True
            return False

        if self.in_block:
            if self.option_value != '':
                self.option_value += '\n'

            self.option_value += self.current_line
            return False

        if self.current_line.startswith('#') or \
                self.current_line.startswith(';'):
            self.option_name = self.current_line[0]
            self.option_type = self.get_type(self.option_name)
            try:
                if self.current_line[1] == ' ':
                    self.option_value = self.current_line[2:]
                else:
                    self.option_value = self.current_line[1:]
            except IndexError:
                self.option_value = ''
            return True

        line_elements = self.current_line.split(' ')
        self.option_name = line_elements[0]  # pop_first
        self.option_type = self.get_type(self.option_name)
        del line_elements[0]
        self.option_value = ' '.join(line_elements)
        return True

    def commit(self):
        '''
        Commit current element.

        Append the current analyzed values to the current_element and use the
        line_parser commit function to store the current_element into the
        elements list. Clear values for the next analyze call.
        '''
        # XXX check with ssh_config
        self.current_element.key.set(self.option_name)
        self.current_element.add_value('status', None)
        self.current_element.add_value('type', self.option_type)
        self.current_element.add_value('value', self.option_value)
        self.current_element.add_value('block', self.option_block)
        self.option_name = None
        self.option_type = None
        self.option_value = None
        self.option_block = False
        line_parser.commit(self)

    def get_insert_template(self):
        '''
        Get Insert Template.

        Return a Configuration Item Model that can be used to insert a new
        instance in the configuration.
        '''
        template = configuration_item_model(0)
        template.key.set('')
        template.add_value('status', None)
        template.add_value('type', config_ssh.TYPE_STRING)
        template.add_value('value', '')
        template.add_value('block', False)
        return template

    def get_text(self):
        '''
        Get Text.

        Process the elements of a previously parsed configuration and return
        a plaintext representation. The result is used to create a config
        that can be consumed by an external application or for advanced users
        to edit previously changed configuration_items.
        The line implementation sorts the current element list by index
        and appends the key and value to a line. With block values a xml-like
        tag is created to surround the value on multiple lines.
        '''
        text = ''
        ordered_elements = sorted(
            self.elements, key=self.get_element_index)
        for element in ordered_elements:
            if text != '':
                text += '\n'
            key = element.key.get()
            value = element.get_value('value')
            if element.get_value('block'):
                text += '<%s>\n' % (key,)
                if value == '' or value is None:
                    text += ''
                else:
                    text += value
                text += '\n</%s>' % (key,)
                continue
            text += key
            if value == '' or value is None:
                continue
            text += ' %s' % (value,)
        return text

    def get_type(self, name):
        '''
        Get Type for Name.

        Return a type constant for the given configuration name.
        '''
        if name in config_ssh.KEYWORDS:
            type = config_ssh.KEYWORDS[name]['type']
        else:
            type = config_ssh.TYPE_OPTION

        return type
