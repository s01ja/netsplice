# -*- coding: utf-8 -*-
# account_type_editor.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Account editor.

Edit account settings for OpenVPN connections.
'''

from . import config as provider_config

from netsplice.plugins.openvpn.gui.account_edit.account_type_editor.\
    account_type_editor import (
        account_type_editor as openvpn_account_type_editor
    )


class account_type_editor(openvpn_account_type_editor):
    '''
    OpenVPN Account Type Editor.

    Custom Editor with syntax highlighter and keyword tooltip.
    '''

    def __init__(self, parent=None, dispatcher=None):
        '''
        Initialize Module.

        Initialize widget and abstrac base, Setup UI.
        '''
        openvpn_account_type_editor.__init__(
            self, parent, dispatcher)
        self.ui.button_import_file.hide()
        self.DEFAULT_ACCOUNT = provider_config.DEFAULT_CONFIG
        self.ui.basic_hints.setText(provider_config.DESCRIPTION)
        self.ui.basic_hints.show()

    def set_model(self, account_model):
        '''
        Set Model.

        Override Model change to display values regardless of previous values.
        '''
        self.editor_model = None
        self.reset_form()
        self._config_changed()
