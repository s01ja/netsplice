# -*- coding: utf-8 -*-
# plugin.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Preferences. Defines how the user input is persisted.
'''

from netsplice.model.plugin_item import (
    plugin_item as plugin_item_model
)
from netsplice.model.validator.version import (
    version as version_validator
)
from netsplice.util import get_logger
from netsplice.util.model.marshalable import marshalable
from netsplice.util.model.field import field

from netsplice.plugins.tor.config import backend as config

logger = get_logger()


class plugin(plugin_item_model):
    def __init__(self, owner):
        plugin_item_model.__init__(self, owner)

        self.default_tor_version = field(
            required=False,
            default=config.DEFAULT_TOR_VERSION,
            validators=[version_validator()])

    def apply_values(self):
        '''
        Apply Values.

        Apply values from the preferences to the config so other
        backend components can use the values.
        '''
        config.DEFAULT_TOR_VERSION = \
            self.default_tor_version.get()
