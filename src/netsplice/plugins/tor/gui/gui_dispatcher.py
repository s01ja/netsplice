# -*- coding: utf-8 -*-
# gui_dispatcher.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''
import sys
from PySide import QtCore

from tornado import gen, httpclient

import dispatcher_endpoints as endpoints
from model.request.version import (
    version as version_model
)
from model.response.version_detail import (
    version_detail as version_detail_model
)
from model.response.version_help import (
    version_help as version_help_model
)
from model.response.version_list import (
    version_list as version_list_model
)
from netsplice.gui import backend_dispatcher_qt_replacement

from netsplice.util import get_logger, basestring
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.model.errors import ValidationError

logger = get_logger()

if sys.platform.startswith('darwin'):
    # Python only threads in gui.
    # Due to bugs in PySide osx may crash at random locations. Use this option
    # to use a threading implementation that mimics the emit/Signal code.
    Signal = backend_dispatcher_qt_replacement.Signal
    QObject = backend_dispatcher_qt_replacement.QObject
    QThread = backend_dispatcher_qt_replacement.QThread
    QMutex = backend_dispatcher_qt_replacement.QMutex
    QCoreApplication = backend_dispatcher_qt_replacement.QCoreApplication
else:
    Signal = QtCore.Signal
    QObject = QtCore.QObject
    QThread = QtCore.QThread
    QMutex = QtCore.QMutex
    QCoreApplication = QtCore.QCoreApplication


class gui_dispatcher(QObject):

    get_version_detail = Signal(basestring, object, object)
    get_version_help = Signal(basestring, object, object)
    get_version_list = Signal(object, object)

    def __init__(self, owning_dispatcher):
        QObject.__init__(self)
        self.owner = owning_dispatcher
        self.get_version_detail.connect(self._get_version_detail)
        self.get_version_help.connect(self._get_version_help)
        self.get_version_list.connect(self._get_version_list)

    @gen.coroutine
    def _get_version_detail(self, version, done_signal, failed_signal):
        '''
        Get Version detail

        Request the backend for a details for the given version
        '''

        try:
            request_model = version_model()
            request_model.version.set(version)
            options = {
                'version': request_model.version.get()
            }
            response = yield self.owner.get(
                endpoints.TOR_UNPRIV_VERSION % options, None)

            response_model = version_detail_model()
            response_model.from_json(response.body)

            done_signal.emit(response_model.to_json())
        except ValidationError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except httpclient.HTTPError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except ServerConnectionError as errors:
            self.owner._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _get_version_help(self, version, done_signal, failed_signal):
        '''
        Get Version detail

        Request the backend for a details for the given version
        '''

        try:
            request_model = version_model()
            request_model.version.set(version)
            options = {
                'version': request_model.version.get()
            }
            response = yield self.owner.get(
                endpoints.TOR_UNPRIV_HELP % options, None)

            response_model = version_help_model()
            response_model.from_json(response.body)

            done_signal.emit(response_model.to_json())
        except ValidationError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except httpclient.HTTPError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except ServerConnectionError as errors:
            self.owner._emit_failure(failed_signal, errors)

    @gen.coroutine
    def _get_version_list(self, done_signal, failed_signal):
        '''
        Get Version detail

        Request the backend for a details for the given version
        '''

        try:
            response = yield self.owner.get(
                endpoints.TOR_UNPRIV_EXECUTABLES, None)

            response_model = version_list_model()
            response_model.from_json(response.body)

            done_signal.emit(response_model.to_json())
        except ValidationError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except httpclient.HTTPError as errors:
            self.owner._emit_failure(failed_signal, errors)
        except ServerConnectionError as errors:
            self.owner._emit_failure(failed_signal, errors)
