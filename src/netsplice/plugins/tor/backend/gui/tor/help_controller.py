# -*- coding: utf-8 -*-
# help_controller.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from tornado import gen

from netsplice.util.errors import NotFoundError
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from .model.request.version import (
    version as version_model
)
from .model.response.version_help import (
    version_help as version_help_model
)

from .tor import tor


class help_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    @gen.coroutine
    def get(self, version):
        '''
        The help from a given executable version.

        request model: {}
        response_model: [{id: '',active: false}]
        '''
        request_model = version_model()
        try:
            request_model.version.set(version)
            response_model = version_help_model()
            tor_dispatcher = tor(self.application)
            version_help = yield tor_dispatcher.get_version_help(
                request_model.version.get())
            response_model.from_json(version_help.to_json())
            self.write(response_model.to_json())
        except ValidationError as errors:
            self.set_status(400)
        except NotFoundError as errors:
            self.set_status(404)
        self.finish()
