# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configuration for gui components.
'''

DESCRIPTION = '''
<p><b>Tor</b> is free software for enabling anonymous
 communication. The name is derived from an acronym for the original software
 project name "The Onion Router".</p>
<p>The plugin supports setting up multiple Tor instances with different
 options. You need to configure your client (browser) to use the socks proxy
 that is created when connected.</p>
'''

PREFERENCES_HELP = '''
<p>Global Tor options. The options are used as default for new accounts.</p>
'''
