# -*- coding: utf-8 -*-
# message_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing messages.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from .message_list_item import (
    message_list_item
)


class message_list(marshalable_list):
    '''
    Marshalable model that contains messages that may be send or received over
    the management connection of a tor instance.
    '''
    def __init__(self):
        marshalable_list.__init__(self, message_list_item)
