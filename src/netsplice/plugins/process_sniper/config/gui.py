# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for About
'''

DESCRIPTION = '''
<p><b>Process Sniper</b> is an account plugin that is able to send signals
 to a process when the connection state changes.</p>
'''

RULE_DIALOG_HELP = '''
<p>Use the Process List to select a filter from the currently running
processes.</p>
<p>Processes running with privileges or as different user cannot be killed.
</p>

<p>Test the Commandline pattern. It will display the PID's that the sniper
kills.</p>
'''

RULE_DIALOG_LAUNCHER_SELECT_EMPTY = '''
<p>No Process Launcher items available.</p>
<p>There are no unprivileged, active launch items defined.</p>
<p>Use the <b>Process List</b> source to kill applications that are not started
with the Process Launcher.</p>
'''
RULE_DIALOG_LAUNCHER_SELECT_EMPTY_DROPPED = '''

<p>No Process Launcher items available.</p> <p>There are no unprivileged,
active launch items defined that match the selected event {event_name}.
Available launch items with <b>{dropped_event_names}</b> have been dropped.
Allowed predecessors for {event_name} are {allowed_event_names}.</p>
<p>Use the <b>Process List</b> source to kill applications that are not started
with the Process Launcher.</p>
'''

RULE_DIALOG_LAUNCHER_SELECT = '''
<p>The list of launch items can be used to kill a previously launched
application.</p>
<p>Only unprivileged, active items that launch on events occurring before the
selected event will be displayed.</p>
'''

PREFERENCES_HELP = '''
<p>Global Process Sniper options. Unlock additional signals and configure
the default signal for new rules.</p>
<p>Define the default signal when creating a new rule.</p>
<p>Define JSON list [{"signal": "SIGPWR"},...] for additional signals.</p>
'''

# Number of matched processes displayed in the TestMatch dialog.
MAX_KILL_MESSAGE_ITEMS = 5
