# -*- coding: utf-8 -*-
# event.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.config import connection as config_connection
from netsplice.plugins.process_sniper.backend.event import (
    names as event_names
)

# Supported event names list for combobox
EVENT_NAMES = [
    config_connection.UP,
    config_connection.DOWN,
    config_connection.CREATED,
    config_connection.SETTING_UP,
    config_connection.CONNECTING,
    config_connection.CONNECTED,
    config_connection.RECONNECTED,
    config_connection.DISCONNECTING,
    config_connection.DISCONNECTING_FAILURE,
    config_connection.DISCONNECTED,
    config_connection.DISCONNECTED_FAILURE,
    config_connection.RECONNECTING
]

# Supported predecessors. used to filter the launcher-items to the current
# selected event
# This is a poor-mans version of the connection state machine. This does not
# need all the intermediate states that are required in backend.connection.
# Changing anything here does NOT change the state machine behavior, it is only
# to have a sane selection for the user.
EVENT_PREDECESSORS = {
    config_connection.CREATED: [],
    config_connection.SETTING_UP: [
        config_connection.CREATED,
    ],
    config_connection.CONNECTING: [
        config_connection.SETTING_UP,
        config_connection.CREATED,
    ],
    config_connection.CONNECTED: [
        config_connection.CONNECTING,
        config_connection.CREATED,
        config_connection.SETTING_UP,
    ],
    config_connection.DISCONNECTED: [
        config_connection.CONNECTED,
        config_connection.CONNECTING,
        config_connection.CREATED,
        config_connection.DISCONNECTING,
        config_connection.RECONNECTED,
        config_connection.RECONNECTING,
        config_connection.SETTING_UP,
    ],
    config_connection.DISCONNECTED_FAILURE: [
        config_connection.DISCONNECTING_FAILURE,
        config_connection.CREATED,
        config_connection.CONNECTED,
        config_connection.CONNECTING,
        config_connection.RECONNECTED,
        config_connection.RECONNECTING,
        config_connection.SETTING_UP,
    ],
    config_connection.RECONNECTED: [
        config_connection.CONNECTED,
        config_connection.CONNECTING,
        config_connection.CREATED,
        config_connection.RECONNECTED,
        config_connection.RECONNECTING,
        config_connection.SETTING_UP,
    ],

    config_connection.DISCONNECTING: [
        config_connection.CONNECTING,
        config_connection.CONNECTED,
        config_connection.CREATED,
        config_connection.RECONNECTED,
        config_connection.RECONNECTING,
        config_connection.SETTING_UP,
    ],
    config_connection.DISCONNECTING_FAILURE: [
        config_connection.CONNECTED,
        config_connection.CONNECTING,
        config_connection.CREATED,
        config_connection.RECONNECTED,
        config_connection.RECONNECTING,
        config_connection.SETTING_UP,
    ],
    config_connection.RECONNECTING: [
        config_connection.CONNECTED,
        config_connection.CONNECTING,
        config_connection.CREATED,
        config_connection.RECONNECTED,
        config_connection.RECONNECTING,
        config_connection.SETTING_UP,
    ],
    config_connection.UP: [],
    config_connection.DOWN: [
        config_connection.UP
    ]
}

# Events emitted into the backend
OUTGOING = [
    config_connection.DISCONNECT,
]

# Events from the backend
INCOMMING = [
    config_connection.CREATED,
    config_connection.SETTING_UP,
    config_connection.CONNECTING,
    config_connection.CONNECTED,
    config_connection.RECONNECTED,
    config_connection.DISCONNECTING,
    config_connection.DISCONNECTING_FAILURE,
    config_connection.DISCONNECTED,
    config_connection.DISCONNECTED_FAILURE,
    config_connection.RECONNECTING,
    config_connection.UP,
    config_connection.DOWN
]

# Events from the backend.privileged.event_controller
INCOMMING_PRIVILEGED = [
    event_names.PROCESS_SNIPER_MODEL
]

# Events from the backend.unprivileged.event_controller
INCOMMING_UNPRIVILEGED = [
    event_names.PROCESS_SNIPER_MODEL
]
