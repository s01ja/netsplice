#!/bin/bash
#
# Test backend api
# execute curl commands on the backend api and evaluates its output. Used
# for quick integration test during development.
# set -x
echo 'this assumes that you disabled HTTPS and HMAC authentication'
echo 'and the backend is running on port 10000 on localhost'

function die(){
    echo "$@"
    section "FAILURE"
    exit 1
}
function section(){
    printf '\n'
    printf -v _hr "%*s" $(tput cols) && echo ${_hr// /-}
    printf '[ %s ]\n' "$@"
}
function tcurl(){
    code=$1
    shift
    content_filter=$1
    shift
    method=$1
    if [ ${method} == "GET" ]; then
        method=""
    else
        method="-X ${method}"
    fi
    shift
    url=$1
    section "curl: ${code} ${url}"
    shift
    if [ -z "$1" ]; then
        data=""
    else
        data="--data '"$@"'"
    fi
    export tcurl_result=/tmp/test.result
    export tcurl_script=/tmp/test.script
    echo "curl -sS -v ${url} ${method} ${data} 2>&1" > ${tcurl_script}
    bash ${tcurl_script} > ${tcurl_result}
    rescode=$(cat ${tcurl_result} | grep "HTTP/1.1 "| sed "s|< HTTP/1.1 \([0-9]*\).*|\1|")
    cat ${tcurl_result}| grep "HTTP/1.1 ${code}" > /dev/null \
      && echo "[OK: code: ${code}]" \
      || die "bad response code ${code} vs ${rescode} for ${method} ${url}"

    result_output=$(cat ${tcurl_result} | grep -v '^>' | grep -v '^<' | grep -v '^\*')
    cat ${tcurl_result} | grep "${content_filter}" > /dev/null \
      && echo "[OK: content: '${content_filter}']" \
      || die "bad response, expecting '${content_filter}' in '${result_output}'"

    export TCURL_RESULT=${result}
}

# preparation:
# * remove preferences

# sniper preferences
function test_sniper_preferences(){
    section "module"
    tcurl 200 "SIGTERM" GET \
      http://localhost:10000/module/preferences/plugin/process_sniper/attribute/default_signal
    tcurl 200 "SIGSTOP" PUT \
      http://localhost:10000/module/preferences/plugin/process_sniper/attribute/default_signal \
         '{"value":"SIGSTOP"}'
    tcurl 200 "SIGSTOP" GET \
      http://localhost:10000/module/preferences/plugin/process_sniper/attribute/default_signal
    tcurl 200 "SIGTERM" PUT \
      http://localhost:10000/module/preferences/plugin/process_sniper/attribute/default_signal \
         '{"value":"SIGTERM"}'
    tcurl 200 "SIGTERM" GET \
      http://localhost:10000/module/preferences/plugin/process_sniper/attribute/default_signal

    tcurl 200 "\\[\\]" GET \
      http://localhost:10000/module/preferences/plugin/process_sniper/attribute/extra_signals
    tcurl 200 "SIGTERM" PUT \
      http://localhost:10000/module/preferences/plugin/process_sniper/attribute/extra_signals \
         '{"value":"[{\"signal\":\"SIGTERM\"}, {\"signal\":\"SIGCONT\"}]"}'
    tcurl 200 "\\[\\]" PUT \
      http://localhost:10000/module/preferences/plugin/process_sniper/attribute/extra_signals \
         '{"value":"[]"}'
}

# sniper module
function test_sniper(){
    sleep 30 &
    kill_pid=$!
    tcurl 200 "sleep', '30'" GET http://localhost:10003/module/process_sniper/process
    tcurl 204 '' POST http://localhost:10003/module/process_sniper/process/pid/${kill_pid}/kill/SIGSTOP
    ps a | grep "^${kill_pid}"
    tcurl 204 '' POST http://localhost:10003/module/process_sniper/process/pid/${kill_pid}/kill/SIGCONT
    ps a | grep "^${kill_pid}"
    tcurl 204 '' POST http://localhost:10003/module/process_sniper/process/pid/${kill_pid}/kill/SIGTERM
    ps a | grep "^${kill_pid}"
}


echo "Hallo developer,
This script creates and deletes preferences and therefore has a forced exit
built in. You have to edit the file to start and should be prepared to recreate
all settings.

Always leave the exit line in git.
"
exit
# ^ required, do not remove.
test_sniper_preferences
test_sniper
