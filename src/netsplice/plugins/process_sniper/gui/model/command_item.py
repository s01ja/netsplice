# -*- coding: utf-8 -*-
# command.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for a single command.
'''

from netsplice.model.validator.boolean import boolean as boolean_validator
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.plugins.process_sniper.model.validator.commandline import (
    commandline as commandline_validator
)
from netsplice.plugins.process_sniper.model.validator.event import (
    event as event_validator
)
from netsplice.plugins.process_sniper.model.validator.signal import (
    signal as signal_validator
)


class command_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.active = field(
            required=True,
            validators=[boolean_validator()])

        self.commandline = field(
            required=True,
            validators=[commandline_validator()])

        self.event = field(
            required=True,
            validators=[event_validator()])

        self.signal = field(
            required=True,
            validators=[signal_validator()])

        self.profile = field(
            required=False,
            default=False,
            validators=[boolean_validator()])
