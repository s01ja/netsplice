# -*- coding: utf-8 -*-
# event_plugin.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Event plugin for backend app.

'''

from tornado import gen
import json
import re

import netsplice.backend.event.origins as origins
import netsplice.backend.event.types as types
from netsplice.util import get_logger
from netsplice.util.errors import NotFoundError

from netsplice.plugins.process_sniper.config.events import EVENT_NAMES


logger = get_logger()


class event_plugin(object):
    '''
    Event loop for the backend.

    Requires modules registered and waits for model-change on that modules
    to process events.
    '''

    def __init__(self, application):
        '''
        Initialize members.

        Requires the user to set application and add modules.
        '''
        self.application = application
        self._kill_index = 0

    def _find_rules(self, event_name, connection_id):
        '''
        Find Rules.

        Find Rules for the given connection_id that match the event.
        '''
        rules = []
        try:
            broker = self.application.get_module('connection').broker
            connection = broker.find_connection_by_connection_id(
                connection_id)
            account = connection.account

            kill_list = account.process_sniper.kill_list
            for (index, item) in enumerate(kill_list):
                if not item.active.get():
                    continue
                if item.event.get() != event_name:
                    continue
                rules.append({
                    'id': index,
                    'rule': item})
        except AttributeError:
            # plugin data not available
            pass
        except NotFoundError:
            # account id not found
            pass
        return rules

    def _is_launched_command(self, launched_item, process):
        '''
        Is launched command.

        Try to find out if the commandline of the launched_item matches
        the commandline of the process.
        This function ignores the executable as applications like firefox
        are launched with '/usr/bin/firefox-bin' and are identified in the
        process with '/opt/firefox/firefox'.
        '''
        commandline_launched = json.loads(launched_item.commandline.get())
        is_launch_item = True
        for index in range(1, len(commandline_launched) - 1):
            item = commandline_launched[index]
            if item not in process.commandline.get():
                logger.warn(
                    'Not killing because process item has a signature'
                    ' different from the launcher: %s / %s.'
                    % (item, process.commandline.get()))
                is_launch_item = False
        return is_launch_item

    def _is_launcher_commandline(self, commandline):
        '''
        Is Launcher Commandline.

        The given commandline is a uuid and therefore considered a launcher
        id.
        '''
        try:
            process_launcher = self.application.get_unprivileged().\
                process_launcher
            launched_map = process_launcher.get_launched_map()
            if commandline in launched_map.keys():
                return True
            return False
        except AttributeError:
            return False

    @gen.coroutine
    def _trigger(self, rule_item):
        '''
        Trigger.

        Find the given commandline in the current process list and yield
        a kill with the signal configured to matching processes.
        '''
        rule_id = rule_item['id']
        kill_item = rule_item['rule']
        kill_commandline = kill_item.commandline.get()
        kill_signal = kill_item.signal.get()
        kill_event = kill_item.event.get()
        if self._is_launcher_commandline(kill_commandline):
            yield self._trigger_process_launcher(
                rule_id, kill_event, kill_commandline, kill_signal)
        else:
            yield self._trigger_process_list(
                rule_id, kill_event, kill_commandline, kill_signal)

    @gen.coroutine
    def _trigger_process_launcher(
            self, rule_id, kill_event, kill_id, kill_signal):
        '''
        Trigger on process launcher.

        Send a signal to the process that was started from the process
        launcher if it exists.
        Has launcher launched pid for uuid and this pid is active?
        '''
        process_launcher = None
        try:
            process_launcher = self.application.get_unprivileged().\
                process_launcher
        except AttributeError:
            logger.warn(
                'Process Launcher plugin is not active. Not killing anything.')
            return
        launched_map = process_launcher.get_launched_map()

        process_sniper = self.application.get_unprivileged().process_sniper
        process_list = yield process_sniper.get_process_list()
        launched_item = launched_map[kill_id]
        process_killed = False
        for process in process_list:
            if launched_item.pid.get() != process.pid.get():
                continue
            if not self._is_launched_command(launched_item, process):
                logger.warn(
                    'The pid %s with the commandline %s does not match %s of'
                    ' the launched process. Rule: %d.'
                    % (str(process.pid.get()),
                        process.commandline.get(),
                        launched_item.commandline.get(),
                        rule_id))
                continue
            logger.warn(
                'Kill process pid: %s that was launched by process_launcher'
                ' with the id %s and the commandline %s/%s. Because the event'
                ' %s occured and the pid and id matched. Rule: %d.'
                % (str(launched_item.pid.get()),
                    kill_id,
                    launched_item.commandline.get(),
                    process.commandline.get(),
                    kill_event,
                    rule_id))
            yield process_sniper.kill_process(
                kill_signal, process.pid.get())
            process_killed = True

        if not process_killed:
            logger.error(
                'The given id:%s/pid:%s/commandline:%s was not found'
                ' in the process list. Not killing anything. Rule: %d'
                % (kill_id,
                    str(launched_item.pid.get()),
                    launched_item.commandline.get(),
                    rule_id))

    @gen.coroutine
    def _trigger_process_list(
            self, rule_id, kill_event, kill_commandline, kill_signal):
        '''
        Trigger on process list.

        Trigger the kill by pid/commandline.
        '''
        process_sniper = self.application.get_unprivileged().process_sniper
        process_list = yield process_sniper.get_process_list()
        process_killed = False

        for process in process_list:
            match = re.match(kill_commandline, process.commandline.get())
            if not match:
                continue
            self._kill_index += 1
            logger.warn(
                '%d. Kill process %d with %s, because the event %s occured'
                ' and the commandline (%s) matched (%s). Rule: %d'
                % (self._kill_index,
                    process.pid.get(),
                    kill_signal,
                    kill_event,
                    process.commandline.get(),
                    kill_commandline,
                    rule_id))
            yield process_sniper.kill_process(
                kill_signal, process.pid.get())
            process_killed = True
        if not process_killed:
            logger.error(
                'No process matching the commandline %s was found.'
                ' Not killing anything. Rule: %d'
                % (kill_commandline, rule_id))

    @gen.coroutine
    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        event_name = event_model_instance.name.get()
        event_type = event_model_instance.type.get()
        event_data = event_model_instance.data.get()

        if event_type == types.NOTIFY:
            for rule in self._find_rules(event_name, event_data):
                yield self._trigger(rule)
