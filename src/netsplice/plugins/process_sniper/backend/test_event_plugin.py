import mock
import uuid
import tornado
import logbook

from tornado.testing import AsyncTestCase
from tornado import gen

from .event_plugin import event_plugin
from preferences.model.plugin_collection import (
    plugin_collection as process_sniper_model
)

import netsplice.backend.log as log_module
import netsplice.backend.gui as gui_module
import netsplice.backend.connection as connection_module
import netsplice.backend.connection.connection as connection
import netsplice.backend.preferences as preferences_module
import netsplice.backend.unprivileged as unprivileged_module
import netsplice.backend.event as event_module
import netsplice.backend.event.names as event_names
import netsplice.backend.event.origins as origins
import netsplice.backend.event.types as types
import netsplice.plugins.process_sniper.config.signals as event_signals
from netsplice.plugins.process_sniper.model.process_list import (
    process_list as process_list_model
)
from netsplice.plugins.process_launcher.unprivileged.process_launcher.model.\
    response.process_item import (
        process_item as process_item_model
    )
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.model.event import event as event_model
from netsplice.util.errors import NotFoundError

from netsplice.backend.preferences.model.account_item import (
    account_item as account_item_model
)
from netsplice.util.ipc.application import application as application

import netsplice.config.process as config
import netsplice.config.connection as config_connection
import netsplice.config.flags as flags


LAUNCH_ID = '61c5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_LAUNCH_ID = '71c5aa5a-d1df-4ee8-9ede-86898effe163'


class mock_unprivileged_plugin_dispatcher(object):
    def __init__(self):
        self.get_process_list_return_value = []
        self.kill_process_called = False

    @gen.coroutine
    def get_process_list(self):
        raise gen.Return(self.get_process_list_return_value)

    @gen.coroutine
    def kill_process(self, signal, pid):
        self.kill_process_called = True


class mock_unprivileged_launcher_plugin_dispatcher(object):
    def __init__(self):
        self.get_launched_map_return_value = dict()

    def get_launched_map(self):
        return self.get_launched_map_return_value


class mock_unprivileged_dispatcher(object):
    def __init__(self):
        self.application = None
        self.process_sniper = mock_unprivileged_plugin_dispatcher()
        self.process_launcher = mock_unprivileged_launcher_plugin_dispatcher()

app = application()
app.add_module(log_module)
app.add_module(gui_module)
app.add_module(event_module)
app.add_module(preferences_module)
app.add_module(connection_module)
app.set_unprivileged(mock_unprivileged_dispatcher())

ACCOUNT_ID = '31c5aa5a-d1df-4ee8-9ede-86898effe163'
OTHER_ACCOUNT_ID = '31c5aa5a-d1df-4ee8-9ede-86898effe164'
CONNECTION_ID = 'c1c5aa5a-d1df-4ee8-9ede-86898effe163'


def add_account():
    pmodel = app.get_module('preferences').model
    account = account_item_model(None)
    account.id.set(ACCOUNT_ID)
    account.name.set('test')
    account.type.set_value(None)
    account.configuration.set('remote host')
    account.import_configuration.set('remote host')
    account.enabled.set(True)
    account.default_route.set(False)
    account.autostart.set(True)
    account.process_sniper = process_sniper_model(None)

    account.process_sniper.kill_list.append(get_kill_item())

    pmodel.accounts.append(account)
    connection_instance = connection(connection_module.broker, account)
    connection_instance.id.set(CONNECTION_ID)
    connection_instance.account = account
    connection_module.broker.connections[ACCOUNT_ID] = (
        connection_instance, None)


def get_event_plugin():
    e = event_plugin(app)
    pmodel = app.get_module('preferences').model
    del pmodel.accounts[:]
    return e


def get_kill_item(account_id=None):
    li = None
    pmodel = app.get_module('preferences').model
    try:
        account = pmodel.accounts.find_by_id(account_id)
        li = account.process_sniper.kill_list[0]
    except NotFoundError:
        li = process_sniper_model(None).kill_list.item_model_class()
    li.active.set(True)
    li.event.set(config_connection.CONNECTED)
    return li


def test__find_rules_returns_empty_when_no_rules_for_account():
    e = get_event_plugin()
    result = e._find_rules('name', 'invalid_account_id')
    assert(result == [])


def test__find_rules_returns_empty_when_no_process_sniper_plugin():
    e = get_event_plugin()
    add_account()
    pmodel = app.get_module('preferences').model
    account = pmodel.accounts[0]
    del account.process_sniper

    result = e._find_rules(config_connection.CONNECTED, CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(result == [])


def test__find_rules_returns_empty_when_no_process_sniper_plugin_values():
    e = get_event_plugin()
    add_account()
    pmodel = app.get_module('preferences').model
    account = pmodel.accounts[0]
    del account.process_sniper.kill_list

    result = e._find_rules(config_connection.CONNECTED, CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(result == [])


def test__find_rules_returns_empty_when_no_active_kill_items():
    e = get_event_plugin()
    add_account()

    ki = get_kill_item(CONNECTION_ID)
    ki.active.set(False)

    result = e._find_rules('undefined_event', CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(result == [])


def test__find_rules_returns_empty_when_no_match_event_in_launch_items():
    e = get_event_plugin()
    del app.get_module('preferences').model.accounts[:]
    add_account()

    result = e._find_rules(config_connection.DISCONNECTED, CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(result == [])


def test__find_rules_returns_launch_items():
    e = get_event_plugin()
    del app.get_module('preferences').model.accounts[:]
    add_account()

    result = e._find_rules(config_connection.CONNECTED, CONNECTION_ID)
    del app.get_module('preferences').model.accounts[:]
    assert(len(result) == 1)


class EventLoopTests(AsyncTestCase):
    '''
    Event Loop Tests.

    Checks the async event_loop methods work
    '''

    @tornado.testing.gen_test
    def test__is_launched_command_with_matching_commandline_returns_true(
            self):
        el = get_event_plugin()
        li = process_item_model()
        pi = process_item_model()
        li.commandline.set('["a", "b", "c"]')
        pi.commandline.set('["a", "b", "c"]')
        result = el._is_launched_command(li, pi)
        assert(result is True)

    @tornado.testing.gen_test
    def test__is_launched_command_with_notmatching_commandline_returns_false(
            self):
        el = get_event_plugin()
        li = process_item_model()
        pi = process_item_model()
        li.commandline.set('["a", "b", "c"]')
        pi.commandline.set('["c", "d", "e"]')
        result = el._is_launched_command(li, pi)
        assert(result is False)

    @tornado.testing.gen_test
    def test__is_launcher_no_launcher_plugin_returns_false(
            self):
        el = get_event_plugin()
        o_process_launcher = app.get_unprivileged().process_launcher
        del(app.get_unprivileged().process_launcher)

        result = el._is_launcher_commandline('not matching commandline')
        app.get_unprivileged().process_launcher = o_process_launcher
        assert(result is False)

    @tornado.testing.gen_test
    def test__is_launcher_not_in_launcher_map_returns_false(
            self):
        el = get_event_plugin()
        lm = dict()
        pim = process_item_model()
        pim.pid.set(999)
        lm[LAUNCH_ID] = pim
        app.get_unprivileged().process_launcher.\
            get_launched_map_return_value = lm
        result = el._is_launcher_commandline('not matching commandline')
        assert(result is False)

    @tornado.testing.gen_test
    def test__is_launcher_in_launcher_map_returns_true(
            self):
        el = get_event_plugin()
        lm = dict()
        pim = process_item_model()
        pim.pid.set(999)
        lm[LAUNCH_ID] = pim
        app.get_unprivileged().process_launcher.\
            get_launched_map_return_value = lm
        result = el._is_launcher_commandline(LAUNCH_ID)
        assert(result is True)

    @mock.patch(
        'netsplice.plugins.process_sniper.backend.event_plugin.'
        '_trigger_process_launcher')
    @mock.patch(
        'netsplice.plugins.process_sniper.backend.event_plugin.'
        '_is_launcher_commandline', return_value=True)
    @tornado.testing.gen_test
    def test__trigger_calls_process_launcher(
            self, mock__is_launcher_commandline,
            mock__trigger_process_launcher):
        ki = get_kill_item()
        el = get_event_plugin()
        el._trigger({
            'id': 0,
            'rule': ki
        })
        mock__trigger_process_launcher.assert_called()

    @mock.patch(
        'netsplice.plugins.process_sniper.backend.event_plugin.'
        '_trigger_process_list')
    @mock.patch(
        'netsplice.plugins.process_sniper.backend.event_plugin.'
        '_is_launcher_commandline', return_value=False)
    @tornado.testing.gen_test
    def test__trigger_calls_process_list(
            self, mock__is_launcher_commandline,
            mock__trigger_process_list):
        ki = get_kill_item()
        el = get_event_plugin()
        el._trigger({
            'id': 0,
            'rule': ki
        })
        mock__trigger_process_list.assert_called()

    @tornado.testing.gen_test
    def test__trigger_process_launcher_does_nothing_without_proces_launcher(
            self):
        el = get_event_plugin()
        o_process_launcher = app.get_unprivileged().process_launcher
        del(app.get_unprivileged().process_launcher)

        with logbook.TestHandler() as log_handler:
            el._trigger_process_launcher(
                0, event_names.ACCOUNT_CONNECTED, LAUNCH_ID, 'SIGINT')
            app.get_unprivileged().process_launcher = o_process_launcher
            assert(log_handler.has_warnings is True)

    @tornado.testing.gen_test
    def test__trigger_process_launcher_logs_error_when_nothing_killed(
            self):
        el = get_event_plugin()
        pm = process_list_model()
        pmi = pm.item_model_class()
        pmi.commandline.set('something to match negatively')
        pmi.pid.set(1)
        pm.append(pmi)
        lm = dict()
        pim = process_item_model()
        pim.pid.set(999)
        lm[LAUNCH_ID] = pim

        app.get_unprivileged().process_sniper.\
            get_process_list_return_value = pm
        app.get_unprivileged().process_launcher.\
            get_launched_map_return_value = lm

        with logbook.TestHandler() as log_handler:
            el._trigger_process_launcher(
                0, event_names.ACCOUNT_CONNECTED, LAUNCH_ID, 'SIGINT')
            assert(log_handler.has_errors is True)

    @mock.patch(
        'netsplice.plugins.process_sniper.backend.event_plugin.'
        '_is_launched_command', return_value=False)
    @tornado.testing.gen_test
    def test__trigger_process_launcher_logs_warn_when_not_launched_command(
            self, mock__is_launched_command):
        el = get_event_plugin()
        pm = process_list_model()
        pmi = pm.item_model_class()
        pmi.commandline.set('something to match negatively')
        pmi.pid.set(999)
        pm.append(pmi)
        lm = dict()
        pim = process_item_model()
        pim.pid.set(999)
        lm[LAUNCH_ID] = pim

        app.get_unprivileged().process_sniper.\
            get_process_list_return_value = pm
        app.get_unprivileged().process_launcher.\
            get_launched_map_return_value = lm

        with logbook.TestHandler() as log_handler:
            el._trigger_process_launcher(
                0, event_names.ACCOUNT_CONNECTED, LAUNCH_ID, 'SIGINT')
            assert(log_handler.has_warnings is True)

    @mock.patch(
        'netsplice.plugins.process_sniper.backend.event_plugin.'
        '_is_launched_command', return_value=True)
    @tornado.testing.gen_test
    def test__trigger_process_launcher_logs_warn_when_killing_process(
            self, mock__is_launched_command):
        el = get_event_plugin()
        pm = process_list_model()
        pmi = pm.item_model_class()
        pmi.commandline.set('something to match negatively')
        pmi.pid.set(999)
        pm.append(pmi)
        lm = dict()
        pim = process_item_model()
        pim.pid.set(999)
        lm[LAUNCH_ID] = pim

        app.get_unprivileged().process_sniper.\
            get_process_list_return_value = pm
        app.get_unprivileged().process_launcher.\
            get_launched_map_return_value = lm

        with logbook.TestHandler() as log_handler:
            el._trigger_process_launcher(
                0, event_names.ACCOUNT_CONNECTED, LAUNCH_ID, 'SIGINT')
            assert(log_handler.has_warnings is True)
        assert(
            app.get_unprivileged().process_sniper.kill_process_called
            is True)

    @tornado.testing.gen_test
    def test__trigger_process_list_logs_error_when_nothing_matches(
            self):
        el = get_event_plugin()
        pm = process_list_model()
        pmi = pm.item_model_class()
        pmi.commandline.set('something to match negatively')
        pmi.pid.set(1)
        pm.append(pmi)
        app.get_unprivileged().process_sniper.get_process_list_return_value = \
            pm
        with logbook.TestHandler() as log_handler:
            el._trigger_process_list(
                0, event_names.ACCOUNT_CONNECTED, '.*nomatch.*', 'SIGINT')
            assert(log_handler.has_errors is True)

    @tornado.testing.gen_test
    def test__trigger_process_list_logs_warn_when_something_matches(
            self):
        el = get_event_plugin()
        pm = process_list_model()
        pmi = pm.item_model_class()
        pmi.commandline.set('something to match positively')
        pmi.pid.set(1)
        pm.append(pmi)
        app.get_unprivileged().process_sniper.get_process_list_return_value = \
            pm

        with logbook.TestHandler() as log_handler:
            el._trigger_process_list(
                0, event_names.ACCOUNT_CONNECTED, '.*match.*', 'SIGINT')
            assert(log_handler.has_warnings is True)

    @mock.patch(
        'netsplice.plugins.process_sniper.backend.event_plugin.'
        '_find_rules')
    @mock.patch(
        'netsplice.plugins.process_sniper.backend.event_plugin.'
        '_trigger')
    @tornado.testing.gen_test
    def test_process_event(self, mock__trigger, mock__find_rules):
        el = get_event_plugin()
        emi = event_model()
        emi.name.set(event_names.ACCOUNT_CONNECTED)
        emi.origin.set(origins.BACKEND)
        emi.type.set(types.NOTIFY)
        emi.data.set(CONNECTION_ID)
        mock__find_rules.return_value = ['mock_rule']
        el.process_event(emi)
        mock__trigger.assert_called()
