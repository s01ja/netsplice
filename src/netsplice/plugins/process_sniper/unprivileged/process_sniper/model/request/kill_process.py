# -*- coding: utf-8 -*-
# kill.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for killing applications.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.plugins.process_sniper.model.validator.pid import (
    pid as pid_validator
)
from netsplice.plugins.process_sniper.model.validator.signal import (
    signal as signal_validator
)


class kill_process(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.pid = field(
            required=True,
            validators=[pid_validator()])

        self.signal = field(
            required=True,
            validators=[signal_validator()])
