# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

PLUGIN_NAME = 'process_launcher'

FIELD_ID = 'id'
FIELD_ACTIVE = 'active'
FIELD_ELEVATED = 'elevated'
FIELD_EVENT_NAME = 'event_name'
FIELD_EXECUTABLE = 'executable'
FIELD_PARAMETERS = 'parameters'
FIELD_WORKING_DIRECTORY = 'working_directory'
FIELD_PROFILE = 'profile'
KEY_REMOVED_PREFIX = '##UI-REMOVED:'
