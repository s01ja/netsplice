# -*- coding: utf-8 -*-
# templates.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for Launcher Templates
'''
from netsplice.config import connection as config_connection


TEMPLATE_GROUPS = [
    'General',
    'Applications',
    'DNS',
    'Security'
]

TEMPLATES = [
    {
        'event_name': config_connection.UP,
        'executable': '',
        'parameters': [],
        'working_directory': '',
        'elevated': False,
        'platform_filter': None,
        'description': '''
            <p><b>Normal Launcher</b>
            <p>Launch any application installed in the system without
            administrative privileges. Pass parameters to the application
            if required.
            </p>
            ''',
        'groups': ['General']
    },
    {
        'event_name': config_connection.UP,
        'executable': '',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': None,
        'description': '''
            <p><b>Elevated Launcher</b>
            <p>Launch a predefined application or script with elevated
            privileges. This can be useful when an operation like
            <i>route</i> needs to modify the system when the connection is
            activated.
            <p>This requires advanced knowledge how to write scripts.
            ''',
        'groups': ['General']
    },
    {
        'event_name': config_connection.UP,
        'executable': 'linux_resolvconf_set_dns.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['!win32', '!darwin'],
        'description': '''
            <p><b>DNS Leak Protection</b>
            <p>Configure DNS from the DHCP options a server may push.
            <p>This is the raw override for systems without a local DNS
            cache. Most distributions come with a cache. Read the documentation
            about how to override the DNS servers from the local network
            properly.
            <p>Remember to define a Restore DNS rule too.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.DOWN,
        'executable': 'linux_resolvconf_restore_dns.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['!win32', '!darwin'],
        'description': '''
            <p><b>Restore DNS</b>
            <p>This is the raw restore for systems without a local DNS
            cache. When no 'backup' is available, this script does nothing.
             ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.UP,
        'executable': 'linux_systemd_set_dns.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['!win32', '!darwin'],
        'description': '''
            <p><b>DNS Leak Protection for systemd</b>
            <p>Configure DNS from the DHCP options a server may push.
            <p>This is an override for systems with a local DNS cache.
            <p>Remember to define a restore DNS rule too.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.DOWN,
        'executable': 'linux_systemd_restore_dns.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['!win32', '!darwin'],
        'description': '''
            <p><b>Restore DNS for systemd</b>
            <p>This is a restore for systems with a local DNS cache.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.UP,
        'executable': 'linux_check_nslookup_domain.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['!win32'],
        'description': '''
            <p><b>DNS Check</b>
            <p>Check that the nameserver(s) the server has pushed are used
            for resolving the domain of the server.
            <p>
            This script will cause the account to disconnect if a server that
            is not in the list of nameservers responds.
            <p>Configure this after a set_dns rule.
            ''',
        'groups': ['DNS', 'Security']
    },
    {
        'event_name': config_connection.UP,
        'executable': 'win32_disable_multicast_resolution.cmd',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['win32'],
        'description': '''
            <p><b>Disable Link-Local Multicast Name Resolution</b>
            <p>Sets a registry entry that instructs Windows to avoid
            multicast resolution (Link-Local Multicast Name Resolution
            (LLMNR)).
            <p>LLMNR is a secondary name resolution protocol. With LLMNR
            queries are sent using multicast over a local network link on a
            single subnet from a client computer to another client computer
            on the same subnet that also has LLMNR enabled.</p>
            <p>If you use this script LLMNR will be disabled on all
            available network adapters.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.UP,
        'executable': 'win32_disable_parallel_ipv6_resolution.cmd',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['win32'],
        'description': '''
            <p><b>Disable parallel IPv6 Resolution</b>
            <p>Sets a registry entry that instructs Windows to disable
            parallel IPv6 Resolution.
            <p>It is the default on Windows that DNS A and AAAA queries are
            executed in parallel on all configured DNS servers, with the
            fastest response being accepted first.
            <p>If you use this script, parallel IPv6 Resolution will be
            disabled on all available network adapters.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.UP,
        'executable': 'win32_disable_smart_multihome_resolution.cmd',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['win32'],
        'description': '''
            <p><b>Disable Smart Multi-Homed Name Resolution</b>
            <p>Sets a registry entry that instructs Windows to disable
            Smart Multi-Homed Resolution (SMHNR).
            <p>Specifies that a Multi-Homed DNS client should optimize name
            resolution across networks.
            <p>If you use this script, Smart Multi-Homed Resolution will be
            disabled on all available network adapters.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.DOWN,
        'executable': 'win32_enable_multicast_resolution.cmd',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['win32'],
        'description': '''
            <p><b>Enable Link-Local Multicast Name Resolution</b>
            <p>Sets a registry entry that instructs Windows to use
            multicast resolution (Link-Local Multicast Name Resolution
            (LLMNR)).
            <p>LLMNR is a secondary name resolution protocol. With LLMNR
            queries are sent using multicast over a local network link on a
            single subnet from a client computer to another client computer on
            the same subnet that also has LLMNR enabled.</p>
            <p>If you use this script, LLMNR will be enabled on all available
            network adapters.
            <p>Use this to restore a previously disabled Multicast Resolution.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.DOWN,
        'executable': 'win32_enable_parallel_ipv6_resolution.cmd',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['win32'],
        'description': '''
            <p><b>Enable parallel IPv6 Resolution</b>
            <p>Sets a registry entry that instructs Windows to enable
            parallel ipv6 resolution.
            <p>DNS A and AAAA queries are executed in parallel on all
            configured DNS servers, with the fastest response being
            theoretically accepted first is the default on Windows.
            <p>If you use this script, parallel IPv6 will be enabled on all
            available network adapters.
            <p>Use this to restore a previously disabled parallel IPv6
            Resolution.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.DOWN,
        'executable': 'win32_enable_smart_multihome_resolution.cmd',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['win32'],
        'description': '''
            <p><b>Enable Smart Multi-Homed Name Resolution</b>
            <p>Sets a registry entry that instructs Windows to enable
            Smart Multi-Home Resolution.
            <p>Smart Multi-Homed Resolution (SMHNR) specifies that a
            multi-homed DNS client should optimize name resolution across
            networks.
            <p>If you use this script, smart multihome resolution will be
            enabled on all available network adapters.
            <p>Use this to restore a previously disabled Smart Multi-Homed
            Resolution.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.UP,
        'executable': 'osx_networksetup_set_dns.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['darwin'],
        'description': '''
            <p><b>Set DNS for OSX</b>
            <p>Sets the DNS server and the DNS search domain to the values
            provided by the VPN service on OSX.
            <p>If you use this script, the DNS servers of all interfaces will
            be reconfigured.
            <p>Remember to add a restore DNS rule too.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.DOWN,
        'executable': 'osx_networksetup_restore_dns.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['darwin'],
        'description': '''
            <p><b>Restore DNS for OSX</b>
            <p>Restores the DNS configuration to the defaults for <b>all</b>
            interfaces.
            <p>If you use this script, DNS servers and DNS domain will be
            reset to the values provided by DHCP for all interfaces.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.UP,
        'executable': 'linux_nm_set_dns.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['!darwin', '!win32'],
        'description': '''
            <p><b>Set DNS for NetworkManager</b>
            <p>Sets the DNS server and the DNS search domain to the values
            provided by the VPN service on OSX.
            <p>If you use this script, the DNS servers of all interfaces will
            be reconfigured.
            <p>Remember to add a restore DNS rule too.
            ''',
        'groups': ['DNS']
    },
    {
        'event_name': config_connection.DOWN,
        'executable': 'linux_nm_restore_dns.sh',
        'parameters': [],
        'working_directory': '',
        'elevated': True,
        'platform_filter': ['!darwin', '!win32'],
        'description': '''
            <p><b>Restore DNS for NetworkManager</b>
            <p>Restores the DNS configuration to the defaults for <b>all</b>
            interfaces.
            <p>If you use this script, DNS servers and DNS domain will be
            reset to the values provided by DHCP for all interfaces.
            ''',
        'groups': ['DNS']
    }]
