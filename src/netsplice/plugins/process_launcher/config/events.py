# -*- coding: utf-8 -*-
# event.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

import netsplice.backend.event.names as names
from netsplice.config import connection as config_connection
from netsplice.config import connection_broker as config_connection_broker
from netsplice.plugins.process_launcher.backend.event import (
    names as event_names
)

EVENT_NAMES = [
    config_connection.UP,
    config_connection.DOWN,
    config_connection.ABORTED,
    config_connection.CREATED,
    config_connection.SETTING_UP,
    config_connection.CONNECTING,
    config_connection.CONNECTED,
    config_connection.RECONNECTED,
    config_connection.DISCONNECTING,
    config_connection.DISCONNECTING_FAILURE,
    config_connection.DISCONNECTED,
    config_connection.DISCONNECTED_FAILURE,
    config_connection.RECONNECTING
]

# Events emitted into the backend
OUTGOING = [
    config_connection.DISCONNECT,
    names.ERROR,
    names.ACCOUNT_REQUEST_DISCONNECT
]

# Events from the backend
INCOMMING = [
    config_connection.UP,
    config_connection.DOWN,
    config_connection.ABORTED,
    config_connection.CREATED,
    config_connection.SETTING_UP,
    config_connection.CONNECTING,
    config_connection.CONNECTED,
    config_connection.RECONNECTED,
    config_connection.DISCONNECTING,
    config_connection.DISCONNECTING_FAILURE,
    config_connection.DISCONNECTED,
    config_connection.DISCONNECTED_FAILURE,
    config_connection.RECONNECTING
]

# Events from the backend.privileged.event_controller
INCOMMING_PRIVILEGED = [
    event_names.PROCESS_LAUNCHER_MODEL
]

# Events from the backend.unprivileged.event_controller
INCOMMING_UNPRIVILEGED = [
    event_names.PROCESS_LAUNCHER_MODEL
]
