# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
import sys
import os

NETWORK_MANAGER = 'NetworkManager'
RESOLVCONF_FILENAME = '/etc/resolv.conf'


def has_networkmanager():
    '''
    Has networkmanager.

    Check if the resolv.conf is controlled by the NetworkManager. This is
    useful to use the systemd elevated scripts to modify the systemd dns
    instead of fighting over the /etc/resolv.conf.

    Returns:
        bool -- The resolv.conf is a symlink to .*NetworkManager.*/resolv.conf
    '''
    if not os.path.islink(RESOLVCONF_FILENAME):
        return False
    full_path = os.path.realpath(RESOLVCONF_FILENAME)
    if NETWORK_MANAGER in full_path:
        return True
    return False


def get_dns_launcher_for_platform():
    '''
    Get DNS Launcher for platform.

    Return a set of launchers that ensure a correct DNS for the current
    platform.

    Returns:
        list -- List of launchers.
    '''
    launcher = [
        'linux_resolvconf_set_dns.sh',
        'linux_resolvconf_restore_dns.sh'
    ]

    if has_networkmanager():
        launcher = [
            'linux_nm_set_dns.sh',
            'linux_nm_restore_dns.sh'
        ]

    if sys.platform.startswith('darwin'):
        launcher = [
            'osx_networksetup_set_dns.sh',
            'osx_networksetup_restore_dns.sh'
        ]

    if sys.platform.startswith('win32'):
        launcher = [
            'win32_disable_multicast_resolution.cmd',
            'win32_disable_parallel_ipv6_resolution.cmd',
            'win32_disable_smart_multihome_resolution.cmd',
            'win32_enable_multicast_resolution.cmd',
            'win32_enable_parallel_ipv6_resolution.cmd',
            'win32_enable_smart_multihome_resolution.cmd',
        ]
    return launcher
