# -*- coding: utf-8 -*-
# parameter_list.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Validator Parameter Lists.
'''

from netsplice.util.model.validator import validator
from netsplice.util import basestring


class parameter_list(validator):
    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Parameter lists are lists of strings. The list may be empty, the
        values in the list may be empty.
        '''
        if not isinstance(value, (list)):
            return False
        if len(value) is 0:
            return True
        for lvalue in value:
            if not isinstance(lvalue, (basestring,)):
                return False
        return True
