# -*- coding: utf-8 -*-
# process.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''
import json

from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util import get_logger

logger = get_logger()


class process(object):
    '''
    '''
    def __init__(self):
        pass

    def launch(
            self, executable, parameters, working_directory, environment):
        '''
        Launch.

        Launch the given executable with the given parameters as a background
        process in the given working directory. The process is detached from
        the unprivileged process so it stays open when the application closes.

        Returns the pid and commandline of the new process.
        '''
        dispatcher = process_dispatcher(
            executable, parameters, None)
        dispatcher.set_working_directory(working_directory)
        dispatcher.set_detach(True)
        dispatcher.set_environment(environment)
        dispatcher.start_application()
        new_pid = dispatcher.get_subprocess_pid()
        logger.info(
            'Started command %s with %s in %s. Got PID: %s'
            % (executable, json.dumps(parameters), working_directory,
                str(new_pid)))

        commandline = json.dumps(dispatcher.get_commandline())
        return (new_pid, commandline)
