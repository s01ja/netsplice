# -*- coding: utf-8 -*-
# gui.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Config for About
'''

DESCRIPTION = '''
<p><b>Process Manager</b> is an account plugin that is able to start
and stop a process when the connection state changes.</p>
'''

LAUNCHER_LABEL = '''
<p>Rules to <b>start</b> external applications.</p>
'''
SNIPER_LABEL = '''
<p>Rules to <b>stop</b> external applications.</p>
'''
