# -*- coding: utf-8 -*-
# backend_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from tornado import gen, httpclient, ioloop
from netsplice.util.ipc.service import service
import netsplice.privileged.dispatcher_endpoints as endpoints


class backend_dispatcher(service):

    def __init__(self, host, port):
        service.__init__(
            self, host, port, None, None,
            'backend', 'privileged')

    @gen.coroutine
    def log(self, log_model_instance):
        body = log_model_instance.to_json()
        try:
            yield self.post(endpoints.BACKEND_PRIVILEGED_LOG, body)
        except httpclient.HTTPError:
            ioloop.IOLoop.current().stop()

    @gen.coroutine
    def event(self, event_model_instance):
        body = event_model_instance.to_json()
        try:
            yield self.post(endpoints.BACKEND_PRIVILEGED_EVENT, body)
        except httpclient.HTTPError:
            ioloop.IOLoop.current().stop()
