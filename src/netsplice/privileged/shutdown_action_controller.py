# -*- coding: utf-8 -*-
# shutdown_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from tornado import ioloop

from netsplice.config import ipc as config
from netsplice.util.ipc.middleware import middleware


class shutdown_action_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def post(self):
        '''
        Shutdown Privileged Server.
        '''
        ioloop.IOLoop.current().call_later(
            config.SHUTDOWN_WAIT_TIME, lambda: ioloop.IOLoop.current().stop())
        self.finish()
