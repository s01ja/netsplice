# -*- coding: utf-8 -*-
# event_loop.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Event Loop for unprivileged App. Waits for model changes and signals them to
the owning process (backend).
'''

from netsplice.util.ipc.event_loop import event_loop as event_loop_base


class event_loop(event_loop_base):
    def __init__(self):
        event_loop_base.__init__(self, 'Unprivileged')
