#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Backend Application.

Backend part of the application which handles the connection management etc.
'''

import atexit
import sys
import time

import netsplice.backend as backend_module
import netsplice.backend.connection as connection_module
import netsplice.backend.event as event_module
import netsplice.backend.gui as gui_module
import netsplice.backend.log as log_module
import netsplice.backend.net as net_module
import netsplice.backend.preferences as preferences_module
import netsplice.backend.privileged as privileged_module
import netsplice.backend.unprivileged as unprivileged_module

from netsplice import __version__ as VERSION
from netsplice.util import get_logger, take_logs

from netsplice.backend.gui_application_dispatcher import (
    gui_application_dispatcher
)
from netsplice.backend.event_loop import event_loop
from netsplice.backend.event import names as event_names
from netsplice.backend.network_dispatcher import network_dispatcher
from netsplice.backend.privileged_dispatcher import privileged_dispatcher
from netsplice.backend.unprivileged_dispatcher import unprivileged_dispatcher
from netsplice.config import flags
from netsplice.config import constants
from netsplice.config import backend as config
from netsplice.config import process as config_process
from netsplice.model.backend import backend as backend_model
from netsplice.plugins import get_plugins
from netsplice.util import commandline
from netsplice.util.errors import AccessError
from netsplice.util.ipc.certificates import certificates
from netsplice.util.ipc.errors import (
    ServerStartFailedError,
    InvalidSharedSecretError
)
from netsplice.util.ipc.route import get_route
from netsplice.util.ipc.shared_secret import shared_secret
from netsplice.util.ipc.server import server
from netsplice.util.process.reaper import reaper

logger = get_logger()


def startup_progress(percent, message):
    '''
    Display startup progress.

    Display a message on stdout that will be interpreted by the GUI.
    '''
    sys.stdout.write(
        '%s: %d %s\n'
        % (config.BACKEND_STARTUP_MESSAGE, percent, message))
    sys.stdout.flush()


def startup_complete(app):
    '''
    Startup complete.

    Display final startup message and trigger a backend event for autostart
    functions.
    '''
    startup_progress(100, config.BACKEND_READY_MESSAGE)
    app.get_module('event').notify(event_names.BACKEND_READY)


@atexit.register
def stop_backend():
    '''
    Handle shutdown.

    Print unpublished log entries.
    '''
    for log_item in take_logs():
        message = ''
        level = 'INFO'
        if isinstance(log_item, (dict,)):
            message = log_item['message']
            level = log_item['level']
        else:
            message = log_item.message.get()
            level = log_item.level.get()
        try:
            if level in ['ERROR', 'CRITICAL']:
                sys.stderr.write(message + '\n')
            else:
                sys.stdout.write(message + '\n')
        except Exception as errors:
            sys.stderr(str(errors))
            pass
        sys.stdout.flush()


def run_app():
    '''
    Run App.

    Starts the main event loop and launches the main window.
    '''
    # Parse arguments and store them
    options = commandline.get_options()

    flags.DEBUG = options.debug

    logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')
    logger.info('Netsplice version %s' % VERSION)
    logger.info('~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~')

    if options.host is not None:
        constants.LOCALHOST = options.host

    if options.port is not None:
        constants.PORT_BACKEND = options.port
        constants.PORT_NET = constants.PORT_BACKEND + 1
        constants.PORT_PRIV = constants.PORT_BACKEND + 2
        constants.PORT_UNPRIV = constants.PORT_BACKEND + 3

    # Defaults
    backend_host = constants.LOCALHOST
    backend_port = constants.PORT_BACKEND
    network_host = constants.LOCALHOST
    network_port = constants.PORT_NET
    privileged_host = constants.LOCALHOST
    privileged_port = constants.PORT_PRIV
    unprivileged_host = constants.LOCALHOST
    unprivileged_port = constants.PORT_UNPRIV

    try:
        preferences_module.model.get_reader().check_access()
        preferences_module.model.get_writer().check_access()
        preferences_module.model.load()
    except AccessError as errors:
        logger.error(
            'Cannot read or write the preferences.'
            ' Make sure you can read and write the config location.'
            ' %s'
            % (str(errors),))
        sys.exit(1)

    reaper_instance = reaper()
    if reaper_instance.kill_other_subprocess(sys.argv):
        time.sleep(config_process.WAIT_AFTER_KILL_OTHER)


    startup_progress(0, 'Initialize certificates')
    try:
        app_certificates = certificates()
        app_certificates.check_access()
        app_certificates.set_progress_handler(5, 25, startup_progress)
        app_certificates.setup()
    except AccessError as errors:
        logger.error(
            'Cannot read the certificates.'
            ' Make sure you can read and write the config location.'
            ' %s'
            % (str(errors),))
        sys.exit(1)

    startup_progress(30, 'Initialize shared secret')
    try:
        shared_secret.check_access()
        shared_secret.setup(options.hmac_secret)
    except IOError as errors:
        logger.error(
            'Failed to write shared secret file %s.'
            % (shared_secret.get_filename(),))
        import traceback
        logger.error(''.join(traceback.format_exc(errors)))
        logger.error(str(errors))
        sys.exit(1)
    except InvalidSharedSecretError:
        logger.error('Bad hmac_secret provided.')
        sys.exit(1)
    except AccessError as errors:
        logger.error(
            'Cannot use shared secret.'
            ' Make sure you can read and write the config location.'
            ' %s'
            % (str(errors),))
        sys.exit(1)
    logger.debug('Configure application glue')
    app = server(backend_host, backend_port, 'backend')
    app.set_network(network_dispatcher(
        network_host, network_port, backend_host, backend_port))
    app.set_model(backend_model())
    app.set_unprivileged(unprivileged_dispatcher(
        unprivileged_host, unprivileged_port, backend_host, backend_port))
    app.set_privileged(privileged_dispatcher(
        privileged_host, privileged_port, backend_host, backend_port))
    app.set_gui(gui_application_dispatcher(app))
    startup_progress(50, 'Start event loop')
    app.set_event_loop(event_loop())

    logger.debug('Add Modules')
    app.add_module(connection_module)
    app.add_module(gui_module)
    app.add_module(net_module)
    app.add_module(preferences_module)
    app.add_module(privileged_module)
    app.add_module(unprivileged_module)
    app.add_module(event_module)
    app.add_module(log_module)
    backend_module.register_module_events(event_module)
    connection_module.register_module_events(event_module)
    gui_module.register_module_events(event_module)
    net_module.register_module_events(event_module)
    privileged_module.register_module_events(event_module)
    unprivileged_module.register_module_events(event_module)
    startup_progress(60, 'Start plugins')
    for plugin in get_plugins():
        app.add_plugin(plugin)
    app.register_plugin_components('util')
    app.register_plugin_components('backend')

    try:
        startup_progress(70, 'Start backend')
        app.start(get_route('netsplice.backend'))
        # Backend needs to listen() before any dispatchers launch
        # This is important on systems that require user-interaction to
        # elevate the privileged dispatcher
        startup_progress(75, 'Start network dispatcher')
        app.get_network().start()
        startup_progress(80, 'Start unprivileged dispatcher')
        app.get_unprivileged().start()
        startup_progress(85, 'Start privileged dispatcher')
        app.get_privileged().start()
        startup_progress(90, 'Wait for dispatchers')
        app.wait_for_subprocesses([
            app.get_network(), app.get_privileged(), app.get_unprivileged()],
            startup_progress, 90, 10,
            lambda: startup_complete(app))
        sys.exit(app.exec_())
    except ServerStartFailedError:
        logger.critical('Application server failed to start.')
        sys.exit(1)


if __name__ == '__main__':
    run_app()
