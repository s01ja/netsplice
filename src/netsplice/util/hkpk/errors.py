# -*- coding: utf-8 -*-
# errors.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
HKPK Errors.

Collection of errors that may occur in the HKPK process.
'''


class CanceledError(Exception):
    '''
    Canceled Error.

    Cancel a download in the process.

    Extends:
        Exception
    '''
    def __init__(self):
        Exception.__init__(self)


class ChecksumError(Exception):
    '''
    Checksum Error.

    Invalid Checksum.

    Extends:
        Exception
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class InvalidError(Exception):
    '''
    Invalid Error.

    Invalid HTTPS Certificate Fingerprint.

    Extends:
        Exception
    '''
    FP_WIDTH = 2
    def __init__(self, invalid_pk_sha256, remote_host, valid_pk_sha256):
        valid_formated_fps = []
        for fingerprint in valid_pk_sha256:
            valid_formated_fps.append(self._format_fingerprint(fingerprint))
        formatted_valid_fingerprints = ',\n'.join(
            valid_formated_fps)
        formatted_invalid_fingerprint = self._format_fingerprint(
            invalid_pk_sha256)

        Exception.__init__(
            self,
            'The TLS fingerprint \n%s\nis not valid for %s.'
            ' It should be one of \n%s'
            % (formatted_invalid_fingerprint,
                remote_host,
                formatted_valid_fingerprints))

    def _format_fingerprint(self, fingerprint):
        '''
        Format Fingerprint.

        Format a fingerprint for display.

        Arguments:
            fingerprint -- string

        Returns:
            string -- formatted fingerprint
        '''
        blocks = []
        if fingerprint is None:
            return ''
        for offset in range(0, len(fingerprint), self.FP_WIDTH):
            blocks.append(fingerprint[offset:offset + self.FP_WIDTH])
        return ' '.join(blocks)


class MaxBufferError(Exception):
    '''
    Max Buffer Error.

    Too much data is downloaded.

    Extends:
        Exception
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class NetworkError(Exception):
    '''
    Network Error.

    Network error (timeout/connect) during download.

    Extends:
        Exception
    '''
    def __init__(self, message, status=None):
        Exception.__init__(self, message)
        self.status = status


class SignatureError(Exception):
    '''
    Signature Error.

    Invalid Signature or Signature does not match.

    Extends:
        Exception
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class TargetError(Exception):
    '''
    Target Error.

    Invalid target file (file exists, cannot be written, no disk space etc).

    Extends:
        Exception
    '''
    def __init__(self, message):
        Exception.__init__(self, message)
