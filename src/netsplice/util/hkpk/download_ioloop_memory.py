# -*- coding: utf-8 -*-
# download_ioloop_memory.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
HKPK HTTPS download in a ioloop

Pinned certificate HTTPS requests.

This implementation returns a unicode string with the received data.
'''
import time
from tornado import gen
from urllib3.exceptions import (
    MaxRetryError, ReadTimeoutError, HostChangedError
)
from netsplice.util import get_logger
from .download import download
from .errors import (
    CanceledError, ChecksumError, InvalidError, MaxBufferError, NetworkError,
    SignatureError
)

logger = get_logger()

MAX_BUFFER_SIZE = 1048576
CHUNK_NOTIFY_COUNT = 1


class download_ioloop_memory(download):
    '''
    Download in IOLoop.

    Wrapper for the download class using a ioloop and yielding notifies.

    The usage is simple::

    .. code-block:: python

        d = download_ioloop_memory(
            url='https://file/to/download',
            signature=base64_encoded_signature,
            sign_key=pem_of_public_sign_key,
            pinset=[Certificate_PublicKey_Fingerprint],
            sha1sum=sha1sum_of_file,
            sha256sum=sha256_sum_of_file)
        result = yield d.start()

    This will download the contents of the url verify the signature and return
    the content of the url to the caller.

    In the case that network exceptions occur (fingerprint invalid,
    remote not found, timeouts) the exceptions will be raised.
    '''
    def __init__(
            self, url, pinset,
            signature=None, sign_key=None,
            sha1sum=None, sha256sum=None):
        '''
        Initialize Download.

        Initialize the class with the required properties and setup a
        connection pool.

        Arguments:
            url (string): HTTPS URL to download.
            target (string): Path to file for storage.
            pinset (list<string>): List of fingerprints for the host \
                certificate.
            signature (string): Base64 encoded signature of the file. \
                Default: None
            sign_key (string): PEM of public key to verify signature. \
                Default: None
            sha1sum (string): SHA1 checksum of the file. \
                Default: None
            sha256sum (string): SHA256 checksum of the file. \
                Default: None
        '''
        download.__init__(
            self, url, target=None, pinset=pinset,
            signature=signature, sign_key=sign_key,
            sha1sum=sha1sum, sha256sum=sha256sum)
        self.registered_notify = []
        self.registered_error = []
        self.registered_signature_ok = []
        self.registered_checksum_ok = []
        self.max_buffer_size = MAX_BUFFER_SIZE
        self.buffer = ''

    @gen.coroutine
    def _chunked_download(self, request):
        '''
        Chunked Download

        Download the contents of the requests in chunks and call notify with
        the current percentage and download speed.

        Arguments:
            request (urllib3.request): Request object for a url.
        Raises:
            IOError - when the contents of the file cannot be written
        '''
        received = 0
        bps_average = 0
        chunk_count = 0
        complete = 0
        bps = 0
        start = time.time()
        length = 0
        content_length_header = request.getheader('Content-Length')
        if content_length_header is not None:
            length = int(content_length_header)
        if length > self.max_buffer_size:
            raise MaxBufferError(
                'Too much data to download into memory: Max: %d'
                % (self.max_buffer_size,))
        for chunk in request.stream(decode_content=True):
            if not self.active:
                raise CanceledError()
            chunk_count += 1
            now = time.time()
            received += len(chunk)
            if bps_average is 0:
                bps_received = received
            else:
                bps_received = received - bps_average

            if length is 0:
                complete = 0
            else:
                complete = float(received) / float(length) * 100
            elapsed = (now - start)
            if elapsed > 0:
                bps = bps_received / elapsed

                if chunk_count % CHUNK_NOTIFY_COUNT == 0 or (now - start > 3):
                    start = time.time()
                    bps_average = received
                    yield self._notify(complete, bps)
            if elapsed < 0:
                start = time.time()

            if len(self.buffer) + len(chunk) > self.max_buffer_size:
                raise MaxBufferError(
                    'Too much data downloaded: Max: %d'
                    % (self.max_buffer_size,))
            self.buffer += chunk
        yield self._notify(100, bps)
        raise gen.Return(received)

    @gen.coroutine
    def _download(self):
        '''
        Download.

        Download the configured URL and move the content to the unchecked file.

        Raises:
            NetworkError -- A network error occurred, the download is canceled.
            IOError -- A io-error occurred, the download is canceled.
        '''
        try:
            url = self.url.url
            pool = self.pool
            self.active = True
            with pool.request('GET', url, preload_content=False) as request:
                if request.status != 200:
                    raise NetworkError(
                        'Failure to request %s: Status: %d'
                        % (url, request.status),
                        status=request.status)

                yield self._chunked_download(request)
        except MaxRetryError as error:
            raise NetworkError('Failed to connect: %s' % (str(error,)))
        except ReadTimeoutError as error:
            raise NetworkError('Timeout occurred: %s' % (str(error,)))
        except HostChangedError as error:
            raise NetworkError(
                'Host Changed (no https url provided): %s' % (str(error,)))

    @gen.coroutine
    def _notify(self, complete, bps):
        '''
        Notify Overload.

        Notify the ioloop about the progress.

        Arguments:
            complete (float): Percentage of completeness (0-100)
            bps (int): Bytes per second
        '''
        for registered in self.registered_notify:
            yield registered(complete, bps)
            yield gen.moment

    @gen.coroutine
    def _notify_checksum_ok(self):
        '''
        Notify Overload.

        Notify the ioloop about the correct checksum.
        '''
        for registered in self.registered_checksum_ok:
            yield registered()
            yield gen.moment

    @gen.coroutine
    def _notify_error(self, message):
        '''
        Notify Overload.

        Notify the ioloop about the progress

        Arguments:
            message (string): Error message that occurred.
        '''
        for registered in self.registered_error:
            yield registered(message)
            yield gen.moment

    @gen.coroutine
    def _notify_signature_ok(self):
        '''
        Notify Overload.

        Notify the ioloop about the correct signature
        '''
        for registered in self.registered_signature_ok:
            yield registered()
            yield gen.moment

    @gen.coroutine
    def cancel(self):
        '''
        Cancel.

        Cancel the active download. Set the active bit to false that
        causes the download to cancel after the next chunk is received.
        '''
        self.active = False
        yield gen.moment

    @gen.coroutine
    def start(self):
        '''
        Start.

        Start the download with the object values.

        Decorators:
            gen.coroutine

        Yields:
            [type] -- [description]
        '''
        self.buffer = ''

        try:
            yield self._download()
            if not self.active:
                raise CanceledError()
            if self.sha1sum is not None or self.sha256sum is not None:
                self._check_checksum_memory(self.buffer)
                yield self._notify_checksum_ok()
            if self.signature is not None:
                self._check_signature_memory(self.buffer)
                yield self._notify_signature_ok()
            raise gen.Return(self.buffer)
        except CanceledError as errors:
            yield self._notify_error('Download canceled.')
            self.buffer = ''
            raise errors
        except ChecksumError as errors:
            yield self._notify_error('Download checksum incorrect.')
            self.buffer = ''
            raise errors
        except InvalidError as errors:
            yield self._notify_error(
                'Invalid pinned remote certificate public key: %s.'
                % (str(errors),))
            raise errors
        except NetworkError as errors:
            yield self._notify_error(
                'Network error: %s.'
                % (str(errors),))
            raise errors
        except SignatureError as errors:
            yield self._notify_error('Invalid signature: %s' % (str(errors),))
            raise errors
        except IOError as errors:
            yield self._notify_error(
                'Failed to store download: %s.'
                % (str(errors),))
            raise errors
