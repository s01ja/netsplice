# -*- coding: utf-8 -*-
# test_hkpk_pool.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Test HKPK HTTPS Connection Pool
'''
from .hkpk_pool import hkpk_pool
from .errors import InvalidError
import mock
from base64 import b64decode

# Random DER file generated like this:
# openssl genrsa -out test.pem -des 512
# openssl req -new -x509 -key test.pem -out openssl_crt.pem -outform pem
# openssl rsa -in test.pem -pubout > test-pub.key
# openssl x509 -in openssl_crt.pem -inform pem -out openssl_crt.der \
#   -outform der
# cat openssl_crt.der | base64 -w 64

# to get the FP from a remote service:
# openssl s_client -servername www.example.com -connect example.com:443 \
# | openssl x509 -pubkey -noout \
# | openssl pkey -pubin -outform der \
# | openssl dgst -sha256 -binary \
# | openssl enc -base64 \
# | base64 -d | hexdump -e '/1 "%02x"'

TEST_DER = b64decode('''
MIIB0zCCAX2gAwIBAgIJAOJkGEMdUsy4MA0GCSqGSIb3DQEBCwUAMEUxCzAJBgNV
BAYTAkFVMRMwEQYDVQQIDApTb21lLVN0YXRlMSEwHwYDVQQKDBhJbnRlcm5ldCBX
aWRnaXRzIFB0eSBMdGQwHhcNMTgwMTEwMjA0MzE2WhcNMTgwMjA5MjA0MzE2WjBF
MQswCQYDVQQGEwJBVTETMBEGA1UECAwKU29tZS1TdGF0ZTEhMB8GA1UECgwYSW50
ZXJuZXQgV2lkZ2l0cyBQdHkgTHRkMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAM4K
1BFrm2zVs6m7A0Px3/57JrYXat6fKGsJh5MoDi6fl2dlUckRoSx8brY3YDgkQahF
P4gLVda6+CP6n0ORwV0CAwEAAaNQME4wHQYDVR0OBBYEFA+eF3dD4vOF51o+J7I8
8bQAF8l9MB8GA1UdIwQYMBaAFA+eF3dD4vOF51o+J7I88bQAF8l9MAwGA1UdEwQF
MAMBAf8wDQYJKoZIhvcNAQELBQADQQAJdu/rrK+CayzvafHDHZ/o4XKYV4teMYKg
bQhAuIOnNIud0Spk/lxcp6bKNtpwn6dMNBUTKduqIMf6esPlRwUy
''')
OTHER_DER = b64decode('''
MIIB1TCCAX+gAwIBAgIJANjReLrhT9f/MA0GCSqGSIb3DQEBCwUAMEYxCzAJBgNV
BAYTAlhYMRQwEgYDVQQIDAtPdGhlci1TdGF0ZTEhMB8GA1UECgwYSW50ZXJuZXQg
V2lkZ2l0cyBQdHkgTHRkMB4XDTE4MDExMDIwNTA1M1oXDTE4MDIwOTIwNTA1M1ow
RjELMAkGA1UEBhMCWFgxFDASBgNVBAgMC090aGVyLVN0YXRlMSEwHwYDVQQKDBhJ
bnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQwXDANBgkqhkiG9w0BAQEFAANLADBIAkEA
sqyTjC/tc/iwLVVrqd+0e3fm1y/PfJHq/7ns8LkdDJ34htYWgMTu/LIE2T/tgJdm
7KAjGLoKmsFsfVR7Ez/AoQIDAQABo1AwTjAdBgNVHQ4EFgQUahNXCkCssB5R7FvZ
sjWfUdpF84kwHwYDVR0jBBgwFoAUahNXCkCssB5R7FvZsjWfUdpF84kwDAYDVR0T
BAUwAwEB/zANBgkqhkiG9w0BAQsFAANBADLcs8WmQsFVn+4mFLzPZJ4rUSiXojVy
AIqHUXWfZan8y5B8eNjbbcHJrtd4Ht1yas+Irc6AjA722hUpASzD2Hk=
''')
TEST_DER_PIN = (
    '514d56272ef6b12d7f9933d7754c0723537fc7971597ac0f78cbf379c49fa93c'
)
TEST_CERT = ('''
-----BEGIN CERTIFICATE-----
MIIFXzCCBEegAwIBAgIIZvTHJ8kQ72gwDQYJKoZIhvcNAQELBQAwgcYxCzAJBgNV
BAYTAlVTMRAwDgYDVQQIEwdBcml6b25hMRMwEQYDVQQHEwpTY290dHNkYWxlMSUw
IwYDVQQKExxTdGFyZmllbGQgVGVjaG5vbG9naWVzLCBJbmMuMTMwMQYDVQQLEypo
dHRwOi8vY2VydHMuc3RhcmZpZWxkdGVjaC5jb20vcmVwb3NpdG9yeS8xNDAyBgNV
BAMTK1N0YXJmaWVsZCBTZWN1cmUgQ2VydGlmaWNhdGUgQXV0aG9yaXR5IC0gRzIw
HhcNMTcxMDAxMTEzNDAwWhcNMTgxMTMwMjMzNDE5WjA+MSEwHwYDVQQLExhEb21h
aW4gQ29udHJvbCBWYWxpZGF0ZWQxGTAXBgNVBAMMECoudG9vbHMuaWV0Zi5vcmcw
ggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCx4Tfo64LWifrb9cJLd/As
St5ybj4TYNGoZh7ErT0yYOXwmbX0enpIVSHuDjkS+c4NyvVpYccE7W4PHTseUIh5
Og4xQRbxsQJkaKXN9UoKypmWNQjDfidd0KnP8+corzfYtnvd836ubpd/98ppTszQ
Bt9dJ5s7Eufm/ghrUnuCEXxys0brweh4uA/L4eu9BkRY3INQsqBiW9yBuDbjnnx5
sqlTiuALyUoqEzkxE70sz6hwz4yNPQGjiK4SADYdHiQr3XnYUwEm7ShPyYaUg07I
4RQuhbOv1G7daUavQSUOeq2L8pLKedl7Mk/3d+j5tE8jXNRcA67YqzrKE19dXV2h
AgMBAAGjggHWMIIB0jAMBgNVHRMBAf8EAjAAMB0GA1UdJQQWMBQGCCsGAQUFBwMB
BggrBgEFBQcDAjAOBgNVHQ8BAf8EBAMCBaAwPAYDVR0fBDUwMzAxoC+gLYYraHR0
cDovL2NybC5zdGFyZmllbGR0ZWNoLmNvbS9zZmlnMnMxLTY3LmNybDBjBgNVHSAE
XDBaME4GC2CGSAGG/W4BBxcBMD8wPQYIKwYBBQUHAgEWMWh0dHA6Ly9jZXJ0aWZp
Y2F0ZXMuc3RhcmZpZWxkdGVjaC5jb20vcmVwb3NpdG9yeS8wCAYGZ4EMAQIBMIGC
BggrBgEFBQcBAQR2MHQwKgYIKwYBBQUHMAGGHmh0dHA6Ly9vY3NwLnN0YXJmaWVs
ZHRlY2guY29tLzBGBggrBgEFBQcwAoY6aHR0cDovL2NlcnRpZmljYXRlcy5zdGFy
ZmllbGR0ZWNoLmNvbS9yZXBvc2l0b3J5L3NmaWcyLmNydDAfBgNVHSMEGDAWgBQl
RYFoUCY4PTstLL7Natm2PbNmYzArBgNVHREEJDAighAqLnRvb2xzLmlldGYub3Jn
gg50b29scy5pZXRmLm9yZzAdBgNVHQ4EFgQUrYq0HAdR15KJB7C3hGIvNlV6X00w
DQYJKoZIhvcNAQELBQADggEBAEq9/95ghu58TWMYixOhYhcnengASKnxMQ4MQNfd
/IJ93jAGVsYZhvOEVttqvugO//T4F6D39u90Hg8juMIKclxfFRxVEyKzIcVWu0wR
f1Yj7qZjKE68DjzAvQubYIocACcTtkv76Zi1xUv1CEmgRa0IOOVkXQ34cRr81+qm
+uELz6eTSySNP3CjCZ6NARnekC8spiRog5W8xXMI9aF9RIYve+OYtr5oldnhek2F
GOAFSGiJjOLpBrbg05NCl8DHwBlfxg9fTQiJ6eQQR3JZ+DmAf2gbRXDTqJMpJPG+
GcIoZNqsb/uIA0ZJ4/9PZfU17L6sIPU8zJiAazoCBP5ukRw=
-----END CERTIFICATE-----
''')
TEST_CERT_PK_SHA1 = (
    'a4cf93868ca3d868f35571762201d740990b1541b387f9b03435f4c8752d2a78'
)
TEST_HOST = 'test.com'


def get_test_object():
    o = hkpk_pool('localhost')
    return o


class mock_connection_sock(object):
    def __init__(self):
        self.der = TEST_DER

    def getpeercert(self, binary_form=True):
        return self.der


class mock_connection(object):
    def __init__(self):
        self.is_verified = False
        self.sock = mock_connection_sock()
        self.host = TEST_HOST


def test_get_pk_sha256_from_der():
    o = get_test_object()
    pk_sha256 = o.get_pk_sha256_from_der(TEST_DER)
    print(pk_sha256)
    assert(pk_sha256 == TEST_DER_PIN)


def test_get_pk_sha256_from_cert():
    o = get_test_object()
    pk_sha256 = o.get_pk_sha256_from_cert(TEST_CERT)
    print(pk_sha256)
    assert(pk_sha256 == TEST_CERT_PK_SHA1)


def test_get_pk_sha256_from_other_der_other_pin():
    o = get_test_object()
    der = OTHER_DER
    pk_sha256 = o.get_pk_sha256_from_der(der)
    assert(pk_sha256 != TEST_DER_PIN)


@mock.patch('urllib3.HTTPSConnectionPool._validate_conn')
def test__validate_conn_with_unverified_returns_false(mock_super):
    o = get_test_object()
    conn = mock_connection()
    conn.is_verified = False
    assert(o._validate_conn(conn) is False)


@mock.patch('urllib3.HTTPSConnectionPool._validate_conn')
def test__validate_conn_with_invalid_pin_raises(mock_super):
    o = get_test_object()
    try:
        conn = mock_connection()
        conn.is_verified = True
        o._validate_conn(conn)
        assert(False)  # expecting exception
    except InvalidError:
        assert(True)


@mock.patch('urllib3.HTTPSConnectionPool._validate_conn')
def test__validate_conn_with_invalid_der_raises(mock_super):
    o = get_test_object()
    try:
        conn = mock_connection()
        conn.is_verified = True
        conn.sock.der = 'invalid' + conn.sock.der + 'invalid'
        o._validate_conn(conn)
        assert(False)  # expecting exception
    except InvalidError:
        assert(True)


@mock.patch('urllib3.HTTPSConnectionPool._validate_conn')
def test__validate_conn_with_valid_pin_returns_true(mock_super):
    o = get_test_object()
    conn = mock_connection()
    conn.is_verified = True
    o.pinset = [TEST_DER_PIN]
    assert(o._validate_conn(conn) is True)
