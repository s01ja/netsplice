# -*- coding: utf-8 -*-
# download_ioloop.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
HKPK HTTPS download in a ioloop

Pinned certificate HTTPS requests.
'''
import time
from tornado import gen
from netsplice.util import get_logger

from .download import download
from .errors import (
    CanceledError, ChecksumError, InvalidError, NetworkError,
    SignatureError, TargetError
)
from urllib3.exceptions import (
    MaxRetryError, ReadTimeoutError, HostChangedError
)
CHUNK_NOTIFY_COUNT = 1
logger = get_logger()


class download_ioloop(download):
    '''
    Download in IOLoop.

    Wrapper for the download class using a ioloop and yielding notifies.

    The usage is simple::

    .. code-block:: python

        d = download_ioloop(
            url='https://file/to/download',
            target='/tmp/file/to/store',
            signature=base64_encoded_signature,
            sign_key=pem_of_public_sign_key,
            pinset=[Certificate_PublicKey_Fingerprint],
            sha1sum=sha1sum_of_file,
            sha256sum=sha256_sum_of_file)
        yield d.start()

    This will download the contents of the url to a \*.incomplete file, then
    move it to a \*.unchecked file and after the signature is verified, leave
    the target.
    In the case that network exceptions occur (fingerprint invalid,
    remote not found, timeouts) the files are removed.
    In the case of signature exceptions, a \*.invalid file will be left for the
    user to inspect.
    When the \*.incomplete, \*.unchecked, \*.invalid or target file do exist,
    the download does not start to prevent overwriting of existing files.
    '''
    def __init__(
            self, url, target, pinset,
            signature=None, sign_key=None,
            sha1sum=None, sha256sum=None):
        '''
        Initialize Download.

        Initialize the class with the required properties and setup a
        connection pool.

        Arguments:
            url (string): HTTPS URL to download.
            target (string): Path to file for storage.
            pinset (list<string>): List of fingerprints for the host \
                certificate.
            signature (string): Base64 encoded signature of the file. \
                Default: None
            sign_key (string): PEM of public key to verify signature. \
                Default: None
            sha1sum (string): SHA1 checksum of the file. \
                Default: None
            sha256sum (string): SHA256 checksum of the file. \
                Default: None
        '''
        download.__init__(self, url, target, pinset, signature, sign_key)
        self.registered_notify = []
        self.registered_error = []
        self.registered_signature_ok = []
        self.registered_checksum_ok = []

    @gen.coroutine
    def _chunked_download(self, request, target_file):
        '''
        Chunked Download

        Download the contents of the requests in chunks and call notify with
        the current percentage and download speed.

        Arguments:
            request (urllib3.request): Request object for a url.
            target_file (file handle): Handle to a file to write to.
        Raises:
            IOError - when the contents of the file cannot be written
        '''
        received = 0
        bps_average = 0
        chunk_count = 0
        complete = 0
        bps = 0
        start = time.time()
        length = 0
        content_length_header = request.getheader('Content-Length')
        if content_length_header is not None:
            length = int(content_length_header)
        for chunk in request.stream(decode_content=True):
            if self.active is False:
                raise CanceledError()
            chunk_count += 1
            now = time.time()
            received += len(chunk)
            if bps_average is 0:
                bps_received = received
            else:
                bps_received = received - bps_average

            if length is 0:
                complete = 0
            else:
                complete = float(received) / float(length) * 100

            elapsed = now - start
            if elapsed > 0:
                bps = bps_received / elapsed

                if chunk_count % CHUNK_NOTIFY_COUNT == 0 or (now - start > 3):
                    start = time.time()
                    bps_average = received
                    yield self._notify(complete, bps)
            if elapsed < 0:
                start = time.time()

            target_file.write(chunk)
        yield self._notify(100, bps)
        raise gen.Return(received)

    @gen.coroutine
    def _download(self):
        '''
        Download.

        Download the configured URL and move the content to the unchecked file.

        Raises:
            NetworkError -- A network error occurred, the download is canceled.
            IOError -- A io-error occurred, the download is canceled.
        '''
        try:
            url = self.url.url
            pool = self.pool
            self.active = True
            with pool.request('GET', url, preload_content=False) as request:
                if request.status != 200:
                    raise NetworkError(
                        'Failure to request %s: Status:%d'
                        % (url, request.status))

                if request.supports_chunked_reads():
                    yield self._chunked_download(
                        request, self._fh_target_incomplete)
                else:
                    self._unchunked_download(
                        request, self._fh_target_incomplete)
        except MaxRetryError as error:
            raise NetworkError('Failed to connect: %s' % (str(error,)))
        except ReadTimeoutError as error:
            raise NetworkError('Timeout occurred: %s' % (str(error,)))
        except HostChangedError as error:
            raise NetworkError(
                'Host Changed (no https url provided): %s' % (str(error,)))

    @gen.coroutine
    def _notify(self, complete, bps):
        '''
        Notify Overload.

        Notify the ioloop about the progress

        Arguments:
            complete (float): Percentage of completeness (0-100)
            bps (int): Bytes per second
        '''
        for registered in self.registered_notify:
            yield registered(complete, bps)
            yield gen.moment

    @gen.coroutine
    def _notify_checksum_ok(self):
        '''
        Notify Overload.

        Notify the ioloop about the correct checksum
        '''
        for registered in self.registered_checksum_ok:
            yield registered()
            yield gen.moment

    @gen.coroutine
    def _notify_error(self, message):
        '''
        Notify Overload.

        Notify the ioloop about the progress

        Arguments:
            message (string): Error message that occurred.
        '''
        for registered in self.registered_error:
            yield registered(message)
            yield gen.moment

    @gen.coroutine
    def _notify_signature_ok(self):
        '''
        Notify Overload.

        Notify the ioloop about the correct signature
        '''
        for registered in self.registered_signature_ok:
            yield registered()
            yield gen.moment

    @gen.coroutine
    def cancel(self):
        '''
        Cancel.

        Cancel the active download. Set the active bit to false that
        causes the download to cancel after the next chunk is received.
        '''
        self.active = False
        yield gen.moment

    @gen.coroutine
    def start(self):
        '''
        Start.

        Start the download with the object values.

        Decorators:
            gen.coroutine
        '''
        try:
            self._occupy_targets()
            yield self._download()
            if self.active is False:
                raise CanceledError()
            self._move_incomplete_to_unchecked()
            if self.sha1sum is not None or self.sha256sum is not None:
                self._check_checksum()
                yield self._notify_checksum_ok()
            if self.signature is not None:
                self._check_signature()
                yield self._notify_signature_ok()
            self._move_unchecked_to_target()
        except TargetError as errors:
            yield self._notify_error(
                'Invalid Target: %s'
                % (str(errors),))
        except CanceledError:
            yield self._notify_error('Canceled.')
            self._release_targets()
        except ChecksumError:
            yield self._notify_error('Invalid checksum.')
            self._move_unchecked_to_invalid()
            self._release_targets()
        except InvalidError as errors:
            yield self._notify_error(
                'Invalid pinned remote certificate public key: %s.'
                % (str(errors),))
            self._release_targets()
        except NetworkError:
            yield self._notify_error('Network error.')
            self._release_targets()
        except SignatureError as errors:
            yield self._notify_error('Invalid signature: %s' % (str(errors),))
            self._move_unchecked_to_invalid()
            self._release_targets()
        except ValueError:
            yield self._notify_error('Failed to store download.')
            self._release_targets()
        except IOError:
            yield self._notify_error('Failed to store download.')
            self._release_targets()
