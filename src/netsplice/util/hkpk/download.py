# -*- coding: utf-8 -*-
# https_hkpk.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
HKPK HTTPS requests.

Pinned certificate HTTPS requests.
'''
import os
import shutil
import hashlib

from M2Crypto import RSA, EVP, BIO
from base64 import b64decode
from urllib3 import Timeout
from urllib3.util.url import parse_url
from urllib3.exceptions import (
    MaxRetryError, ReadTimeoutError, HostChangedError
)
from netsplice.config import flags
from netsplice.util import get_logger
from .errors import (
    CanceledError, ChecksumError, InvalidError, NetworkError,
    SignatureError, TargetError
)
from .hkpk_pool import hkpk_pool
from .config import (
    TIMEOUT_CONNECT, TIMEOUT_READ
)
logger = get_logger()
CHECKSUM_READ_BUF_SIZE = 65536


class download(object):
    '''
    Download.

    Pinned certificate download with signature check.
    This class wraps https downloads to check the server certificate with a
    list of known fingerprints, download the file in chunks with progress
    notification and check the downloaded file with a openssl signature.

    The usage is simple::

    .. code-block:: python

        d = download(
            url='https://file/to/download',
            target='/tmp/file/to/store',
            signature=base64_encoded_signature,
            sign_key=pem_of_public_sign_key,
            pinset=[Certificate_PublicKey_Fingerprint],
            sha1sum=sha1sum_of_file,
            sha256sum=sha256_sum_of_file)
        d.start()

    This will download the contents of the url to a \*.incomplete file, then
    move it to a \*.unchecked file and after the signature is verified, leave
    the target.
    In the case that network exceptions occur (fingerprint invalid,
    remote not found, timeouts) the files are removed.
    In the case of signature exceptions, a \*.invalid file will be left for the
    user to inspect.
    When the \*.incomplete, \*.unchecked, \*.invalid or target file do exist,
    the download does not start to prevent overwriting of existing files.
    '''
    def __init__(
            self, url, target, pinset,
            signature=None, sign_key=None,
            sha1sum=None, sha256sum=None):
        '''
        Initialize Download.

        Initialize the class with the required properties and setup a
        connection pool.

        Arguments:
            url (string): HTTPS URL to download.
            target (string): Path to file for storage.
            pinset (list<string>): List of fingerprints for the host \
                certificate.
            signature (string): Base64 encoded signature of the file. \
                Default: None
            sign_key (string): PEM of public key to verify signature. \
                Default: None
            sha1sum (string): SHA1 checksum of the file. \
                Default: None
            sha256sum (string): SHA256 checksum of the file. \
                Default: None
        '''
        self.url = parse_url(url)
        self.target = target
        self.target_incomplete = '%s.incomplete' % (self.target,)
        self.target_unchecked = '%s.unchecked' % (self.target,)
        self.target_invalid = '%s.invalid' % (self.target,)
        self.signature = signature
        self.sign_key = sign_key
        self.sha1sum = sha1sum
        self.sha256sum = sha256sum
        self._fh_target = None
        self._fh_target_unchecked = None
        self._fh_target_incomplete = None
        self._fh_target_invalid = None
        self.active = False

        self.pool = hkpk_pool(
            self.url.netloc,
            timeout=Timeout(connect=TIMEOUT_CONNECT, read=TIMEOUT_READ),
            pinset=pinset)

    def _check_checksum(self):
        '''
        Check Checksum.

        Check the checksum of the unchecked file.
        This method reads the file in chunks and updates the hash function.

        Raises:
            ChecksumError -- The checksum is invalid.
        '''
        sha1 = hashlib.sha1()
        sha256 = hashlib.sha256()
        self._fh_target_unchecked.seek(0)
        while True:
            data = self._fh_target_unchecked.read(CHECKSUM_READ_BUF_SIZE)
            if not data:
                break
            sha1.update(data)
            sha256.update(data)
        if self.sha256sum is not None:
            if self.sha256sum != sha256.hexdigest():
                raise ChecksumError('Verify of sha256 checksum failed')
        if self.sha1sum is not None:
            if self.sha1sum != sha1.hexdigest():
                raise ChecksumError('Verify of sha1 checksum failed')

    def _check_checksum_memory(self, data):
        '''
        Check Checksum.

        Check the checksum of any memory with the available checksums.

        Raises:
            ChecksumError -- The checksum is invalid.
        '''
        logger.debug(
            'check checksum [%s] and [%s] on %d bytes'
            % (self.sha1sum, self.sha256sum, len(data)))
        if self.sha1sum is not None:
            sha1 = hashlib.sha1()
            sha1.update(data)
            if self.sha1sum != sha1.hexdigest():
                raise ChecksumError(
                    'Verify of sha1 checksum failed.')
        if self.sha256sum is not None:
            sha256 = hashlib.sha256()
            sha256.update(data)
            if self.sha256sum != sha256.hexdigest():
                raise ChecksumError(
                    'Verify of sha256 checksum failed.')

    def _check_signature(self):
        '''
        Check Signature.

        Check the signature of the unchecked file.
        This method allocates memory for the contents of the unchecked file.

        Raises:
            SignatureError -- The signature is invalid or a error in loading \
                the signature occurred.
        '''
        # !! large memory block
        self._fh_target_unchecked.seek(0)
        data = self._fh_target_unchecked.read()
        self._check_signature_memory(data)

    def _check_signature_memory(self, data):
        '''
        Check Signature.

        Check the signature of any memory with the stored signature.

        Raises:
            SignatureError -- The signature is invalid or a error in loading \
                the signature occurred.
        '''
        if flags.VERBOSE > 1:
            logger.debug(
                'check signature [%s] with [%s] on %d bytes'
                % (self.signature, self.sign_key, len(data)))
        try:
            bio_pub_key = BIO.MemoryBuffer(bytes(self.sign_key))
            rsa_pub_key = RSA.load_pub_key_bio(bio_pub_key)
            verify_evp = EVP.PKey(md='sha1')

            assign_rsa_result = verify_evp.assign_rsa(rsa_pub_key)
            if assign_rsa_result is 0:
                raise SignatureError(
                    'assign_rsa failed: Failed to load public key')

            verify_evp.verify_init()
            verify_update_result = verify_evp.verify_update(data)
            if verify_update_result is 0:
                raise SignatureError(
                    'verify_update failure')
            elif verify_update_result is -1:
                raise SignatureError(
                    'verify_update other error')

            verify_final_result = verify_evp.verify_final(
                b64decode(self.signature))

            if verify_final_result is 0:
                raise SignatureError('Verification of signature failed.')
            elif verify_final_result is -1:
                raise SignatureError(
                    'Verification of signature failed, some library error occurred.')
        except BIO.BIOError as errors:
            raise SignatureError(
                'Invalid key to verify the signature with.'
                ' (BIOError: %s).' % (str(errors),))
        except EVP.EVPError as errors:
            raise SignatureError(
                'Invalid key to verify the signature with.'
                ' (EVPError: %s).' % (str(errors),))
        except RSA.RSAError as errors:
            raise SignatureError(
                'Invalid key to verify the signature with.'
                ' (RSAError: %s).' % (str(errors),))


    def _chunked_download(self, request, target_file):
        '''
        Chunked Download

        Download the contents of the requests in chunks and call notify with
        the current percentage and download speed.

        Arguments:
            request (urllib3.request): Request object for a url.
            target_file (file handle): Handle to a file to write to.
        Raises:
            IOError - when the contents of the file cannot be written
        '''
        received = 0
        for chunk in request.stream(decode_content=True):
            if self.active is False:
                raise CanceledError()
            received += len(chunk)
            target_file.write(chunk)
        return received

    def _download(self):
        '''
        Download.

        Download the configured URL and move the content to the unchecked file.

        Raises:
            NetworkError -- A network error occurred, the download is canceled.
            IOError -- A io-error occurred, the download is canceled.
        '''
        try:
            url = self.url.url
            pool = self.pool
            self.active = True

            with pool.request('GET', url, preload_content=False) as request:
                if request.status != 200:
                    raise NetworkError(
                        'Failure to request %s: Status:%d'
                        % (url, request.status))

                if request.supports_chunked_reads():
                    self._chunked_download(
                        request, self._fh_target_incomplete)
                else:
                    self._unchunked_download(
                        request, self._fh_target_incomplete)
        except MaxRetryError as error:
            raise NetworkError('Failed to connect: %s' % (str(error,)))
        except ReadTimeoutError as error:
            raise NetworkError('Timeout occurred: %s' % (str(error,)))
        except HostChangedError as error:
            raise NetworkError(
                'Host Changed (no https url provided): %s' % (str(error,)))

    def _move_incomplete_to_unchecked(self):
        '''
        Move Incomplete to unchecked.

        Unlock the incomplete and unchecked file handles and move
        the incomplete file to the unchecked file (assuming the download has
        completed).

        Raises:
            IOError -- failed to remove or move the file.
        '''
        self._fh_target_incomplete.close()
        self._fh_target_incomplete = None
        self._fh_target_unchecked.close()
        self._fh_target_unchecked = None
        os.remove(self.target_unchecked)
        shutil.move(self.target_incomplete, self.target_unchecked)
        self._fh_target_unchecked = open(self.target_unchecked, 'rb')

    def _move_unchecked_to_invalid(self):
        '''
        Move unchecked to invalid.

        Unlock the invalid and unchecked file handle and move the unchecked
        file to the invalid file. This method may be used to save a invalid
        signature download for further inspection.

        Raises:
            IOError -- failed to remove or move the file.
        '''
        self._fh_target_unchecked.close()
        self._fh_target_unchecked = None
        self._fh_target_invalid.close()
        self._fh_target_invalid = None
        os.remove(self.target_invalid)
        shutil.move(self.target_unchecked, self.target_invalid)

    def _move_unchecked_to_target(self):
        '''
        Move unchecked to target.

        Unlock the unchecked and target file handle and move the unchecked
        file to the target file. This method is used to move a target where
        the signature was not checked.
        file to the target filename.

        Raises:
            IOError -- failed to remove or move the file.
        '''
        self._fh_target_unchecked.close()
        self._fh_target_unchecked = None
        self._fh_target_invalid.close()
        self._fh_target_invalid = None
        self._fh_target.close()
        self._fh_target = None
        os.remove(self.target)
        os.remove(self.target_invalid)
        shutil.move(self.target_unchecked, self.target)

    def _occupy_targets(self):
        '''
        Occupy Targets.

        Check that all target files do not exist. Open internal file handles
        for the target files that ensure that during the download no other
        process creates the files.

        Raises:
            TargetError -- Exception to be raised when the target already \
                exists or cannot be opened.
        '''
        if os.path.exists(self.target):
            raise TargetError(
                '%s exists as file. Remove it first.'
                % (self.target,))
        if os.path.exists(self.target_incomplete):
            raise TargetError(
                '%s exists as file. Remove it first.'
                % (self.target_incomplete,))
        if os.path.exists(self.target_unchecked):
            raise TargetError(
                '%s exists as file. Remove it first.'
                % (self.target_unchecked,))
        if os.path.exists(self.target_invalid):
            raise TargetError(
                '%s exists as file. Remove it first.'
                % (self.target_invalid,))
        try:
            self._fh_target = open(self.target, 'wb')
            self._fh_target_incomplete = open(self.target_incomplete, 'wb')
            self._fh_target_invalid = open(self.target_invalid, 'wb')
            self._fh_target_unchecked = open(self.target_unchecked, 'wb')
        except IOError as errors:
            self._release_targets()
            raise TargetError(
                'Unable to open filehandles for the target files: %s'
                % (str(errors),))

    def _release_targets(self):
        '''
        Release Targets.

        Release the target file handles and remove them. Only release when the
        handles are not None.
        This is a exception recovery method catching the IO errors that may
        occur when closing or removing the targets.
        '''
        if self._fh_target is not None:
            try:
                self._fh_target.close()
                os.remove(self.target)
            except IOError:
                pass
            self._fh_target = None
        if self._fh_target_incomplete is not None:
            try:
                self._fh_target_incomplete.close()
                os.remove(self.target_incomplete)
            except IOError:
                pass
            self._fh_target_incomplete = None
        if self._fh_target_invalid is not None:
            try:
                self._fh_target_invalid.close()
                os.remove(self.target_invalid)
            except IOError:
                pass
            self._fh_target_invalid = None
        if self._fh_target_unchecked is not None:
            try:
                self._fh_target_unchecked.close()
                os.remove(self.target_unchecked)
            except IOError:
                pass
            self._fh_target_unchecked = None

    def _unchunked_download(self, request, target):
        '''
        Unchunked Download.

        Download the complete request into the target file without
        notification.

        Arguments:
            request (urllib3.request): Request object for a url.
            target_file (file handle): Handle to a file to write to.

        Raises:
            IOError - when the contents of the file cannot be written
       '''
        shutil.copyfileobj(request, self._fh_target_incomplete)

    def cancel(self):
        '''
        Cancel Download.

        Set the download inactve and cause the process to be canceled.
        When the cancel operation is not called async, the download will
        continue until the next step (signature check after download) and
        then free the resources.
        '''
        self.active = False

    def remove_targets(self):
        '''
        Remove Targets.

        Remove any target that exists and needs to be occupied.
        This is a convenience method that may be used to clear the way for
        a download.
        '''
        if os.path.exists(self.target):
            try:
                os.remove(self.target)
            except IOError:
                pass
        if os.path.exists(self.target_incomplete):
            try:
                os.remove(self.target_incomplete)
            except IOError:
                pass
        if os.path.exists(self.target_invalid):
            try:
                os.remove(self.target_invalid)
            except IOError:
                pass
        if os.path.exists(self.target_unchecked):
            try:
                os.remove(self.target_unchecked)
            except IOError:
                pass

    def start(self):
        try:
            self._occupy_targets()
            self._download()
            self._move_incomplete_to_unchecked()
            if self.sha256sum is not None or self.sha1sum is not None:
                self._check_checksum()
            if self.signature is not None:
                self._check_signature()
            self._move_unchecked_to_target()
        except TargetError:
            print('Invalid Target')
            raise
        except CanceledError:
            print('Canceled.')
            self._release_targets()
            raise
        except ChecksumError:
            print('Checksum error.')
            self._release_targets()
            raise
        except InvalidError:
            print('Invalid pinned remote certificate public key.')
            self._release_targets()
            raise
        except NetworkError:
            print('Network error.')
            self._release_targets()
            raise
        except SignatureError as errors:
            print('Invalid signature: %s' % (str(errors),))
            self._move_unchecked_to_invalid()
            self._release_targets()
            raise
        except IOError:
            print('Failed to store download')
            self._release_targets()
            raise
