# -*- coding: utf-8 -*-
# hkpk_pool.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
HKPK HTTPS Connection Pool

Customized HTTPS pool with a verify_conn method that checks the fingerprint
of the connection with a list of known fingerprints.
'''
from .download import download
from .errors import (
    SignatureError, NetworkError, TargetError, InvalidError
)
from urllib3.exceptions import (
    MaxRetryError, ReadTimeoutError
)
import mock


# echo 'test' | openssl dgst -sha1 -sign test.key -out test.sig
SIGNATURE = (
    'LZVp2NLVuMGa4Py5+5se4cH4O+IcFtNkyfP+NK00r2yYX5mDmc96WBcGJL8iKMHewTbgue6'
    'OPR/l49KC0JMmW1xs2uxTVumcU0jAEoBWNLtujiQnqCJUySoJUFINwjMvy+AwaJfbUvlEiq'
    '+RDmmrKFFS7hBI7n23fYSFReM2AsbXcx6qoBMHUndvxlX4Db/x4zyF3wemcfzlyTKOCad9w'
    'bBj4OO4zzJxlkFcE2eC+otk9GD+hFPRrmMH2upY6fNIWRZcrD3FUXrFuo2SrCphqA84atMv'
    '1XgXOGNMsKRbv544vOwLMQ3ZpuCSX2KtG2DJU5KvqmMnFUJXNP1Fg1c75Q=='
)
TEST_PUB_KEY = ('''-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJpaAbOojavWwlz+B1zYnILHs5xeo7SX
99Ngqsss+zkhJ/Lf5yILFwOTb23w+D0Izb6s3lQ+X7Kw3Mj11QCa0gkCAwEAAQ==
-----END PUBLIC KEY-----
''')
TEST_PUB_SIGNED = 'test\n'
TEST_PUB_SIGNATURE = ('''
GagugZ7se3eHDqMEh/V5HFk6VtiNK2rF+4kYxDppxx3VD9VOjrTHoa0KCS1zW8CS
CoSRR65Izu6YEjnxrZjH9A==
''')

TEST_PUB_GPG_KEY = ('''-----BEGIN PUBLIC KEY-----
MIIEIjANBgkqhkiG9w0BAQEFAAOCBA8AMIIECgKCBAEA6b09PeZ7PWkYusKnr3c4
Q8JJsmCUNk05gb81pt2qQe8/iw3AE3ApwlZO+m4nqAL4yRiYkSZBHKsd4x4FgvXc
zvhl3z4IHGX8EDSxvwyCvC4mHoRRjd6vdD07xq8FUY+eltCzaw0mLRa8GpN9MYRf
1NYuHNLpaTm6oey/TBvZ+ur8zEXkjBpAHvp3JyGnEQBNmKmAht/RFEXpxtVolQVb
2W2UeOd5wg8qUKA4MTFab++Hnrbw35ZYKOl9D/fdobvO0qr+d/Pwa7bkOK52cpEG
eTC72+PIDBG3DSeGlOLvsazVr/yAlTZGvFQ7mRR3h4OcJTe87WqoneP5EivGqyED
WlKkUwEeU3W878xln3ALfOo6mu96qiyW9KyuCO43ikXtstMnmIluVeRCKsyJZrRc
zc94Wr8esM1SEvG2R0JumJ6D35EiTprrKZ3if1N8uMgkaVd+E6ui7nnJT7BfEMKo
Z5VUqrF9e4iCcLnQGhY/JiaxkbV27QZ1PPMy/iupRK10gaSSJ5TRv+v46sBWC+af
eOe4AAdTV5zDQE3MABlz4ft2xNJUUj3vYjhAfWKyuDPnFwvpXJ5LjnqPyQzl3tXy
/TLz3Kgaz21ZFJzoCETvNhUHlPJACTIXwIdUHs1VF7rKba+HzU9gGY44Aqz97mw7
4UWpgjMs/fJFoxg9FtBbWm4cO6hNkWWfF/DNmGPxkenB/N2GCvO/uWX9tub6xoW7
1VowZElch5KBv7uagrauhGCD5tnwLV8kqMp3Cl/FQMv3Rx9bLGk9VEo0g9wyD6To
B01txX9Pex63ONebvjxmD69Cgcen9u8Kcvzeie2DYNa3G3/aCqF8Pf6w3PaQZu1r
tK/cl0iQqCiSIVcxr04LzhqbkqVKHGuY7w6IhRv/GCyiSPzylE+3xdvlBbYVvsrc
2xX/ey0TrcnshcHTz9qsgnu8ibPzGNBQiw7RfA0AXcO0uWKm00t+HNAbVrHfBtfC
2T2/Yd0fHy+yeGkIEKbAgMbGY54lvlqtxolhhKyXe51AI4+Tm1y+WfWx7Y5mVgr4
QJKpnqJhEh1a/U06dXJo+RQueqMOSAeY23wQXjnUodV62KYXX1569LHDuw6k4jIz
9f7Jwv556gG50jAkkJ34l3ieT7RLcR0sU8RggaD6ydgmbccQB07SPxSaQJXZVnp2
nFLV55KWEDS6J+HmG/7ssQIKaPf0qgOd5mkZKlGshXdlKOh2uXSVK4Umq/Ddo+iX
gZme+Oxxb5LJkymqhu9ztGZBYtjLXm7OsJ0b9z0M4Uu+Duy1ox2Lsa+kL8y17Yu4
MGnibUK5KuH5G/3+406NVhIHYI+DhGCmbmAR24fvyvGQNlulYCgNAwuwOcaD7mxb
qwIDAQAB
-----END PUBLIC KEY-----''')

TEST_PUB_GPG_SIGNED = 'test'

# Signature with subkey fp: FCFB 6801 AF03 F12B 47F7  28E4 55B1 F9D3 C24B 82DB
TEST_PUB_GPG_SIGNATURE = ('''
fr4KIvuVxTmu0JtkmS0qHKVhUt/ZWdjhw22bM6ijwXPpddC/uqthmfoU/DRCEUPc
sze4QiIIbcDsnK+QYLf83PKkEj8UarKofa8lLVQjEKMr2nV8mpCllH9YA92kVHY8
p2FJ1LxNdewfFXwb//c+uvKZ1sI+nqkNFygGYYrl558Ec0MWJYbAqtyZKtAZkVsf
Kpm9wfpAT64Lyo2BfnYZtGdgYgAtyj/qoyIBEgz4B+W9YhgvcDk2pYx8teQAK4oZ
6K8TrkiAMlk/hWwYUJrPr0BKsu5/HiI10+r6PNFQufVfY2FIhPPaxloGAzPJguMd
dj1nnBxiJYLinhvxTxDHAcX1XOHqof8mvsJQ6qIw91QmQi1oagAZ8Sv5J6ox2rMf
cBGNwEzqrIvWuEO5gsD4fGVfcnA1h8PPqGWn3NzScQlQhWPR/OgP8UzWjRcUq/qI
oDF1UaBCjVH8NMKW2dwVVJRclb//YOtG/F4b0kDMp0g74e1VfFY50dJGZ0q80Ter
lliFnHI/r0xK8NGpTAZ1jTKzpp9RtQTx3FeUbHo5A0/Yk1qoN/UQY0nDdLY+Gkf2
rVat2d+X6R9EDocYaQkDDRwBgkm0UOIwYIyBU9AJ5LB47PjH9XpfKOuGIkv/miMT
0hpcMC4LA3FPSCZtHijnyN13cLhGpFpW/GM2XQseGq63co3O9fygAQRqitQp6sN9
6PjTpoosRU72gwjsuCEJwRHe1z8ZC9B8sqjIGdix2nBZr2YPW24ski2NPBjwnDR5
NolGrOyi+A+7sXCRuCrnwuaav/7dnb0kDgBHXfRxHRqnBLwUorIAaWOxbwWVMPSk
rQFe0KH+E980JVpVhVB+1S3WF6+DkqLMjm/c7iAMwQTEVf9xsOie/6Aygr2jz96i
zJFpsG0YQynxoRFL7Dq3sIB1UP6orB8sNexebXkFqoeEMcJg89+mXE0+RCIzTbug
gkEMaGo3Vg+amdon3RMNBaizaU/LgF7DVNv5Oi2h+vVEjodgzReXGwp02/PLR8m+
r+8ST1pHR6WYTr78hYBFJ0cOajrXJVhLiF0NkC3FdGRDRjmGWuC4FQpeMnggB8t5
m5YRctXD8pyIdrMpFD6UlqMUHtTTw8iYn6eBFTWTbaFjzh4p3YGH+gCoLwqzCwq7
QE6KBu1A3AgYbcxXfzlRFrvztyJj/n/Nf9lzPMmcORh95ozbvU22Kmwds9oT8Jdc
/lxrUgHZPpc9ilJ9R+i6ttAwmSgbBlEACsxRxmbabCS55eg6vvZnyPmRrQ06D86o
jehiLffh/QrxxWIpozFkaQ5mX0qBEcON9I7gRZg1L2w6smiqmWGYS/F6bIH6XQ9M
2wKL4MpTzXjd8HgvpW1Kow==
''')

# Public Key as acquired from a HSM device.
#
# export PIN=12345678
# export KEY_ID=10
# sc-hsm-tool --initialize --so-pin 3537363231383830 --pin ${PIN} \
#   --dkek-shares 1
# sc-hsm-tool --create-dkek-share hsm-dkek-share-1.pbe
# sc-hsm-tool --import-dkek-share hsm-dkek-share-1.pbe
# pkcs11-tool --module opensc-pkcs11.so --login --pin ${PIN} \
#   --keypairgen --key-type rsa:2048 --id ${KEY_ID} \
#   --label "HSM RSA Key Test"
# or
# find key-references that are free: pkcs15-tool --dump
# export KEY_REFERENCE=1
# sc-hsm-tool --unwrap-key wrap-key-1.bin --key-reference ${KEY_REFERENCE} \
#   --pin ${PIN}
# pkcs15-tool --read-public-key ${KEY_ID}
TEST_PUB_HSM_KEY = ('''-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnUhbEpssf76bkw31Xak8
20PnyMB8drmWqsBgSDytV7Up+GOgQaAF05jJkSi1RE/vxiazEhX2rbAteUp3ASnA
N6TnMo61YIz6erffvZJQJdqmJ/K4XkiFAi2lHtGqAmI1K2XO1qi+ikTamkKNSgJH
yh3Npi0TgxsIfZ1K4k6bi3y/TaS2ukdStsCqR1uGkS5bC4D5wCXD0tSdmfgF0LMv
Qg61RCGClmIe6MHfAEi8eX5xGXicwbhNVTXbxP64x1xsjK9F+gFwlEHkdRlOSjn7
6mZugYS8K0L8ofywiCfix++cul/Nh6YrYD4ldnuLI94UzvCO/8N46ES9wiVJKZt3
aQIDAQAB
-----END PUBLIC KEY-----
''')

TEST_PUB_HSM_SIGNED = 'test'

# Signature created with the HSM token.
#
# echo -n test \
#   | pkcs11-tool -p $PIN --id $SIGN_KEY --sign -m SHA1-RSA-PKCS \
#   | base64 -w64
TEST_PUB_HSM_SIGNATURE = (
    'U0/rmTC/ulWnoMO4ICH4vhOGCksR2BzhyZV8LU488GE2KjwSVShqDrp75OM33JiL'
    '10a9lEXmpvxoK2ITTxltdA6d4Cj/UKb/lkPWdpnsjh7anK5RNI/FWwFif+CfiI/3'
    'I7ZMQUMaOiZFv2/Hj8atI7/v9Y2iRt0CUgYhtJG5hlSZMJ0h8GMc8mUjBxsCbdeA'
    'nuZ4n0CIcr0vvzLQA1N3VrFZ4rQ2GCa58xRTNFCxf3vTp22oYBeUuKgyt2EfbnNM'
    'QS6eA7tNAwH+SjE5Z+3Zka/K0tA8Jxu6o/rEBZ1dgcCtO9byYp9eoJLGoEMg419Z'
    'Wi+5tNQD+R6TDCXNQ7fQIQ=='
)


class mock_filehandle(object):
    def __init__(self):
        self.data = ''
        self._close_called = False

    def close(self):
        self._close_called = True

    def read(self):
        return self.data

    def seek(self, offset):
        pass

    def write(self, new_data):
        self.data += new_data


class mock_request(object):
    def __init__(self):
        self.data = []
        self.header = {
            'Content-Length': 0
        }
        self._supports_chunked_reads = True
        self._supports_chunked_reads_sideeffect = None
        self.status = 200

    def getheader(self, header_name):
        return self.header[header_name]

    def stream(self, decode_content=True):
        return self.data

    def supports_chunked_reads(self):
        if self._supports_chunked_reads_sideeffect:
            raise self._supports_chunked_reads_sideeffect
        return self._supports_chunked_reads

    def read(self, l):
        return self.data

    def __exit__(self, a, b, c):
        pass

    def __enter__(self):
        return self


class mock_connection_pool(object):
    def __init__(self):
        self._request = mock_request()

    def request(self, method, url, preload_content=False):
        return self._request


def get_test_object():
    o = download('https://localhost/file', '/tmp/_test_download_file', [])
    o._fh_target = mock_filehandle()
    o._fh_target_incomplete = mock_filehandle()
    o._fh_target_invalid = mock_filehandle()
    o._fh_target_unchecked = mock_filehandle()
    o.signature = SIGNATURE
    o.active = True
    return o


@mock.patch('M2Crypto.BIO.MemoryBuffer')
@mock.patch('M2Crypto.RSA.load_pub_key_bio')
@mock.patch('M2Crypto.EVP.PKey.assign_rsa')
def test_check_signature_assign_rsa_fail_raises(
        mock_assign_rsa, mock_memorybuffer, mock_load_pub_key_bio):
    o = get_test_object()
    try:
        mock_assign_rsa.return_value = 0
        o._check_signature()
        assert(False)  # expect Signature Error
    except SignatureError:
        assert(True)


@mock.patch('M2Crypto.RSA.load_pub_key_bio')
@mock.patch('M2Crypto.BIO.MemoryBuffer')
@mock.patch('M2Crypto.EVP.PKey.verify_update')
@mock.patch('M2Crypto.EVP.PKey.assign_rsa')
def test_check_signature_verify_update_fail_raises(
        mock_assign_rsa, mock_verify_update, mock_memorybuffer,
        mock_load_pub_key_bio):
    o = get_test_object()
    try:
        mock_assign_rsa.return_value = 1
        mock_verify_update.return_value = 0
        o._check_signature()
        assert(False)  # expect Signature Error
    except SignatureError:
        assert(True)


@mock.patch('M2Crypto.RSA.load_pub_key_bio')
@mock.patch('M2Crypto.BIO.MemoryBuffer')
@mock.patch('M2Crypto.EVP.PKey.verify_update')
@mock.patch('M2Crypto.EVP.PKey.assign_rsa')
def test_check_signature_verify_update_fail_other_raises(
        mock_assign_rsa, mock_verify_update, mock_memorybuffer,
        mock_load_pub_key_bio):
    o = get_test_object()
    try:
        mock_assign_rsa.return_value = 1
        mock_verify_update.return_value = -1
        o._check_signature()
        assert(False)  # expect Signature Error
    except SignatureError:
        assert(True)


@mock.patch('M2Crypto.RSA.load_pub_key_bio')
@mock.patch('M2Crypto.BIO.MemoryBuffer')
@mock.patch('M2Crypto.EVP.PKey.verify_final')
@mock.patch('M2Crypto.EVP.PKey.verify_update')
@mock.patch('M2Crypto.EVP.PKey.assign_rsa')
def test_check_signature_verify_final_fail_raises(
        mock_assign_rsa, mock_verify_update, mock_verify_final,
        mock_memorybuffer, mock_load_pub_key_bio):
    o = get_test_object()
    try:
        mock_assign_rsa.return_value = 0
        mock_verify_update.return_value = 1
        mock_verify_final.return_value = 0
        o._check_signature()
        assert(False)  # expect Signature Error
    except SignatureError:
        assert(True)


@mock.patch('M2Crypto.RSA.load_pub_key_bio')
@mock.patch('M2Crypto.BIO.MemoryBuffer')
@mock.patch('M2Crypto.EVP.PKey.verify_final')
@mock.patch('M2Crypto.EVP.PKey.verify_update')
@mock.patch('M2Crypto.EVP.PKey.assign_rsa')
def test_check_signature_verify_final_fail_other_raises(
        mock_assign_rsa, mock_verify_update, mock_verify_final,
        mock_memorybuffer, mock_load_pub_key_bio):
    o = get_test_object()
    try:
        mock_assign_rsa.return_value = 0
        mock_verify_update.return_value = 1
        mock_verify_final.return_value = -1
        o._check_signature()
        assert(False)  # expect Signature Error
    except SignatureError:
        assert(True)


@mock.patch('M2Crypto.RSA.load_pub_key_bio')
@mock.patch('M2Crypto.BIO.MemoryBuffer')
@mock.patch('M2Crypto.EVP.PKey.verify_final')
@mock.patch('M2Crypto.EVP.PKey.verify_update')
@mock.patch('M2Crypto.EVP.PKey.assign_rsa')
def test_check_signature_verify_final_success_returns(
        mock_assign_rsa, mock_verify_update, mock_verify_final,
        mock_memorybuffer, mock_load_pub_key_bio):
    o = get_test_object()
    mock_assign_rsa.return_value = 1
    mock_verify_update.return_value = 1
    mock_verify_final.return_value = 1
    o._check_signature()
    assert(True)


def test_chunked_download_small_file_complete_in_f():
    o = get_test_object()
    r = mock_request()
    r.data = ['0', '1', '2', '3', '4']
    r.header['Content-Length'] = 5
    f = mock_filehandle()
    o._chunked_download(r, f)
    result = f.data
    assert(result == '01234')


def test_chunked_download_large_file_complete_in_f():
    o = get_test_object()
    r = mock_request()
    l = 0
    for i in range(0, 1000):
        r.data.append(str(i))
        l += len(str(i))
    r.header['Content-Length'] = l
    f = mock_filehandle()
    o._chunked_download(r, f)
    result = f.data
    expect = ''.join(r.data)
    assert(result == expect)


def test_download_404_raises():
    o = get_test_object()
    o.pool = mock_connection_pool()
    o.pool._request.status = 404
    try:
        o._download()
        assert(False)  # Network error expected
    except NetworkError:
        assert(True)


def test_download_500_raises():
    o = get_test_object()
    o.pool = mock_connection_pool()
    o.pool._request.status = 500
    try:
        o._download()
        assert(False)  # Network error expected
    except NetworkError:
        assert(True)


def test_download_maxretry_raises():
    o = get_test_object()
    o.pool = mock_connection_pool()
    o.pool._request.status = 200
    try:
        o.pool._request._supports_chunked_reads_sideeffect = MaxRetryError(
            '', '', '')
        o._download()
        assert(False)  # Network error expected
    except NetworkError:
        assert(True)


def test_download_readtimeout_raises():
    o = get_test_object()
    o.pool = mock_connection_pool()
    o.pool._request.status = 200
    try:
        o.pool._request._supports_chunked_reads_sideeffect = ReadTimeoutError(
            '', '', '')
        o._download()
        assert(False)  # Network error expected
    except NetworkError:
        assert(True)


@mock.patch('netsplice.util.hkpk.download.download._chunked_download')
def test_download_chunk_supported_downloads_chunked(mock_chunked_download):
    o = get_test_object()
    o.pool = mock_connection_pool()
    o.pool._request.status = 200
    o.pool._request._supports_chunked_reads = True
    o._download()
    mock_chunked_download.assert_called()


@mock.patch('shutil.copyfileobj')
@mock.patch('netsplice.util.hkpk.download.download._chunked_download')
def test_download_chunk_supported_downloads_one(
        mock_chunked_download, mock_copyfileobj):
    o = get_test_object()
    o.pool = mock_connection_pool()
    o.pool._request.status = 200
    o.pool._request._supports_chunked_reads = False
    o._download()
    mock_copyfileobj.assert_called()
    mock_chunked_download.assert_not_called()


@mock.patch('os.remove')
@mock.patch('shutil.move')
@mock.patch('__builtin__.open')  # py3:builtins.open
def test_move_incomplete_to_unchecked(mock_remove, mock_move, mock_open):
    o = get_test_object()
    o._move_incomplete_to_unchecked()
    mock_open.assert_called()
    mock_move.assert_called()
    mock_remove.assert_called()


@mock.patch('os.remove')
@mock.patch('shutil.move')
def test_move_unchecked_to_invalid(mock_remove, mock_move):
    o = get_test_object()
    o._move_unchecked_to_invalid()
    mock_move.assert_called()
    mock_remove.assert_called()


@mock.patch('os.remove')
@mock.patch('shutil.move')
def test_move_unchecked_to_target(mock_remove, mock_move):
    o = get_test_object()
    o._move_unchecked_to_target()
    mock_move.assert_called()
    mock_remove.assert_called()


@mock.patch('os.path.exists')
def test_occupy_targets_raises_if_exists(mock_exists):
    o = get_test_object()
    try:
        mock_exists.side_effect = [True]
        o._occupy_targets()
        assert(False)  # Expect TargetError
    except TargetError:
        assert(True)
    try:
        mock_exists.side_effect = [False, True]
        o._occupy_targets()
        assert(False)  # Expect TargetError
    except TargetError:
        assert(True)
    try:
        mock_exists.side_effect = [False, False, True]
        o._occupy_targets()
        assert(False)  # Expect TargetError
    except TargetError:
        assert(True)
    try:
        mock_exists.side_effect = [False, False, False, True]
        o._occupy_targets()
        assert(False)  # Expect TargetError
    except TargetError:
        assert(True)


@mock.patch('os.path.exists')
@mock.patch('__builtin__.open')
def test_occupy_targets_opens_files(mock_open, mock_exists):
    o = get_test_object()
    mock_open.return_value = mock_filehandle()
    mock_exists.return_value = False
    o._occupy_targets()
    assert(o._fh_target is not None)
    assert(o._fh_target_incomplete is not None)
    assert(o._fh_target_invalid is not None)
    assert(o._fh_target_unchecked is not None)


@mock.patch('os.remove')
@mock.patch('os.path.exists')
@mock.patch('__builtin__.open')
def test_occupy_targets_openfails_raises(mock_open, mock_exists, mock_remove):
    o = get_test_object()
    mock_open.side_effect = IOError(13, 'test')
    mock_exists.return_value = False
    try:
        o._occupy_targets()
        assert(False)  # TargetError expected
    except TargetError:
        assert(True)


@mock.patch('os.remove')
def test_release_targets(mock_remove):
    o = get_test_object()
    o._release_targets()
    assert(o._fh_target is None)
    assert(o._fh_target_incomplete is None)
    assert(o._fh_target_invalid is None)
    assert(o._fh_target_unchecked is None)


@mock.patch('os.remove')
def test_release_targets_with_exceptions(mock_remove):
    o = get_test_object()
    mock_remove.side_effect = [
        IOError(13, 'test'),
        IOError(13, 'test'),
        IOError(13, 'test'),
        IOError(13, 'test')
    ]
    o._release_targets()
    assert(o._fh_target is None)
    assert(o._fh_target_incomplete is None)
    assert(o._fh_target_invalid is None)
    assert(o._fh_target_unchecked is None)


@mock.patch('os.remove')
def test_release_targets_with_partial_released(mock_remove):
    o = get_test_object()
    o._fh_target = None
    o._fh_target_incomplete = None
    o._fh_target_invalid = None
    o._fh_target_unchecked = None
    o._release_targets()
    assert(o._fh_target is None)
    assert(o._fh_target_incomplete is None)
    assert(o._fh_target_invalid is None)
    assert(o._fh_target_unchecked is None)


@mock.patch('os.remove')
@mock.patch('os.path.exists')
def test_remove_targets(mock_exists, mock_remove):
    o = get_test_object()
    o.remove_targets()
    assert(mock_exists.call_count == 4)
    assert(mock_remove.call_count == 4)


@mock.patch('os.remove')
@mock.patch('os.path.exists')
def test_remove_targets_ignores_ioerrors(mock_exists, mock_remove):
    o = get_test_object()
    mock_remove.side_effect = [
        IOError(13, 'test'),
        IOError(13, 'test'),
        IOError(13, 'test'),
        IOError(13, 'test')
    ]
    o.remove_targets()
    assert(mock_exists.call_count == 4)
    assert(mock_remove.call_count == 4)


@mock.patch('os.remove')
@mock.patch('os.path.exists')
def test_remove_targets_ignores_nonexisting(mock_exists, mock_remove):
    o = get_test_object()
    mock_exists.return_value = False
    o.remove_targets()
    assert(mock_exists.call_count == 4)
    assert(mock_remove.call_count == 0)


@mock.patch('netsplice.util.hkpk.download.download._release_targets')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_unchecked_to_invalid')
@mock.patch('netsplice.util.hkpk.download.download._move_unchecked_to_target')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_incomplete_to_unchecked')
@mock.patch('netsplice.util.hkpk.download.download._check_signature')
@mock.patch('netsplice.util.hkpk.download.download._download')
@mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
def test_start(
        mock_occupy_targets, mock_download, mock_checksignature,
        mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
        mock_move_unchecked_to_invalid,
        mock_release_targets):
    o = get_test_object()
    o.start()
    mock_occupy_targets.assert_called()
    mock_download.assert_called()
    mock_move_incomplete_to_unchecked.assert_called()
    mock_checksignature.assert_called()
    mock_move_unchecked_to_target.assert_called()

    mock_release_targets.assert_not_called()
    mock_move_unchecked_to_invalid.assert_not_called()


@mock.patch('netsplice.util.hkpk.download.download._release_targets')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_unchecked_to_invalid')
@mock.patch('netsplice.util.hkpk.download.download._move_unchecked_to_target')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_incomplete_to_unchecked')
@mock.patch('netsplice.util.hkpk.download.download._check_signature')
@mock.patch('netsplice.util.hkpk.download.download._download')
@mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
def test_start_without_signature(
        mock_occupy_targets, mock_download, mock_checksignature,
        mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
        mock_move_unchecked_to_invalid,
        mock_release_targets):
    o = get_test_object()
    o.signature = None
    o.start()
    mock_occupy_targets.assert_called()
    mock_download.assert_called()
    mock_move_incomplete_to_unchecked.assert_called()
    mock_checksignature.assert_not_called()
    mock_move_unchecked_to_target.assert_called()

    mock_release_targets.assert_not_called()
    mock_move_unchecked_to_invalid.assert_not_called()


@mock.patch('netsplice.util.hkpk.download.download._release_targets')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_unchecked_to_invalid')
@mock.patch('netsplice.util.hkpk.download.download._move_unchecked_to_target')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_incomplete_to_unchecked')
@mock.patch('netsplice.util.hkpk.download.download._check_signature')
@mock.patch('netsplice.util.hkpk.download.download._download')
@mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
def test_start_handles_invalid_fp(
        mock_occupy_targets, mock_download, mock_checksignature,
        mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
        mock_move_unchecked_to_invalid,
        mock_release_targets):
    o = get_test_object()
    o.signature = None
    try:
        mock_download.side_effect = InvalidError(
            'invalid-fp', 'remote', 'valid-fp')
        o.start()
        assert(False)  # expect InvalidError raised
    except InvalidError:
        assert(True)
        mock_release_targets.assert_called()

    mock_move_unchecked_to_invalid.assert_not_called()


@mock.patch('netsplice.util.hkpk.download.download._release_targets')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_unchecked_to_invalid')
@mock.patch('netsplice.util.hkpk.download.download._move_unchecked_to_target')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_incomplete_to_unchecked')
@mock.patch('netsplice.util.hkpk.download.download._check_signature')
@mock.patch('netsplice.util.hkpk.download.download._download')
@mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
def test_start_handles_network_error(
        mock_occupy_targets, mock_download, mock_checksignature,
        mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
        mock_move_unchecked_to_invalid,
        mock_release_targets):
    o = get_test_object()
    o.signature = None
    try:
        mock_download.side_effect = NetworkError('test')
        o.start()
        assert(False)  # expect InvalidError raised
    except NetworkError:
        assert(True)
        mock_release_targets.assert_called()

    mock_move_unchecked_to_invalid.assert_not_called()


@mock.patch('netsplice.util.hkpk.download.download._release_targets')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_unchecked_to_invalid')
@mock.patch('netsplice.util.hkpk.download.download._move_unchecked_to_target')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_incomplete_to_unchecked')
@mock.patch('netsplice.util.hkpk.download.download._check_signature')
@mock.patch('netsplice.util.hkpk.download.download._download')
@mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
def test_start_handles_signature_error(
        mock_occupy_targets, mock_download, mock_checksignature,
        mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
        mock_move_unchecked_to_invalid,
        mock_release_targets):
    o = get_test_object()
    o.signature = ''
    try:
        mock_checksignature.side_effect = SignatureError('test')
        o.start()
        assert(False)  # expect InvalidError raised
    except SignatureError:
        assert(True)
        mock_release_targets.assert_called()
        mock_move_unchecked_to_invalid.assert_called()


@mock.patch('netsplice.util.hkpk.download.download._release_targets')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_unchecked_to_invalid')
@mock.patch('netsplice.util.hkpk.download.download._move_unchecked_to_target')
@mock.patch(
    'netsplice.util.hkpk.download.download.'
    '_move_incomplete_to_unchecked')
@mock.patch('netsplice.util.hkpk.download.download._check_signature')
@mock.patch('netsplice.util.hkpk.download.download._download')
@mock.patch('netsplice.util.hkpk.download.download._occupy_targets')
def test_start_handles_io_error(
        mock_occupy_targets, mock_download, mock_checksignature,
        mock_move_incomplete_to_unchecked, mock_move_unchecked_to_target,
        mock_move_unchecked_to_invalid,
        mock_release_targets):
    o = get_test_object()
    o.signature = None
    try:
        mock_download.side_effect = IOError(13, 'test')
        o.start()
        assert(False)  # expect InvalidError raised
    except IOError:
        assert(True)
        mock_release_targets.assert_called()

    mock_move_unchecked_to_invalid.assert_not_called()


def test_check_signature_memory():
    o = get_test_object()
    o.signature = TEST_PUB_SIGNATURE
    o.sign_key = TEST_PUB_KEY
    o._check_signature_memory(TEST_PUB_SIGNED)


def test_check_signature_memory_hsm():
    o = get_test_object()
    o.signature = TEST_PUB_HSM_SIGNATURE
    o.sign_key = TEST_PUB_HSM_KEY
    o._check_signature_memory(TEST_PUB_HSM_SIGNED)


def test_check_signature_memory_gpg():
    o = get_test_object()
    o.signature = TEST_PUB_GPG_SIGNATURE
    o.sign_key = TEST_PUB_GPG_KEY
    o._check_signature_memory(TEST_PUB_GPG_SIGNED)
