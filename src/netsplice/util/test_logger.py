import os
import sys
from StringIO import StringIO
from . import get_logger
from .logger import logger as logger_singleton
from .path import (get_location, mkdir_p)
from ..config import flags
from ..config import log as config


def get_logfile_name():
    log_path = get_location('logs')
    mkdir_p(log_path)
    filename = '%(log_path)s/test.log' % dict(log_path=log_path)
    print 'LOGFILENAME:', filename
    return filename


def test_get_logger_write_files_creates_file():
    '''
    Check that the logger creates a file.
    '''
    filename = get_logfile_name()
    logger_singleton.logger = None
    config.WRITE_LOG_FILES = True
    get_logger('test')
    config.WRITE_LOG_FILES = False
    assert(os.path.isfile(filename))


def test_get_logger_creates_rollover_file():
    '''
    Check that the logger creates a file and rolls over
    '''
    filename = get_logfile_name()
    logger_singleton.logger = None
    config.WRITE_LOG_FILES = True
    get_logger('test')
    assert(os.path.isfile(filename))
    logger_singleton.logger = None
    get_logger('test')
    config.WRITE_LOG_FILES = False
    assert(os.path.isfile(filename + '.1'))


def test_get_logger_with_debug_creates_file():
    '''
    Check that the logger creates a file with debug-mode on.
    '''
    flags.DEBUG = True
    filename = get_logfile_name()
    config.WRITE_LOG_FILES = True
    logger_singleton.logger = None
    get_logger('test')
    config.WRITE_LOG_FILES = False
    assert(os.path.isfile(filename))


def test_logger_writes_text():
    '''
    Check that the log-message appears in logfile.
    '''
    filename = get_logfile_name()
    logger_singleton.logger = None
    config.WRITE_LOG_FILES = True
    logger = get_logger('test')
    out = StringIO()
    err = sys.stderr
    sys.stderr = out
    logger.info('test info message')
    logger.debug('test debug message')
    logger.error('test error message')
    logger.critical('test critical message')
    sys.stderr = err
    assert(os.path.isfile(filename))
    log_content = open(filename, 'r').read()
    config.WRITE_LOG_FILES = False
    assert(log_content.find('test info message') > 0)
    assert(log_content.find('test debug message') > 0)
    assert(log_content.find('test error message') > 0)
    assert(log_content.find('test critical message') > 0)


def teardown():
    '''
    Cleanup files created by the logger tests.
    '''
    print '--- cleanup ---'
    try:
        os.remove(get_logfile_name())

    except:
        pass

    try:
        os.remove(get_logfile_name() + '.1')

    except:
        pass
