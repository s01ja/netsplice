# -*- coding: utf-8 -*-
# errors.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Errors that may be raised from the code.
'''


class AccessError(Exception):
    '''
    Error to be raised when the user has insufficient permissions.
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class ActiveError(Exception):
    '''
    Error to be raised when a connection is active and therefore
    cannot be acted on (eg delete).
    '''
    def __init__(self):
        Exception.__init__(self)


class NotActiveError(Exception):
    '''
    Error to be raised when a connection is not connected and therefore
    cannot be acted on (eg disconnect, reconnect).
    '''
    def __init__(self):
        Exception.__init__(self)


class NotEmptyError(Exception):
    '''
    Error to be raised when a group needs to be empty
    before removal.
    '''
    def __init__(self):
        Exception.__init__(self)


class NotUniqueError(Exception):
    '''
    Error to be raised when a entry needs to be unique.
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class ConnectedError(Exception):
    '''
    Error to be raised when a connection is connected and therefore
    cannot be acted on (eg delete).
    '''
    def __init__(self):
        Exception.__init__(self)


class NotConnectedError(Exception):
    '''
    Error to be raised when a connection is not connected and therefore
    cannot be acted on (eg disconnect, reconnect).
    '''
    def __init__(self):
        Exception.__init__(self)


class InsufficientStorageError(Exception):
    '''
    Error to be raised when a connection cannot be setup because there is not
    enough memory to store the config.
    '''
    def __init__(self):
        Exception.__init__(self)


class InvalidConfigError(Exception):
    '''
    Error to be raised when a connection cannot be setup because the config is
    invalid (eg not parseable by openvpn).
    '''
    def __init__(self):
        Exception.__init__(self)


class NoCredentialsProvidedError(Exception):
    '''
    Error to be raised when a connection requires credentials but the user
    did not provide them.
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class NotFoundError(Exception):
    '''
    Error to be raised when a connection cannot be found by id.
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class NotInitializedError(Exception):
    '''
    Error to be raised when a connection was not setup but its members are
    called.
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class NotRegisteredEventError(Exception):
    '''
    Error to be raised when a connection was not setup but its members are
    called.
    '''
    def __init__(self, event_name, event_origin, registered_events):
        message = (
            'Event "%s" is not registered for origin "%s" (not one of: %s)'
            % (event_name, event_origin, registered_events))
        Exception.__init__(self, message)


class InvalidUsageError(Exception):
    '''
    Error to be raised when the API of a class is not used in a documented
    manner. The message is for developers.
    '''
    def __init__(self, message):
        Exception.__init__(self, message)


class UnknownParserTypeError(Exception):
    '''
    Error to be raised when a parser is requested for a unknown type.
    '''
    def __init__(self, message):
        Exception.__init__(self, message)
