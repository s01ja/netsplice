# -*- coding: utf-8 -*-
# win32.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Provide executable filename for given name.
'''


class win32(object):

    def __init__(self):
        pass

    def get(self, name):
        if name.lower().endswith('.exe'):
            return name
        if name.lower().endswith('.cmd'):
            return name
        if name.lower().endswith('.bat'):
            return name
        if name.lower().endswith('.ps1'):
            return name
        if name.lower().endswith('.ps2'):
            return name
        return '%s.exe' % (name.lower(),)
