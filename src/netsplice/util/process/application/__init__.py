# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Application module to abstract the execution of external applications.
In contrast to processes this module wraps Applications installed to the os
and uses os-methods to launch the process of the application and identify
the pid related to the application.
'''
from netsplice.util.process.application.darwin import (
    darwin as darwin_application
)
from netsplice.util.process.application.posix import (
    posix as posix_application
)
from netsplice.util.process.application.win32 import (
    win32 as win32_application
)


def factory(sys_platform):
    '''
    Takes the given sys_platform (should be sys.platform format) and
    returns application wrapper. Assumes a posix environment unless
    the given platform needs something special.
    '''
    if sys_platform.startswith('darwin'):
        return darwin_application()
    if sys_platform.startswith('win32'):
        return win32_application()
    return posix_application()
