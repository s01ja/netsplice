# -*- coding: utf-8 -*-
# posix.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Elevator for POSIX systems.

Modify given argv list so it the argv including executable_name is passed to
popen. dispatcher.start() will execute this list.

Executes the application as-is. policy.d should handle the elevation as
configured on install. Optionally config.GRAPHICAL_SUDO_APPLICATION can be
set.
'''
import netsplice.config.process as config
from netsplice.util.process.elevator.base import base


class posix(base):
    '''
    Posix Elevator.

    Implements privilege elevation for posix sytems using a graphical sudo
    application when execute_source is false.
    '''

    def __init__(self):
        '''
        Initialize.

        No members required.
        '''
        pass

    def modify_argv_list(self, popen_argv_list):
        '''
        Get Elevate popen argv list.

        Modify the given popen argv list to prefix with sudo or equivialent.
        '''
        modified_popen_argv_list = []
        if config.execute_source:
            return popen_argv_list
        if config.GRAPHICAL_SUDO_APPLICATION is not None:
            if isinstance(config.GRAPHICAL_SUDO_APPLICATION, (list,)):
                modified_popen_argv_list.extend(
                    config.GRAPHICAL_SUDO_APPLICATION)
            else:
                modified_popen_argv_list.append(str(
                    config.GRAPHICAL_SUDO_APPLICATION))
        for argument in popen_argv_list:
            modified_popen_argv_list.append(argument)

        return modified_popen_argv_list
