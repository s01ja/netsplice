import mock
import os
import sys
from . import factory
from .location import location
from ..errors import ProcessError
import netsplice.config.process as config
import netsplice.config.flags as flags
from netsplice.util import basestring


def test_source_executable_posix_returns_pwd_venv_build():
    config.execute_source = True
    location_instance = factory('linux')
    expected = os.path.join(os.getcwd(), 'var', 'build', 'executable')
    result = location_instance.get('executable')
    print(result, os.getcwd())
    assert(result == expected)


def test_prod_executable_posix_returns_pwd_venv_build():
    config.execute_source = False
    location_instance = factory('linux')
    expected = os.path.join(
        os.path.dirname(__file__), 'libexec', 'executable')
    result = location_instance.get('executable')
    print(result, expected)
    assert(result == expected)


def test_source_executable_darwin_returns_pwd_venv_build():
    config.execute_source = True
    location_instance = factory('darwin')
    expected = os.path.join(os.getcwd(), 'var', 'build', 'executable')
    result = location_instance.get('executable')
    print(result, os.getcwd())
    assert(result == expected)


def test_prod_executable_darwin_returns_pwd_venv_build():
    config.execute_source = False
    location_instance = factory('darwin')
    expected = os.path.join(
        os.path.dirname(__file__), 'libexec', 'executable')
    result = location_instance.get('executable')
    print(result, expected)
    assert(result == expected)


def test_source_executable_win32_returns_pwd_venv_build():
    config.execute_source = True
    location_instance = factory('win32')
    expected = os.path.join(os.getcwd(), 'var', 'build', 'executable')
    result = location_instance.get('executable')
    print(result, os.getcwd())
    assert(result == expected)


def test_prod_executable_win32_returns_pwd_venv_build():
    config.execute_source = False
    location_instance = factory('win32')
    expected = os.path.join(
        os.path.dirname(__file__), 'libexec', 'executable')
    result = location_instance.get('executable')
    print(result, expected)
    assert(result == expected)


@mock.patch('netsplice.util.logger')
def test_get_binary_name_source_returns_python_for_netsplice_apps(
        mock_logger):
    config.execute_source = True
    l = location()
    l.set_name('NetspliceBackendApp')
    result = l.get_binary_name()
    print('result: %s' % (result,))
    assert('/python' in result)

    l = location()
    l.set_name('NetspliceNetApp')
    result = l.get_binary_name()
    print('result: %s' % (result,))
    assert('/python' in result)

    l = location()
    l.set_name('NetsplicePrivilegedApp')
    result = l.get_binary_name()
    print('result: %s' % (result,))
    assert('/python' in result)


@mock.patch('netsplice.util.logger')
def test_get_binary_name_prod_returns_name_for_netsplice_apps(mock_logger):
    config.execute_source = False
    l = location()
    l.set_name('NetspliceBackendApp')
    result = l.get_binary_name()
    print('result: %s' % (result,))
    assert(result.endswith('NetspliceBackendApp'))

    l = location()
    l.set_name('NetspliceNetApp')
    result = l.get_binary_name()
    print('result: %s' % (result,))
    assert(result.endswith('NetspliceNetApp'))

    l = location()
    l.set_name('NetsplicePrivilegedApp')
    result = l.get_binary_name()
    print('result: %s' % (result,))
    assert(result.endswith('NetsplicePrivilegedApp'))


@mock.patch('netsplice.util.logger')
def test_get_binary_name_prod_win32_returns_exe_name_for_any_app(mock_logger):
    org_platform = sys.platform
    sys.platform = 'win32'
    config.execute_source = False
    l = location()
    l.set_name('AnyApp')
    result = l.get_binary_name()
    sys.platform = org_platform
    print('result: %s' % (result,))
    assert(result.endswith('anyapp.exe'))


@mock.patch('netsplice.util.process.location.location.logger.error')
def test_get_binary_path_prod_raises_for_nonexisting(mock_logger):
    config.execute_source = False
    l = location()
    l.set_name('some-executable-name')
    try:
        result = l.get_binary_path()
        print(result)
        assert(False)  # No Error raised
    except ProcessError:
        assert(True)  # Error Raised as some-executable-name does not exist


@mock.patch('netsplice.util.logger')
def test_get_binary_path_prod_returns_string_for_existing(mock_logger):
    config.execute_source = False
    l = location()
    l.set_name(os.path.basename(__file__))
    l._self_executable_path = os.path.dirname(__file__)
    expected = os.path.join(
        os.path.dirname(__file__),
        os.path.basename(__file__))
    print(expected)
    try:
        result = l.get_binary_path()
        print(result, expected)
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        assert(False)


@mock.patch('netsplice.util.logger')
@mock.patch('netsplice.util.process.dispatcher.os.path.exists')
def test_get_binary_path_prod_win32_returns_exe_string_for_existing(
        mock_exists, mock_logger):
    config.execute_source = False
    org_platform = sys.platform
    sys.platform = 'win32'
    mock_exists.return_value = True
    l = location()
    l.set_name(os.path.basename(__file__))
    l._self_executable_path = os.path.dirname(__file__)

    expected = os.path.join(
        os.path.dirname(__file__),
        os.path.basename(__file__) + '.exe')
    try:
        result = l.get_binary_path()
        print(result, expected)
        sys.platform = org_platform
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        sys.platform = org_platform
        assert(False)


def side_effect_exists_for_binary_path(arg):
    if arg == '/usr/sbin/openvpn':
        return True
    if arg == '/usr/bin/openssl':
        return True
    if arg == '/some/path/some-prefix/some-executable':
        return True
    if arg == '/tmp/some-prefix/some-version/openvpn':
        return True
    return False


def side_effect_exists_for_resource_path(arg):
    if arg == '/some/path/some-prefix/some-resource':
        return True
    if arg == '/tmp/some-prefix/some-version/geoipv6':
        return True
    return False


@mock.patch('netsplice.util.logger')
@mock.patch('netsplice.util.process.dispatcher.os.path.exists')
def test_get_binary_path_with_prefix_locates_prefixed_executable(
        mock_exists, mock_logger):
    config.execute_source = False
    l = location()
    l.set_name('some-executable')
    l._self_executable_path = '/some/path'
    l.set_prefix('some-prefix')

    org_platform = sys.platform
    sys.platform = 'win32'
    mock_exists.return_value = True
    expected = '/some/path/some-prefix/some-executable.exe'
    try:
        result = l.get_binary_path()
        print(result, expected)
        sys.platform = org_platform
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        sys.platform = org_platform
        assert(False)


@mock.patch('netsplice.util.logger')
@mock.patch('netsplice.util.process.dispatcher.os.path.exists')
def test_get_binary_path_with_prefix_locates_not_prefixed_for_openssl(
        mock_exists, mock_logger):
    config.execute_source = False
    l = location()
    l.set_name('openssl')
    l._self_executable_path = '/tmp'
    l.set_prefix('some-prefix')

    mock_exists.side_effect = side_effect_exists_for_binary_path

    expected = os.path.join(
        '/',
        'usr',
        'bin',
        'openssl')
    try:
        result = l.get_binary_path()
        print(result, expected)
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        assert(False)


@mock.patch('netsplice.util.logger')
@mock.patch('netsplice.util.process.dispatcher.os.path.exists')
def test_get_binary_path_with_prefix_and_version_openvpn(
        mock_exists, mock_logger):
    '''
    Check that openvpn with prefix and version returns correct path.
    '''
    config.execute_source = False
    l = location()
    l.set_name('openvpn')
    l.set_prefix('some-prefix')
    l.set_version('some-version')
    l._self_executable_path = '/tmp'

    mock_exists.side_effect = side_effect_exists_for_binary_path

    expected = os.path.join(
        '/',
        'tmp',
        'some-prefix',
        'some-version',
        'openvpn')
    try:
        result = l.get_binary_path()
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        assert(False)


@mock.patch('netsplice.util.logger')
@mock.patch('netsplice.util.process.location.location.os.path.exists')
def test_get_binary_path_with_prefix_locates_with_prefix(
        mock_exists, mock_logger):
    config.execute_source = False
    l = location()
    l.set_name('some-executable')
    l.set_prefix('some-prefix')
    l._self_executable_path = '/some/path'

    mock_exists.side_effect = side_effect_exists_for_binary_path

    expected = '/some/path/some-prefix/some-executable'
    try:
        result = l.get_binary_path()
        print(result, expected)
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        assert(False)


def test_get_binary_search_path_source_returns_path_with_libexec_build():
    location_instance = location()
    location_instance.execute_source = True
    location_instance._self_executable_path = '/tmp'
    location_instance._install_path = '/tmp/install_path'
    r = location_instance.get_binary_search_paths()
    expected = ['/tmp', '/tmp/install_path/var/build']
    print(expected, r)
    assert(r == expected)


def test_get_binary_search_path_prod_returns_path_with_libexec():
    location_instance = location()
    location_instance.execute_source = False
    location_instance._self_executable_path = '/tmp'
    location_instance._install_path = '/tmp/install_path'
    r = location_instance.get_binary_search_paths()
    expected = ['/tmp', '/tmp/libexec']
    print(expected, r)
    assert(r == expected)


@mock.patch('netsplice.util.process.location.location.logger.debug')
def test_get_binary_search_path_logs_if_debug(
        mock_logger):
    flags.DEBUG = True
    location_instance = location()
    location_instance.execute_source = False
    location_instance._self_executable_path = '/tmp'
    location_instance._install_path = '/tmp/install_path'
    r = location_instance.get_binary_search_paths()
    flags.DEBUG = False
    mock_logger.assert_called()


def test_get_install_path_returns_string():
    location_instance = location()
    result = location_instance.get_script_path()
    print(result)
    assert(isinstance(result, (basestring, )))


def test_get_install_path_returns_checkout_root():
    location_instance = location()
    result = location_instance.get_install_path()
    test_path = os.path.dirname(os.path.realpath(__file__))
    expected = test_path.replace('src/netsplice/util/process/location', '')
    print(result, expected)
    assert(result == expected)


def test_get_script_path_returns_string():
    location_instance = location()
    result = location_instance.get_script_path()
    assert(isinstance(result, (basestring, )))


def test_get_binary_search_paths_custom_search_path_does_only_return_custom():
    location_instance = location()
    location_instance.set_custom_search_paths(['/bin', '/usr/bin'])
    result = location_instance.get_binary_search_paths()
    print(result)
    assert(result[0] == '/bin')
    assert(result[1] == '/usr/bin')



@mock.patch('netsplice.util.logger')
@mock.patch('netsplice.util.process.dispatcher.os.path.exists')
def test_get_resource_path_with_prefix_locates_prefixed_executable(
        mock_exists, mock_logger):
    config.execute_source = False
    l = location()
    l.set_name('some-resource')
    l._self_executable_path = '/some/path'
    l.set_prefix('some-prefix')

    org_platform = sys.platform
    sys.platform = 'win32'
    mock_exists.return_value = True
    expected = '/some/path/some-prefix/some-resource'
    try:
        result = l.get_resource_path()
        print(result, expected)
        sys.platform = org_platform
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        sys.platform = org_platform
        assert(False)


@mock.patch('netsplice.util.logger')
@mock.patch('netsplice.util.process.dispatcher.os.path.exists')
def test_get_resource_path_with_prefix_and_version_tor_geoip(
        mock_exists, mock_logger):
    '''
    Check that tor geoip with prefix and version returns correct path.
    '''
    config.execute_source = False
    l = location()
    l.set_name('geoipv6')
    l.set_prefix('some-prefix')
    l.set_version('some-version')
    l._self_executable_path = '/tmp'

    mock_exists.side_effect = side_effect_exists_for_resource_path

    expected = os.path.join(
        '/',
        'tmp',
        'some-prefix',
        'some-version',
        'geoipv6')
    try:
        result = l.get_resource_path()
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        assert(False)


@mock.patch('netsplice.util.logger')
@mock.patch('netsplice.util.process.location.location.os.path.exists')
def test_get_resource_path_with_prefix_locates_with_prefix(
        mock_exists, mock_logger):
    config.execute_source = False
    l = location()
    l.set_name('some-resource')
    l.set_prefix('some-prefix')
    l._self_executable_path = '/some/path'

    mock_exists.side_effect = side_effect_exists_for_resource_path

    expected = '/some/path/some-prefix/some-resource'
    try:
        result = l.get_resource_path()
        print(result, expected)
        assert(result == expected)  # No Error raised
    except Exception as errors:
        print(errors)
        assert(False)

