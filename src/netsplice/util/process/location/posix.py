# -*- coding: utf-8 -*-
# posix.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Location for binaries inside the /opt/netsplice/libexec directory.
Falls back to /usr/bin/openvpn
'''
import os
from netsplice.config import process as config
from netsplice.util.process.location.location import location


class posix(location):

    def __init__(self):
        location.__init__(self)

    def get(self, binary_name):

        binary_path = os.path.join(
            self.get_prefix_path(),
            config.LIBEXEC)
        if config.execute_source:
            # Developer/ExecuteSource mode
            binary_path = os.path.join(
                self.get_prefix_path(),
                config.LIBEXEC_BUILD)
        return os.path.join(binary_path, binary_name)
