# -*- coding: utf-8 -*-
# dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Process Dispatcher.

Process that finds its executable depending on the operating environment and
by configuration.
'''

import os
import subprocess
import sys
import threading
import netsplice.config.process as config

from netsplice.config import flags

from netsplice.model.process import (
    process as process_model
)
from netsplice.util import get_logger, get_timer
from netsplice.util.stream_to_log import stream_to_log
from netsplice.util.errors import NotInitializedError
from netsplice.util.process.errors import ProcessError
from netsplice.util.process.application import (
    factory as application_factory
)
from netsplice.util.process.elevator import (
    factory as elevator_factory
)
from netsplice.util.process.location import (
    factory as location_factory
)
from netsplice.util.process.reaper import reaper

logger = get_logger()
timer = get_timer()


class dispatcher(object):
    '''
    Dispatcher.

    that finds its executable depending on the operating environment and
    by configuration.
    '''

    def __init__(
            self, name, options,
            model, model_instance=None, elevated=False, unique=False):
        '''
        Initialize.

        Initialize members from options.
        '''
        if flags.DEBUG:
            logger.debug('Process dispatcher for %s' % (name,))
        self.elevated = elevated
        self.unique = unique
        self.execute_source = config.execute_source
        self.model = model
        if self.model is None:
            self.model = process_model()
        if model_instance is not None:
            self.model_instance = model_instance
        else:
            self.model_instance = self.model
        self._name = name
        self._prefix = None
        self._custom_search_paths = None
        self._version = None
        self._environment = None
        self._expected_error_code = None
        self._logger_extra = None
        self._logger_filter = None
        self._no_tty = False
        self._detach = False
        self._working_directory = None

        self._arguments = []
        self._model = process_model()
        self._process = None
        self._reaper = reaper()

        self._threading_lock = threading.RLock()
        self._stderr_reader = None
        self._stdout_reader = None

        self.set_arguments_from_options(options)

    def force_stop(self):
        '''
        Force Stop.

        Kill subprocess with the popen arguments used to create it
        '''
        self._reaper.kill_other_subprocess(self._popen_arguments)
        self._popen_arguments = None
        self._process = None
        self._stdout_reader = None
        self._stderr_reader = None

    def get_subprocess(self):
        '''
        Get Subprocess.

        Return the subprocess instance for polling in higher classes.
        '''
        if self._process is None:
            raise NotInitializedError('Not started yet')
        return self._process

    def get_subprocess_pid(self):
        '''
        Get Subprocess PID.

        Return the Subprocess pid.
        '''
        active_subprocess = self.get_subprocess()
        return active_subprocess.pid

    def get_commandline(self):
        '''
        Get commandline.

        Return the list that has been passed to the subprocess.
        '''
        return self._popen_arguments

    def set_arguments_from_options(self, options):
        '''
        Set Arguments from Options.

        Set the arguments for the process using a dictionary or a list.
        When the value of the dictionary is None, it is not added
        {'--host': 'localhost'} will start
        binary_path '--host' 'localhost'

        {'--hostportfile': 'localhost','12345','/etc/passwd'} will start
        binary_path '--hostportfile' 'localhost' '12345' '/etc/passwd'

        Beware, the dictionary order seems random, only use when the called
        process is able to use the arguments in any order:
        {'opt1': None, 'opt2': None, '1234': None}
        will start
        binary_path '1234' 'opt1' 'opt2'

        When the options are a list, the order really matters to the caller:
        ['opt1', 'opt2', '1234']
        binary_path 'opt1' 'opt2' '1234'
        '''
        self._arguments = []

        if isinstance(options, (list,)):
            for option in options:
                self._arguments.append(option)
            return

        for option in iter(options.keys()):
            self._arguments.append(str(option))
            value = options[option]
            if value is None:
                continue
            if isinstance(value, (list,)):
                for list_value in value:
                    self._arguments.append(str(list_value))
                continue
            self._arguments.append(str(value))

    def set_custom_search_paths(self, search_paths):
        '''
        Set the search paths to be used instead of the application libexec
        locations.
        '''
        self._custom_search_paths = search_paths

    def set_detach(self, detach):
        '''
        Set Detach.

        Ensure that the process is started in a separate process group
        '''
        self._detach = detach

    def set_environment(self, environment):
        '''
        Set Environment.

        Set a named_value_list as environment for the process.
        '''
        self._environment = dict()
        for named_value in environment:
            self._environment[named_value.name.get()] = named_value.value.get()

    def set_expected_error_code(self, expected_error_code):
        '''
        Set Expected Error Code.

        Avoid raising a exception when the expected error code occured during
        start_subprocess_sync
        '''
        self._expected_error_code = expected_error_code

    def set_logger_extra(self, extra):
        '''
        Set Logger Extra.

        Set extra value that is passed to the stream_to_log class
        '''
        self._logger_extra = extra

    def set_logger_filter(self, filter):
        '''
        Set Logger Filter.

        Set logger filter that hides certain messages from logging.
        '''
        self._logger_filter = filter

    def set_no_tty(self, no_tty):
        '''
        Set No TTY.

        Ensure that the process does not inherit the tty
        '''
        self._no_tty = no_tty

    def set_prefix(self, prefix):
        '''
        Set Prefix.

        Set a Prefix that will be inserted between the binary name and the
        search path.
        '''
        self._prefix = prefix

    def set_threading_lock(self, rlock_instance):
        '''
        Set Threading Lock.

        Set the threading lock that is passed to the stdout/stderr reader.
        '''
        self._threading_lock = rlock_instance

    def set_version(self, version):
        '''
        Set Version.

        Set the version for the binary. Enforces that the given version is
        located regardless of any system-libexec-path's.
        '''
        self._version = version

    def set_working_directory(self, working_directory):
        '''
        Set Working Directory.

        Set the working directory for the binary.
        '''
        if working_directory == '':
            self._working_directory = None
        else:
            self._working_directory = working_directory

    def start_application(self):
        '''
        Start Application.

        Start a process with the binary and the arguments.
        The application is then detached and exists in its own process
        structure and keeps running even if this process shuts down.
        Applications may not be elevated and it is not possible to access
        the applications stdout/stderr.
        Use this method instead of start_subprocess to handle applications
        in the operatingsystem that use wrappers (.app,.lnk).
        '''
        application = application_factory(sys.platform)
        application.set_arguments(self._arguments)
        application.set_environment(self._environment)
        application.set_detach(True)
        application.set_name(self._name)
        application.set_prefix(self._prefix)
        application.set_working_directory(self._working_directory)
        self._popen_arguments = application.get_process_arguments()
        self._process = application.start()

    def start_application_sync(self):
        '''
        Start Application and wait.

        Start a process with the binary and the arguments.
        The application is not detached and captures the stderr/stdout
        output. Use this method instead of
        start_subprocess to handle applications in the operatingsystem
        that use wrappers (.app,.lnk).
        Receive the returncode when the process finishes.
        '''
        application = application_factory(sys.platform)
        application.set_arguments(self._arguments)
        application.set_environment(self._environment)
        application.set_detach(False)
        application.set_name(self._name)
        application.set_prefix(self._prefix)
        application.set_working_directory(self._working_directory)
        self._popen_arguments = application.get_process_arguments()
        self._process = application.start()
        self._process.wait()
        return self._process.returncode

    def start_subprocess(self):
        '''
        Start Subprocess.

        Start the process with the binary and the arguments. Installs a stdout
        and stderr reader that commit messages into the log.
        '''
        timer.start('subprocess %s' % (self._name,))
        location = location_factory(sys.platform)
        location.set_name(self._name)
        location.set_version(self._version)
        location.set_prefix(self._prefix)
        location.set_custom_search_paths(self._custom_search_paths)

        popen_arguments = []
        popen_arguments.append(location.get_binary_path())
        popen_function = subprocess.Popen
        if self.execute_source and \
                self._name in config.SUBAPP_NAMES:
            popen_arguments.append(os.path.join(
                location.get_script_path(), '%s.py' % (self._name, )))

        for argument in self._arguments:
            popen_arguments.append(argument)
        if self.elevated:
            logger.info('elevating call: %s' % (str(popen_arguments),))
            elevator = elevator_factory(sys.platform)
            popen_arguments = elevator.modify_argv_list(popen_arguments)
            popen_function = elevator.get_process_constructor()
        try:
            if flags.DEBUG:
                logger.info('subprocess.Popen(%s)' % (str(popen_arguments),))

            startupinfo = None
            # The following is true only on Windows.
            if hasattr(subprocess, 'STARTUPINFO'):
                # On Windows, subprocess calls will show a command window by
                # default when run from Pyinstaller with the ``--noconsole``
                # option. Avoid this distraction. The Subprocesses Backend,
                # Net, Privileged should be usable as commandline tools for
                # admins, but the enduser must not see that.
                startupinfo = subprocess.STARTUPINFO()
                startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            if self.unique:
                self._reaper.kill_other_subprocess(popen_arguments)
            if self._process:
                self.stop()
            preexec_fn = None
            if self._no_tty:
                preexec_fn = os.setsid
            if self._detach:
                preexec_fn = os.setpgrp
            if self._working_directory is not None:
                os.chdir(self._working_directory)
            environment = None
            if self._environment is not None:
                environment = os.environ.copy()
                for (key, value) in self._environment.items():
                    if value is None and key in environment.keys():
                        del environment[key]
                        continue
                    environment[str(key)] = str(value)
            self._popen_arguments = popen_arguments
            self._process = popen_function(
                popen_arguments,
                stdin=subprocess.PIPE,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                startupinfo=startupinfo,
                preexec_fn=preexec_fn,
                env=environment)
            self._stdout_reader = stream_to_log(
                self._process.stdout,
                'stdout_' + self._name,
                lock=self._threading_lock,
                logger_extra=self._logger_extra,
                filter=self._logger_filter,
                process=self._process)
            self._stderr_reader = stream_to_log(
                self._process.stderr,
                'stderr' + self._name,
                lock=self._threading_lock,
                logger_extra=self._logger_extra,
                filter=self._logger_filter,
                process=self._process)
            self._stdout_reader.start()
            self._stderr_reader.start()
            if flags.DEBUG:
                logger.debug('Subprocess is now active')
        except subprocess.CalledProcessError as errors:
            # set_error(1000)
            message = 'CalledProcessError: %s' % (str(errors),)
            logger.error(message)
            raise ProcessError(message)
        except OSError as errors:
            # set_error(1001)
            message = 'OSError: %s' % (str(errors),)
            logger.error(message)
            raise ProcessError(message)
        try:
            timer.start('subprocess communicate %s' % (self._name,))
            try:
                out, errors = self._process.communicate(timeout=1)
            except subprocess.TimeoutExpired:
                if self._process.returncode is not None:
                    self.stop()
            timer.stop('subprocess communicate %s' % (self._name,))
        except AttributeError:
            # python 2 has no subprocess.TimeoutExpired
            # do not log, ping uses this and outputs every second or so
            pass
        timer.stop('subprocess %s' % (self._name,))

    def start_subprocess_sync(self):
        '''
        Start Subprocess Sync.

        Start the subprocess and wait for it to complete, capturing all
        output.
        '''
        location = location_factory(sys.platform)
        location.set_name(self._name)
        location.set_version(self._version)
        location.set_prefix(self._prefix)
        location.set_custom_search_paths(self._custom_search_paths)

        check_output_arguments = []
        check_output_arguments.append(location.get_binary_path())

        if self.execute_source and \
                self._name in config.SUBAPP_NAMES:
            check_output_arguments.append(
                os.path.join(
                    location.get_script_path(),
                    '%s.py' % (self._name, )))

        environment = None
        if self._environment is not None:
            environment = os.environ.copy()
            for (key, value) in self._environment.items():
                if value is None and key in environment.keys():
                    del environment[key]
                    continue
                environment[str(key)] = str(value)

        for argument in self._arguments:
            check_output_arguments.append(argument)
        try:
            if flags.DEBUG:
                message = (
                    'subprocess.check_output(%s)'
                    % (str(check_output_arguments),))
                logger.info(message)
            if self.unique:
                self._reaper.kill_other_subprocess(check_output_arguments)
            output = subprocess.check_output(
                check_output_arguments,
                stderr=subprocess.STDOUT,
                env=environment)
            if flags.DEBUG:
                message = 'Subprocess completed'
                logger.info(message)
        except subprocess.CalledProcessError as errors:
            if self._expected_error_code is not errors.returncode:
                # Only raise when the errorcode is not expected
                logger.error(str(errors))
                raise ProcessError(errors.output)
            output = errors.output
        return output

    def stop(self):
        '''
        Stop.

        Release all threads and resources to avoid defunct processes.
        '''
        if self._process:
            try:
                self._process.terminate()
            except OSError:
                pass
        if self._stdout_reader:
            self._stdout_reader.stop()
        if self._stderr_reader:
            self._stderr_reader.stop()
        self._process = None
        self._popen_arguments = []
        self._stdout_reader = None
        self._stderr_reader = None
