# -*- coding: utf-8 -*-
# __init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Parser Factory. Produces parser for all know types
'''
import sys

from netsplice.util.parser.line import line as line_parser
from netsplice.util.errors import UnknownParserTypeError

this = sys.modules[__name__]
this.parser = dict()


def get_parser():
    '''
    Get Parser.

    Return all registered parser - for testing purposes.
    '''
    return this.parser


def register_parser(type, constructor):
    '''
    Register Parser.

    Register the parser with the given constructor for the given type.

    Arguments:
        type -- string to identify the parser in the code later.
        constructor -- class reference to construct with factory.
    '''
    this.parser[type] = constructor


def reset_registered():
    '''
    Reset Registered.

    Forget all registered parsers - for testing purposes.
    '''
    this.parser = dict()


def factory(type):
    '''
    Factory.

    Return a parser for the given type if it exists.

    Arguments:
        type -- string for a registered parser.

    Raises a {UnknownParserTypeError} when no parser was registered for type.
    '''
    try:
        return this.parser[type]()
    except KeyError:
        raise UnknownParserTypeError(type)

# Register Line parser by default.
register_parser('Line', line_parser)
