# -*- coding: utf-8 -*-
# base.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Configuration Parser Base.

Base class for configuration parser. Define functions that are common
to all parsers or only need minor changes for the specific implementations.
'''
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.model.configuration_list import (
    configuration_list as configuration_list_model
)
from netsplice.util.errors import (
    InvalidUsageError
)


class base(object):
    '''
    Parser Base.

    Functions common to all parsers. The base parser is able to split text
    files into elements that were separated by spaces.
    '''

    def __init__(self):
        '''
        Initialize.

        Initialize members.
        '''
        self.elements = []
        self.current_element = configuration_item_model(0)
        self.current_token = ''

    def analyze(self):
        '''
        Analyze.

        Store the current token to the current element values when the token
        is not empty.
        This function is likely to be overloaded by a specialized parser.
        '''
        if self.current_token == '':
            return False
        self.current_element.values.add_value('token', self.current_token)
        return True

    def commit(self):
        '''
        Commit current element.

        Append the current element to the elements list and prepare a new
        instance for the next analyze calls.
        This function is likely to be overloaded by a specialized parser.
        '''
        self.elements.append(self.current_element)
        self.current_element = configuration_item_model(len(self.elements))

    def get_config(self):
        '''
        Get Config.

        Transform the current element list to a configuration list model.
        This function will be used to get a structured list of configuration
        items for display or manipulation.
        This function is likely to be overloaded by a specialized parser.
        '''
        config = configuration_list_model()
        for element in self.elements:
            config.append(element)
        return config

    def get_element_index(self, configuration_item):
        '''
        Get Element Index.

        Return the index of the given configuration item or raise a
        InvalidUsageError when no configuration item is provided.
        '''
        if configuration_item is None:
            raise InvalidUsageError(
                'configuration_item_model instance required')
        return configuration_item.index.get()

    def get_insert_template(self):
        '''
        Get Insert Template.

        Return a Configuration Item Model that can be used to insert a new
        instance in the configuration.
        '''
        template = configuration_item_model(0)
        template.add_value('token', '')
        return template

    def get_text(self):
        '''
        Get Text.

        Process the elements of a previously parsed configuration and return
        a plaintext representation. The result is used to create a config
        that can be consumed by an external application or for advanced users
        to edit previously changed configuration_items.
        This function is likely to be overloaded by a specialized parser.
        '''
        text = ''
        for item in self.elements:
            if text != '':
                text += ' '
            text += item.values.get_value('token')
        return text

    def reset(self):
        '''
        Reset.

        Reset the list of elements and the current element to a empty state.
        '''
        self.elements = []
        self.current_element = configuration_item_model(len(self.elements))

    def set_config(self, configuration_list):
        '''
        Set Config.

        Set a configuration list to the internal elements. This is usualy
        used to transform a programmatically changed configuration list to
        a plaintext config file that can be saved or displayed.
        '''
        if configuration_list is None:
            raise InvalidUsageError(
                'configuration_list instance required')
        self.reset()
        for item in configuration_list:
            self.elements.append(item)

    def set_text(self, text):
        '''
        Set Text.

        Process the given text with the algorithm required for the source.
        The base implementation splits the text on whitespaces.
        This function is likely to be overloaded by a specialized parser.
        '''
        if text is None:
            raise InvalidUsageError('text must not be None')
        self.reset()
        tokens = text.split()
        for token in tokens:
            self.current_token = token
            if self.analyze():
                self.commit()
