# -*- coding: utf-8 -*-
# line.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Line based Configuration Parser.

Parser that evaluates each line in a text. The configuration_item will have
the line as key and as named value 'line'.
'''
from netsplice.util.parser.base import base as parser_base
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.util.errors import (
    InvalidUsageError
)


class line(parser_base):
    '''
    Line Parser.

    Use the parser_base implementation for set_config, reset.
    '''

    def __init__(self):
        '''
        Initialize.

        Initialize members and base.
        '''
        parser_base.__init__(self)
        self.current_line = None

    def analyze(self):
        '''
        Analyze.

        Store the current line to the current element values when the line
        is not empty.
        '''
        if self.current_line is None:
            return False
        self.current_element.key.set(self.current_line)
        self.current_element.add_value('line', self.current_line)
        return True

    def commit(self):
        '''
        Commit current element.

        Append the current element to the elements list and prepare a new
        instance for the next analyze calls.
        '''
        parser_base.commit(self)
        self.current_line = None

    def get_insert_template(self):
        '''
        Get Insert Template.

        Return a Configuration Item Model that can be used to insert a new
        instance in the configuration.
        '''
        template = configuration_item_model(0)
        template.key.set('')
        template.add_value('line', '')
        return template

    def get_text(self):
        '''
        Get Text.

        Process the elements of a previously parsed configuration and return
        a plaintext representation. The result is used to create a config
        that can be consumed by an external application or for advanced users
        to edit previously changed configuration_items.
        The line implementation sorts the current element list by index
        and appends the 'line' values separated by a newline to the
        return-string.
        '''
        text = ''
        ordered_elements = sorted(
            self.elements, key=self.get_element_index)
        for element in ordered_elements:
            if text != '':
                text += '\n'
            text += element.get_value('line')
        return text

    def set_text(self, text):
        '''
        Set Text.

        Process the given text with the algorithm required for the source.
        The line implementation splits the text on newlines.
        '''
        if text is None:
            raise InvalidUsageError('text must not be none')

        self.reset()
        lines = text.split('\n')
        for line in lines:
            if line == '':
                continue
            self.current_line = line
            if self.analyze():
                self.commit()
