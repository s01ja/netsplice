# -*- coding: utf-8 -*-
# test_line.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Tests for Parser Line
'''

import mock
import os
import sys
from .line import line
from netsplice.model.configuration_item import (
    configuration_item as configuration_item_model
)
from netsplice.model.configuration_list import (
    configuration_list as configuration_list_model
)
from netsplice.util.errors import (
    InvalidUsageError
)


def get_instance():
    instance = line()
    return instance


def test_analyze_with_empty_current_line_returns_false():
    instance = get_instance()
    instance.current_line = None
    result = instance.analyze()
    print(result)
    # analyze should not do anything with empty current
    assert(result is False)


def test_analyze_with_current_line_returns_true():
    instance = get_instance()
    instance.current_line = 'testvalue'
    result = instance.analyze()
    # analyze should not do anything with empty current
    print(result)
    assert(result is True)


def test_analyze_with_current_line_sets_current_element():
    instance = get_instance()
    instance.current_line = 'testvalue'
    instance.analyze()
    assert(instance.current_element.key.get() == 'testvalue')
    assert(instance.current_element.get_value('line') == 'testvalue')


def test_commit_resets_current_line():
    instance = get_instance()
    instance.current_line = 'testvalue'
    instance.commit()
    assert(instance.current_line is None)
    instance.current_line = 'testvalue'
    instance.analyze()
    instance.commit()


def test_get_insert_template_returns_empty_configuration_item():
    instance = get_instance()
    result = instance.get_insert_template()
    assert(isinstance(result, (configuration_item_model,)))
    assert(result.get_key() == '')
    assert(result.get_value('line') == '')


def test_get_text_without_elements_returns_empty_string():
    instance = get_instance()
    result = instance.get_text()
    assert(result == '')


def test_get_text_without_elements_returns_multiline_string():
    instance = get_instance()
    instance.current_line = 'testline'
    instance.analyze()
    instance.commit()
    instance.current_line = 'testline'
    instance.analyze()
    instance.commit()
    result = instance.get_text()
    assert(result == 'testline\ntestline')


def test_get_text_with_one_element_returns_line_without_trailing_newline():
    instance = get_instance()
    instance.current_line = 'testline'
    instance.analyze()
    instance.commit()
    result = instance.get_text()
    assert(result == 'testline')


def test_set_text_with_none_raises():
    instance = get_instance()
    try:
        instance.set_text(None)
        assert(False)  # expected Exception
    except InvalidUsageError:
        assert(True)  # named Exception raised
    except Exception as error:
        print(error)
        assert(False)  # unknown Exception raised


@mock.patch('netsplice.util.parser.line.line.analyze')
def test_set_text_with_single_line_calls_analyze(mock_analyze):
    instance = get_instance()
    instance.set_text('line')

    assert(mock_analyze.called == 1)


@mock.patch('netsplice.util.parser.line.line.analyze')
def test_set_text_with_multi_line_produces_multi_elements(mock_analyze):
    instance = get_instance()
    instance.set_text('line\nline')
    assert(mock_analyze.called is True)
    assert(len(instance.elements) == 2)


@mock.patch('netsplice.util.parser.line.line.commit')
@mock.patch('netsplice.util.parser.line.line.analyze')
def test_set_text_with_single_line_calls_commit(mock_analyze, mock_commit):
    instance = get_instance()
    instance.set_text('line')

    assert(mock_commit.called == 1)


@mock.patch('netsplice.util.parser.line.line.commit')
@mock.patch('netsplice.util.parser.line.line.analyze')
def test_set_text_with_empty_string_not_calls_commit_or_analyze(
        mock_analyze, mock_commit):
    instance = get_instance()
    instance.set_text('')

    assert(mock_analyze.called == 0)
    assert(mock_commit.called == 0)
