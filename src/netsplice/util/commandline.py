# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Parses the command line arguments passed to the application.
'''

import argparse


# XXX DRY me with variables and functions ...

def build_parser_netsplice():
    '''
    All the options for the arg parser.
    The options are intended for the netsplice/NetspliceApp command
    and reflect doc/man/netsplice.1.rst.
    '''
    parser = argparse.ArgumentParser(
        description='Launches the Netsplice client.',
        epilog='Copyright 2016 Netsplice Project')

    parser.add_argument('-d', '--debug', action='store_true',
                        help=('Launch Netsplice in debug mode, write'
                              ' debug info to stdout or logfile.'))
    parser.add_argument('-V', '--version', action='store_true',
                        help='Display version and exits.')

    # log options
    parser.add_argument('-l', '--log', nargs='?',
                        action='store', dest='logfile',
                        help='Write Log to file')
    # port option
    parser.add_argument('-p', '--port', nargs='?',
                        type=int,
                        action='store', dest='port',
                        help='Connect to specified port. Defaults to 5151')

    # owner-service port option
    parser.add_argument('-o', '--port-owner', nargs='?',
                        type=int,
                        action='store', dest='port_owner',
                        help=(
                            'Connect back to specific port'
                            ' (to send logs and status'))

    # owner-service host option
    parser.add_argument('-O', '--host-owner', nargs='?',
                        action='store', dest='host_owner',
                        help=(
                            'Connect back to specific host'
                            ' (to send logs and status'))

    # host option
    parser.add_argument('-H', '--host', nargs='?',
                        action='store', dest='host',
                        help='connect to specified host/ip. '
                             'Defaults to localhost')

    # hmac secret
    parser.add_argument('-S', '--hmac_secret', nargs='?',
                        action='store', dest='hmac_secret',
                        help='Specify a 64byte secret for signing REST calls'
                             'Defaults to random')

    # config home option (NetsplicePrivilegedApp)
    parser.add_argument('-c', '--config-home', nargs='?',
                        action='store', dest='config_home',
                        help='Specify a location that contains the configs'
                             'Defaults to ${HOME}/.config')
    # kill pid option (NetsplicePrivilegedApp)
    parser.add_argument('-k', '--kill', nargs='?',
                        type=int,
                        action='store', dest='kill',
                        help='Specify a pid to be killed.')

    return parser


def build_parser_netsplice_gui():
    '''
    All the options for the arg parser.
    The options are intended for the netsplice-gui/NetspliceGuiApp command
    and reflect doc/man/netsplice-gui.1.rst.
    '''
    parser = argparse.ArgumentParser(
        description='Launches the Netsplice GUI.',
        epilog='Copyright 2016 Netsplice Project')

    parser.add_argument('-d', '--debug', action='store_true',
                        help=('Launch Netsplice in debug mode, write '
                              'debug info to stdout or logfile.'))
    parser.add_argument('-V', '--version', action='store_true',
                        help='Display version and exits.')

    # log option
    parser.add_argument('-l', '--log', nargs='?',
                        action='store', dest='logfile',
                        help='Write Log to file')

    # key option
    parser.add_argument('-k', '--key', nargs='?',
                        action='store', dest='connectionkey',
                        help='Sign all privileged commands for '
                             'NetsplicePrivileged')

    # port option
    parser.add_argument('-p', '--port', nargs='?',
                        type=int,
                        action='store', dest='port',
                        help='Connect to specified port. Defaults to 5151')

    # host option
    parser.add_argument('-h', '--host', nargs='?',
                        action='store', dest='host',
                        help='connect to specified host/ip. '
                             'Defaults to localhost')
    return parser


def build_parser_netsplice_privileged():
    '''
    All the options for the arg parser.
    The options are intended for the
    netsplice-privileged/NetsplicePrivilegedApp command
    and reflect doc/man/netsplice-privileged.1.rst.
    '''
    parser = argparse.ArgumentParser(
        description='Launches the Netsplice Privileged.',
        epilog='Copyright 2016 Netsplice Project')

    parser.add_argument('-d', '--debug', action='store_true',
                        help=('Launch Netsplice in debug mode, write '
                              'debug info to stdout or logfile.'))
    parser.add_argument('-V', '--version', action='store_true',
                        help='Display version and exits.')

    # log option
    parser.add_argument('-l', '--log', nargs='?',
                        action='store', dest='logfile',
                        help='Write Log to file')

    # key option
    parser.add_argument('-k', '--key', nargs='?',
                        action='store', dest='connectionkey',
                        help='Allow only commands signed by clients with the '
                             'same key')

    # port option
    parser.add_argument('-p', '--port', nargs='?',
                        type=int,
                        action='store', dest='port',
                        help='Listen on specified port. Defaults to 5151')

    # owner-service port option
    parser.add_argument('-o', '--port-owner', nargs='?',
                        type=int,
                        action='store', dest='port_owner',
                        help=(
                            'Connect back to specific port'
                            ' (to send logs and status'))

    # owner-service host option
    parser.add_argument('-O', '--host-owner', nargs='?',
                        action='store', dest='host_owner',
                        help=(
                            'Connect back to specific host'
                            ' (to send logs and status'))

    # host option
    parser.add_argument('-h', '--host', nargs='?',
                        action='store', dest='host',
                        help='Listen on specified host/ip. '
                             'Defaults to localhost')
    return parser


def get_options():
    '''
    Get the command line options used when the app was started.

    :return: the command options
    :rtype: argparse.Namespace
    '''
    parser = build_parser_netsplice()
    opts, unknown = parser.parse_known_args()

    return opts
