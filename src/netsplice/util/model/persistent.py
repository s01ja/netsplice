# -*- coding: utf-8 -*-
# persistent.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Persistent.

Persistence base for marshaled models. To be used as mixin to marshalables.
Overwrites commit to handle the storage of the different required outputs.
  - preferences.backend -> /preferences/backend.json (object)
  - preferences.overrides -> /preferences/overrides.json (array)
  - preferences.plugins -> /preferences/plugins/plugin_name.json (array)
  - preferences.accounts -> /preferences/accounts/account_id.json (inst col)
    + /preferences/accounts/account_id.plugin_name.json when instance plugins
    are used.
'''

from netsplice.util.errors import NotFoundError
from netsplice.util.model.errors import ValidationError
from netsplice.util.model.marshalable import marshalable
from netsplice.util.model.reader import reader as reader_dispatcher
from netsplice.util.model.writer import writer as writer_dispatcher

from netsplice.util import get_logger

import netsplice.config.model as config

logger = get_logger()

PLUGIN_PATH = 'plugins'


class persistent(object):
    '''
    Base class for all models that are stored on disk and read later in time.
    '''

    def __init__(self, owner, location=None):
        self._owner = owner
        self._location = location
        self._migrated = False
        self._reader = None
        self._writer = None

    def commit(self):
        '''
        Commit.

        Commit the fields and persist them.
        Calls all committers that are expected to do nothing when they are not
        responsible for the collection or instance.
        '''
        self.commit_plugins()
        self.commit_plugin_instance()
        self.commit_instance()
        self.commit_instances()
        marshalable.commit(self)

    def commit_instance(self):
        '''
        Commit Instance.

        Commit core value. When the current model has a id-attribute, a
        instance in the property path (accounts/{account_id}.js) is written.
        Otherwise a instance matching the location in the preferences root
        will be written.
        '''
        if isinstance(self, (list,)):
            return
        location = self.get_location()
        if location is None:
            return
        if PLUGIN_PATH in location:
            return
        writer = self.get_writer()
        try:
            instance_id = self.id.get()
            writer.set_model(self.get_owner())
            writer.instance(location, instance_id)
        except AttributeError:
            writer.set_model(self)
            writer.root_object(location)

    def commit_instances(self):
        '''
        Commit Instances.

        Write the core instances.
        '''
        if not isinstance(self, (list,)):
            return
        location = self.get_location()
        if location is None:
            return
        if PLUGIN_PATH in location:
            return
        writer = self.get_writer()
        writer.set_model(self)
        if self.separate_instances:
            writer.instances(location)
        else:
            writer.root_object(location)

    def commit_plugins(self, name=None):
        '''
        Commit Plugins.

        Commit general plugin values.
        '''
        location = self.get_location()
        if location is None:
            return
        if PLUGIN_PATH not in location:
            return
        if isinstance(self.get_owner(), (list,)):
            return
        writer = self.get_writer()
        for plugin_item in self.get_owner().plugins:
            plugin_name = plugin_item.name.get()
            if name is not None and name != plugin_name:
                continue
            writer.set_model(plugin_item)
            writer.plugin(location, plugin_name)
            plugin_item.apply_values()

    def commit_plugin_instance(self):
        '''
        Commit Plugin Instances.

        Commit the plugin values for instances.
        '''
        if not isinstance(self.get_owner(), (list,)):
            return
        location = self.get_owner().get_location()
        if location is None:
            return
        if PLUGIN_PATH not in self.get_owner().__dict__.iterkeys():
            return
        writer = self.get_writer()
        for plugin_item in self.get_owner().plugins:
            plugin_name = plugin_item.name.get()
            try:
                self.__dict__[plugin_name]
                writer.set_model(self)
                writer.instance_plugin(
                    location, self.id.get(), plugin_name)
            except KeyError:
                logger.warn(
                    'No Plugin value for %s/%s.%s.json'
                    % (location, self.id.get(), plugin_name))
                continue

    def get_location(self):
        '''
        Get Location.

        Get the location where the model is persisted.
        Return the name of the location when no owner is defined.
        Return the attribute in the owning model when it is a direct property.
        Return the collection name in the owning model when it is a instance
        in one of the collections.
        '''
        if self.get_owner() is None:
            return self._location
        for attr in self.get_owner().__dict__:
            if isinstance(self.get_owner().__dict__[attr], (type(self),)):
                return attr
        for attr in self.get_owner().__dict__:
            if not isinstance(self.get_owner().__dict__[attr], (list,)):
                continue
            for item in self.get_owner().__dict__[attr]:
                if item != self:
                    continue
                return attr
        if self._location is None:
            return self.get_owner().get_location()
        return self._location

    def get_migrated(self):
        '''
        Get Migrated.

        Get the migration status of the model.
        '''
        return self._migrated

    def get_owner(self, parent=None):
        '''
        Get owner.

        Return owner that was passed during creation.
        '''
        return self._owner

    def get_reader(self, parent=None):
        '''
        Get Reader.

        Return Reader that can load the persisted information. Traverses the
        owners so only the root needs to
        '''
        return reader_dispatcher(None)

    def get_root(self, parent=None):
        '''
        Get Root.

        Return root of the persistent marshalables.
        '''
        if self.get_owner() is None:
            return self
        return self.get_owner().get_root(parent)

    def get_writer(self, parent=None):
        '''
        Get Writer.

        Return writer that can write the persisted information.
        '''
        return writer_dispatcher(None)

    def load(self):
        '''
        Load.

        Load the persistent information.
        '''
        self.load_plugins()
        self.load_plugin_instances()
        self.load_instance()
        self.load_collection()

    def load_collection(self):
        '''
        Load Collection.

        Load core values. Read all instances into the owning collection.
        '''
        if not isinstance(self, (list,)):
            return
        location = self.get_location()
        if location is None:
            return
        if PLUGIN_PATH in location:
            return
        reader = self.get_reader()
        reader.set_model(self)
        if self.separate_instances:
            reader.instances(location)
        else:
            reader.root_object(location, allow_fail=True)

    def load_instance(self):
        '''
        Load Instance.

        Load core value. When the current model has a id-attribute, a instance
        in the property path (accounts/{account_id}.js) is loaded. Otherwise
        a instance matching the location in the preferences root will be
        loaded.
        '''
        if isinstance(self, (list,)):
            return
        location = self.get_location()
        if location is None:
            return
        if PLUGIN_PATH in location:
            return
        reader = self.get_reader()
        try:
            reader.set_model(self.get_owner())
            reader.instance(location, self.id.get())
            if self.get_owner().get_migrated():
                self.set_migrated(True)
        except AttributeError:
            reader.set_model(self)
            reader.root_object(location, allow_fail=True)

    def load_plugins(self):
        '''
        Load Plugins.

        Load persistent plugin values. All registered plugins are read.
        '''
        location = self.get_location()
        if location is None:
            return
        if PLUGIN_PATH not in location:
            return
        if isinstance(self.get_owner(), (list,)):
            return
        reader = self.get_reader()
        for plugin_item in self:
            plugin_name = plugin_item.name.get()
            reader.set_model(plugin_item)
            try:
                reader.plugin(PLUGIN_PATH, plugin_name, allow_fail=True)
                plugin_item.apply_values()
                if plugin_item.get_migrated():
                    self.set_migrated(True)
            except ValidationError:
                logger.warn('Invalid plugin config: %s' % (plugin_name,))
                continue

    def load_plugin_instances(self):
        '''
        Load Plugin Instances.

        Load persistent plugin values. When the owning marshalable is a list,
        all items will issue a instance_plugin read.
        '''
        if not isinstance(self.get_owner(), (list,)):
            return
        location = self.get_location()
        if location is None:
            return
        if PLUGIN_PATH not in location:
            return
        reader = self.get_reader()
        item_location = self.get_owner().get_location()
        for item in self.get_owner():
            for plugin_item in self:
                plugin_name = plugin_item.name.get()
                # load stored settings for each item
                try:
                    reader.set_model(item)
                    reader.instance_plugin(
                        item_location, item.id.get(),
                        plugin_name)
                    if item.get_migrated():
                        self.set_migrated(True)
                except ValidationError:
                    logger.warn(
                        'Invalid Plugin Instance config: %s/%s.%s.json'
                        % (item_location, item.id.get(), plugin_name))
                    continue

    def remove_by_id(self, instance_id):
        '''
        '''
        writer = self.get_writer()
        location = self.get_location()
        writer.unlink_item(location, instance_id)

    def remove(self, instance):
        '''
        '''
        writer = self.get_writer()
        location = self.get_location()
        try:
            if self.separate_instances:
                writer.set_model(instance)
                writer.unlink(location)
        except AttributeError:
            pass
        try:
            writer.set_model(instance)
            for plugin_item in self.plugins:
                writer.unlink_plugin(location, plugin_item.name.get())
        except AttributeError:
            pass
        try:
            list.remove(self, instance)
        except NotFoundError:
            pass

    def set_migrated(self, migration_status):
        '''
        Set Migrated.

        Set the migrated model flag.
        '''
        self._migrated = migration_status

    def set_reader(self, reader):
        '''
        Set Reader.

        Set the reader object.
        '''
        self._reader = reader

    def set_writer(self, writer):
        '''
        Set Writer.

        Set the writer object.
        '''
        self._writer = writer
