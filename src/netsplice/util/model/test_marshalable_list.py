# -*- coding: utf-8 -*-
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Test marshal wrapper.
'''

import json

from .field import field
from .marshalable import marshalable
from .marshalable_list import marshalable_list
from netsplice.config import flags as config_flags


class simple_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=False)
        self.name = field(required=False)
        self.address = field(required=False)


class simple_required_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=True)
        self.name = field(required=True)
        self.address = field(required=True)

class simple_clean_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=False)
        self.clean_called = False
    def clean(self):
        self.clean_called = True


class nested_model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.id = field(required=False)
        self.simples = simple_list_model()


class simple_list_model(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, simple_model)


def test_to_json_with_simple_model_returns_string():
    m = simple_model()
    e = '{"id": null, "name": null, "address": null}'
    a = m.to_json()
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_to_json_with_simple_model_debug_returns_formated_string():
    m = simple_model()
    e = '{"id": null, "name": null, "address": null}'
    config_flags.DEBUG = True
    a = m.to_json()
    config_flags.DEBUG = False
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))
    assert(',\n' in a)


def test_to_json_with_empty_simple_list_model_returns_string():
    m = simple_list_model()
    e = '[]'
    a = m.to_json()
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_to_json_with_simple_list_model_with_values_returns_string():
    m = simple_list_model()
    mi = simple_model()
    mi.id.set(0)
    mi.name.set('netsplice')
    mi.address.set('https://netsplice.org')
    m.append(mi)

    e = '[{"id": 0, "name": "netsplice", "address": "https://netsplice.org"}]'
    a = m.to_json()
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_to_json_with_simple_list_model_with_multiple_values_returns_string():
    m = simple_list_model()
    mi = simple_model()
    mi.id.set(0)
    mi.name.set('netsplice')
    mi.address.set('https://netsplice.org')
    m.append(mi)
    mi = simple_model()
    mi.id.set(0)
    mi.name.set('other')
    mi.address.set('https://other.org')
    m.append(mi)
    e = ('[{"id": 0, "name": "netsplice", '
         '"address": "https://netsplice.org"}, '
         '{"id": 0, "name": "other", '
         '"address": "https://other.org"}'
         ']'
         )
    a = m.to_json()
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_from_json_with_empty_list_cleans_items():
    m = simple_list_model()
    mi = simple_model()
    mi.id.set(0)
    mi.name.set('netsplice')
    mi.address.set('https://netsplice.org')
    m.append(mi)
    assert(len(m) is 1)
    m.from_json('[]')
    assert(len(m) is 0)


def test_from_json_with_items_fills_items():
    m = simple_list_model()
    m.from_json('[{"name": "name1"}, {"name": "name2"}]')
    assert(len(m) is 2)
    assert(m[0].name.get() == "name1")
    assert(m[1].name.get() == "name2")


def test_from_json_with_values_for_nested_model_sets_values():
    m = nested_model()

    j = (
        '{"id": 0, "simples": [{"id":"0", "name": "netsplice",'
        ' "address": "https://netsplice.org"}]}'
    )
    m.from_json(j)
    print(m, m.to_json())
    assert(m.id.get() is 0)
    assert(len(m.simples) is 1)
    assert(m.simples[0].address.get() == 'https://netsplice.org')


def test_to_json_with_empty_values_for_nested_model_sets_values():
    m = nested_model()
    m.id.set(0)
    e = '{"id": 0, "simples": []}'
    a = m.to_json()
    print('expected', e)
    print('actual', a)
    assert(json.loads(a) == json.loads(e))


def test_is_valid_jsons_instance_returns_false():
    m = marshalable_list(simple_model)
    result = m.is_valid_jsons('{"id": "test"}')
    print(result)
    assert(result is False)


def test_is_valid_jsons_one_invalid_instance_returns_false():
    m = marshalable_list(simple_required_model)
    result = m.is_valid_jsons(
        '['
        '{"id": "id1", "name":"name", "address": "address"},'
        '{"id": "id2", "name":"name", "address": "address"},'
        '{"invalid": "simple"}'
        ']')
    print(result)
    assert(result is False)

def test_clean_calls_clean_for_items():
    m = marshalable_list(simple_clean_model)
    i = m.item_model_class()
    m.append(i)
    m.clean()
    assert(i.clean_called is True)
