# -*- coding: utf-8 -*-
# reader.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Read marshaled objects from files.
'''
import os
import re

from netsplice.config import flags
from netsplice.config import backend as config
from netsplice.util.errors import (AccessError, NotFoundError)
from netsplice.util.model.errors import ValidationError
from netsplice.util.path import get_preferences_location
from netsplice.util import get_logger

logger = get_logger()


class reader(object):
    '''
    Reader.

    Preference reader that acts on a model.
    '''
    def __init__(self, model=None):
        self.model = model

    def check_access(self):
        '''
        Check access.

        Check that we can access the locations for the preference files.
        '''
        location = get_preferences_location('')
        if not os.path.exists(location):
            # first start / cleaned preferences
            return
        if not os.access(location, os.R_OK):
            raise AccessError(
                'Preferences location %s cannot be read'
                % (location,))

    def instance(self, property_name, instance_id):
        '''
        Instance.

        Read a json file with the given instance_id as filename in a
        preferences_location defined by property_name. Works on a instance
        model and ensures that a existing instance with that id is overwritten.
        When a ValidationError occurs during load a migration is attempted and
        if that fails too, a existing instance is not changed and no new
        instance is created.
        '''
        pathname = get_preferences_location(property_name)
        filename = os.path.join(pathname, '%s.json' % (instance_id,))
        try:
            owner_model = self.model.get_owner()
            try:
                item_model = owner_model.find_by_id(instance_id)
                # persistent.remove implements unlink
                # this is not what we want here
                list.remove(owner_model, item_model)
                restore_on_validation_error = True
            except NotFoundError:
                item_model = owner_model.item_model_class(owner_model)
                restore_on_validation_error = False

            try:
                with open(filename) as file_handle:
                    file_content = file_handle.read(config.MAX_CONFIG_SIZE)
                    if flags.DEBUG:
                        logger.debug(
                            'Preferences instance read: %s %s'
                            % (filename, file_content))
                    if file_content is None:
                        file_content = '{}'
                    try:
                        item_model.from_json(file_content)
                    except ValueError:
                        item_model.migrate('{}')
                        self.model.set_migrated(True)
                    except ValidationError:
                        item_model.migrate(file_content)
                        self.model.set_migrated(True)
                    owner_model.append(item_model)
            except ValidationError:
                if restore_on_validation_error:
                    # keep old (default) value
                    owner_model.append(item_model)
                raise
        except IOError as errors:
            self.model.migrate('{}')
            self.model.set_migrated(True)
            logger.error(
                'Failed to read Preferences instance. %s %s'
                % (errors.message, filename))

    def instance_collection(self, property_name, instance_id):
        '''
        Instance.

        Read a json file with the given instance_id as filename in a
        preferences_location defined by property_name. Works on a collection
        model and ensures that a existing instance with that id is overwritten.
        When a ValidationError occurs during load a migration is attempted and
        if that fails too, a existing instance is not changed and no new
        instance is created.
        '''
        pathname = get_preferences_location(property_name)
        filename = os.path.join(pathname, '%s.json' % (instance_id,))
        try:
            try:
                item_model = self.model.find_by_id(instance_id)
                # persistent.remove implements unlink,
                # this is not what we want here
                list.remove(self.model, item_model)
                restore_on_validation_error = True
            except NotFoundError:
                item_model = self.model.item_model_class(self.model)
                restore_on_validation_error = False

            try:
                with open(filename) as file_handle:
                    file_content = file_handle.read(config.MAX_CONFIG_SIZE)
                    if flags.DEBUG:
                        logger.debug(
                            'Preferences instance read: %s %s'
                            % (filename, file_content))
                    if file_content is None:
                        file_content = '{}'
                    try:
                        item_model.from_json(file_content)
                    except ValueError:
                        item_model.migrate('{}')
                        self.model.set_migrated(True)
                    except ValidationError:
                        item_model.migrate(file_content)
                        self.model.set_migrated(True)
                    self.model.append(item_model)
            except ValidationError:
                if restore_on_validation_error:
                    # keep old value
                    self.model.append(item_model)
                raise
        except IOError as errors:
            self.model.migrate('{}')
            self.model.set_migrated(True)
            logger.error(
                'Failed to read Preferences instance. %s %s'
                % (errors.message, filename))

    def instance_plugin(self, property_name, item_id, plugin_name):
        '''
        Read Instance Plugin.

        Read plugin values for a instance. The model has to be the instance
        that has extended attributes.
        Raises ValidationError when the JSON has a bad structure and the
        data could not be migrated.
        Raises KeyError when the given plugin is not registered and
        initialized.
        '''

        instance_name = '%s.%s.json' % (item_id, plugin_name)
        pathname = get_preferences_location(property_name)
        filename = os.path.join(pathname, instance_name)
        try:
            with open(filename) as file_handle:
                file_content = file_handle.read(config.MAX_CONFIG_SIZE)
                if flags.DEBUG:
                    logger.debug(
                        'Preferences instance plugin read: %s %s'
                        % (filename, file_content))
                if file_content is None:
                    file_content = '{}'
                try:
                    self.model.__dict__[plugin_name].from_json(file_content)
                except ValueError:
                    self.model.__dict__[plugin_name].migrate('{}')
                    self.model.set_migrated(True)
                except ValidationError:
                    self.model.__dict__[plugin_name].migrate(file_content)
                    self.model.set_migrated(True)
                self.model.__dict__[plugin_name].name.set(plugin_name)

        except IOError as errors:
            self.model.__dict__[plugin_name].migrate('{}')
            self.model.__dict__[plugin_name].name.set(plugin_name)
            self.model.set_migrated(True)
            logger.error(
                'Failed to read instance plugin preference. %s %s'
                % (str(errors), filename))

    def instances(self, model_attribute_name):
        '''
        Instances.

        Read a collection of instances from a directory. Ignores files that
        match the plugin pattern (id.plugin_name.json) and skips files that
        have no valid json.
        '''
        pathname = get_preferences_location(model_attribute_name)
        plugin_pattern = re.compile(r'.*\.[^\.]*\.json')
        try:
            pathitems = os.listdir(pathname)
            for item in pathitems:
                filename = os.path.join(pathname, item)
                if not os.path.isfile(filename):
                    continue
                if not item.endswith('.json'):
                    continue
                if plugin_pattern.search(item):
                    continue

                item_id = item[:-5]  # without '.json' -> see writer
                try:
                    self.instance_collection(model_attribute_name, item_id)
                except ValidationError as errors:
                    logger.warn(
                        'Validation errors for instance for %s exist: %s %s'
                        % (model_attribute_name, str(errors), filename))
                    continue
        except OSError:
            self.model.migrate('{}')
            self.model.set_migrated(True)
            logger.warn(
                'No instances for %s exist in %s'
                % (model_attribute_name, pathname))
            # no instances available (dir does not exist)
        except IOError as errors:
            self.model.migrate('{}')
            self.model.set_migrated(True)
            logger.error(
                'Failed to read Preferences instances. %s %s'
                % (errors.message, pathname))

    def plugin(self, property_path, plugin_name, allow_fail=False):
        '''
        Read Plugin.

        Read plugin values for general plugin settings.
        Raises ValidationError when the json has a bad structure.
        '''

        instance_name = '%s.json' % (plugin_name)
        pathname = get_preferences_location(property_path)
        filename = os.path.join(pathname, instance_name)
        try:
            with open(filename) as file_handle:
                file_content = file_handle.read(config.MAX_CONFIG_SIZE)
                if flags.DEBUG:
                    logger.debug(
                        'Preferences plugin read: %s %s'
                        % (filename, file_content))
                if file_content is None:
                    file_content = '{}'
                try:
                    self.model.from_json(file_content)
                except ValueError:
                    self.model.migrate('{}')
                    self.model.set_migrated(True)
                except ValidationError:
                    self.model.migrate(file_content)
                    self.model.set_migrated(True)
                self.model.name.set(plugin_name)
        except IOError as errors:
            self.model.migrate('{}')
            self.model.set_migrated(True)
            self.model.name.set(plugin_name)
            if errors.errno == 2 and allow_fail:
                pass
            else:
                logger.error(
                    'Failed to read preferences instance. %s %s'
                    % (errors.message, filename))

    def root_object(self, object_name, allow_fail=False):
        '''
        Root Object.

        Read a json file with the given object_name in the root
        preferences_location and load the values into the current model.
        Raises ValidationError when the json has a bad structure.
        '''
        pathname = get_preferences_location('')
        filename = os.path.join(pathname, '%s.json' % (object_name,))
        try:
            with open(filename) as file_handle:
                file_content = file_handle.read(config.MAX_CONFIG_SIZE)
                if flags.DEBUG:
                    logger.debug(
                        'Preferences root object read: %s %s'
                        % (filename, file_content))
                if file_content is None:
                    file_content = '{}'
                try:
                    self.model.from_json(file_content)
                except ValueError:
                    self.model.migrate('{}')
                    self.model.set_migrated(True)
                except ValidationError:
                    self.model.migrate(file_content)
                    self.model.set_migrated(True)
        except IOError as errors:
            self.model.migrate('{}')
            self.model.set_migrated(True)
            if errors.errno == 2 and allow_fail:
                pass
            else:
                logger.error(
                    'Failed to read Preferences root object. %s %s'
                    % (str(errors), filename))

    def set_model(self, model):
        '''
        Set Model.

        Set the model that the reader operates on.
        '''
        self.model = model
