# -*- coding: utf-8 -*-
# writer.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Write marshaled objects to files.
'''
import os

from netsplice.config import flags
from netsplice.util import get_logger
from netsplice.util.errors import (
    AccessError, NotFoundError
)
from netsplice.util.files import set_urw_only
from netsplice.util.files import safe_write
from netsplice.util.path import (
    mkdir_p,
    get_preferences_location
)

logger = get_logger()


class writer(object):
    def __init__(self, model=None):
        self.model = model

    def check_access(self):
        '''
        Check access.

        Check that we can access the locations for the preference files.
        '''
        location = get_preferences_location('')
        if not os.path.exists(location):
            # first start / cleaned preferences
            return
        if not os.access(location, os.W_OK):
            raise AccessError(
                'Preferences location %s cannot be written.'
                % (location,))

    def instance(self, property_name, instance_id):
        '''
        Instance.

        Write a json file with the given instance_id as filename in a
        preferences_location defined by property_name. Works on a collection
        model and exports only the instance that is found for the instance_id.
        '''
        pathname = get_preferences_location(property_name)
        filename = os.path.join(pathname, '%s.json' % (instance_id,))
        try:
            mkdir_p(pathname)
            instance = self.model.find_by_id(instance_id)
            instance_json = instance.to_json()
            if flags.DEBUG:
                logger.debug(
                    'Preferences instance write: %s %s'
                    % (filename, instance_json))
            safe_write(filename, instance_json)
            set_urw_only(filename)
        except NotFoundError:
            logger.error(
                'Failed to write nonexistent instance %s %s'
                % (property_name, instance_id))
        except IOError as errors:
            logger.error(
                'Failed to write preference instance: %s[%s] %s %s'
                % (property_name, instance_id, str(errors), filename))
        except OSError as errors:
            logger.error(
                'Failed to write preference instance %s to %s'
                % (property_name, filename,))

    def instance_plugin(self, property_name, instance_id, plugin_name):
        '''
        Write Instance Plugin.

        Write plugin values for a instance. The model has to be the instance
        that has extended attributes.
        Raises KeyError when the given plugin is not registered and
        initialized.
        '''

        instance_name = '%s.%s.json' % (instance_id, plugin_name)
        pathname = get_preferences_location(property_name)
        filename = os.path.join(pathname, instance_name)
        try:
            mkdir_p(pathname)
            instance_json = self.model.__dict__[plugin_name].to_json()
            if flags.DEBUG:
                logger.debug(
                    'Preferences Plugin write: %s %s'
                    % (filename, instance_json))
            safe_write(filename, instance_json)
            set_urw_only(filename)
        except IOError as errors:
            logger.error(
                'Failed to write preference instance plugin %s: %s to %s %s'
                % (plugin_name, property_name, filename, str(errors)))
        except OSError as errors:
            logger.error(
                'Failed to write preference instance plugin %s: %s to %s %s'
                % (plugin_name, property_name, filename, str(errors)))

    def instances(self, property_name):
        '''
        Instances.

        Write a collection of instances to a directory.
        '''
        pathname = get_preferences_location(property_name)
        try:
            mkdir_p(pathname)
            for list_item in self.model:
                self.instance(property_name, list_item.id.get())
        except IOError as errors:
            logger.error(
                'Failed to write preference instances: %s %s'
                % (property_name, str(errors)))
        except OSError as errors:
            logger.error(
                'Failed to write preference instances %s to %s'
                % (property_name, pathname,))

    def plugin(self, property_path, plugin_name):
        '''
        Write Plugin.

        Write plugin values for general plugin settings.
        '''
        instance_name = '%s.json' % (plugin_name)
        pathname = get_preferences_location(property_path)
        filename = os.path.join(pathname, instance_name)
        try:
            mkdir_p(pathname)
            instance_json = self.model.to_json()
            safe_write(filename, instance_json)
            set_urw_only(filename)
        except IOError as errors:
            logger.error(
                'Failed to write preference instance: %s to %s %s'
                % (property_path, filename, str(errors)))
        except OSError as errors:
            logger.error(
                'Failed to write preference instance %s to %s %s'
                % (property_path, filename, str(errors)))

    def root_object(self, object_name):
        '''
        Root Object.

        Write a json file with the given object_name in the root
        preferences_location and store the values of the current model.
        '''
        try:
            mkdir_p(get_preferences_location())
        except OSError as errors:
            logger.error(
                'Failed to create the preference location %s'
                % (get_preferences_location(),))
            raise
        pathname = get_preferences_location('')
        filename = os.path.join(pathname, '%s.json' % (object_name,))
        try:
            collection_json = self.model.to_json()
            safe_write(filename, collection_json)
            set_urw_only(filename)
        except IOError as errors:
            logger.error(
                'Failed to write preference array: %s %s'
                % (object_name, str(errors)))

    def set_model(self, model):
        '''
        Set Model.

        Set the model that the writer operates on.
        '''
        self.model = model

    def unlink(self, property_name):
        '''
        Unlink.

        Remove the file that represents the model.
        '''
        pathname = get_preferences_location()
        filename = os.path.join(
            pathname,
            property_name,
            '%s.json' % (self.model.id.get(),))
        try:
            os.remove(filename)
        except OSError as errors:
            pass

    def unlink_item(self, property_name, instance_id):
        '''
        Unlink item.

        Remove the file that represents the model for that item.
        '''
        pathname = get_preferences_location()
        filename = os.path.join(
            pathname,
            property_name,
            '%s.json' % (instance_id,))
        try:
            os.remove(filename)
        except OSError as errors:
            pass

    def unlink_plugin(self, property_name, plugin_name):
        '''
        Unlink Plugin.

        Remove the file that represents the model for that plugin.
        '''
        pathname = get_preferences_location()
        filename = os.path.join(
            pathname,
            property_name,
            '%s.%s.json' % (self.model.id.get(), plugin_name))
        try:
            os.remove(filename)
        except OSError as errors:
            pass
