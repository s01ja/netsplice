# -*- coding: utf-8 -*-
# validator.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Base class for the validator object.
'''

from netsplice.util.model.errors import ValidationError


class validator(object):
    '''
    Abstract base for validators. Requires the author of a validator to
    implement the is_valid method that returns True or False.
    '''

    def __init__(self, exp_or=[]):
        self.exp_or = exp_or

    def validate(self, value):
        '''
        Validate the given value. Returns True when the value is considered
        valid or raises a ValidationError when the value does not validate.
        '''
        if self.is_valid(value):
            return True
        raise ValidationError(
            'value [%s] is not valid for %s' % (value, type(self)))

    def is_valid_or(self, value):
        '''
        Is valid or

        Check the exp_or list validators and return True when one of their
        state is true.
        '''
        for or_validator in self.exp_or:
            state = or_validator.is_valid(value)
            if state:
                return True
        return False

    def is_valid(self, value):
        '''
        Abstract to be implemented by validators.
        Validate the given value. Return True when the value is considered
        valid. Return False when the value is considered invalid.
        '''
        raise NotImplementedError()
