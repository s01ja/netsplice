# -*- coding: utf-8 -*-
# marshalable_list_persistent.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Persistent marshalable list.

Extend marshalable lists to generate readable json and with persistent
functionality.
'''

import json

from netsplice.config import flags
from netsplice.util.model.errors import ValidationError
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.util.model.persistent import persistent


class marshalable_list_persistent(persistent, marshalable_list):
    '''
    Base class for all collection models that are stored to persistent memory.

    The marshal takes care to validate the objects upon receiving and to clean
    the values before sending. It is a kind of Structure Definition for JSON.
    To make use of the marshalable let your model inherit from marshalable and
    define all members using the util.model.field class.
    '''

    def __init__(self, item_model_class, owner, prefix=None):
        marshalable_list.__init__(self, item_model_class)
        persistent.__init__(self, owner, prefix)
        self.separate_instances = False

    def to_json(self):
        '''
        Serialize the current model to JSON using json.dumps. All items will
        get marshaled. Fields in model will be ignored.
        '''
        return json.dumps(
            self.get_json_list(),
            indent=4, separators=(',', ': '))

    def from_json(self, jsons):
        '''
        From JSON

        Use the instances of the json array to fill the marshalable_items.
        This implementation is required to pass the owner that is not required
        for normal marshalables.
        '''
        json_list = json.loads(jsons)
        if not isinstance(json_list, (list)):
            raise ValidationError('JSON is not of type array')

        # Put all value is a clean_list first to prevent half-filling the
        # instances when some value is invalid.
        clean_model_list = list()
        for json_item in json_list:
            item_model = self.item_model_class(self.get_owner())
            item_model.from_dict(json_item)
            clean_model_list.append(item_model)

        # All values are cleaned, all required values are available now set
        # the items to the list.
        del self[:]
        for item_model in clean_model_list:
            self.append(item_model)
