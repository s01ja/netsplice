# -*- coding: utf-8 -*-
# cleaner.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''


class cleaner(object):
    '''
    Abstract Base for cleaning model data. Always implements a 'clean' method
    that takes a value and returns a cleaned value.
    '''
    def __init__(self, clean_value):
        self.clean_value = clean_value

    def clean(self, value):
        '''
        Clean the given value for internal processing. As the nature of python
        is to have dynamic typed values. always check for the correct value.
        When the value cannot be cleaned, return the default value.
        '''
        raise NotImplementedError()
