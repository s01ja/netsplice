# -*- coding: utf-8 -*-
# configuration_modules_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import json
import inspect
import os.path

from netsplice.util.ipc.middleware import middleware


class configuration_modules_controller(middleware):
    '''
    Emit a JSON containing all available modules.A GET on a method can be used
    to implement an information exchange for the required JSON structure.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        Return a JSON object that describes the endpoints with their comments.
        '''
        description = []
        for pattern, handler in self.application.handlers:
            for spec in handler:
                module_path = inspect.getfile(spec.handler_class)
                module = os.path.basename(os.path.dirname(module_path))
                if module not in description:
                    description.append(module)
        self.write(json.dumps(description, indent=4, separators=(',', ': ')))
        self.set_status(200)
        self.finish()

    def describe(self, class_instance):
        '''
        Create description for the handler class instance using inspect.
        '''
        description = inspect.getdoc(class_instance)
        if description is None:
            description = "No documentation available."
        return description
