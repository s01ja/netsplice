# -*- coding: utf-8 -*-
# errors.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Errors that may be raised from module code.
'''


class InvalidSharedSecretError(Exception):
    '''
    Error to be raised when the signature module cannot load the secret from
    file.
    '''
    def __init__(self):
        BaseException.__init__(self, 'Invalid shared secret.')


class ModuleNotFoundError(Exception):
    '''
    Error to be raised when a module is requested from the application and the
    module does not exist.
    '''
    def __init__(self, module_name):
        BaseException.__init__(self, module_name)


class ModuleAlreadyRegisterdError(Exception):
    '''
    Error to be raised when a module is requested from the application and the
    module does not exist.
    '''
    def __init__(self, module_name):
        BaseException.__init__(self, module_name)


class NoSharedSecretError(Exception):
    '''
    Error to be raised the shared secret did not appear within
    MAX_WAIT_FOR_SECRET time.
    '''
    def __init__(self):
        BaseException.__init__(self, 'No shared secret file.')


class ServerConnectionError(Exception):
    '''
    Error to be raised when the ipc server cannot be reached or yields
    a http-error
    '''
    def __init__(self, message):
        BaseException.__init__(self, message)


class ServerStartFailedError(Exception):
    '''
    Error to be raised when a application server fails to start
    '''
    def __init__(self, message):
        BaseException.__init__(self, message)
