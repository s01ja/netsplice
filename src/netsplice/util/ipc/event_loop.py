# -*- coding: utf-8 -*-
# event_loop.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Event Loop Base for Apps
'''

from tornado import gen, ioloop
from netsplice.config import ipc as config
from netsplice.model.event import event as event_model
from netsplice.model.log_item import log_item as log_item_model
from netsplice.util import get_logger_handler, get_logger

logger = get_logger()

# events that are defined in backend.events.names but required here

MODEL_CHANGED = 'model_changed'
NOTIFY = 'notify'
SUBPROCESS_STARTED = 'backend.event.subprocess_started'


class event_loop(object):
    def __init__(self, name):
        self._name = name
        self.application = None
        self.dispatcher = None
        self.modules = []
        self.plugin_modules = []
        self.events_processed = -1

    def model_changed(self, model_name):
        '''
        Model Changed.

        Create a model changed event.
        '''
        try:
            assert(callable(self.application.owner.event))
            model_changed_event = event_model()
            model_changed_event.type.set(MODEL_CHANGED)
            model_changed_event.name.set(model_name)
            self.application.owner.event(model_changed_event)
        except AttributeError:
            pass
        except AssertionError:
            pass

    def notify_started(self):
        '''
        Notify started.

        Notify the owner (if any) that the eventloop has started.
        '''
        try:
            assert(callable(self.application.owner.event))
            notify_started_event = event_model()
            notify_started_event.type.set(NOTIFY)
            notify_started_event.name.set(SUBPROCESS_STARTED)
            notify_started_event.data.set(self._name)
            self.application.owner.event(notify_started_event)
        except AttributeError:
            pass
        except AssertionError:
            pass
        logger.debug('event loop for %s started' % (self._name,))

    @gen.coroutine
    def process_event(self, event_model_instance):
        '''
        Process the event in the event_model_instance.

        Evaluate the event-type, name and origin and call handler functions.
        '''
        raise NotImplementedError()

    def publish_logs(self):
        '''
        Publish Logs.

        Publish logs to the owning application.
        '''
        try:
            assert(callable(self.application.owner.log))
            logs = get_logger_handler().take_logs()
            for log_dict_item in logs:
                log_item = log_item_model()
                if isinstance(log_dict_item, (dict,)):
                    log_item.set_from_dict(log_dict_item)
                else:
                    log_item.from_json(log_dict_item.to_json())
                self.application.owner.log(log_item)
        except AttributeError:
            logger.warn('no owner of application')
        except AssertionError:
            logger.warn('owner.event not callable')

    def register_module(self, module):
        '''
        Register the given module for events on module_changed.

        Interface to ipc.server
        '''
        self.modules.append(module)

    def register_plugin_module(self, plugin_module):
        '''
        Register the given plugin module for execution during event handling.

        Interface to ipc.server
        '''
        self.plugin_modules.append(plugin_module)

    @gen.coroutine
    def schedule_wait_for_log_model_change(self):
        '''
        Schedule log_model_change wait.

        Avoid endless stacks and ensure that log model changes are processed.
        '''
        try:
            assert(callable(self.application.owner.log))
            ioloop.IOLoop.current().call_later(
                config.LOOP_TIMER_LOG_MODEL_CHANGE,
                self.wait_for_log_model_change)
        except AttributeError:
            pass
        except AssertionError:
            pass

    @gen.coroutine
    def schedule_wait_for_module_model_change(self, module):
        '''
        Schedule module model changes.

        Avoid endless stacks and ensure that model changes are processed.
        '''
        try:
            assert(callable(module.model.model_changed))
            ioloop.IOLoop.current().call_later(
                config.LOOP_TIMER_MODULE_MODEL_CHANGE,
                lambda: self.wait_for_module_model_change(module))
        except AttributeError:
            pass
        except AssertionError:
            pass

    def set_dispatcher(self, dispatcher):
        '''
        Set Dispatcher.

        Set the dispatcher used for events
        '''
        self.dispatcher = dispatcher

    @gen.coroutine
    def start(self):
        '''
        Start a model_changed listener for all modules. Starts all waiters at
        once, allowing multiple modules to trigger the change event.
        '''
        yield self.schedule_wait_for_log_model_change()
        for module in self.modules:
            yield self.schedule_wait_for_module_model_change(module)
        self.notify_started()

    @gen.coroutine
    def wait_for_log_model_change(self):
        '''
        Get Logs from logbook and store them in the log module.

        Takes any logger.* items and convert them to log items that can be
        sent to the owning application.
        '''
        state = yield self.application.model.log.model_changed()
        if state:
            self.publish_logs()
        self.schedule_wait_for_log_model_change()

    @gen.coroutine
    def wait_for_module_model_change(self, module):
        '''
        Wait for module model changes.

        Checks the model of the module if it changed and process log and event
        instances in the model.
        '''
        state = yield module.model.model_changed()
        if isinstance(module.model, (event_model,)):
            events = []
            events.extend(module.model)
            del module.model[:]
            for event_model_instance in events:
                yield self.process_event(event_model_instance)
        if state:
            self.model_changed('%s.model' % (module.name,))
        self.schedule_wait_for_module_model_change(module)
