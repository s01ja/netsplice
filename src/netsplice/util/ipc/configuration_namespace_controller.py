# -*- coding: utf-8 -*-
# configuration_namespace_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

import json
import inspect

from netsplice.util.ipc.middleware import middleware
from tornado.web import RequestHandler


class configuration_namespace_controller(middleware):
    '''
    Emit a JSON containing all available NS endpoints and supported methods
    (GET, POST, DELETE, etc). A GET on a method can be used to implement an
    information exchange for the required JSON structure.
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        Return a JSON object that describes the endpoints with their comments.
        '''
        description = {}
        for pattern, handler in self.application.handlers:
            for spec in handler:
                description[spec.regex.pattern] = self.describe(
                    spec.handler_class)
        self.write(json.dumps(description, indent=4, separators=(',', ': ')))
        self.set_status(200)
        self.finish()

    def describe(self, class_instance):
        '''
        Create description for the handler class instance using inspect.
        '''
        properties = dir(class_instance)
        class_comment = inspect.getdoc(class_instance)
        if class_comment is None:
            class_comment = "No documentation available."
        described_properties = {"description": class_comment}
        for property_name in properties:
            if property_name.upper() in RequestHandler.SUPPORTED_METHODS:
                function_reference = getattr(class_instance, property_name)
                filename = inspect.getfile(function_reference)
                # XXX better way to find out it is 'our source?'
                # Otherwise we see all sources, including the tornado ones that
                # just pass.
                if 'src/netsplice/' not in filename:
                    continue
                args = inspect.getargspec(function_reference)
                comment = inspect.getdoc(function_reference)
                if comment is None:
                    comment = "No documentation available."
                described_properties[property_name.upper()] = {
                    "description": comment,
                    "args": args[0]}
        return described_properties
