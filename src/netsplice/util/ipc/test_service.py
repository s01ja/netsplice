# -*- coding: utf-8 -*-
# service.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

from .service import service


def get_service():
    host = 'localhost'
    port = '1'
    server_role = 'test'
    client_role = 'none'
    return service(host, port, None, None, server_role, client_role)


def test_get_query_with_parameters_requests_correct_query_string():
    '''
    Check that a HTTPRequest is constructed with the correct query values
    '''
    result = get_service()._get_query({
        'key1': 'value1',
        'key2': 'value2'
    })
    print(result)
    assert('key1=value1' in result)
    assert('key2=value2' in result)
    assert('&' in result)


def test_get_query_with_none_parameters_requests_correct_query_string():
    '''
    Check that a HTTPRequest is constructed with the correct query values
    '''
    result = get_service()._get_query(None)
    print(result)
    assert(result == '')
