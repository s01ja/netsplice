# -*- coding: utf-8 -*-
# server.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import re

from netsplice.config.routes import endpoints
from netsplice.util import get_logger

logger = get_logger()


def get_route(namespace):
    '''
    Build URLSpec List for the given namespace
    Defines namespace.module.controller class names to be imported by tornado
    '''
    exported_endpoints = []
    try:
        for endpoint in endpoints[namespace]:
            class_name = get_route_class(namespace, endpoint[0], endpoint[1])
            exported_endpoints.append((
                endpoint[0],
                class_name))
    except KeyError:
        logger.warn('no endpoints for %s' % (namespace,))
    return exported_endpoints


def get_module_route(namespace, endpoint_list):
    '''
    Build URLSpec List for the given namespace
    Defines namespace.module.controller class names to be imported by tornado
    '''
    exported_endpoints = []
    for endpoint in endpoint_list:
        class_name = get_route_class(namespace, endpoint[0], endpoint[1])
        exported_endpoints.append((
            endpoint[0],
            class_name))
    return exported_endpoints


def get_route_class(namespace, endpoint, simple_name):
    '''
    Get Route Class.

    Return the class_name for the routes by combining the values to
    valid python imports.

    By default the class is named
    namespace.simple_name_controller.simple_name_controller. For module
    endpoints the module injected.

    When the route cannot be resolved, double-check the endpoint_END_/
    '''
    options = {
        'ns': namespace,
        'cls': simple_name,
    }
    class_name = (
        '%(ns)s.'
        '%(cls)s_controller.%(cls)s_controller'
    ) % options
    if '/module/' in endpoint:
        # Endpoints that are in a module will need more
        # magic on the class_name
        match_default = re.match(
            r'/module/([^/]*)', endpoint, re.M | re.I)
        match_plugin = re.match(
            r'/module/plugin/([^/]*)/', endpoint, re.M | re.I)
        if match_plugin:
            match = match_plugin
        else:
            match = match_default
        options['module'] = '%s.' % (match.group(1),)
        class_name = (
            '%(ns)s.%(module)s'
            '%(cls)s_controller.%(cls)s_controller'
        ) % options
    return class_name
