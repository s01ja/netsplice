# -*- coding: utf-8 -*-
# configuration_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
'''

import json

from netsplice.config import ipc as config
from netsplice.util.ipc.middleware import middleware


class configuration_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def get(self):
        '''
        Get Configuration for service.
        '''
        description = {
            "namespace": {
                "uri": config.REST_ENDPOINT_NAMESPACE,
                "description": ""
            },
            "modules": {
                "uri": config.REST_ENDPOINT_MODULES,
                "description": ""
            },
            "status": {
                "uri": config.REST_ENDPOINT_STATUS,
                "description": ""
            },
            "version": {
                "uri": config.REST_ENDPOINT_VERSION,
                "description": ""
            },
        }
        self.write(json.dumps(description, indent=4, separators=(',', ': ')))

        # XXX inline magic
        self.set_status(200)
        self.finish()
