# -*- coding: utf-8 -*-
# server.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Management server
'''

import socket
import threading

from .channel import channel

VERBOSE = 0


class server(channel):
    '''
    Public server interface that exposes the channels to the application.
    '''
    def __init__(self, host, port, handler):
        self.create_socket()
        self.set_address(host, port)
        self._socket.bind(self._address)
        self._socket.listen(0)
        self._accept()
        self._handler = handler
        if VERBOSE > 0:
            print('management server initialized: %s %d' % (host, port))

    def _accept(self):
        accept_thread = threading.Thread(target=self._accept_thread)
        accept_thread.daemon = True
        accept_thread.start()

    def _accept_thread(self):
        pair = self._socket.accept()
        if pair is not None:
            management_connection, address = pair

            try:
                self._handler(management_connection)
            except Exception as errors:
                if VERBOSE > 0:
                    print('Management server handler error: %s' % (errors,))

            if VERBOSE > 0:
                print('close listening socket')
            try:
                self._socket.shutdown(socket.SHUT_RDWR)
            except IOError:
                pass
            try:
                self._socket.close()
            except IOError:
                pass
