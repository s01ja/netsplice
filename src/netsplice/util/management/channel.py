# -*- coding: utf-8 -*-
# channel.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Management channels abstract implementation.
'''

import socket
import threading

from .envelope import envelope as management_envelope
from .envelope import envelope_stream as management_envelope_stream

# 0: production
# 5: a lot
# 10: in-development of this class: output data
VERBOSE = 0

# 8-128: pretty fast processing of incoming messages
# >128: dependent on 'common' message size
# >1024: pile up of messages before they get processed
MAX_RECV = 64


class channel(object):

    def __init(self):
        self._socket = None
        self._address = None

    def connect(self):
        # raise if no socket
        # raise if no address
        self._socket.connect(self._address)

    def create_socket(self):
        channel_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        channel_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.set_socket(channel_socket)

    def get_address(self):
        return self._address

    def get_socket(self):
        return self._socket

    def set_address(self, host, port):
        host_ip = socket.gethostbyname(host)
        self._address = (host_ip, port)

    def set_socket(self, channel_socket):
        self._socket = channel_socket


# class server_channel(channel):

#     def __init(self, channel_socket):
#         channel.__init__(self)
#         self.set_socket(channel_socket)

# class client_channel(channel):
#     def __init__(self, host, port):
#         channel.__init__(self)
#         client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#         client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#         self.set_socket(client_socket)
#         host_ip = socket.gethostbyname(host)
#         self._address = (host_ip, port)

class signal_channel(channel):

    def __init__(self):
        channel.__init__(self)

    def emit(self, envelope):
        self._socket.sendall(str(envelope) + management_envelope.separator)
        if VERBOSE > 5:
            print('signal emit:', self._address, envelope)


class receive_channel(channel):

    def __init__(self):
        channel.__init__(self)
        self._signals = []
        self.receiving = False
        self._signals_lock = threading.RLock()

    def receive(self):
        self._receiving = True
        receive_thread = threading.Thread(target=self._receive_thread)
        receive_thread.daemon = True
        receive_thread.start()

    def _receive_thread(self):
        envelope_stream = management_envelope_stream()
        while self._receiving:
            data = self._socket.recv(MAX_RECV)
            if data == '':
                self._signals.append(management_envelope({
                    "management_socket": "closed"}))
                self._socket.close()
                self._receiving = False
                break
            if VERBOSE > 10:
                print('Management Client Received: [%s]' % (data,))
            envelope_stream.append(data)
            if envelope_stream.has_envelopes():
                self._signals_lock.acquire()
                while envelope_stream.has_envelopes():
                    self._signals.append(envelope_stream.take_envelope())
                self._signals_lock.release()

        # NOTE: There may be unprocessed envelopes at this point but we will
        # discard them as the application should shut down.

    def has_signals(self):
        return len(self._signals) > 0

    def take_signal(self):
        self._signals_lock.acquire()
        signal = self._signals[0]
        del self._signals[0]
        self._signals_lock.release()
        return signal
