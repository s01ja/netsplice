# -*- coding: utf-8 -*-
# network_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Network event dispatcher.
'''

import os
import subprocess
import sys

from tornado import gen

import netsplice.backend.dispatcher_endpoints as endpoints
from netsplice.config import flags
from netsplice.model.process import (
    process as process_model
)
from netsplice.backend.net.model.request.download import (
    download as download_request_model
)
from netsplice.backend.net.model.response.download_id import (
    download_id as download_id_response_model
)
from netsplice.backend.net.model.response.download_status import (
    download_status as download_status_response_model
)
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.errors import ProcessError
from netsplice.util.hkpk.errors import (
    InvalidError, NetworkError, SignatureError, TargetError
)
from netsplice.util.ipc.service import service
from netsplice.util.errors import NotFoundError
from netsplice.util import get_logger

logger = get_logger()


class network_dispatcher(service, process_dispatcher):

    def __init__(self, host, port, host_owner, port_owner):
        service.__init__(
            self, host, port, host_owner, port_owner,
            'network', 'backend')
        self.wait_for_service = True
        self.process_model = process_model()
        process_dispatcher.__init__(
            self, 'NetspliceNetApp', {
                '--host': self.host,
                '--port': self.port,
                '--host-owner': self.host_owner,
                '--port-owner': self.port_owner
            },
            model=self.process_model,
            unique=True)

    def start(self):
        '''
        Start.

        Start the service process and notify the GUI in case it fails.
        '''
        try:
            self.start_subprocess()
        except ProcessError as errors:
            message = 'Cannot start subprocess: %s' % (str(errors),)
            logger.critical(message)
            gui_model = self.application.get_module('gui').model
            gui_model.events.notify_error('Cannot start subprocess')

    @gen.coroutine
    def cancel_download(self, id):
        '''
        Cancel Download.

        Cancel the download with the given id in the network backend.

        Decorators:
            gen.coroutine

        Arguments:
            id (string): Id of the download.
        '''
        options = dict(
            download_id=id
        )
        try:
            yield self.delete(endpoints.DOWNLOAD_ID % options)
        except NotFoundError:
            logger.error(
                'No download with the given id exists.')
            raise

    @gen.coroutine
    def download_status(self, id):
        '''
        Download Status.

        Request the status of the download with the given id from the network
        backend.

        Decorators:
            gen.coroutine

        Arguments:
            id (string): Id of the download.
        '''
        options = dict(
            download_id=id
        )
        try:
            response_model = download_status_response_model()
            response = yield self.get(endpoints.DOWNLOAD_ID % options, None)
            response_model.from_json(response.body)
            raise gen.Return(response_model)
        except NotFoundError:
            logger.error(
                'No download with the given id exists.')
            raise

    @gen.coroutine
    def download(
            self, url, fingerprints, destination,
            signature=None, sign_key=None,
            sha1sum=None, sha256sum=None):
        '''
        Request download.

        Request the network backend to download the given URL (https only)
        with a certificate fingerprint and a optional signature.

        Decorators:
            gen.coroutine

        Arguments:
            url (string): URL to download.
            fingerprints (list{string}): sha256 certificate public key \
                fingerprint(s).
            destination (string): path to store the result.

        Keyword Arguments:
            signature (string): base64 encoded openssl signature of the \
                result. (default: {None})
            sign_key (string): base64 encoded openssl public key to verify \
                the signature. (default: {None})
            sha1sum (string): Expected sha1 checksum. (default: {None})
            sha256sum (string): Expected sha256 checksum. (default: {None})

        Yields:
            string -- download-id
        '''
        try:
            request_model = download_request_model()
            response_model = download_id_response_model()
            request_model.url.set(url)
            request_model.fingerprints.set_list(fingerprints)
            request_model.signature.set(signature)
            request_model.sign_key.set(sign_key)
            request_model.sha1sum.set(sha1sum)
            request_model.sha256sum.set(sha256sum)
            request_model.destination.set(destination)
            response = yield self.post(
                endpoints.DOWNLOAD, request_model.to_json())
            response_model.from_json(response.body)
            logger.debug('started download: %s' % (response_model.to_json(),))
            raise gen.Return(response_model)
        except InvalidError:
            logger.error('Failed to verify TLS fingerprint of remote service.')
            raise
        except NetworkError:
            logger.error('The remote failed to respond in time.')
            raise
        except SignatureError:
            logger.error(
                'The resource was downloaded but the signature did not match.')
            raise
        except TargetError:
            logger.error(
                'The destination could not be written, download was canceled')
            raise

    @gen.coroutine
    def request_shutdown(self):
        '''
        Request Shutdown.

        Request the network backend to stop all operations and shut down.

        Decorators:
            gen.coroutine

        Yields:
            [type] -- [description]
        '''
        try:
            yield self.post(endpoints.SHUTDOWN, '{}')
        except:
            pass
