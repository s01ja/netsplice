# -*- coding: utf-8 -*-
# credential_action_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Credential Controller.

Controller implementation for sending credentials from the GUI.
'''
from tornado import gen

from netsplice.backend.gui.model.request.credential import (
    credential as credential_model)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import NotFoundError


class credential_action_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    @gen.coroutine
    def delete(self):
        '''
        Cancel a requested credential input.

        '''
        gui_model = self.application.get_module('gui').model
        gui_model.credentials.cancel()
        self.set_status(200)
        self.finish()

    @gen.coroutine
    def post(self):
        '''
        Create Credential.

        Creates a credential for the backend.
        '''
        request_model = credential_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            gui_model = self.application.get_module('gui').model
            new_credential = gui_model.credentials.item_model_class()
            new_credential.from_json(request_model.to_json())
            gui_model.credentials.add(new_credential)
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2109, errors)
            self.set_status(400)
        except ValueError as errors:
            self.set_error_code(2110, errors)
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2111, errors)
            self.set_status(404)
        self.finish()
