# -*- coding: utf-8 -*-
# events_controller.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Events Controller
Controller implementation for long polling the gui-model change event. The
event will be notified when the gui-model is commited or the event heartbeat
timeout has passed.
'''

from tornado import gen

from netsplice.util.ipc.middleware import middleware


class events_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    @gen.coroutine
    def get(self):
        '''
        Get Events.

        Long-polling endpoint that returns a 200 when the gui-model has
        changed.
        '''

        yield self.application.get_module('gui').model.model_changed()

        self.set_status(200)
        self.finish()
