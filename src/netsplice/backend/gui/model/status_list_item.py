# -*- coding: utf-8 -*-
# status_list_item_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Model for listing accounts.
'''
from netsplice.model.validator.enum import enum as enum_validator
from netsplice.model.validator.message import message as message_validator
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class status_list_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.state = field(
            required=False,
            validators=[enum_validator(['failure', 'warning', 'success'])])

        self.message = field(
            required=False,
            validators=[message_validator()])
