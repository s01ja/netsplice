# -*- coding: utf-8 -*-
# connection_list_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing connections.

Marshalable List of connection_list_items.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.config.connection import INITIALIZED
from netsplice.backend.gui.model.connection_list_item import (
    connection_list_item)
from netsplice.util.errors import (
    NotFoundError, ConnectedError)


class connection_list(marshalable_list):
    '''
    Connection List.

    Marshalable model that contains multiple items of
    connection_list_item_model.
    '''

    def __init__(self):
        '''
        Initialize Module.

        Initializes the marshalable_list.
        '''
        marshalable_list.__init__(self, connection_list_item)

    def add_connection(self, connection_model_instance):
        '''
        Add Connection.

        Add the given (module) connection_model_instance to the connection_list
        by creating a new instance, populate it with values and append it to
        the collection.
        '''
        connection = self.create_connection(
            connection_model_instance.id.get())
        connection.state.set(connection_model_instance.state.get())

        connection.update_connection_status(
            connection_model_instance)
        self.append(connection)
        return connection

    def create_connection(self, connection_id):
        '''
        Create Connection.

        Create a new connection to be added to the connection list.
        '''
        connection = self.item_model_class()
        connection.id.set(connection_id)
        connection.state.set(INITIALIZED)
        return connection

    def delete_connection(self, connection_id):
        '''
        Delete Connection.

        Delete the connection with the given connection_id from the
        connection_list.
        '''
        found = False
        for index, connection in enumerate(self):
            if connection.id.get() == connection_id:
                found = True
                if connection.is_active():
                    raise ConnectedError()
                del self[index]
                break
        if not found:
            raise NotFoundError(connection_id)

    def find_by_id(self, connection_id):
        '''
        Find By Id.

        Find the connection with the given connection_id or raise a
        NotFoundError.
        '''
        for index, connection in enumerate(self):
            if connection.id.get() == connection_id:
                return connection
        raise NotFoundError(connection_id)

    def find_by_account_id(self, account_id):
        '''
        Find By Id.

        Find the connection with the given account_id or raise a
        NotFoundError.
        '''
        connections = []
        for index, connection in enumerate(self):
            if connection.account_id.get() == account_id:
                connections.append(connection)
        if len(connections) is 0:
            raise NotFoundError(account_id)
        return connections

    def remove_by_account_id(self, account_id):
        '''
        Remove connection(s) for account_id.

        Remove all connections for the given account_id.
        '''
        connections = self.find_by_account_id(account_id)
        for connection in connections:
            self.delete_connection(connection.id.get())

    def remove_by_connection_id(self, connection_id):
        '''
        Remove connection(s) for connection_id.

        Remove all connections for the given account_id.
        '''
        removed = False
        while len(self) > 0:
            try:
                connection = self.find_by_id(connection_id)
                self.remove(connection)
                removed = True
            except NotFoundError:
                break
        if not removed:
            raise NotFoundError(
                'No connection with the connection_id %s was in the list'
                % (connection_id,))

    def reset_by_account_id(self, account_id):
        '''
        Reset connection for account_id.

        Resets all connections for the given account_id to a initial state.
        '''
        connections = self.find_by_account_id(account_id)
        for connection in connections:
            connection_id = connection.id.get()
            if connection_id is None:
                continue
            connection.reset()
