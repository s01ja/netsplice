# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about
'''


import time

from netsplice.backend.gui.model.connection_list import (
    connection_list as connection_list_model
)
from netsplice.backend.event import origins
from netsplice.config import connection as config_connection
from netsplice.backend.gui.model.event_list import (
    event_list as event_list_model
)
from netsplice.model.credential_list import (
    credential_list as credential_list_model
)
from netsplice.model.event import event as event_model
from netsplice.util.errors import NotFoundError
from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable


class model(marshalable):
    def __init__(self):
        marshalable.__init__(self)
        self.connections = connection_list_model()
        self.failed_connections = connection_list_model()
        self.credentials = credential_list_model()
        self.log_index = field(default=0)
        self.events = event_list_model()

    def get_next_event_index(self):
        '''
        Get Next Event Index.

        Increase the current event_index and return the increased value.
        '''
        return self.events.get_next_index()

    def get_next_log_index(self):
        '''
        Get Next Log Index.

        Increase the current log_index and return the increased value.
        '''
        self.log_index.set(self.log_index.get() + 1)
        return self.log_index.get()

    def new_event(self):
        '''
        New Event.

        Create a new event with the correct origin, a increased id and
        the current time.
        '''
        event = event_model()
        event.index.set(self.get_next_event_index())
        event.date.set(time.time())
        event.origin.set(origins.GUI)
        return event

    def add_event(self, event):
        '''
        Add Event.

        Add the given event to the event list and commit the gui model so the
        GuiApp can pick up the event.
        '''
        event.index.set(self.get_next_event_index())
        self.events.append(event)
        self.commit()

    def update_connections(self, broker_connections):
        '''
        Update Connections

        Update gui-connections from broker connections.

        Arguments:
            broker_connections (dict): dict of connection,chain tuples
        '''
        rm_connection = list()
        rm_failed_connection = list()
        changed = False
        try:
            connection_values = broker_connections.values()
        except AttributeError:
            connection_values = broker_connections.itervalues()
        for (broker_connection, chain) in connection_values:
            gui_connection = None
            try:
                gui_connection = self.connections.find_by_id(
                    broker_connection.id.get())
            except NotFoundError:
                gui_connection = self.connections.add_connection(
                    broker_connection.model)
                changed = True
            gui_connection.update_data(broker_connection.model)
            gui_connection.account_id.set(
                broker_connection.account.id.get())
            state = broker_connection.state
            failed = state == config_connection.DISCONNECTED_FAILURE
            failed |= state == config_connection.ABORTED
            if failed:
                rm_connection.append(gui_connection)
                try:
                    self.failed_connections.find_by_id(gui_connection.id.get())
                except NotFoundError:
                    self.failed_connections.append(gui_connection)
                    changed = True
            else:
                try:
                    self.failed_connections.find_by_id(gui_connection.id.get())
                    rm_failed_connection.append(gui_connection)
                except NotFoundError:
                    pass
            gui_connection.state.set(state)

        for gui_connection in self.connections:
            in_broker = False
            try:
                connection_values = broker_connections.values()
            except AttributeError:
                connection_values = broker_connections.itervalues()
            for (broker_connection, chain) in connection_values:
                if gui_connection.id.get() == broker_connection.id.get():
                    in_broker = True
                    break
            if not in_broker:
                rm_connection.append(gui_connection)

        for gui_connection in self.failed_connections:
            in_broker = False
            try:
                connection_values = broker_connections.values()
            except AttributeError:
                connection_values = broker_connections.itervalues()
            for (broker_connection, chain) in connection_values:
                if gui_connection.id.get() == broker_connection.id.get():
                    in_broker = True
                    break
            if not in_broker:
                rm_failed_connection.append(gui_connection)

        for gui_connection in rm_connection:
            try:
                self.connections.remove_by_connection_id(
                    gui_connection.id.get())
                changed = True
            except NotFoundError:
                continue

        for gui_connection in rm_failed_connection:
            try:
                self.failed_connections.remove_by_connection_id(
                    gui_connection.id.get())
                changed = True
            except NotFoundError:
                continue

        if changed:
            self.commit()
