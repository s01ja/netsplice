# -*- coding: utf-8 -*-
# credential.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Credentials.

Model for exchanging credential information.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.account_id_value import (
    account_id_value as account_id_value_validator
)
from netsplice.model.validator.username_value import (
    username_value as username_value_validator
)
from netsplice.model.validator.password_value import (
    password_value as password_value_validator
)
from netsplice.model.validator.store_password import (
    store_password as store_password_validator
)


class credential(marshalable):
    '''
    Model for Credentials.

    This model stores sensitive information for a connection. It is used to
    pass login details to the privileged backend and is optionally stored.
    '''

    def __init__(self):
        '''
        Initialize Model.

        Initialize Model.
        '''
        marshalable.__init__(self)

        self.account_id = field(
            required=True,
            validators=[account_id_value_validator()]
        )
        self.username = field(
            required=True,
            default='',
            validators=[username_value_validator()]
        )
        self.password = field(
            required=True,
            default='',
            validators=[password_value_validator()]
        )
        self.store_password = field(
            required=True,
            default=None,
            validators=[store_password_validator()]
        )
