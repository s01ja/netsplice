# -*- coding: utf-8 -*-
# privileged_dispatcher.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''
Dispatcher for unprivileged actions.
'''

from socket import error as socket_error

from tornado import gen, httpclient

import netsplice.backend.dispatcher_endpoints as endpoints
from netsplice.model.process import (
    process as process_model
)
from netsplice.util import get_logger
from netsplice.util.ipc.errors import ServerConnectionError
from netsplice.util.ipc.service import service
from netsplice.util.path import get_path_prefix
from netsplice.util.process.dispatcher import dispatcher as process_dispatcher
from netsplice.util.process.errors import ProcessError
from netsplice.util.model.errors import ValidationError


logger = get_logger()


class unprivileged_dispatcher(service, process_dispatcher):

    def __init__(self, host, port, host_owner, port_owner):
        service.__init__(
            self, host, port, host_owner, port_owner,
            'unprivileged', 'backend')
        self.wait_for_service = True
        self.process_model = process_model()
        process_dispatcher.__init__(
            self, 'NetspliceUnprivilegedApp', {
                '--host': self.host,
                '--port': self.port,
                '--host-owner': self.host_owner,
                '--port-owner': self.port_owner
            },
            elevated=False,
            model=self.process_model,
            unique=True)

    def start(self):
        '''
        Start.

        Start the service process and notify the gui in case it fails.
        '''
        try:
            self.start_subprocess()
        except ProcessError as errors:
            message = (
                'Cannot start subprocess: %s. The App %s is required to change'
                ' the network and routing preferences on this system. You need'
                ' to allow that it modifies your system.'
                % (str(errors), self._name))
            logger.critical(message)
            gui_model = self.application.get_module('gui').model
            gui_model.events.notify_error('Cannot start subprocess')

    @gen.coroutine
    def shutdown(self):
        try:
            response = yield self.post(endpoints.SHUTDOWN, '{}')
            raise gen.Return(response)
        except gen.Return as return_value:
            raise return_value
        except socket_error as errors:
            self.set_error_code(2112, str(errors))
            raise ServerConnectionError(str(errors))
        except httpclient.HTTPError as errors:
            self.set_error_code(2113, str(errors))
            raise ServerConnectionError(str(errors))
