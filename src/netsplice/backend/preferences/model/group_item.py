# -*- coding: utf-8 -*-
# group_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about
'''

from netsplice.config import model as config_model
from netsplice.config import backend as config_backend
from netsplice.util.model.field import field
from netsplice.util.model.marshalable_persistent import marshalable_persistent
from netsplice.model.validator.enum import (
    enum as enum_validator
)
from netsplice.model.validator.group_id import (
    group_id as group_id_validator
)
from netsplice.model.validator.group_id_value import (
    group_id_value as group_id_value_validator
)
from netsplice.backend.preferences.model.validator.group_name import (
    group_name as group_name_validator
)
from netsplice.backend.preferences.model.validator.weight import (
    weight as weight_validator
)
from netsplice.model.validator.boolean import boolean as boolean_validator
from netsplice.model.validator.version import (
    version as version_validator
)


class group_item(marshalable_persistent):
    def __init__(self, owner):
        marshalable_persistent.__init__(self, owner)

        self.id = field(
            required=True,
            validators=[group_id_validator()])

        self.name = field(
            required=True,
            validators=[group_name_validator()])

        self.parent = field(
            required=False,
            default=None,
            validators=[group_id_value_validator()])

        self.weight = field(
            required=False,
            default=0,
            validators=[weight_validator()])

        self.autostart = field(
            required=False,
            default=0,
            validators=[boolean_validator()])

        self.connect_mode = field(
            required=False,
            default=config_backend.CONNECT_MODE_SEQUENCE,
            validators=[enum_validator(config_backend.CONNECT_MODES)])

        self.failure_mode = field(
            required=False,
            default=config_backend.FAILURE_MODE_IGNORE,
            validators=[enum_validator(config_backend.FAILURE_MODES)])

        self.collapsed = field(
            required=False,
            default=0,
            validators=[boolean_validator()])

        self.enabled = field(
            required=False,
            default=True,
            validators=[boolean_validator()])

        self.immutable = field(
            required=False,
            default=False,
            validators=[boolean_validator()])

        self.version = field(
            required=True,
            default=config_model.VERSION,
            validators=[version_validator()])

    def update(self, group):
        '''
        Update.

        Update the group from another group item.
        '''
        self.name.set(group.name.get())
        self.parent.set(group.parent.get())
        self.weight.set(group.weight.get())
        self.autostart.set(group.autostart.get())
        self.collapsed.set(group.collapsed.get())
        self.connect_mode.set(group.connect_mode.get())
        self.failure_mode.set(group.failure_mode.get())
        self.enabled.set(group.enabled.get())

    def update_new(self, create_group_instance):
        '''
        Update New.

        Update the group from a create-group request.
        '''
        self.name.set(create_group_instance.name.get())
        self.parent.set(create_group_instance.parent.get())
        self.weight.set(create_group_instance.weight.get())
