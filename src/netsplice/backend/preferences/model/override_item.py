# -*- coding: utf-8 -*-
# override_item.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for config override items.
'''

from netsplice.config import model as config_model
from netsplice.util.model.field import field
from netsplice.util.model.marshalable_persistent import marshalable_persistent
from netsplice.backend.preferences.model.validator.account_type import (
    account_type as account_type_validator
)
from netsplice.backend.preferences.model.validator.override_name import (
    override_name as override_name_validator
)
from netsplice.backend.preferences.model.validator.override_value import (
    override_value as override_value_validator
)
from netsplice.model.validator.version import (
    version as version_validator
)


class override_item(marshalable_persistent):
    def __init__(self, owner):
        marshalable_persistent.__init__(self, owner)

        self.name = field(
            required=True,
            validators=[override_name_validator()])

        self.type = field(
            default='OpenVPN',
            required=True,
            validators=[account_type_validator()])

        self.value = field(
            required=True,
            validators=[override_value_validator()])

        self.version = field(
            required=True,
            default=config_model.VERSION,
            validators=[version_validator()])
