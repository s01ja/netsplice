# -*- coding: utf-8 -*-
# backend.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for preferences concerning the Backend.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable_persistent import marshalable_persistent
from netsplice.model.arg_list import arg_list
from netsplice.model.validator.min import (
    min as min_validator
)
from netsplice.model.validator.max import (
    max as max_validator
)
from netsplice.model.validator.store_password import (
    store_password as store_password_validator
)
import netsplice.config.ipc as config_ipc
import netsplice.config.process as config_process
import netsplice.config.backend as config_backend


class backend(marshalable_persistent):
    def __init__(self, owner):
        marshalable_persistent.__init__(self, owner)

        self.max_shutdown_time = field(
            default=config_backend.MAX_SHUTDOWN_TIME,
            validators=[min_validator(1), max_validator(300)])

        self.sequence_wait_after_connect = field(
            default=config_backend.MAX_SEQUENCE_WAIT_AFTER_CONNECT_TIME,
            validators=[min_validator(0), max_validator(30)])

        self.ipc_certificate_valid_days = field(
            default=config_ipc.CERTIFICATE_VALID_DAYS,
            validators=[min_validator(1), max_validator(365)])

        self.graphical_sudo_application = arg_list(
            config_process.GRAPHICAL_SUDO_APPLICATION)

        self.store_password_default = field(
            default=config_backend.STORE_PASSWORD_DEFAULT,
            validators=[store_password_validator()])

    def apply_values(self):
        '''
        Apply Values.

        Apply values from the preferences to the config so other
        backend components can use the values.
        '''
        config_backend.MAX_SHUTDOWN_TIME = self.max_shutdown_time.get()
        config_backend.MAX_SEQUENCE_WAIT_AFTER_CONNECT_TIME = \
            self.sequence_wait_after_connect.get()
        config_backend.STORE_PASSWORD_DEFAULT = \
            self.store_password_default.get()
        config_process.GRAPHICAL_SUDO_APPLICATION = \
            self.graphical_sudo_application.to_list()
        config_ipc.CERTIFICATE_VALID_DAYS = \
            self.ipc_certificate_valid_days.get()
