# -*- coding: utf-8 -*-
# override_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Configuration override_list.
'''

from netsplice.backend.preferences.model.override_item import (
    override_item as override_item_model
)
from netsplice.util.errors import (
    NotFoundError, NotUniqueError
)
from netsplice.util.model.marshalable_list_persistent import (
    marshalable_list_persistent
)


class override_list(marshalable_list_persistent):
    '''
    Override List Model.
    '''
    def __init__(self, owner):
        marshalable_list_persistent.__init__(
            self, override_item_model, owner)

    def apply_values(self):
        '''
        Apply Values.

        Apply values from the preferences to the config so other
        components can use the values.
        '''
        pass

    def find(self, name, type):
        '''
        Find.

        Return item when both name and type match or raise a NotFoundError.
        '''
        for item in self:
            if item.name.get() == name and item.type.get() == type:
                return item
        raise NotFoundError(name)

    def create(self, name, type):
        '''
        Create.

        Create new override with the given name and type. Raises a
        NotUniqueError when this override already exsits.
        '''
        try:
            self.find(name, type)
            raise NotUniqueError('%s %s is not unique' % (name, type))
        except NotFoundError:
            pass
        new_item = self.item_model_class(self.get_owner())
        new_item.name.set(name)
        new_item.type.set(type)
        return new_item

    def remove(self, name, type):
        '''
        Remove.

        Remove an override with the given name and type.
        '''
        remove_item = self.find(name, type)
        super(marshalable_list_persistent, self).remove(remove_item)
