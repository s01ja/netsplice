# -*- coding: utf-8 -*-
# test___init__.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Backend Priv Model.

Check model functions.
'''

from netsplice.backend.privileged.model.connection_list_item import (
    connection_list_item as connection_list_item
)
from netsplice.backend.privileged.model.connection_list import (
    connection_list as connection_list
)
from netsplice.backend.privileged.model.response.connection_status import (
    connection_status as connection_status_model
)
from netsplice.backend.privileged.model import model

CONNECTION_ID = '00000000-0000-0000-0000-000000000001'
MISSING_ID = '00000000-0000-0000-0000-000000000002'
OTHER_CONNECTION_ID = '00000000-0000-0000-0000-000000000003'


class mock_model(object):
    def __init__(self):
        self.commit_called = False

    def commit(self):
        self.commit_called = True


def test_set_connections_with_empty_clears_connection_list():
    m = model()
    mm = mock_model()
    m.commit = mm.commit
    ci = connection_list_item()
    ci.id.set(CONNECTION_ID)
    ci.origin.set('some_plugin_name')
    m.connections.append(ci)
    assert(len(m.connections) == 1)
    m.set_connections('some_plugin_name', connection_list())
    assert(len(m.connections) == 0)


def test_set_connections_with_new_adds_new():
    m = model()
    mm = mock_model()
    m.commit = mm.commit
    cl = connection_list()
    ci = connection_list_item()
    ci.id.set(CONNECTION_ID)
    ci.type.set('OpenVPN')
    cl.append(ci)
    assert(len(m.connections) == 0)
    m.set_connections('unknown', cl)
    assert(len(m.connections) == 1)
    assert(mm.commit_called)


def test_set_connections_with_abandoned_removes_abandoned():
    m = model()
    mm = mock_model()
    m.commit = mm.commit
    cl = connection_list()
    ci = connection_list_item()
    ci.id.set(CONNECTION_ID)
    ci.type.set('OpenVPN')
    ci.origin.set('some_plugin_name')
    m.connections.append(ci)
    assert(len(m.connections) == 1)
    m.set_connections('some_plugin_name', cl)
    assert(len(m.connections) == 0)
    assert(mm.commit_called)


def test_set_connections_with_abandoned_removes_abandoned_even_active():
    m = model()
    mm = mock_model()
    m.commit = mm.commit
    cl = connection_list()
    ci = connection_list_item()
    ci.id.set(CONNECTION_ID)
    ci.type.set('OpenVPN')
    ci.origin.set('some_plugin_name')
    ci.active.set(True)
    m.connections.append(ci)
    assert(len(m.connections) == 1)
    m.set_connections('some_plugin_name', cl)
    assert(len(m.connections) == 0)
    assert(mm.commit_called)


def test_set_connections_with_changes_calls_commit():
    m = model()
    mm = mock_model()
    m.commit = mm.commit
    cl = connection_list()
    cii = connection_list_item()
    cii.id.set(CONNECTION_ID)
    cii.type.set('OpenVPN')
    cii.active.set(False)
    ciu = connection_list_item()
    ciu.id.set(CONNECTION_ID)
    ciu.type.set('OpenVPN')
    cii.active.set(True)
    cl.append(ciu)
    m.connections.append(cii)
    m.set_connections('unknown', cl)
    assert(mm.commit_called)


def test_set_connections_with_changes_updates_items():
    m = model()
    mm = mock_model()
    m.commit = mm.commit
    cl = connection_list()
    cii = connection_list_item()
    cii.id.set(CONNECTION_ID)
    cii.type.set('OpenVPN')
    cii.active.set(False)
    ciu = connection_list_item()
    ciu.id.set(CONNECTION_ID)
    ciu.type.set('OpenVPN')
    ciu.active.set(True)
    cl.append(ciu)
    m.connections.append(cii)
    ci_before = m.connections.find_by_id(CONNECTION_ID)
    assert(ci_before.active.get() is False)
    m.set_connections('unknown', cl)
    ci_result = m.connections.find_by_id(CONNECTION_ID)
    assert(ci_result.active.get() is True)
    assert(mm.commit_called)


def test_set_connection_status_adds_connection_when_not_found():
    m = model()
    mm = mock_model()
    m.commit = mm.commit
    si = connection_status_model()
    si.id.set(CONNECTION_ID)
    si.type.set('OpenVPN')
    assert(len(m.connections) == 0)
    m.set_connection_status('unknown', CONNECTION_ID, si)
    assert(len(m.connections) == 1)


def test_set_connection_status_updates_connection_when_found():
    m = model()
    mm = mock_model()
    m.commit = mm.commit
    si = connection_status_model()
    si.id.set(CONNECTION_ID)
    si.type.set('OpenVPN')
    si.active.set(False)
    cii = connection_list_item()
    cii.id.set(CONNECTION_ID)
    cii.type.set('OpenVPN')
    cii.active.set(True)
    m.connections.append(cii)

    assert(len(m.connections) == 1)
    m.set_connection_status('unknown', CONNECTION_ID, si)
    assert(len(m.connections) == 1)
    result = m.connections.find_by_id(CONNECTION_ID)
    assert(result.active.get() is False)
