# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for GUI process. Stores all information the GUI should know about
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.model.event import event as event_model


class model(marshalable_list):
    def __init__(self):
        marshalable_list.__init__(self, event_model)
        self._index = 0

    def _get_next_index(self):
        '''
        Get Next Index.

        Increase the internal index and return the increased value.
        '''
        self._index += 1
        return self._index

    def new_event(self, type_name, name):
        '''
        New Event.

        Create a new event instance, prepare the instance and set a new index.
        '''
        event_model_instance = event_model()
        event_model_instance.prepare(type_name, name)
        event_model_instance.index.set(self._get_next_index())
        return event_model_instance
