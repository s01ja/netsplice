# -*- coding: utf-8 -*-
# account.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Account Dispatcher.

Dispatcher functions to be used in controllers concerning accounts.
'''
from tornado import gen
from netsplice.backend.connection.chain import (
    chain as connection_chain
)
from netsplice.util import get_logger

logger = get_logger()


class account(object):
    '''
    Stateless Account dispatcher.

    Middleware creates instance to expose application and other modules to
    the functions.
    '''

    def __init__(self, application):
        '''
        Initialize Module.

        Setup members from application.
        '''
        self.application = application
        self.gui_model = self.application.get_module('gui').model
        self.preferences_model = application.get_module(
            'preferences').model
        self.connection_broker = self.application.get_module(
            'connection').broker

    def _get_chain(self, account_id):
        '''Get Chain.

        Return a connection_chain from the preferences

        Arguments:
            account_id (string): id of a account

        Returns:
            connection_chain -- instance with account_list, sequence and
                                failure mode set.
        '''

        accounts = self.preferences_model.accounts
        chains = self.preferences_model.chains
        account_chain = chains.get_connection_chain(
            account_id, accounts, connection_chain)
        return account_chain

    @gen.coroutine
    def connect(self, account_id):
        '''
        Connect.

        Connect the Account that is described with the account_id.
        '''
        account_model_instance = self.preferences_model.accounts.find_by_id(
            account_id)
        chain = self._get_chain(account_id)
        self.connection_broker.connect(
            account_model_instance, chain)

    @gen.coroutine
    def connect_all(self, account_id):
        '''
        Connect All.

        Connect all children of the account.
        '''
        chain = self._get_chain(account_id)
        self.connection_broker.connect_all(chain)

    @gen.coroutine
    def disconnect(self, account_id):
        '''
        Disconnect.

        Disconnect the Account that is identified by account_id.
        Depending on the Account configuration other Accounts are
        disconnected before.
        '''
        account_model_instance = self.preferences_model.accounts.find_by_id(
            account_id)
        chain = self._get_chain(account_id)
        self.connection_broker.disconnect(
            account_model_instance, chain)

    @gen.coroutine
    def reconnect(self, account_id):
        '''
        Reconnect.

        Reconnect the Account that is identified by account_id.
        '''
        account_model_instance = self.preferences_model.accounts.find_by_id(
            account_id)
        chain = self._get_chain(account_id)
        self.connection_broker.reconnect(
            account_model_instance, chain)

    @gen.coroutine
    def reset(self, account_id):
        '''
        Reset.

        Reset the Account that is identified by account_id.
        '''
        account_model_instance = self.preferences_model.accounts.find_by_id(
            account_id)
        self.connection_broker.reset(
            account_model_instance)
