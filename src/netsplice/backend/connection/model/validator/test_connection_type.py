# -*- coding: utf-8 -*-
# test_connection_type.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Tests for Connection Type validator.

Checks Validator.
'''

from netsplice.backend.connection.model.validator.connection_type import (
    connection_type
)


def get_test_object():
    validator = connection_type()
    return validator


def test_is_valid_with_none_returns_false():
    validator = get_test_object()
    result = validator.is_valid(None)
    assert(result is False)


def test_is_valid_with_int_returns_false():
    validator = get_test_object()
    result = validator.is_valid(123)
    assert(result is False)


def test_is_valid_with_empty_string_returns_false():
    validator = get_test_object()
    result = validator.is_valid('')
    assert(result is False)


def test_is_valid_with_known_types_returns_true():
    validator = get_test_object()
    result = validator.is_valid('OpenVPN')
    assert(result is True)
    result = validator.is_valid('Tor')
    assert(result is True)
