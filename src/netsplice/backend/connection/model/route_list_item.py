# -*- coding: utf-8 -*-
# route_list_item.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing routes.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.ip_address import (
    ip_address as ip_address_validator
)


class route_list_item(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.net = field(
            required=True,
            validators=[ip_address_validator()])
        self.netmask = field(
            required=True,
            validators=[ip_address_validator()])
        self.gateway = field(
            required=True,
            validators=[ip_address_validator()])

    def to_env(self):
        '''
        To Environment.

        Convert the route to a string suitable for environment variables.
        '''
        return (
            'net %s netmask %s gw %s'
            % (self.net.get(), self.netmask.get(), self.gateway.get()))
