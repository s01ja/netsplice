# -*- coding: utf-8 -*-
# route_list.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing routes.
'''

from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.backend.connection.model.route_list_item import (
    route_list_item
)


class route_list(marshalable_list):
    '''
    Marshalable model that contains multiple items of
    route_list_item_model.
    '''
    def __init__(self):
        marshalable_list.__init__(self, route_list_item)

    def add(self, net, netmask, gateway):
        '''
        Append the given state and message to the route_list.
        '''
        route_item = route_list_item()
        route_item.net.set(net)
        route_item.netmask.set(netmask)
        route_item.gateway.set(gateway)
        self.append(route_item)
