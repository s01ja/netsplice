# -*- coding: utf-8 -*-
# connection_list_item_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for listing connections.

This model is used for the gui connections that are constantly updated.
'''

from tornado import locks

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.named_value_list import (
    named_value_list as named_value_list_model
)
from netsplice.model.validator.account_id_value import (
    account_id_value as account_id_value_validator
)
from netsplice.model.validator.connection_id import (
    connection_id as connection_id_validator
)
from netsplice.model.validator.connection_id_value import (
    connection_id_value as connection_id_value_validator
)
import netsplice.config.backend as config
from netsplice.backend.connection.model.validator.connection_type import (
    connection_type as connection_type_validator
)
from netsplice.backend.connection.model.status_list import (
    status_list as status_list_model
)
from netsplice.backend.connection.model.route_list import (
    route_list as route_list_model
)
from netsplice.config import connection as config_connection
from netsplice.model.validator.boolean import boolean as boolean_validator
from netsplice.model.validator.max import (
    max as max_validator
)
from netsplice.model.validator.min import (
    min as min_validator
)

TEMPORARY_UUID = '00000000-0000-0000-0000-000c08ec4189'


class connection_item(marshalable):
    '''
    Connection Item.

    Field definitions and model functions.
    '''

    def __init__(self):
        '''
        Initialize.

        Initialize field attributes and backend-only members.
        '''
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[connection_id_validator()])

        self.privileged_id = field(
            required=True,
            validators=[connection_id_value_validator()])

        self.unprivileged_id = field(
            required=True,
            validators=[connection_id_value_validator()])

        self.account_id = field(
            default=None,
            required=False,
            validators=[account_id_value_validator()])

        self.type = field(
            required=True,
            validators=[connection_type_validator()])

        self.failed = field(
            required=True,
            default=False,
            validators=[boolean_validator()])

        self.progress = status_list_model()
        self.errors = status_list_model()

        self.environment = named_value_list_model()

        self.routes = route_list_model()

        self.state = field(
            default=None,
            required=True,
            validators=[])

        self.last_state = field(
            default=None,
            required=True,
            validators=[])

        self.state_condition = locks.Condition()
        self.removed = False

    def has_temporary_id(self):
        '''
        Connection Has Temporary Id.

        Returns True if the connection_id is temporary and has no priv
        counterpart.
        '''
        return self.id.get() == TEMPORARY_UUID

    def reset(self):
        '''
        Reset

        Reset the values to initialized.
        '''
        self.reset_environment()
        self.reset_routes()
        self.state.set(config_connection.INITIALIZED)
        self.last_state.set(config_connection.INITIALIZED)
        self.progress = status_list_model()
        self.errors = status_list_model()

    def reset_environment(self):
        '''
        Reset environment.

        Reset the environment with default values.
        '''
        del self.environment[:]
        self.environment.add_value(
            'netsplice_account_id', self.account_id.get())
        self.environment.add_value(
            'netsplice_connection_type', self.type.get())
        self.environment.add_value(
            'netsplice_connection_id', self.id.get())

    def reset_routes(self):
        '''
        Reset routes.

        Clear all stored routes.
        '''
        del self.routes[:]

    def update_data(self, connection_model_instance):
        '''
        Update Data.

        Update the data of the connection list item with the data from a
        privileged request.
        '''
        updated = False
        updated |= self.state.set(
            connection_model_instance.state.get())
        updated |= self.update_connection_status(connection_model_instance)
        return updated

    def update_connection_status(self, connection_model_instance):
        '''
        Update Connection Status.

        Interface: Update connection status for connections. Overload
        connection_model_instance: model with fields of connection status
        '''
        updated = False
        if self.state.get() != connection_model_instance.state.get():
            self.state.set(connection_model_instance.state.get())
            updated = True
        return updated
