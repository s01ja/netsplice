# -*- coding: utf-8 -*-
# connection_plugin.py
# Copyright (C) 2017 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Base for all connection plugins.

Defines the functions shared by all connections and the interface all
connections should implement.
'''
import sys
from tornado import gen, ioloop
from netsplice.config import backend as config_backend
from netsplice.config import connection as config_connection
from netsplice.backend.connection.errors import BadConnectionTypeError
from netsplice.util import get_logger

logger = get_logger()

this = sys.modules[__name__]
this.plugins = {}


def register_plugin(connection_type, constructor):
    '''
    Register plugin.

    Register a constructor for the given connection type.
    '''
    this.plugins[connection_type] = constructor


def factory(connection_type, application, module_model):
    '''
    Factory that returns a dispatcher instance for the given connection type.
    '''
    try:
        return this.plugins[connection_type](application, module_model)
    except KeyError:
        raise BadConnectionTypeError(connection_type)


class connection_plugin(object):
    '''
    Connection Base.

    Requires submodules to reimplement the functions marked as pure virtual.
    '''

    def __init__(self, type, application, connection):
        '''
        Initialize module.

        Extract privileged dispatcher from application and initialize the
        checked log index.
        '''
        self.application = application
        self.privileged = self.application.get_privileged()
        self.unprivileged = self.application.get_unprivileged()
        self.gui_model = self.application.get_module('gui').model
        self.connection = connection
        self.log_connection_id = None
        self.type = type
        self.checked_log_index = -1
        self._get_status_scheduled = False

        self.event_module = self.application.get_module('event')

    @gen.coroutine
    def _call_get_status(self):
        '''
        Call get_status.

        Scheduled call to get_status with reschedule to avoid that plugins
        reschedule while rescheduled.

        Decorators:
            gen.coroutine
        '''
        self._get_status_scheduled = False
        result = yield self.get_status()
        if result is not None:
            self.schedule_get_status()

    @gen.coroutine
    def _update_connection(self):
        '''
        Update handling for any connection.

        Processes the priv model connections and evaluates a status-change
        from those messages. When the status changes, the connect_failed bit
        may be set.
        '''

        log_model = self.application.get_module('log').model
        log = log_model.filter_by_extra(
            'connection_id', self.log_connection_id,
            self.checked_log_index + 1)

        for log_item in log:
            self.checked_log_index = log_item.index.get()
            ioloop.IOLoop.current().call_later(
                0, self.check_log_item, log_item)

    def schedule_get_status(self):
        '''
        Schedule get status.

        Creates a detached stack for getting the status of a connected
        connection.
        When the connection state is disconnecting, no get_status is
        scheduled to avoid 404 errors in logs.
        When the connection state is not connected, reschedule.
        '''
        if self.connection.in_disconnecting():
            # avoid 404 in log
            return
        if self._get_status_scheduled:
            # avoid multiple schedules
            return
        self._get_status_scheduled = True
        ioloop.IOLoop.current().call_later(
            config_backend.STATUS_UPDATE_PERIOD,
            lambda: self._call_get_status())

    @gen.coroutine
    def abort(self):
        '''
        Abort the process.

        Pure Virtual function to be implemented by a submodule.
        Free all resources that were allocated.
        '''
        raise NotImplementedError(
            'abort was not implemented')

    @gen.coroutine
    def connect(self):
        '''
        Start the connection procedure.

        Pure Virtual function to be implemented by a submodule.
        Setup the tunnel, bring up devices.
        Setup routes etc for the current connection-type.
        Should call self.schedule_get_status(self.connection.id.get())
        '''
        raise NotImplementedError(
            'connect was not implemented')

    def connect_finish(self):
        '''
        Trigger CONNECT_FINISH

        Trigger a CONNECT_FINISH in the connection.
        '''
        self.connection.trigger(config_connection.CONNECT_FINISH)
        self.event_module.notify(
            config_connection.CONNECT_FINISH,
            self.connection.id.get())

    def connect_init(self):
        '''
        Trigger CONNECT

        Trigger a CONNECT in the connection.
        '''
        self.connection.trigger(config_connection.CONNECT)
        self.event_module.notify(
            config_connection.CONNECT,
            self.connection.id.get())

    def connect_process(self):
        '''
        Trigger CONNECT_PROCESS

        Trigger a CONNECT_PROCESS in the connection.
        '''
        self.connection.trigger(config_connection.CONNECT_PROCESS)
        self.event_module.notify(
            config_connection.CONNECT_PROCESS,
            self.connection.id.get())

    @gen.coroutine
    def disconnect(self):
        '''
        Shutdown an active connection.

        Pure Virtual function to be implemented by a submodule.
        close tunnel, bring down devices,
        restore routes etc for the current connection-type.
        '''
        raise NotImplementedError(
            'disconnect was not implemented')

    def disconnect_finish(self):
        '''
        Trigger DISCONNECT_FINISH

        Trigger a DISCONNECT_FINISH in the connection.
        '''
        self.connection.trigger(config_connection.DISCONNECT_FINISH)
        self.event_module.notify(
            config_connection.DISCONNECT_FINISH,
            self.connection.id.get())

    def disconnect_abort(self):
        '''
        Trigger DISCONNECT_ABORT

        Trigger a DISCONNECT_ABORT in the connection.
        '''
        if self.connection.state == config_connection.ABORTED:
            return
        self.connection.trigger(config_connection.DISCONNECT_ABORT)
        self.event_module.notify(
            config_connection.DISCONNECT_ABORT,
            self.connection.id.get())

    def disconnect_init(self):
        '''
        Trigger DISCONNECT

        Trigger a DISCONNECT in the connection.
        '''
        self.connection.trigger(config_connection.DISCONNECT)
        self.event_module.notify(
            config_connection.DISCONNECT,
            self.connection.id.get())

    def disconnect_process(self):
        '''
        Trigger DISCONNECT_PROCESS

        Trigger a DISCONNECT_PROCESS in the connection.
        '''
        self.connection.trigger(config_connection.DISCONNECT_PROCESS)
        self.event_module.notify(
            config_connection.DISCONNECT_PROCESS,
            self.connection.id.get())

    def disconnect_failure_finish(self):
        '''
        Trigger DISCONNECT_FAILURE_FINISH

        Trigger a DISCONNECT_FAILURE_FINISH in the connection.
        '''
        self.connection.trigger(config_connection.DISCONNECT_FAILURE_FINISH)
        self.event_module.notify(
            config_connection.DISCONNECT_FAILURE_FINISH,
            self.connection.id.get())

    def disconnect_failure_init(self):
        '''
        Trigger DISCONNECT_FAILURE

        Trigger a DISCONNECT_FAILURE in the connection.
        '''
        self.connection.trigger(config_connection.DISCONNECT_FAILURE)
        self.event_module.notify(
            config_connection.DISCONNECT_FAILURE,
            self.connection.id.get())

    def disconnect_failure_process(self):
        '''
        Trigger DISCONNECT_FAILURE_PROCESS

        Trigger a DISCONNECT_FAILURE_PROCESS in the connection.
        '''
        self.connection.trigger(config_connection.DISCONNECT_FAILURE_PROCESS)
        self.event_module.notify(
            config_connection.DISCONNECT_FAILURE_PROCESS,
            self.connection.id.get())

    @gen.coroutine
    def get_status(self):
        '''
        Return the status of the current connection.

        Return stats and connection state and update own model.
        Pure Virtual function to be implemented by a submodule.
        Beware, twisted terminology:
        privileged.get_status -> {active,map_of_stats}
        connections.model..status -> more like a log of messages that
        influenced the status
        gui.status -> {active.map_of_stats}
        '''
        raise NotImplementedError(
            'get_status was not implemented')

    @gen.coroutine
    def reconnect(self):
        '''
        Reconnect a previously connected connection.

        Pure Virtual function to be implemented by a submodule.
        (eg to reinitialize devices)
        '''
        raise NotImplementedError(
            'reconnect was not implemented')

    def reconnect_finish(self):
        '''
        Trigger RECONNECT_FINISH

        Trigger a RECONNECT_FINISH in the connection
        '''
        self.connection.trigger(config_connection.RECONNECT_FINISH)
        self.event_module.notify(
            config_connection.RECONNECT_FINISH,
            self.connection.id.get())

    def reconnect_init(self):
        '''
        Trigger RECONNECT

        Trigger a RECONNECT in the connection.
        '''
        self.connection.trigger(config_connection.RECONNECT)
        self.event_module.notify(
            config_connection.RECONNECT,
            self.connection.id.get())

    def reconnect_process(self):
        '''
        Trigger RECONNECT_PROCESS

        Trigger a RECONNECT_PROCESS in the connection.
        '''
        self.connection.trigger(config_connection.RECONNECT_PROCESS)
        self.event_module.notify(
            config_connection.RECONNECT_PROCESS,
            self.connection.id.get())

    def reset(self):
        '''
        Reset the connection so a non-failed state.

        After this call the GUI no longer displays errors.
        '''
        log_model = self.application.get_module('log').model
        log = log_model.filter_by_extra(
            'account_id', self.connection.account.id.get())
        if len(log) > 0:
            self.checked_log_index = log[-1].index.get()
        else:
            self.checked_log_index = 0

    def setup_finish(self):
        '''
        Trigger SETUP_FINISH

        Trigger a SETUP_FINISH in the connection.
        '''
        self.connection.trigger(config_connection.SETUP_FINISH)
        self.event_module.notify(
            config_connection.SETUP_FINISH,
            self.connection.id.get())

    def setup_cancel(self):
        '''
        Trigger SETUP_CANCEL

        Trigger a SETUP_CANCEL in the connection.
        '''
        self.connection.trigger(config_connection.SETUP_CANCEL)
        self.event_module.notify(
            config_connection.SETUP_CANCEL,
            self.connection.id.get())

    @gen.coroutine
    def setup(self, connection_model_instance, account_model_instance):
        '''
        Setup a connection with the given account details.

        Pure Virtual function to be implemented by a submodule.
        connection_model_instance: model for connection to be updated
        account_model_instance: model with all values required for setup.
        gen.Return(connection_model_instance): a connection_item used for
        managing the connection.
        '''
        raise NotImplementedError(
            'setup was not implemented')

    @gen.coroutine
    def update(self):
        '''
        Update the connection from messages from the connection process.

        return: gen.Return(status_changed) - True when a status change was
        detected.
        '''
        result = []
        if self.connection.state != config_connection.INITIALIZED:
            result = yield self._update_connection()
        raise gen.Return(result)

    def check_log_item(self, message):
        '''
        Handle messages of the processes.

        Pure Virtual function to be implemented by a submodule.
        @param message - string that comes from the connection process
        @returns boolean - indicator that the connection state has changed
        '''
        raise NotImplementedError(
            'update connection status was not implemented')
