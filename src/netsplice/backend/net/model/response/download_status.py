# -*- coding: utf-8 -*-
# download_status.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for download status.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.boolean import (
    boolean as boolean_validator
)
from netsplice.model.validator.date import (
    date as date_validator
)
from netsplice.model.validator.max import (
    max as max_validator
)
from netsplice.model.validator.min import (
    min as min_validator
)
from netsplice.model.validator.uri import (
    uri as uri_validator
)
from netsplice.model.validator.message import (
    message as message_validator
)
from netsplice.model.validator.none import (
    none as none_validator
)


class download_status(marshalable):
    def __init__(self):
        marshalable.__init__(self)

        self.active = field(
            required=True,
            validators=[boolean_validator()])

        self.url = field(
            required=True,
            validators=[uri_validator()])

        self.start_date = field(
            required=True,
            validators=[date_validator()])

        self.complete = field(
            required=True,
            validators=[min_validator(0), max_validator(100)])

        self.bps = field(
            required=True,
            validators=[min_validator(0)])

        self.errors = field(
            required=True,
            validators=[boolean_validator()])

        self.error_message = field(
            required=True,
            validators=[none_validator(exp_or=[message_validator()])])

        self.signature_ok = field(
            required=True,
            validators=[none_validator(exp_or=[boolean_validator()])])

        self.checksum_ok = field(
            required=True,
            validators=[none_validator(exp_or=[boolean_validator()])])
