# -*- coding: utf-8 -*-
# model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for Unprivileged process.

Stores all information the unprivileged should know about.
'''

from netsplice.util.errors import NotFoundError
from netsplice.util.model.marshalable import marshalable
from netsplice.backend.unprivileged.model.connection_list import (
    connection_list
)
from netsplice.util import get_logger

logger = get_logger()


class model(marshalable):
    '''
    Module model.

    Marshalable model for connections.
    '''

    def __init__(self):
        '''
        Initialize Module.

        Creates a empty list of connections.
        '''
        marshalable.__init__(self)
        self.connections = connection_list()

    def set_connections(self, origin, connection_list_model):
        '''
        Set all connections.

        Updates the list of known connections with the given
        connection_list_model. Removes abandoned connections and adds new.
        '''
        changed = False
        deleted_connections = []
        for connection in self.connections:
            # No find-by-id because its a response-model
            if connection.origin.get() != origin:
                continue
            found = False
            for connection_item in connection_list_model:
                if connection_item.id.get() == connection.id.get():
                    found = True
                    break
            if found:
                continue
            # When privileged did not return the connection, it can be
            # considered inactive, otherwise they will not delete.
            connection.active.set(False)
            deleted_connections.append(connection)

        for connection in deleted_connections:
            try:
                self.connections.delete_connection(connection.id.get())
                changed = True
            except NotFoundError:
                pass

        for connection in connection_list_model:
            try:
                connection_item = self.connections.find_by_id(
                    connection.id.get())
                changed |= connection_item.update_data(connection)
                continue
            except NotFoundError:
                self.connections.add_connection(origin, connection)
                changed = True
        if changed:
            self.commit()

    def set_connection_status(
            self,
            origin,
            connection_id,
            connection_status_model_instance):
        '''
        Set Connection Status.

        Find the connection with the given id and update its data. When the
        connection is not found, it is added to the connections list.
        '''
        try:
            connection = self.connections.find_by_id(connection_id)
            connection.update_data(connection_status_model_instance)
        except NotFoundError:
            self.connections.add_connection(
                origin, connection_status_model_instance)
        self.commit()
