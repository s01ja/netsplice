# -*- coding: utf-8 -*-
# connection_list_item_model.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for connection items.

Stores values that identify a connection.
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable import marshalable
from netsplice.model.validator.connection_id import (
    connection_id as connection_id_validator
)
from netsplice.backend.unprivileged.model.validator.connection_type import (
    connection_type as connection_type_validator
)
from netsplice.model.validator.boolean import boolean as boolean_validator


class connection_list_item(marshalable):
    '''
    Backend unprivileged connection list item.

    Used to store information about the connection in the unprivileged backend.
    '''

    def __init__(self):
        '''
        Initialize Module.

        Define fields that are marshalable.
        '''
        marshalable.__init__(self)

        self.id = field(
            required=True,
            validators=[connection_id_validator()])

        self.active = field(
            default=False,
            required=True,
            validators=[boolean_validator()])

        self.origin = field(
            required=True,
            validators=[])

        self.type = field(
            required=True,
            validators=[connection_type_validator()])

    def update_data(self, connection_model_instance):
        '''
        Update the item from the given connection_model_instance.

        Update the data of the connection list item with the data from an
        unprivileged request.
        '''
        changed = False
        if self.active.get() != connection_model_instance.active.get():
            self.active.set(connection_model_instance.active.get())
            changed = True
        return changed
