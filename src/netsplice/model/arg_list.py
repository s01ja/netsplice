# -*- coding: utf-8 -*-
# arg_list.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Model for argument lists
'''

from netsplice.util.model.field import field
from netsplice.util.model.marshalable_list import marshalable_list
from netsplice.model.arg_item import arg_item as arg_item_model


class arg_list(marshalable_list):

    def __init__(self, arg_list_items=[]):
        marshalable_list.__init__(self, arg_item_model)
        self.from_string_list(arg_list_items)

    def from_string_list(self, arg_list_items):
        '''
        From List.

        Create the arg_list from a list of strings.
        '''
        for arg in arg_list_items:
            arg_item = self.item_model_class()
            arg_item.arg.set(arg)
            self.append(arg_item)

    def to_list(self):
        '''
        To List.

        Create a list of strings from the args in the arg_list.
        '''
        arg_list_items = []
        for arg in self:
            arg_list_items.append(arg.arg.get())
        return arg_list_items
