# -*- coding: utf-8 -*-
# version.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

import re
from netsplice.util.model.validator import validator
from netsplice.util import basestring


class version(validator):
    '''
    Version Validator. Validates that a value is a version string.
    '''

    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Ensure the given value is a string that contains
        numbers and dots and possibly a v. Versions also are not
        negative values:
        e.g. "v2.4.0-vanilla"
        '''
        if not isinstance(value, (basestring, int)):
            return False
        if isinstance(value, (int,)):
            if value < 0:
                return False
            return True

        rx_valid_no_suffix = re.compile('^(v[0-9\.]+)$')
        rx_valid_suffix = re.compile('^(v[0-9\.]+-[a-z0-9\-_]+)$')
        rx_valid_no_prefix = re.compile('^([0-9\.]+)$')
        rx_valid_no_prefix_git = re.compile(
            '^([0-9\.]+\+[0-9]+\.g[a-f0-9]+)$')
        rx_valid_no_prefix_git_dirty = re.compile(
            '^([0-9\.]+\+[0-9]+\.g[a-f0-9]+\.dirty)$')

        if re.match(rx_valid_no_suffix, value):
            return True
        if re.match(rx_valid_no_prefix, value):
            return True
        if re.match(rx_valid_no_prefix_git, value):
            return True
        if re.match(rx_valid_no_prefix_git_dirty, value):
            return True
        if re.match(rx_valid_suffix, value):
            return True
        return False
