# -*- coding: utf-8 -*-
# byte.py
# Copyright (C) 2016 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from netsplice.util.model.validator import validator
from netsplice.util import long


class byte(validator):
    '''
    Byte validator ensures that the given value is a positive number.
    '''

    def __init__(self):
        validator.__init__(self)

    def is_valid(self, value):
        '''
        Ensure the given value is a numeric value that is positive or None.
        '''
        if value is None:
            return True
        if not isinstance(value, (int, long, float)):
            return False
        if value < 0:
            return False
        return True
