# -*- coding: utf-8 -*-
# download.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
Download process control. Uses the hkpk download module to start and control
a download.
'''

from tornado import gen
from netsplice.model.event import event as event_model
from netsplice.backend.event import names as event_names
from netsplice.backend.event import types as event_types
from netsplice.util import get_logger
from netsplice.util.hkpk.download_ioloop import download_ioloop
from netsplice.util.hkpk.download_ioloop_memory import download_ioloop_memory
from netsplice.util.hkpk.errors import (
    InvalidError, MaxBufferError, NetworkError, SignatureError, TargetError
)

logger = get_logger()


class download(object):

    def __init__(self, model, app):
        self.model = model
        self.app = app
        self.process = None

    @gen.coroutine
    def cancel(self):
        '''
        Cancel.

        Cancel a active download by setting its model to inactive and
        triggering the cancel method in the process that will cleanup
        any allocated resources.
        '''
        self.model.active.set(False)
        self.app.get_module('download').model.commit()
        if self.process is not None:
            yield self.process.cancel()
        yield gen.moment

    @gen.coroutine
    def start(self):
        '''
        Start Download.

        Start the download process and handle when a error occurs. The download
        will be stored in the file that the model.destination specifies.

        Decorators:
            gen.coroutine
        '''
        self.process = download_ioloop(
            self.model.url.get(),
            self.model.destination.get(),
            pinset=self.model.fingerprints.get_list(),
            signature=self.model.signature.get(),
            sign_key=self.model.sign_key.get(),
            sha1sum=self.model.sha1sum.get(),
            sha256sum=self.model.sha256sum.get())
        self.process.registered_notify.append(self.notify)
        self.process.registered_error.append(self.notify_error)
        self.process.registered_signature_ok.append(self.notify_signature_ok)
        self.process.registered_checksum_ok.append(self.notify_checksum_ok)
        self.model._process = self
        self.model.active.set(True)
        yield self.process.start()
        self.model.active.set(False)

    @gen.coroutine
    def receive(self):
        '''
        Start Download.

        Start the download process and handle when a error occurs.
        The response from the remote service will be returned as string.

        Decorators:
            gen.coroutine
        '''
        self.process = download_ioloop_memory(
            url=self.model.url.get(),
            destination=None,
            pinset=self.model.fingerprints.get_list(),
            signature=self.model.signature.get(),
            sign_key=self.model.sign_key.get(),
            sha1sum=self.model.sha1sum.get(),
            sha256sum=self.model.sha256sum.get())
        self.process.registered_notify.append(self.notify)
        self.process.registered_error.append(self.notify_error)
        self.process.registered_signature_ok.append(self.notify_signature_ok)
        self.process.registered_checksum_ok.append(self.notify_checksum_ok)
        self.model._process = self
        try:
            self.model.active.set(True)
            result = yield self.process.start()
            self.model.active.set(False)
            raise gen.Return(result)
        except InvalidError:
            pass
        except NetworkError:
            pass
        except SignatureError:
            pass
        except IOError:
            pass

    @gen.coroutine
    def notify(self, complete, bps):
        '''
        Notify Overload.

        Notify the ioloop about the progress.

        Arguments:
            complete (float): Percentage of completeness (0-100)
            bps (int): Bytes per second
        '''
        self.model.bps.set(int(bps))
        self.model.complete.set(int(complete))
        self.app.get_module('download').model.commit()
        yield gen.moment

    @gen.coroutine
    def notify_error(self, message):
        '''
        Notify Overload.

        Notify the ioloop about an error.

        Arguments:
            message (string): Reason for the error.
        '''
        self.model.errors.set(True)
        self.model.error_message.set(message)
        self.model.active.set(False)
        self.app.get_module('download').model.commit()
        yield gen.moment

    @gen.coroutine
    def notify_signature_ok(self):
        '''
        Notify Overload.

        Notify the ioloop about the completed signature check.
        '''
        self.model.signature_ok.set(True)
        self.app.get_module('download').model.commit()
        yield gen.moment

    @gen.coroutine
    def notify_checksum_ok(self):
        '''
        Notify Overload.

        Notify the ioloop about the completed checksum check.
        '''
        self.model.checksum_ok.set(True)
        self.app.get_module('download').model.commit()
        yield gen.moment
