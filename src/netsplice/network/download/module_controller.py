# -*- coding: utf-8 -*-
# module_controller.py
# Copyright (C) 2018 Netsplice
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
'''

from tornado import ioloop
from netsplice.backend.net.model.request.download import (
    download as download_request_model
)
from netsplice.backend.net.model.response.download_id import (
    download_id as download_id_response_model
)
from netsplice.backend.net.model.response.download_status import (
    download_status as download_status_response_model
)
from netsplice.util.ipc.middleware import middleware
from netsplice.util.model.errors import ValidationError
from netsplice.util.errors import NotFoundError
from netsplice.util import get_logger

logger = get_logger()


class module_controller(middleware):
    '''
    '''
    def __init__(self, request, response):
        middleware.__init__(self, request, response)

    def delete(self, download_id):
        '''
        Cancel active download.

        Arguments:
            id (string): id of active download.
        '''
        try:
            module = self.application.get_module('download')
            module_model = module.model
            download_instance = module_model.downloads.find_by_id(
                download_id)
            download_process = download_instance._process
            ioloop.IOLoop.current().call_later(
                0, lambda: download_process.cancel())
            self.set_status(202)
        except NotFoundError as errors:
            self.set_error_code(2295, errors)
            self.set_status(404)

        self.finish()

    def get(self, download_id):
        '''
        Get the status of a active download.

        Arguments:
            id (string): id of active download.
        '''
        try:
            response_model = download_status_response_model()
            module = self.application.get_module('download')
            module_model = module.model
            download_instance = module_model.downloads.find_by_id(
                download_id)
            response_model.url.set(
                download_instance.url.get())
            response_model.start_date.set(
                download_instance.start_date.get())
            response_model.complete.set(
                download_instance.complete.get())
            response_model.bps.set(
                download_instance.bps.get())
            response_model.errors.set(
                download_instance.errors.get())
            response_model.error_message.set(
                download_instance.error_message.get())
            response_model.signature_ok.set(
                download_instance.signature_ok.get())
            response_model.checksum_ok.set(
                download_instance.signature_ok.get())
            response_model.active.set(
                download_instance.active.get())

            self.write(response_model.to_json())
            self.set_status(200)
        except NotFoundError as errors:
            self.set_error_code(2291, errors)
            self.set_status(404)

        self.finish()

    def post(self):
        '''
        Start the download of a remote resource.
        '''

        request_model = download_request_model()
        response_model = download_id_response_model()
        try:
            request_model.from_json(self.request.body.decode('utf-8'))
            module = self.application.get_module('download')
            module_model = module.model
            download_model = module_model.downloads.create(request_model)
            download_process = module.download(
                download_model, self.application)
            ioloop.IOLoop.current().call_later(
                0, lambda: download_process.start())
            response_model.id.set(download_model.id.get())
            self.write(response_model.to_json())
            self.set_status(200)
        except ValidationError as errors:
            self.set_error_code(2292, errors)
            self.set_status(400)
        except ValueError as errors:
            self.set_error_code(2293, errors)
            self.set_status(400)
        except NotFoundError as errors:
            self.set_error_code(2294, errors)
            self.set_status(404)
        self.finish()
