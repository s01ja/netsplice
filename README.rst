Netsplice
=========

*VPN configuration made usable*


**Netsplice** is a multiplatform desktop client for VPN services.

It is written in python using `PySide`_ and licensed under the GPL3.

.. _`PySide`: http://qt-project.org/wiki/PySide


Read the Docs!
------------------

The latest documentation is available at ?.


License
=======

Netsplice is released under the terms of the `GNU GPL version 3`_ or later.

.. _`GNU GPL version 3`: http://www.gnu.org/licenses/gpl.txt


Contributing
============

We welcome contributions! see  the `Contribution guidelines <CONTRIBUTING.rst>`_

