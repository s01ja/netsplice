
Error Codes with Context
------------------------

This is a list of error codes with the file and the line before the set_error_code
is set in the code:


2266
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/process_launcher/backend/privileged_dispatcher.py` Line 86


2265
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/process_launcher/backend/privileged_dispatcher.py` Line 82


2264
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/process_launcher/backend/privileged_dispatcher.py` Line 78


2263
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/process_launcher/backend/privileged_dispatcher.py` Line 74


2262
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/process_launcher/backend/unprivileged_dispatcher.py` Line 105


2261
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/process_launcher/backend/unprivileged_dispatcher.py` Line 101


2260
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/process_launcher/backend/unprivileged_dispatcher.py` Line 97


2259
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/process_launcher/backend/unprivileged_dispatcher.py` Line 93


2236
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/process_sniper/backend/unprivileged_dispatcher.py` Line 114


2235
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/process_sniper/backend/unprivileged_dispatcher.py` Line 111


2234
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/process_sniper/backend/unprivileged_dispatcher.py` Line 108


2233
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/process_sniper/backend/unprivileged_dispatcher.py` Line 105


2232
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/process_sniper/backend/unprivileged_dispatcher.py` Line 83


2231
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/process_sniper/backend/unprivileged_dispatcher.py` Line 79


2230
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/process_sniper/backend/unprivileged_dispatcher.py` Line 75


2229
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/process_sniper/backend/unprivileged_dispatcher.py` Line 71


2052
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 272


2051
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 269


2050
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 266


2049
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 263


2062
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 237


2061
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 234


2060
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 231


2059
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 228


2066
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 207


2065
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 204


2064
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 201


2063
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 198


2078
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 178


2077
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 175


2076
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 172


2075
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 169


2074
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 149


2073
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 146


2072
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 143


2071
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 140


2082
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 117


2081
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 114


2080
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 111


2079
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 108


2086
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 85


2085
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 82


2084
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 79


2083
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/privileged_dispatcher.py` Line 76



~~~~

    ./src/netsplice/plugins/openvpn/backend/connection/openvpn/test_connection.py-174-


~~~~

    ./src/netsplice/plugins/openvpn/backend/connection/openvpn/test_connection_integration.py-111-

2258
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 130


2257
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 127


2256
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 124


2255
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 121


2254
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 100


2253
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 97


2252
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 94


2251
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 91


2250
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 70


2249
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 67


2248
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 64


2247
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/openvpn/backend/unprivileged_dispatcher.py` Line 61


2193
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 269


2192
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 266


2191
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 263


2190
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 260


2189
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 236


2188
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 233


2187
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 230


2186
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 227


2185
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 206


2184
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 203


2183
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 200


2182
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 197


2181
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 177


2180
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 174


2179
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 171


2178
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 168


2177
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 148


2176
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 145


2175
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 142


2174
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 139


2173
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 116


2172
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 113


2171
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 110


2170
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 107


2169
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 84


2168
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 81


2167
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 78


2166
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/ssh/backend/unprivileged_dispatcher.py` Line 75


2165
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 267


2164
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 264


2163
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 261


2162
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 258


2157
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 236


2156
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 233


2155
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 230


2154
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 227


2153
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 207


2152
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 204


2151
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 201


2150
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 198


2273
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 178


2272
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 175


2271
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 172


2270
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 169


2149
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 148


2148
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 145


2147
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 142


2146
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 139


2145
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 116


2144
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 113


2143
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 110


2142
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 107


2141
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 84


2140
~~~~

* Exception: ValueError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 81


2139
~~~~

* Exception: ValidationError
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 78


2138
~~~~

* Exception: socket_error
* :file:`src/netsplice/plugins/tor/backend/unprivileged_dispatcher.py` Line 75


2104
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/group_connect_action_controller.py` Line 55


2103
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/group_connect_action_controller.py` Line 52


2286
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/chain_connect_action_controller.py` Line 62


2285
~~~~

* Exception: ConnectedError
* :file:`src/netsplice/backend/gui/chain_connect_action_controller.py` Line 59


2284
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/gui/chain_connect_action_controller.py` Line 56


2283
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/chain_connect_action_controller.py` Line 52


2003
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/account_connect_action_controller.py` Line 62


2002
~~~~

* Exception: ConnectedError
* :file:`src/netsplice/backend/gui/account_connect_action_controller.py` Line 59


2021
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/gui/account_connect_action_controller.py` Line 56


2020
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/account_connect_action_controller.py` Line 52


2015
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/account_reconnect_action_controller.py` Line 61


2014
~~~~

* Exception: ConnectedError
* :file:`src/netsplice/backend/gui/account_reconnect_action_controller.py` Line 58


2025
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/gui/account_reconnect_action_controller.py` Line 55


2024
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/account_reconnect_action_controller.py` Line 51


2106
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/group_disconnect_action_controller.py` Line 55


2105
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/group_disconnect_action_controller.py` Line 52


2108
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/gui/log_controller.py` Line 115


2107
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/log_controller.py` Line 111


2013
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/account_disconnect_action_controller.py` Line 62


2012
~~~~

* Exception: ConnectedError
* :file:`src/netsplice/backend/gui/account_disconnect_action_controller.py` Line 59


2023
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/gui/account_disconnect_action_controller.py` Line 56


2022
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/account_disconnect_action_controller.py` Line 52


2111
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/credential_action_controller.py` Line 69


2110
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/gui/credential_action_controller.py` Line 66


2109
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/credential_action_controller.py` Line 63


2115
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/group_reconnect_action_controller.py` Line 55


2114
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/group_reconnect_action_controller.py` Line 52


2097
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/gui/account_reset_action_controller.py` Line 61


2096
~~~~

* Exception: ConnectedError
* :file:`src/netsplice/backend/gui/account_reset_action_controller.py` Line 58


2095
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/gui/account_reset_action_controller.py` Line 55


2094
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/gui/account_reset_action_controller.py` Line 51


2054
~~~~

    ./src/netsplice/backend/privileged_dispatcher.py-180        yield self.force_shutdown()

2053
~~~~

* Exception: socket_error
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 175


2058
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 164


2057
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 161


2056
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 158


2055
~~~~

* Exception: socket_error
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 155


2070
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 134


2069
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 131


2068
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 128


2067
~~~~

* Exception: socket_error
* :file:`src/netsplice/backend/privileged_dispatcher.py` Line 125


2246
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/net/log_controller.py` Line 70


2245
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/net/log_controller.py` Line 66


2244
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/net/event_controller.py` Line 56


2243
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/net/event_controller.py` Line 52


2267
~~~~

* Exception: NotRegisteredEventError
* :file:`src/netsplice/backend/net/event_controller.py` Line 48


2113
~~~~

* Exception: httpclient.HTTPError
* :file:`src/netsplice/backend/unprivileged_dispatcher.py` Line 89


2112
~~~~

* Exception: socket_error
* :file:`src/netsplice/backend/unprivileged_dispatcher.py` Line 86


2031
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/privileged/log_controller.py` Line 84


2029
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/privileged/log_controller.py` Line 80


2030
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/privileged/event_controller.py` Line 56


2028
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/privileged/event_controller.py` Line 52


2268
~~~~

* Exception: NotRegisteredEventError
* :file:`src/netsplice/backend/privileged/event_controller.py` Line 48


2217
~~~~

* Exception: AttributeError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 165


2211
~~~~

* Exception: KeyError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 161


2210
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 157


2209
~~~~

* Exception: InvalidUsageError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 153


2208
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 149


2207
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 145


2214
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 113


2213
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 109


2212
~~~~

* Exception: KeyError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 105


2204
~~~~

* Exception: InvalidUsageError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 101


2203
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/complex_controller.py` Line 97


2200
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/grouped_account_controller.py` Line 131


2199
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/grouped_account_controller.py` Line 127


2198
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/grouped_account_controller.py` Line 123


2228
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/grouped_account_controller.py` Line 84


2227
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/grouped_account_controller.py` Line 80


2226
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/grouped_account_controller.py` Line 76


2118
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/plugin_controller.py` Line 127


2117
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/plugin_controller.py` Line 123


2116
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/plugin_controller.py` Line 119


2242
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/plugin_controller.py` Line 85


2241
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/plugin_controller.py` Line 81


2119
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/plugin_controller.py` Line 77


2224
~~~~

* Exception: InvalidUsageError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 198


2035
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 194


2034
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 190


2048
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 186


2223
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 158


2033
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 154


2032
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 150


2222
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 121


2221
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 117


2220
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 113


2225
~~~~

* Exception: InvalidUsageError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 87


2039
~~~~

* Exception: NotEmptyError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 83


2038
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 79


2037
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 75


2036
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/group_controller.py` Line 71


2279
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 166


2278
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 162


2277
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 158


2276
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 125


2275
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 121


2274
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 117


2282
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 89


2281
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 85


2280
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/chain_controller.py` Line 81


2089
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/plugin_collection_controller.py` Line 181


2088
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/plugin_collection_controller.py` Line 177


2087
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/plugin_collection_controller.py` Line 174


2240
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/plugin_collection_controller.py` Line 126


2239
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/plugin_collection_controller.py` Line 122


2238
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/plugin_collection_controller.py` Line 118


2237
~~~~

* Exception: KeyError
* :file:`src/netsplice/backend/preferences/plugin_collection_controller.py` Line 114


2137
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 170


2136
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 166


2135
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 162


2134
~~~~

* Exception: NotUniqueError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 135


2133
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 131


2132
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 127


2131
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 123


2130
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 97


2129
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 67


2128
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 63


2127
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/override_controller.py` Line 59


2201
~~~~

* Exception: ConnectedError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 180


2091
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 176


2090
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 172


2005
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 147


2004
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 143


2011
~~~~

* Exception: ConnectedError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 85


2010
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 81


2009
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 77


2008
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/account_controller.py` Line 73


2219
~~~~

* Exception: KeyError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 139


2126
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 135


2206
~~~~

* Exception: InvalidUsageError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 131


2125
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 127


2124
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 123


2216
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 98


2215
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 94


2202
~~~~

* Exception: InvalidUsageError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 90


2218
~~~~

* Exception: KeyError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 86


2026
~~~~

* Exception: NotFoundError
* :file:`src/netsplice/backend/preferences/value_controller.py` Line 82


2195
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/unprivileged/log_controller.py` Line 83


2194
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/unprivileged/log_controller.py` Line 79


2197
~~~~

* Exception: ValueError
* :file:`src/netsplice/backend/unprivileged/event_controller.py` Line 56


2196
~~~~

* Exception: ValidationError
* :file:`src/netsplice/backend/unprivileged/event_controller.py` Line 52


2269
~~~~

* Exception: NotRegisteredEventError
* :file:`src/netsplice/backend/unprivileged/event_controller.py` Line 48


Sorted Error Codes
------------------

This is a list of error codes used in the application::

    2002 backend/gui/account_connect_action_controller.py
    2003 backend/gui/account_connect_action_controller.py
    2004 backend/preferences/account_controller.py
    2005 backend/preferences/account_controller.py
    2008 backend/preferences/account_controller.py
    2009 backend/preferences/account_controller.py
    2010 backend/preferences/account_controller.py
    2011 backend/preferences/account_controller.py
    2012 backend/gui/account_disconnect_action_controller.py
    2013 backend/gui/account_disconnect_action_controller.py
    2014 backend/gui/account_reconnect_action_controller.py
    2015 backend/gui/account_reconnect_action_controller.py
    2020 backend/gui/account_connect_action_controller.py
    2021 backend/gui/account_connect_action_controller.py
    2022 backend/gui/account_disconnect_action_controller.py
    2023 backend/gui/account_disconnect_action_controller.py
    2024 backend/gui/account_reconnect_action_controller.py
    2025 backend/gui/account_reconnect_action_controller.py
    2026 backend/preferences/value_controller.py
    2028 backend/privileged/event_controller.py
    2029 backend/privileged/log_controller.py
    2030 backend/privileged/event_controller.py
    2031 backend/privileged/log_controller.py
    2032 backend/preferences/group_controller.py
    2033 backend/preferences/group_controller.py
    2034 backend/preferences/group_controller.py
    2035 backend/preferences/group_controller.py
    2036 backend/preferences/group_controller.py
    2037 backend/preferences/group_controller.py
    2038 backend/preferences/group_controller.py
    2039 backend/preferences/group_controller.py
    2048 backend/preferences/group_controller.py
    2049 openvpn/backend/privileged_dispatcher.py
    2050 openvpn/backend/privileged_dispatcher.py
    2051 openvpn/backend/privileged_dispatcher.py
    2052 openvpn/backend/privileged_dispatcher.py
    2053 netsplice/backend/privileged_dispatcher.py
    2054 netsplice/backend/privileged_dispatcher.py
    2055 netsplice/backend/privileged_dispatcher.py
    2056 netsplice/backend/privileged_dispatcher.py
    2057 netsplice/backend/privileged_dispatcher.py
    2058 netsplice/backend/privileged_dispatcher.py
    2059 openvpn/backend/privileged_dispatcher.py
    2060 openvpn/backend/privileged_dispatcher.py
    2061 openvpn/backend/privileged_dispatcher.py
    2062 openvpn/backend/privileged_dispatcher.py
    2063 openvpn/backend/privileged_dispatcher.py
    2064 openvpn/backend/privileged_dispatcher.py
    2065 openvpn/backend/privileged_dispatcher.py
    2066 openvpn/backend/privileged_dispatcher.py
    2067 netsplice/backend/privileged_dispatcher.py
    2068 netsplice/backend/privileged_dispatcher.py
    2069 netsplice/backend/privileged_dispatcher.py
    2070 netsplice/backend/privileged_dispatcher.py
    2071 openvpn/backend/privileged_dispatcher.py
    2072 openvpn/backend/privileged_dispatcher.py
    2073 openvpn/backend/privileged_dispatcher.py
    2074 openvpn/backend/privileged_dispatcher.py
    2075 openvpn/backend/privileged_dispatcher.py
    2076 openvpn/backend/privileged_dispatcher.py
    2077 openvpn/backend/privileged_dispatcher.py
    2078 openvpn/backend/privileged_dispatcher.py
    2079 openvpn/backend/privileged_dispatcher.py
    2080 openvpn/backend/privileged_dispatcher.py
    2081 openvpn/backend/privileged_dispatcher.py
    2082 openvpn/backend/privileged_dispatcher.py
    2083 openvpn/backend/privileged_dispatcher.py
    2084 openvpn/backend/privileged_dispatcher.py
    2085 openvpn/backend/privileged_dispatcher.py
    2086 openvpn/backend/privileged_dispatcher.py
    2087 backend/preferences/plugin_collection_controller.py
    2088 backend/preferences/plugin_collection_controller.py
    2089 backend/preferences/plugin_collection_controller.py
    2090 backend/preferences/account_controller.py
    2091 backend/preferences/account_controller.py
    2094 backend/gui/account_reset_action_controller.py
    2095 backend/gui/account_reset_action_controller.py
    2096 backend/gui/account_reset_action_controller.py
    2097 backend/gui/account_reset_action_controller.py
    2103 backend/gui/group_connect_action_controller.py
    2104 backend/gui/group_connect_action_controller.py
    2105 backend/gui/group_disconnect_action_controller.py
    2106 backend/gui/group_disconnect_action_controller.py
    2107 backend/gui/log_controller.py
    2108 backend/gui/log_controller.py
    2109 backend/gui/credential_action_controller.py
    2110 backend/gui/credential_action_controller.py
    2111 backend/gui/credential_action_controller.py
    2112 netsplice/backend/unprivileged_dispatcher.py
    2113 netsplice/backend/unprivileged_dispatcher.py
    2114 backend/gui/group_reconnect_action_controller.py
    2115 backend/gui/group_reconnect_action_controller.py
    2116 backend/preferences/plugin_controller.py
    2117 backend/preferences/plugin_controller.py
    2118 backend/preferences/plugin_controller.py
    2119 backend/preferences/plugin_controller.py
    2124 backend/preferences/value_controller.py
    2125 backend/preferences/value_controller.py
    2126 backend/preferences/value_controller.py
    2127 backend/preferences/override_controller.py
    2128 backend/preferences/override_controller.py
    2129 backend/preferences/override_controller.py
    2130 backend/preferences/override_controller.py
    2131 backend/preferences/override_controller.py
    2132 backend/preferences/override_controller.py
    2133 backend/preferences/override_controller.py
    2134 backend/preferences/override_controller.py
    2135 backend/preferences/override_controller.py
    2136 backend/preferences/override_controller.py
    2137 backend/preferences/override_controller.py
    2138 tor/backend/unprivileged_dispatcher.py
    2139 tor/backend/unprivileged_dispatcher.py
    2140 tor/backend/unprivileged_dispatcher.py
    2141 tor/backend/unprivileged_dispatcher.py
    2142 tor/backend/unprivileged_dispatcher.py
    2143 tor/backend/unprivileged_dispatcher.py
    2144 tor/backend/unprivileged_dispatcher.py
    2145 tor/backend/unprivileged_dispatcher.py
    2146 tor/backend/unprivileged_dispatcher.py
    2147 tor/backend/unprivileged_dispatcher.py
    2148 tor/backend/unprivileged_dispatcher.py
    2149 tor/backend/unprivileged_dispatcher.py
    2150 tor/backend/unprivileged_dispatcher.py
    2151 tor/backend/unprivileged_dispatcher.py
    2152 tor/backend/unprivileged_dispatcher.py
    2153 tor/backend/unprivileged_dispatcher.py
    2154 tor/backend/unprivileged_dispatcher.py
    2155 tor/backend/unprivileged_dispatcher.py
    2156 tor/backend/unprivileged_dispatcher.py
    2157 tor/backend/unprivileged_dispatcher.py
    2162 tor/backend/unprivileged_dispatcher.py
    2163 tor/backend/unprivileged_dispatcher.py
    2164 tor/backend/unprivileged_dispatcher.py
    2165 tor/backend/unprivileged_dispatcher.py
    2166 ssh/backend/unprivileged_dispatcher.py
    2167 ssh/backend/unprivileged_dispatcher.py
    2168 ssh/backend/unprivileged_dispatcher.py
    2169 ssh/backend/unprivileged_dispatcher.py
    2170 ssh/backend/unprivileged_dispatcher.py
    2171 ssh/backend/unprivileged_dispatcher.py
    2172 ssh/backend/unprivileged_dispatcher.py
    2173 ssh/backend/unprivileged_dispatcher.py
    2174 ssh/backend/unprivileged_dispatcher.py
    2175 ssh/backend/unprivileged_dispatcher.py
    2176 ssh/backend/unprivileged_dispatcher.py
    2177 ssh/backend/unprivileged_dispatcher.py
    2178 ssh/backend/unprivileged_dispatcher.py
    2179 ssh/backend/unprivileged_dispatcher.py
    2180 ssh/backend/unprivileged_dispatcher.py
    2181 ssh/backend/unprivileged_dispatcher.py
    2182 ssh/backend/unprivileged_dispatcher.py
    2183 ssh/backend/unprivileged_dispatcher.py
    2184 ssh/backend/unprivileged_dispatcher.py
    2185 ssh/backend/unprivileged_dispatcher.py
    2186 ssh/backend/unprivileged_dispatcher.py
    2187 ssh/backend/unprivileged_dispatcher.py
    2188 ssh/backend/unprivileged_dispatcher.py
    2189 ssh/backend/unprivileged_dispatcher.py
    2190 ssh/backend/unprivileged_dispatcher.py
    2191 ssh/backend/unprivileged_dispatcher.py
    2192 ssh/backend/unprivileged_dispatcher.py
    2193 ssh/backend/unprivileged_dispatcher.py
    2194 backend/unprivileged/log_controller.py
    2195 backend/unprivileged/log_controller.py
    2196 backend/unprivileged/event_controller.py
    2197 backend/unprivileged/event_controller.py
    2198 backend/preferences/grouped_account_controller.py
    2199 backend/preferences/grouped_account_controller.py
    2200 backend/preferences/grouped_account_controller.py
    2201 backend/preferences/account_controller.py
    2202 backend/preferences/value_controller.py
    2203 backend/preferences/complex_controller.py
    2204 backend/preferences/complex_controller.py
    2206 backend/preferences/value_controller.py
    2207 backend/preferences/complex_controller.py
    2208 backend/preferences/complex_controller.py
    2209 backend/preferences/complex_controller.py
    2210 backend/preferences/complex_controller.py
    2211 backend/preferences/complex_controller.py
    2212 backend/preferences/complex_controller.py
    2213 backend/preferences/complex_controller.py
    2214 backend/preferences/complex_controller.py
    2215 backend/preferences/value_controller.py
    2216 backend/preferences/value_controller.py
    2217 backend/preferences/complex_controller.py
    2218 backend/preferences/value_controller.py
    2219 backend/preferences/value_controller.py
    2220 backend/preferences/group_controller.py
    2221 backend/preferences/group_controller.py
    2222 backend/preferences/group_controller.py
    2223 backend/preferences/group_controller.py
    2224 backend/preferences/group_controller.py
    2225 backend/preferences/group_controller.py
    2226 backend/preferences/grouped_account_controller.py
    2227 backend/preferences/grouped_account_controller.py
    2228 backend/preferences/grouped_account_controller.py
    2229 process_sniper/backend/unprivileged_dispatcher.py
    2230 process_sniper/backend/unprivileged_dispatcher.py
    2231 process_sniper/backend/unprivileged_dispatcher.py
    2232 process_sniper/backend/unprivileged_dispatcher.py
    2233 process_sniper/backend/unprivileged_dispatcher.py
    2234 process_sniper/backend/unprivileged_dispatcher.py
    2235 process_sniper/backend/unprivileged_dispatcher.py
    2236 process_sniper/backend/unprivileged_dispatcher.py
    2237 backend/preferences/plugin_collection_controller.py
    2238 backend/preferences/plugin_collection_controller.py
    2239 backend/preferences/plugin_collection_controller.py
    2240 backend/preferences/plugin_collection_controller.py
    2241 backend/preferences/plugin_controller.py
    2242 backend/preferences/plugin_controller.py
    2243 backend/net/event_controller.py
    2244 backend/net/event_controller.py
    2245 backend/net/log_controller.py
    2246 backend/net/log_controller.py
    2247 openvpn/backend/unprivileged_dispatcher.py
    2248 openvpn/backend/unprivileged_dispatcher.py
    2249 openvpn/backend/unprivileged_dispatcher.py
    2250 openvpn/backend/unprivileged_dispatcher.py
    2251 openvpn/backend/unprivileged_dispatcher.py
    2252 openvpn/backend/unprivileged_dispatcher.py
    2253 openvpn/backend/unprivileged_dispatcher.py
    2254 openvpn/backend/unprivileged_dispatcher.py
    2255 openvpn/backend/unprivileged_dispatcher.py
    2256 openvpn/backend/unprivileged_dispatcher.py
    2257 openvpn/backend/unprivileged_dispatcher.py
    2258 openvpn/backend/unprivileged_dispatcher.py
    2259 process_launcher/backend/unprivileged_dispatcher.py
    2260 process_launcher/backend/unprivileged_dispatcher.py
    2261 process_launcher/backend/unprivileged_dispatcher.py
    2262 process_launcher/backend/unprivileged_dispatcher.py
    2263 process_launcher/backend/privileged_dispatcher.py
    2264 process_launcher/backend/privileged_dispatcher.py
    2265 process_launcher/backend/privileged_dispatcher.py
    2266 process_launcher/backend/privileged_dispatcher.py
    2267 backend/net/event_controller.py
    2268 backend/privileged/event_controller.py
    2269 backend/unprivileged/event_controller.py
    2270 tor/backend/unprivileged_dispatcher.py
    2271 tor/backend/unprivileged_dispatcher.py
    2272 tor/backend/unprivileged_dispatcher.py
    2273 tor/backend/unprivileged_dispatcher.py
    2274 backend/preferences/chain_controller.py
    2275 backend/preferences/chain_controller.py
    2276 backend/preferences/chain_controller.py
    2277 backend/preferences/chain_controller.py
    2278 backend/preferences/chain_controller.py
    2279 backend/preferences/chain_controller.py
    2280 backend/preferences/chain_controller.py
    2281 backend/preferences/chain_controller.py
    2282 backend/preferences/chain_controller.py
    2283 backend/gui/chain_connect_action_controller.py
    2284 backend/gui/chain_connect_action_controller.py
    2285 backend/gui/chain_connect_action_controller.py
    2286 backend/gui/chain_connect_action_controller.py
     connection/openvpn/test_connection_integration.py
     connection/openvpn/test_connection.py

Non-Unique Error Codes
----------------------

This is a list of error codes over-used in the application::

    
    end

This file is generated using the 'make codeerrors > doc/errorcodes.rst'
make-target and should be updated before releases.
