.. _developer-api-privileged:

==============
API-Privileged
==============

This document describes the components of the privileged application.

Every privileged request url has the following elements:

========= ====================================================================
Path      Description
========= ====================================================================
module    The module that is responsible for handling the request. Modules are
          "openvpn", "systeminfo".
action    The action that is called in the module. Those are module dependent.
          "list-connections", "connect",
          "reconnect", "disconnect", "setup", "delete"
========= ====================================================================

Every privileged request url may have the following elements:

================== =================================================
Parameter          Description
================== =================================================
{connection-id}    unique-id for a connection that can be controlled
{process-id}       unique-id for a process that can be controlled
================== =================================================

Events
------

All events that are captured from the subprocesses are signaled to the backend /
connected process using a REST request from the privileged process to the
backend.

**Dispatcher Event Structure**::

    POST /module/priv/event
    {
        "id" : "uuid-for-event",
        "index": 0,
        "date": 0,
        "name": "name_of_the_event",
        "type": "event-type",
        "data": {
            "optional": "arguments"
        }
    }

============ ===============================================================
Attribute    Description
============ ===============================================================
**index**    Always-increasing number to ensure correct order in logs and
             prevent out-of-order event execution.
**date**     Milliseconds since 1970-01-01T00:00:00Z
**args**     The events that can be dispatched may need to send more complex
             structured data to the backend. Therefore the structure is not
             defined here.
============ ===============================================================

Log Messages
------------

All log-messages that are captured from the privileged processes are signaled to
the backend / connected process using a REST request from the privileged process
to the backend.

**Log Message Structure**::

    POST /module/priv/log
    {
        "id" : "uuid-for-event",
        "index": 0,
        "date": 0,
        "level": "debug | info | warning | error | critical",
        "module": "openvpn",
        "context": "/file/under/exection.py:L123"
        "message": "message that needs to be logged"
    }


============ ============================================================
Attribute    Description
============ ============================================================
**index**    Always-increasing number to ensure correct order in logs and
             prevent out-of-order event execution.
**date**     Milliseconds since 1970-01-01T00:00:00Z
**level**    Loglevel that can be filtered in the User Interface
**context**  Location where the log message was triggered
**module**   The origin of the log message
============ ============================================================

Modules
-------

The privileged API exposes the modules that are registered. The modules do not interfere nor interconnect.

XXX should be
/module/{module_name}/collectionplural/{collection_uuid}

OpenVPN Module
++++++++++++++

The following actions are supported by the OpenVPN Module.


List Connections
~~~~~~~~~~~~~~~~

The "list-connections" action lists information about currently setup
connections. For each connection, the response includes the connection id and
whether or not it is currently connected.

**REST Request**::

    GET /module/openvpn/connections

**REST Response**::

    [
        {
           "id": "connection-id-1",
           "connected": false
        },
        {
           "id": "connection-id-2",
           "connected": true
        }
    ]

Connect
~~~~~~~

The "connect" action requests the privileged application to start a previously
setup connection.


**REST Request**::

    POST /module/openvpn/connections/<connection-id>/connect


**REST Response**::

    {
       "id": "connection-id"
    }

**REST Errors**

The connect action may return the following errors:

====== ==============================
Code   Description
====== ==============================
404    Connection not found
403    Already connected / connecting
500    Generic failure to connect
502    OpenVPN communication failure
====== ==============================

Delete
~~~~~~

The "delete" action requests the privileged application to remove a previously
setup connection. Delete only works for disconnected connections.

**REST Request**::

    DELETE /module/openvpn/connections/<connection_id>


**REST Response**::

    {
        "id": "connection-uuid"
    }

**REST Errors**

The delete action may return the following errors:

====== ==============================
Code   Description
====== ==============================
404    Connection not found
403    Still Connected cannot delete
500    Generic failure to delete
====== ==============================

Disconnect
~~~~~~~~~~

The "disconnect" action requests the privileged application to disconnect a
previously connected connection. Disconnect only works for connected
connections.

**REST Request**::

    POST /module/openvpn/connections/<connection_id>/disconnect

**REST Response**::

    {
        "id": "connection-id"
    }

**REST Errors**

The Disconnect action may return the following errors:

====== ==============================
Code   Description
====== ==============================
404    Connection not found
403    Not connected
500    Generic failure to disconnect
502    OpenVPN communication failure
====== ==============================


Reconnect
~~~~~~~~~

The "reconnect" action requests the privileged application to reconnect a
previously connected connection. Reconnect only works for connected connections.

**REST Request**::

    POST /module/openvpn/connections/<connection_id>/reconnect

**REST Response**::

    {
        "id": "connection-id"
    }

**REST Errors**

The Reconnect action may return the following errors:

====== ==============================
Code   Description
====== ==============================
404    Connection not found
403    Not connected
500    Generic failure to reconnect
502    OpenVPN communication failure
====== ==============================


Setup
~~~~~

The "setup" action requests the privileged application to setup a connection
with the required parameters. The privileged application stores the details for
the connection in memory and allows future requests to connect or remove the
configuration by name.

**REST Request**::

    POST /module/openvpn/connections
    {
        "config": "jsonencoded openvpn configuration"
    }

**REST Response**::

    {
       "id": "new-connection-id-to-be-used-in-future-requests"
    }

**REST Errors**

The Setup action may return the following errors:

====== ==============================
Code   Description
====== ==============================
400    Config is invalid
507    Insufficient storage to setup
====== ==============================

Debug
~~~~~

To create a connection using "curl" first disable HTTPS and CHECK_SIGNATURE in
netsplice.config.ipc, then start the privileged App (as root) and invoke the
endpoints. The following code assumes that you have multiple terminals open,
root access and bash as shell:

Setup Server:::

    sed -i '|HTTPS = True|HTTPS = False' src/netsplice/config/ipc.py
    sed -i '|CHECK_SIGNATURE = True|CHECK_SIGNATURE = False' src/netsplice/config/ipc.py
    python src/netsplice/NetsplicePrivilegedApp.py --host localhost --port 10000
    sed -i '|HTTPS = False|HTTPS = True' src/netsplice/config/ipc.py
    sed -i '|CHECK_SIGNATURE = True|CHECK_SIGNATURE = True' src/netsplice/config/ipc.py

Setup a Connection:::

    CONFIG={\"username\":\"\",\"password\":\"\",\"config\":\"$(echo $(cat /some/vpn/config/file|grep -v '^#' | sed 's|"|\\"|g' | sed 's|^|SSS|;s|$|EEE|g') | sed 's|EEE SSS|\\n|g;s|EEE$||;s|^SSS||')\"}

    curl localhost:10000/module/openvpn/connections -X PUT --data "${CONFIG}" -v

Connect the setup-connection:::

    curl localhost:10000/module/openvpn/connections/1/connect -X POST -v

Disconnect the connected-connection:::

    curl localhost:10000/module/openvpn/connections/1/disconnect -X POST -v

Reconnect the connected-connection:::

    curl localhost:10000/module/openvpn/connections/1/reconnect -X POST -v

Delete the disconnected-connection:::

    curl localhost:10000/module/openvpn/connections/1 -X DELETE -v


Systeminfo Module
+++++++++++++++++

The following actions are supported by the Systeminfo Module.

Routes
~~~~~~

The "routes" action requests the privileged application to extract information
about the routes for reports.

**REST Request**::

    GET /module/systeminfo/routes
    {
    }

**REST Response**::

    [
        {
            "destination": "0.0.0.0",
            "gateway": "0.0.0.0",
            "genmask": "0.0.0.0",
            "flags": "UHG",
            "metric": 123,
            "ref": 0,
            "use": 0,
            "interface": "tun0"
        }
    ]

Interfaces
~~~~~~~~~~

The "interfaces" action requests the privileged application to extract
information about the interfaces configured on the host for reports.

**REST Request**::

    GET /module/systeminfo/interfaces
    {
    }

**REST Response**::

    [
        {
            "name": "tun0",
            "flags": ["UP", "POINTTOPOINT", "RUNNING", "NOARP", "MULTICAST"],
            "mtu": 1500,
            "inet": "172.16.17.18",
            "inet6": "fe80:ffff:ffff:ffff:ffff",
            "prefixlen": 64,
            "ether": "ff:ff:ff:ff:ff:ff"
            "netmask": "255.255.255.255",
            "destination": "172.16.17.1",
            "rx-packets": 1,
            "rx-bytes": 1500,
            "rx-errors": 0,
            "rx-dropped": 0,
            "rx-overruns": 0,
            "tx-packets": 1,
            "tx-bytes": 1500,
            "tx-errors": 0,
            "tx-dropped": 0,
            "tx-overruns": 0,
            "raw": "tun0: flags=...\n\tinet 172...\n\tinet6 ......"
        }
    ]
