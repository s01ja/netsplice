.. _`developer-plugin-process-launcher`:

Process Launcher Developer
==========================

The privileged scripts that are provided with the installation are examples for
common tasks. The content of those scripts should be reviewed by the user as the
commands are executed with root privileges. Some of them should be in the python
application, exposed as api to avoid logic in bash scripts. When there are
use-cases, please expose us so we can integrate them.

Return Codes
------------

Netsplice acts on the return-codes. A return code of 0 is a success, a nonzero
return code is an error. The application handles the following return-codes
special:

0: success, do nothing
32: fatal disconnect, disconnect the connection that triggered the script and
show the logwindow.
64: fatal, show the logwindow

In order to display a useful message in the logwindow, the text must be written
to stderr.

Basic
-----

The most basic script will look like this:

.. code-block:: bash
    :caption: test-script.sh
    :name: test-script.sh

    #!/bin/bash

    set > /tmp/test_script.set.txt
    urxvt -c "less /tmp/test_script.set.txt"

The following environment is created by the IPv6 IPredator.se connections:

Connected
~~~~~~~~~

.. code-block:: bash

    openvpn_common_name=eeyaivohngik.openvpn.ipredator.se
    openvpn_config=/tmp/tmprpq5oV
    openvpn_daemon=0
    openvpn_daemon_log_redirect=0
    openvpn_daemon_pid=29031
    openvpn_daemon_start_time=1496919656
    openvpn_dev=tunX
    openvpn_dev_type=tun
    openvpn_foreign_option_1='dhcp-option DOMAIN ipredator.se'
    openvpn_foreign_option_2='dhcp-option DNS 46.246.46.46'
    openvpn_foreign_option_3='dhcp-option DNS 194.132.32.23'
    openvpn_ifconfig_broadcast=46.246.49.255
    openvpn_ifconfig_ipv6_local=2001:67c:1350:105::d
    openvpn_ifconfig_ipv6_netbits=64
    openvpn_ifconfig_ipv6_remote=2001:67c:1350:105::1
    openvpn_ifconfig_local=46.246.49.180
    openvpn_ifconfig_netmask=255.255.255.0
    openvpn_link_mtu=1561
    openvpn_local_port_1=0
    openvpn_local_port_2=0
    openvpn_local_port_3=0
    openvpn_proto_1=udp
    openvpn_proto_2=udp
    openvpn_proto_3=udp
    openvpn_redirect_gateway=1
    openvpn_remote_1=ipv6.openvpn.ipredator.se
    openvpn_remote_2=ipv6.openvpn.ipredator.me
    openvpn_remote_3=ipv6.openvpn.ipredator.es
    openvpn_remote_port_1=1194
    openvpn_remote_port_2=1194
    openvpn_remote_port_3=1194
    openvpn_route_gateway_1=192.168.5.1
    openvpn_route_ipv6_gateway_1=2001:67c:1350:105::1
    openvpn_route_ipv6_network_1=2000::/3
    openvpn_route_net_gateway=192.168.5.1
    openvpn_route_netmask_1=255.255.255.255
    openvpn_route_network_1=46.246.49.130
    openvpn_route_vpn_gateway=46.246.49.1
    openvpn_script_context=init
    openvpn_tls_digest_0=74:e0:a0:36:08:b5:c8:c9:47:09:38:97:4b:b6:f6:4f:43:4b:41:79
    openvpn_tls_digest_1=48:dc:0b:05:18:8d:91:6c:16:54:29:1d:4b:8f:8c:b1:43:d6:3a:99
    openvpn_tls_id_0='C=SE, ST=Bryggland, L=Oeldal, O=Royal Swedish Beer Squadron, CN=eeyaivohngik.openvpn.ipredator.se, emailAddress=hostmaster@ipredator.se'
    openvpn_tls_id_1='C=SE, ST=Bryggland, L=Oeldal, O=Royal Swedish Beer Squadron, OU=Internetz, CN=Royal Swedish Beer Squadron CA, emailAddress=hostmaster@ipredator.se'
    openvpn_tls_serial_0=3837
    openvpn_tls_serial_1=12078339272342960243
    openvpn_tls_serial_hex_0=0e:fd
    openvpn_tls_serial_hex_1=a7:9e:e1:93:0c:a6:f8:73
    openvpn_trusted_ip=46.246.49.130
    openvpn_trusted_port=1194
    openvpn_tun_mtu=1500
    openvpn_untrusted_ip=46.246.49.130
    openvpn_untrusted_port=1194
    openvpn_verb=3

Disconnected
~~~~~~~~~~~~

.. code-block:: bash

    openvpn_common_name=eeyaivohngik.openvpn.ipredator.se
    openvpn_config=/tmp/tmprpq5oV
    openvpn_daemon=0
    openvpn_daemon_log_redirect=0
    openvpn_daemon_pid=29031
    openvpn_daemon_start_time=1496919656
    openvpn_dev=tunX
    openvpn_dev_type=tun
    openvpn_foreign_option_1='dhcp-option DOMAIN ipredator.se'
    openvpn_foreign_option_2='dhcp-option DNS 46.246.46.46'
    openvpn_foreign_option_3='dhcp-option DNS 194.132.32.23'
    openvpn_ifconfig_broadcast=46.246.49.255
    openvpn_ifconfig_ipv6_local=2001:67c:1350:105::d
    openvpn_ifconfig_ipv6_netbits=64
    openvpn_ifconfig_ipv6_remote=2001:67c:1350:105::1
    openvpn_ifconfig_local=46.246.49.180
    openvpn_ifconfig_netmask=255.255.255.0
    openvpn_link_mtu=1561
    openvpn_local_port_1=0
    openvpn_local_port_2=0
    openvpn_local_port_3=0
    openvpn_proto_1=udp
    openvpn_proto_2=udp
    openvpn_proto_3=udp
    openvpn_redirect_gateway=1
    openvpn_remote_1=ipv6.openvpn.ipredator.se
    openvpn_remote_2=ipv6.openvpn.ipredator.me
    openvpn_remote_3=ipv6.openvpn.ipredator.es
    openvpn_remote_port_1=1194
    openvpn_remote_port_2=1194
    openvpn_remote_port_3=1194
    openvpn_route_gateway_1=192.168.5.1
    openvpn_route_ipv6_gateway_1=2001:67c:1350:105::1
    openvpn_route_ipv6_network_1=2000::/3
    openvpn_route_net_gateway=192.168.5.1
    openvpn_route_netmask_1=255.255.255.255
    openvpn_route_network_1=46.246.49.130
    openvpn_route_vpn_gateway=46.246.49.1
    openvpn_script_context=init
    openvpn_tls_digest_0=74:e0:a0:36:08:b5:c8:c9:47:09:38:97:4b:b6:f6:4f:43:4b:41:79
    openvpn_tls_digest_1=48:dc:0b:05:18:8d:91:6c:16:54:29:1d:4b:8f:8c:b1:43:d6:3a:99
    openvpn_tls_id_0='C=SE, ST=Bryggland, L=Oeldal, O=Royal Swedish Beer Squadron, CN=eeyaivohngik.openvpn.ipredator.se, emailAddress=hostmaster@ipredator.se'
    openvpn_tls_id_1='C=SE, ST=Bryggland, L=Oeldal, O=Royal Swedish Beer Squadron, OU=Internetz, CN=Royal Swedish Beer Squadron CA, emailAddress=hostmaster@ipredator.se'
    openvpn_tls_serial_0=3837
    openvpn_tls_serial_1=12078339272342960243
    openvpn_tls_serial_hex_0=0e:fd
    openvpn_tls_serial_hex_1=a7:9e:e1:93:0c:a6:f8:73
    openvpn_trusted_ip=46.246.49.130
    openvpn_trusted_port=1194
    openvpn_tun_mtu=1500
    openvpn_untrusted_ip=46.246.49.130
    openvpn_untrusted_port=1194
    openvpn_verb=3

Advanced
--------

The scripts in the process_launcher libexec directory are too complex for just
four lines. Therefore a ```.subroutines.inc.sh``` is used to have common methods
on solving common problems. They output of errors, return the correct exit codes
and provide basic ip-address handling.
When scripts are provided with a ```.debug``` file, this file is sourced before
the script is executed when the ```DEBUG``` environment variable is nonempty.
This allows to check the script with the expected (or unexpected) environment
variables.
