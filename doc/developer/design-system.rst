.. _design-system:

Design System
=============

The lack of design consistency is one of the biggest problems in product
development. A design system is the right solution to prevent inconsistencies.

The design system isn't a standard carved in stone, nor is it a one-time
deliverable. It's the heart of your product development process, not an artifact
of the process.

This document is not the style guide that describes 'the colors' or the
roundness of the application, this document is about the consistency of the
application.

The process to use this document is relatively simple:

 # Compare pieces of the interface with their representation in code and
   identify logical chunks.
 # List all the pieces of the interface in categories derived from the code
   environment. Add additional suggestions of categories where necessary.
 # Take notes of any inconsistencies between elements. If possible list classes
   and files for future reference.

This document is based on the great description of @marcintreder
`The Minimum Viable Design System`_.

_`The Minimum Viable Design System`: https://medium.com/@marcintreder/the-minimum-viable-design-system-75ca1806848a

Building Blocks
---------------

The smallest, most general and most reusable pieces of the interface such as
colors, typographic scales, grid definitions or icons.

Interface Inventory
~~~~~~~~~~~~~~~~~~~

Main Window
'''''''''''

The Main Window is a list of accounts ordered in a hierarchy. It contains the
main menu, the account list with a toolbar, a multi-purpose Create/Connect
Button and a statusbar.

The Main Window is placed at the right side of the display by default and saves
the location on shutdown.

Account List
""""""""""""

The account list allows to connect, group, order and view the configured
account items and account groups.
During drag&drop indicators are shown where the drop will occur.
The indicator is a transparent (50%) 20px height red (#FF0000) rectangle on the
top or bottom of the account item.

Account Item
""""""""""""

A account item has multiple states:

Disconnected (default): no color background
Connecting: yellow (#FFB353) color background
Connected: green (#008A48) color background
Failed: red (#FF7F53) color background
Disabled: blue (#055E97) foreground color

All items have a active (selected) state and a hovering state as well.

The active, not connected item gets the background color #055E97. Connecting and
Connected use the inactive background color and set the left border to #055E97.
The Failed state uses #FF4100 when active.

Account Group
"""""""""""""

Account groups show the Account items that are in it by adding a margin for all
account items. When the group is active, a 4px solid blue (#044F7F) border
around the group with a rounded (2px) corner is added. The name of the group is
displayed in bold letters when the group is active.

Statusbar
"""""""""

The Statusbar is a mainwindow statusbar aligned to the right. It displays
labels (copyright and version) a optional count of connections and a heartbeat
indicator icon (connected green lines for heartbeat ok, only red dots for
heartbeat failure)

Menu
""""

The (main) menu consists of four entries: Actions, Preferences, Debug and Help.
Actions are the functions the user can select without a selected account.
Preferences opens the Preference Dialog
Debug are actions that help the user to find problems with a connection or
create reports for the provider.
Help are actions that help the user to user the application.
Actions in 'Help' and 'Debug' are redundant.

All Menu entries display the shortcut right-aligned. The operating system rules
may reposition entries.

Dialogs
'''''''

Dialogs are displayed at the side of the main window with the most space (left
when the main window is positioned on the right side of the display, right when
the mainwindow is positioned on the left side of the display.

Dialogs have one or more actions at their bottom right that confirm/close the
dialog contents.

Dialogs follow the `Gnome Human Interface Guidelines`_:

Visual elements are organized from top-to-bottom and left-to-right.
Bottom-right: ok/finish/continue, Bottom-left:cancel/back

The elements in the dialog should have 10px margin to the window border and
avoid more margins when they are nested.

_`Gnome Human Interface Guidelines`: https://developer.gnome.org/hig/stable/

Sub Dialogs
"""""""""""

Dialogs opened from Dialogs are placed relative to the main window and will
appear over the calling dialog. They need the user to confirm or cancel the
input.

Confirm / Alert Dialogs
"""""""""""""""""""""""

Use Confirm (Yes/No) dialogs for destructive actions
Use Alert Dialogs (Ok) for messages to the user for actions that cannot be
represented in another way.


Tabs
''''

Tabs are the base elements for complex dialogs and group the contents in
multiple panes. Tabs are always displayed on top with a descriptive text label.
Tabs are not nested.

The only exception is the About dialog that uses tabs to display the various
plugins and their text and optional sub-tabs after the main about text.

The tab contents have 10px margin to the widget border and avoid more margins
when they are nested.

Pushbutton
''''''''''

Push Buttons have a general width of 120px.

Togglebutton
''''''''''''

Toggle Buttons have a small icon that indicates the toggled state on the right
side, no border and no outline. They have a increased left margin of 30px.

Checkbox
''''''''

Checkboxes are displayed left of their buddy label.

Combobox
''''''''

Comboboxes or Dropdown lists are displayed right of their label. The label is
displayed in bold. The label is aligned right.

Lineedit
''''''''

Lineedits are displayed right of their label. The label is displayed in bold

Key-Values Table
''''''''''''''''

 * Lines are even-odd colored


Listview
''''''''

In dialog listviews are operatingsystem styled. The one exception is the
logviewer listview as it displays multiline elements and colors the entries
depending to the loglevel.

Toolbars
''''''''

Toolbars are displayed over the widget they control.

Lineedit in toolbars
''''''''''''''''''''

Lineedits that let the user input a filter apply the filter while typing. The
filter resets, when the data-background changes or a dedicated Clear Pushbutton
is pressed. Filtering lineedits are above the items that are filtered.

Unconfirmed Storage
~~~~~~~~~~~~~~~~~~~

When the user inputs data, the change is commited without confirmation. This
method is used in preferences and with the account list drag&drop.

Confirmed Storage
~~~~~~~~~~~~~~~~~

When the user inputs data, the user has to confirm (ok, create) or cancel the
data. This method is used in account settings.

XXX we need to decide on one to have consistent experience.

Writing Style
~~~~~~~~~~~~~

Text plays an important role in user interfaces. Take the time to ensure that
any text you use is clearly written and easy to understand.


 * Text should adopt a neutral tone and speak from the point of view of the
   product. Pronouns like "you" or "my" should therefore be avoided wherever
   possible. However, if they are unavoidable "your" is preferable to "my".
 * Sentences should not be constructed from text in several controls, and each
   label should be treated as being self-contained. Sentences that run from one
   control to another will often not make sense when translated into other
   languages.
 * Header capitalization should be used for any headings, including header bar
   headings and page, tab and menu titles. It should also be used for short
   control labels that do not normally form proper sentences, such as button
   labels, switch labels and menu items.
   * Capitalize the first letter of:
   * All words with four or more letters.
   * Verbs of any length, such as "Be", "Are", "Is", "See" and "Add".
   * The first and last word.
   * Hyphenated words; for example: "Self-Test" or "Post-Install".

   For example: "Create a Document", "Find and Replace", "Document Cannot Be
   Found". Messages are in 3rd person and avoid direct user contact. ("After
   entering username" instead of "After you enter the username")


Grid / Border Alignment
~~~~~~~~~~~~~~~~~~~~~~~

 * An alignment point is an imaginary vertical or horizontal line through your
   window that touches the edge of one or more labels or controls in the window.
   Minimize the number of these - the fewer there are, the cleaner and simpler
   your layout will appear, and the easier it will be for people to understand.
 * Align content and controls in your layout exactly. The eye is very sensitive
   to aligned and unaligned objects. If visual elements do not line up, it will
   be hard for someone to scan them. Elements that do not quite line up will be
   distracting.

Organize visual elements from top-to-bottom and left-to-right. This is the
direction that people from western locales tend to read an interface, so that
the items at the top-left will be encountered first. This ordering gives
interfaces a hierarchy: those components that are viewed first are perceived to
have priority over those that come after them. For this reason, you should place
dominant controls above and to the left of the controls and content that they
affect. Header bars are a key design pattern in this respect.

Labels
~~~~~~

* If possible, right-justify labels. This will avoid large gaps between labels
  and their associated controls. This type of right- justification is not
  possible if you have indented controls: here left- justification should be
  used instead.

Icons and Assets
~~~~~~~~~~~~~~~~

Color Palette
~~~~~~~~~~~~~

Typographic Scales
~~~~~~~~~~~~~~~~~~

UI Patterns
-----------

The pieces of the interface directly used to build experiences.

Templates
~~~~~~~~~

Core files (such as variables, icon font definitions etc.) to unify styling.

Modules
~~~~~~~

Full functionalities built out of components (e.g. toolbar, search)

Components
~~~~~~~~~~

Independent and repeatedly used pieces of the interface built out of elements
(e.g. toolbar item, page loader)

Elements
~~~~~~~~

The smallest individual pieces of the interface which act as building blocks
for components (e.g. buttons, form fields...).


Rules
-----

Guidelines for implementing and growing the design system.


Design Principles
~~~~~~~~~~~~~~~~~

Implementation Guidelines
~~~~~~~~~~~~~~~~~~~~~~~~~

Security
''''''''

The application can be paranoid and does not trust the environment it is in.
The separation of concerns in separate backends is just one of many constructs
that ensure that the user is safe.

Certificates/HTTPS
""""""""""""""""""

Local certificate based communication to avoid other users eavedropping or
interfering.

Model
"""""

All data passed between the backends is described in models with validation.

No Logs
"""""""

Avoid traces on the system.

Preferences
"""""""""""

Not encrypted because its 600 and credentials are not stored/stored in secure
os keystore.
Preferences may be core-preferences or preferences of a plugin.


Privileged / Unprivileged / Network / Backend
"""""""""""""""""""""""""""""""""""""""""""""

Implement logic at the right location thus making the models reusable.

Backend
```````

All the procedural works are done by the backend
and it delegates it to subprocesses. Therefore the backend has the complexity
and logic in modules, classes and methods. The other applications only provide
their part.


Gui
```

The GUI is a initial interface for the backend. It dictates on some points what
the backend has to provide, but uses methods that can now be accessed by gtk,
ios, java or ruby. We choose Qt (PySide) and see the advantages and the issues
of that solution. The GUI focuses on advanced users, therefore it has some
advanced controls that need explanation.

Editorial Guidelines
~~~~~~~~~~~~~~~~~~~~


