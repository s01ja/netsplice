.. _architecture:

Architecture
============

This document describes the components of the application, their interaction
and purpose.  Names marked with a * are not final yet.

.. graphviz::

    digraph G {
        subgraph cluster_backend {
            style=filled;
            color=lightgrey;
            "Backend Logic"[shape=box];
            "Backend API" -> "Backend Middleware" -> "Backend Logic";
            "Backend Logic" -> "Application Monitor"[arrowhead=none,arrowtail=none];
            "Backend Logic" -> "Key Storage"[arrowhead=none,arrowtail=none];
            "Backend Logic" -> "Logging"[arrowhead=none,arrowtail=none];
            "Backend Logic" -> "Privileged Model"[arrowhead=none,arrowtail=none];
            "Backend Logic" -> "Network Model"[arrowhead=none,arrowtail=none];
            "Backend Logic" -> "GUI Model"[arrowhead=none,arrowtail=none];
            label = "Backend App";
        }
        subgraph cluster_priv {
            style=filled;
            color=lightgrey;
            "Privileged Logic"[shape=box];
            "Privileged API" -> "Privileged Middleware" -> "Privileged Logic";
            label = "Privileged App";
        }
        subgraph cluster_unpriv {
            style=filled;
            color=lightgrey;
            "Unprivileged Logic"[shape=box];
            "Unprivileged API" -> "Unprivileged Middleware" -> "Unprivileged Logic";
            label = "Unprivileged App";
        }
        subgraph privileged_processes {
            style=filled;
            color=lightgrey;
            label = "Privileged Processes";
            "openvpn1";
            "openvpn2";
        }
        subgraph unprivileged_processes {
            style=filled;
            color=lightgrey;
            label = "Unprivileged Processes";
            "tor";
            "ssh";
        }
        subgraph cluster_net {
            style=filled;
            color=lightgrey;
            "Net Logic"[shape=box];
            "Net API" -> "Net Middleware" -> "Net Logic";
            label = "Net App";
        }

        "UI (Qt-Client)" -> "Backend API"[label="REST"];
        "UI (CLI-Client)" -> "Backend API"[label="REST"];
        "Privileged Logic" -> "Backend API"[label="REST"]
        "Privileged Logic" -> "openvpn1"[label="starts"];
        "Privileged Logic" -> "openvpn2"[label="starts"];
        "Unprivileged Logic" -> "tor"[label="starts"];
        "Unprivileged Logic" -> "ssh"[label="starts"];
        "Net Logic" -> "Backend API"[label="REST"];
        "Net Logic" -> "Remote Services";
        "Backend Logic" -> "Privileged API"[label="REST"];
        "Backend Logic" -> "Unprivileged API"[label="REST"];
        "Backend Logic" -> "Net API"[label="REST"];
    }

UI
--

Qt-based interface that the user sees.  The UI process acts as a stand alone
process and is essentially stateless.  All required information is fetched
from the backend.  This is to allow other frontends eg a cli to exist as
well using the REST API the backend exposes.  When the UI is killed, backend
processes are not affected.  For some exceptions (startup, shoutdown) the UI
will signal the backend so it can update its model and decide what to do.
::

    UIApp
      Acquire shared secret
        - Found: connect to BackendApp
        - Not found: start BackendApp and re-acquire shared secret
      While running:
        https://backend/gui/model -> JSON -> populate/update GUI model
          - Keep connection open to get model updates (long polling)
        Update UI based on model
        Handle UI events, translate them to REST API calls:
          https://backend/gui/action/[action-name]

Backend
-------

API based invocation and maintenance of state of the connections. Accepts
connections to a parameterized TCP port on localhost.  The backend can be
started on its own without any UI frontends present.  E.g.  in cases where
the backend is installed as a service or started by a system management
process - for example systemd - for headless operations.  Creates management
services that the Net and Privileged applications use to connect. Defines the
data model for the application and synchronizes the data with a storage.::

    BackendApp
      Acquire shared secret
        - Found: startup REST API
        - Not found: generate secret, startup REST API
      Load Model
      (Process Startup rules)
      While running:
        Process REST API
        Process cron Rules
      endpoints:
        /gui/model[/${collection}[/${instance}[/${attribute}]]]
          CREATE: signal user wants to create instance
          GET/READ: expose state, keep open for updates
          PUT/UPDATE: signal user want to update instance
          DELETE: signal user wants to delete instance
        /gui/action/${action-name}
          POST: signal user wants to execute procedure
        /priv/model[/${collection}[/${instance}[/${attribute}]]]
          CREATE: signal privileged wants to create instance
          GET/READ: expose state, keep open for updates
          PUT/UPDATE: signal privileged want to update instance
          DELETE: signal privileged wants to delete instance
        /priv/action/${action-name}
          POST: signal from privileged process events

The backend is separated into several modules. They are named by the actors
that will access the modules. The actors usually request models and invoke
controllers that modify model data. Additionally the actors may invoke
processes that will return immediately and report their state through models.
The logic which subroutines are invoked is implemented in the backend in
controllers that act on events.

The following processes are handled by the backend::

    Connect a single account
    Connect a account group
    Disconnect a single account
    Disconnect a account group

Connect a single account
~~~~~~~~~~~~~~~~~~~~~~~~

To connect a single account the backend will need to setup a connection in the
privileged backend using the configuration and credentials stored for the
account. Then the privileged backend is instructed to connect the connection
and all events and log entries are collected for that connection. The backend
will process those events and log entries to monitor the state of the
connection and updates the gui model if appropriate.

Connect a account group
~~~~~~~~~~~~~~~~~~~~~~~

To connect a account group the backend needs to evaluate all accounts of that
group and create a list of accounts to connect. It will connect each account
in the user-defined order and advance to the next account when it detects that
a connect operation has completed.

Net
---

Network connections that may be invoked by the :ref:`User<term-user>` like
download updates, post registration details, acquire visible IP etc.

The Net application runs as a dedicated user and connects to a parameterized
backend port on localhost.  It refuses to start when it cannot connect to the
backend.  It shuts down itself and all its child processes when the socket
is closed by the backend.  This behavior is similar to ``openvpn
--management-client``.
::

    NetApp
      Acquire shared secret
        - Found: connect to BackendApp
        - Not found: shutdown / error-code
      Start REST API
      While running:
        process REST API
        check BackendApp is alive || shutdown

      endpoints:
        /config
          GET/READ: request remote configuration resource
        /update
          GET/READ: request remote update binary
        /news
          GET/READ: request remote news about binary
        /provider-list
          GET/READ: request remote provider list
        /support
          CREATE: post support information

Privileged
----------

The privileged application runs as ROOT and connects to a parameterized
backend port on localhost. (XXX this is the simple implementation which is
OK to start with. It should be kept in mind that ROOT operations should
always imply a privsep setup) It refuses to start when it cannot connect to
the backend.  It shuts down itself and all its child processes when the
socket is closed by the backend.  This behavior is similar to ``openvpn
--management-client``.
::

    PrivilegedApp
      Acquire shared secret
        - Found: connect to BackendApp
        - Not found: shutdown / error-code
      Start REST API
      While running:
        process REST API
        check backendApp is alive || shutdown

      endpoints:
        /openvpn[/${connection-identifier}]
          CREATE: push complete configuration to privileged model, get
          connection-identifier. starts OpenVPN process
          GET/READ:
            - / get list of available connection-identifiers
            - /${connection-identifier} get state/output of connection
          UPDATE:
            - send (filtered, named) commands to existing OpenVPN process
          DELETE:
            - shutdown OpenVPN process
        /support[/${connection-identifier}]
          GET/READ: compile support information
        /${plugin}
          endpoint to be registered by a plugin

Privileged application that controls the invocation of software that needs
elevated OS access.  Exposes its interface with REST and ensures that only
the backend process can use it.  Collects system information only root can
access (eg firewall rules, routing).

configures, starts and stops OpenVPN
configures, starts and stops firewall
configures dns leak prevention

REST
----

The processes use REST to communicate. Each process defines an interface that
allows only specific calls.  This makes it possible for reviewers to validate
each component for security and functionality.

The frameworks evaluated for the IPC are:

* bottlepy
  size: 69kb / 1dep
  route by @(/resource/<variable>)
  authentication: bottle-cork (556kb / 2dep)
  middleware: beaker (300kb / 2dep)
  ssl using custom ServerAdapter, no additional dep, allows socket config
* cherrypy
  size: 435kb / 1dep
  route by object hierarchy
  authentication builtin
  middleware builtin
  ssl builtin, not for configuration through API
* flask
  size: 1158kb / 4deps
  route by @(/resource/<int:variable>)
  authentication: bottle-cork (556kb / 2dep)
  middleware: beaker (300kb / 2dep)
  ssl using ssl_context
  windows uses
* tornado
  size: 816kb / 6 deps
  should be used as server for other frameworks anyway
  route: regexp -> class mapping
  middleware: prepare member function
  ssl: builtin, pass ssl_context
  longpolling
* webpy
  size: 90kb
  ssl: pyOpenSSL +1141kb / 6deps

Netsplice uses `tornado<http://www.tornadoweb.org/>` because of the limited
dependencies and its possibilities to configure the ssl-socket. Flask is too
heavy-weight and Cherrypy's SSL does not allow the configuration of the
ciphers/protocols. Web.py is very minimal and contains too much 'quick hack'
comments. Flask, Cherrypy, Bottle suggest using tornado when long-polling and
SSL come into play. Tornado was initiated by facebook and is now an open source
(Apache 2.0) project.

Process Separation
------------------

To ensure separation of duties Netsplice is divided into four general
processes (NOTE: can be more esp when privsep is used).  Each process defines
an interface that allows only specific calls.  This makes it possible for
reviewers to validate each component for security and functionality.

The process that the user starts is the GUI-application.  This application
is located in the PATH of the OS and may be called from the start-menu
(windows), as Starter (.application) on Linux and as entry-point for the
Netsplice.app on OS X.  As the GUI should be fairly stupid in regards to
business logic one of its first actions is to check if there is a
Backend-application.  If there is no backend already started it needs to
start one.  If there is a backend process reuse it.

The Backend-application will be located in a library location, not accessible
from PATH or being 'hidden' in the .app Resources for OS X.  The GUI-application
starts the Backend-application with a host (localhost) and a configured (free)
port as parameter. It then uses a HttpClient to connect to the Backend-
application to acquire a view-model that can be used to populate controls. The
model endpoint is a long-polling request that is restarted every time it
completes. Actions initiated by the user are signaled to the backend on separate
REST endpoints.


Process communication
---------------------

The processes 'backend', 'privileged' and 'network' are designed as REST
services. Those services are secured using certificates generated by
the backend and shared between processes. Each service uses a certificate
authority to sign the service (server) key. The same CA is used to sign client-
keys. The client validates the connection with this CA. The creation of
certificates will occur when ``openssl validate`` returns errors (eg not found,
expired) for any of the required certificates. After the client-certificates are
generated the server-certificate-keys are deleted to prevent abuse of those
certificates. The backend application will load ``backend.crt`` (+ key + ca)
for its REST service and ``privileged-backend- client`` (+ key + ca) for the
connection to the privileged process.  The privileged process will load
``privileged.crt`` (+ key + ca) for its REST service and
``backend-privileged-client.crt`` for the connection to the backend process.
The same applies for the network application.  The GUI application will only load
``backend-gui-client.crt`` and connect to the backend service.

When the GUI starts the backend for the first time, the keys are created
by the backend. The backend will not start its children (privileged and network)
until the keys are created. The GUI will notice that the backend is not
available during this period and shows a progress bar to the user detail the
start-up procedure.

.. graphviz::

    digraph G {
        subgraph cluster_backend {
            style=filled;
            color=lightgrey;
            "BackendDI"[shape=box];
            "Setup"[shape=box];
            "backend.ca.crt";
            label = "Backend App";
        }
        subgraph cluster_privileged {
            style=filled;
            color=lightgrey;
            "PrivilegedDI"[shape=box];
            "privileged.ca.crt";
            "privileged-backend-client.crt";
            "backend-privileged-client.crt";
            label = "Privileged App";
        }
        subgraph cluster_network {
            style=filled;
            color=lightgrey;
            "NetworkDI"[shape=box];
            "network.ca.crt";
            "network-backend-client.crt";
            "backend-network-client.crt";
            label = "Network App";
        }
        subgraph cluster_gui {
            style=filled;
            color=lightgrey;
            "GUIDI"[shape=box];
            "backend-gui-client.crt";
            label = "GUI App";
        }
        "BackendDI"->"Setup"
        "Setup"->"backend.ca.crt"[label="generates"]
        "Setup"->"privileged.ca.crt"[label="generates"]
        "Setup"->"network.ca.crt"[label="generates"]

        "backend.ca.crt"->"backend-gui-client.crt"[label="signs"]
        "backend.ca.crt"->"backend-privileged-client.crt"[label="signs"]
        "backend.ca.crt"->"backend-network-client.crt"[label="signs"]
        "privileged.ca.crt"->"privileged-backend-client.crt"[label="signs"]
        "network.ca.crt"->"network-backend-client.crt"[label="signs"]

        "GUIDI"->"backend-gui-client.crt"[label="uses"]
        "NetworkDI"->"backend-network-client.crt"[label="uses"]
        "PrivilegedDI"->"backend-privileged-client.crt"[label="uses"]
        "BackendDI"->"privileged-backend-client.crt"[label="uses"]
        "BackendDI"->"network-backend-client.crt"[label="uses"]

        "backend-gui-client.crt"->"BackendService"
        "backend-network-client.crt"->"BackendService"
        "backend-privileged-client.crt"->"BackendService"

        "privileged-backend-client.crt"->"PrivilegedService"
        "network-backend-client.crt"->"NetworkService"
    }


Attack scenarios
----------------

A user application may be used to shutdown the OpenVPN connection exposing all
network traffic of applications that rely on the channel.

    This scenario should be impossible as long as the user-application does not
    kill the pid of a involved process. The REST services are secured using
    client-certificates.

A malicious application may read the secrets passed to the privileged
application by recording the local traffic.

    This scenario should be impossible as the communication between the
    processes is encrypted.

A real risk assessment should be executed in a later state of development.


Management server
-----------------

A class ``util.ipc.server`` is used by the applications that expect clients to
connect. It is used to setup the REST endpoints and accept connections using
existing certificates. The server may contain dispatcher 'links' that are using
the same ioloop to communicate (eg BackendService and Backend-Privileged-Client
in one process). The server logic may use the dispatchers to pass messages. The
server has no app-specific overload and may be used as in this example:::

    app = server(options.host, options.port, 'certificate-prefix')
    app.set_owner(backend_dispatcher(backend_host, backend_port))
    app.set_network(None)
    app.set_privileged(None)
    app.start([
        (r"/configuration", configuration_controller),
        ...
        ])
    sys.exit(app.exec_())


Management client
-----------------

A class ``util.ipc.service`` is used by the applications that connect to a
``util.ipc.server``. It is used to setup the connection and serve as a base-
class for specialized clients that expose a server-specific interface to the
business logic. The base implementation wraps the GET/POST HttpClients with
their SSL setup to convenient functions

A management client may be implemented like this:::

    class some_dispatcher(service):

        def __init__(self, host, port):
            service.__init__(
                self, host, port,
                'server-certificate-prefix', 'client-certificate-prefix')

        def handle_server_response(self, response):
            print 'server response', response

        def do_something_on_server(self):
            self.post('endpoint/on/server', '{}', self.handle_server_response)

The application may now use the management client as this:::

    remote = some_dispatcher(backend_host, backend_port)
    remote.do_something_on_server()

The application needs to run remote.exec_() (blocking, eg in a thread) at some
point or the requests are never dispatched to the server.
