.. _terminology:

Terminology
===========

This document describes terms that are used by users and developers related to
the software. This document uses nouns for naming and orders them
alphabetically. It defines link-labels for other documents and references
itself.

.. _term-account:

Account
-------

User configurable entry in the GUI that contains all relevant information
about how to connect to a :ref:`Provider<term-provider>`.

.. _term-account-group:

Account Group
-------------

User configurable hierarchical group of :ref:`Accounts<term-account>` that
defines an order how accounts are displayed and connected.


.. _term-action:

Action
------

UI Button Element that is usualy triggered with a mouse-click or from a Menu
Entry. A Action can have a enabled and disabled state and needs at least two
text elements: the Action Label - usualy the Text on the Button and the Action
Tooltip - usualy the Text that is displayed when the mouse is hovering the
Button

.. _term-backend:

Backend
-------

Application component that implements the business functionality and brokers
between the other components.

.. _term-client:

Client
------

Application that connects to a :ref:`Provider<term-provider>`.

.. _term-config:

Config
------

Configs are Provider presented configuration files. They vary in the selection
of encryption algorithms to the IP-protocol used. OpenVPN needs a Config to
connect to a service of the Provider.

.. _term-connection:

Connection
----------

Connections are created from :ref:`Accounts<term-account>` by the backend to
handle state-changes and collect :ref:`Log<term-log>` information.

.. _term-credential:

Credential
----------

Credentials are the secrets that are required to authenticate to a  :ref:`Provider<term-provider>` or to unlock a certificate.

.. _term-gui:

GUI
---

Application component that is visible to the user. It displays status and
dialogs and is controlled from the :ref:`Backend<term-backend>`.


.. _term-isp:

ISP
---

Service that provides the :ref:`Client<term-client>` with open internet access
and allows it to connect to :ref:`Provider<term-provider>`.

.. _term-log:

Log
---

The application stores technical information in a log that can be reviewed
by the :ref:`User<term-user>` to evaluate reasons for failures. The log is
a list of log items that relate to :ref:`Connections<term-connection>` or the application in general.

.. _term-openvpn:

OpenVPN
-------

Opensource technology that uses virtual private network techniques for
creating secure point-to-point or site-to-site connections.

.. _term-install-package:

Install Package
---------------

Installer for a specific platform that allows normal user to install the
application with the native means of the operatingsystem.

.. _term-model:

Model
-----

A model is a technical description of values that can be stored or that can
be exchanged between application components. Models define fields that can
be serialized and unserialized.

.. _term-model-field:

Model Field
-----------

A Model Field defines a name, type, default-value and validator for a value
in a Model.

.. _term-preferences:

Preferences
-----------

The persistent settings of a :ref:`User<term-user>` that include UI preferences
as well as :ref:`Account<term-account>` and
:ref:`Account Groups<term-account-group>`.

.. _term-privileged-backend:

Privileged Backend
------------------

Application component that executes privileged operations for the
:ref:`Backend<term-backend>`.

.. _term-privileged-operation:

Privileged Operation
--------------------

An operation that is not allowed as normal :ref:`User<term-user>`.

.. _term-provider:

Provider
--------

Provider is any service that allows clients to connect using
:ref:`OpenVPN<term-openvpn>`.

.. _term_statusbar:

Statusbar
---------

The statusbar is a small informative bar that displays the application state
and version. It is visible for the mainwindow.

.. _term_systray:

Systray
-------

Most desktop environments provide a notification area that allows applications
to put an icon with a context-menu and occasional messages in there.

.. _term-user:

User
----

The user logged in on the computer. The user has no privileges to change the
network configuration, but can access the internet and read and write files in
the users home directory.
