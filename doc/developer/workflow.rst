.. _workflow:

Development Workflow
====================

This section documents the workflow that the Netsplice team follows and
expects for the contributions.

While reading this guide, have in mind the two rules of contributing code:

* The first rule of code contribution is: Nobody will push unreviewed code to the mainline branches.
* The second rule of code contribution is: Nobody will push unreviewed code to the mainline branches.

Code formatting
---------------
In one word: `PEP8`_.

`autopep8` allows some magic formatting.

.. _`PEP8`: http://www.python.org/dev/peps/pep-0008/
.. _`autopep8`: http://pypi.python.org/pypi/autopep8

Dependencies
------------
When a new dependency is introduced, please add it under ``pkg/requirements``
or ``pkg/test-requirements`` as appropiate, under the proper module section.

Git flow
--------
We are basing our workflow on what is described in `A successful git branching
model <http://nvie.com/posts/a-successful-git-branching-model/>`_.

However, we use a slightly modified setup in which each developer maintains
her own feature branch in her private repo. After a code review, this feature
branch is rebased onto the authoritative integration branch.

A couple of tools that help to follow this process are  `git-flow`_ and
`git-sweep`_.

.. _`git-flow`: https://github.com/nvie/gitflow
.. _`git-sweep`: http://pypi.python.org/pypi/git-sweep

Code review and merges into integration branch
-----------------------------------------------
All code ready to be merged into the integration branch is expected to:

* Have tests
* Have documentation

* Pass existing tests: do **nosetests**. Rebasing against the current tip of
  the integration when possible is preferred in order to keep a clean
  history.

Using Github
------------

Fork the repository on github. Always create a branch from *master* with some
descriptive name the following name:

``bug/some_descriptive_text``

or

``feature/some_descriptive_text``

Work there, push it, and create a pull request against the *develop* branch in
the main repository. Now wait until *another* developer comments or merges the
work. Join on ``#netsplice-dev`` at `freenode <https://freenode.net>`_ to get
quick responses.

The code will get reviewed/discussed by someone else on the team. In case that
the discussion requests some more changes, do the following::

  git checkout <your branch>

*Edit*

Simple commit, this doesn't need a good commit message::

  git commit -avm "Fix"

This will help reorder the commits and squash them (so that the
final commit list has good representative messages)::

  git rebase -i develop

Since now the local history is rewritten a force push is required::

  git push <your remote> +<your branch>

This will update the pull request automatically, but it won't notify us
about the update again, so add a comment saying so, or re-ping the
reviewer.

Other methods
-------------

Feel free to use any other methods like format-patch and mail or whatever
method.
