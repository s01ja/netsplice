.. _developer-plugins:

Plugins
=======

To allow the implementation of multiple types of VPN's, Netsplice was designed
to have plugins. The plugins are part of the source code and will not be loaded
into a compiled package.

To implement a plugin it needs to be registered in the
``src/netsplice/plugins/__init__.py``. Registered plugins will be loaded when
the application starts.

Read more in the :ref:`Plugin API<plugins-api-interfaces>` documentation.

In general everything can be extended, as the current language is python. Some
more likely locations have helper functions that allow the extensions to grow
in a structured way:

  * ``netsplice.backend.connections.register_plugin``
  * ``netsplice.backend.preferences.model.plugins.register_plugin``
  * ``netsplice.[backend|gui|privileged].event_loop.register_plugin_module``
  * ``netsplice.[backend|gui|privileged].event_loop.register_module``
  * ``netsplice.gui.account_edit.register_account_type``
  * ``netsplice.gui.register_account_type_pixmap``

To add and activate a plugin the following methods are used:

  * ``netsplice.ipc.application.add_plugin``
  * ``netsplice.ipc.application.register_plugin_components``


Plugin Structure
----------------

The structure of a plugin in general should look like this:::

    src/netsplice
       |- plugins
       |   |- plugin1
       |   |   |- component1 (eg pure-rest-endpoint)
       |   |   |   |- plugin_module
       |   |   |   |   |- model...
       |   |   |   |   \- __init__.py
       |   |   |   \- __init__.py
       |   |   |- component2 (eg gui)
       |   |   |   |- control_module
       |   |   |   |   |- views
       |   |   |   |   |   \- control.ui
       |   |   |   |   |- Makefile
       |   |   |   |   |- plugin_model.py
       |   |   |   |   \- __init__.py
       |   |   |   |- model
       |   |   |   |   |- request...
       |   |   |   |   |- response...
       |   |   |   |   |- plugin_model.py
       |   |   |   |   \- __init__.py
       |   |   |   |- Makefile
       |   |   |   \- __init__.py
       |   |   |- config
       |   |   |   |- plugin_private_config.py
       |   |   |   \- __init__.py
       |   |   |- res
       |   |   |   |- private_plugin_resources
       |   |   |   \- __init__.py
       |   |   \- __init__.py
       |   |- plugin2 ...
       |   \- __init__.py
       |- core_components
       \- *App.py

As you can see this mirrors the core structure and allows the development of
core components that are moved to a plugin once all the core interfaces have
been established.

Resources
---------

A plugin may contain resources in the res directory such as images, locales,
certificates - anything that is not a py-config. The plugin is responsible to
load all files by itself. The only exception are the qtresources that are
compiled along with the core resources.

Add Plugins
-----------

Plugins are added by the main application during startup. The main application
references the plugins for later registration of the required components.
All plugins are added to all applications.

Register Plugins
----------------

When the core modules are loaded, the plugins may register to those modules.

The registration is done in a order that respects the plugin dependencies
if they are defined. Plugin dependencies are defined by module references.
Each App will try to import plugin components and the registration method allows
``KeywordError`` to occur.

During the registration process most of the environment is available. The only
exception is that the QApplication is not yet started, therefore it is not
possible to communicate with the user and create widgets. All widget plugins
need support of the attached widgets.

Plugin API
----------

Read the :ref:`Plugin API <plugins-api>` to learn how to implement extensions to
the Application.
