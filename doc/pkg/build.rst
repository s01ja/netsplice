.. _pkg-build:

Build
=====

Building Netsplice requires the following preconditions:

* a OSX Host with Virtualbox installed
  any machine with more than 4gb of ram and Intel CPU will do
* a virtual machine running debian or rancher-os

  * default installation with sshd
  * bridged network interface
  * hostname: build-debian
  * disk: 32?gb
  * memory: 4gb
  * software:

    * docker-engine
    * docker-compose
    * git-core
* a virtual machine running osx

  * bridged network interface
  * hostname: build-osx
  * disk: 32?gb
  * memory: 2gb
  * software:

    * python
    * ssh server

Process
=======

The build process is triggered by jenkins on build-debian on a regular basis.
Jenkins has multiple dependent jobs that produce installers. Jenkins is a
privileged docker-container that is allowed to control the build-debian
docker. It launches the build-slaves in a predefined order with prepared
sourcecode

* build-debian

  * docker-compose.yml.in

    * jenkins
    * slaves

      * sign
      * sync
      * package
      * sysdebian
      * sysfedora24
      * systesting
      * sysubuntu16.04
      * sysubuntusnap
      * sysosx
      * syswin32
      * upload

Jenkins
-------

The jenkins host will regulary start the slave 'sync' to fetch the git
repository. On success it will trigger the 'package' slave that will produce
sourcecode tars that will be used by the subsequent builds. The systesting
slave will then be triggered and on success the other sys\* slaves will be
triggered. Those take the sourcecode tars and run platform dependent tasks,
producing the installables. Now the installables need to get signed with a gpg
and uploaded to a public location.

Package
-------

Uses the Synced GIT repository to checkout branches or tags. Creates a
sourcecode package from the develop branch
`Netsplice-[BUILDNUMBER]-[GITHASH].tar.bz2` and a master branch
`Netsplice-[TAG].tar.bz2` Those tars are copied to a slave-shared location
so the other slaves may access them. Prior to packaging the script checks if
the package already exists and exits the process. After copying cleanup in the
slave-shared location is executed to prevent the builds from filling up the
server disk. Always leaves the git checked out to master branch after
finished.

Sign
----

Signs the intallables with a GPG key so the signature can be verified after
download. The sign key should only be accessible to this container. If the
sign key is missing no signatures are created.

Sync
----

Executes a `git fetch --all --prune` on the git repository to fetch the
latest state.


Sys\*
-----

Platform dependent build scripts. For debian its debhelper, fedora uses
rpmbuild, windows uses wine in debian and osx connects to build-osx to
excecute. All build scripts will produce a installable located at the slave-
shared location or will fail with a non-zero exit code. For now only the osx
installer requires to be built on a actual osx system. this might change in
the future.

Upload
------

rsyncs the source-packages, the installables and their signatures to a public
accessible system for download. The remote system has to ensure that it has
enough disk-space and clean up the build files when required.
