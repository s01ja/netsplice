.. _pkg-debian:

Debian
======

Installation
------------

.. begin-user-installation

* Download the debian installer `debian/netsplice-[VERSION]_amd64.deb`
* Double-click the debian installer.
* Enter Administrative Password.
* Click Install.

To alternatively install using the terminal type::

.. code-block:: console

    sudo apt-get update
    sudo dpkg -i netsplice_${VERSION}_amd64.deb
    sudo apt-get install -f

The Application is now installed and added to the Applications Menu in the
Subsection 'System'.

.. end-user-installation

Hints
~~~~~

.. begin-hints

In Gnome 3 the Topbar does not provide a notification or systray area by
default. Instead all notification icons are located in a bar in the bottom left
corner.

To have the Netsplice icon in the topbar, install the shell extension
`TopIcons Plus`.

.. end-hints

Package
-------

The debian package comes in multiple flavors. Debian 8 (jessie) and Debian 9
(stretch). Both are build in containers that contain only the required packages
like python, pyside, a compiler and debian packages the pip packages require.

When the package is build `pkg/debian-build/slaves/sysdebianX/bin/build.sh`
is executed. This script takes care of the container environment by correcting
the location of `ip` and runs the rules to build the openvpn packages and
the pyinstaller commands to produce binaries for the debian platform.

To run the package build without all other packages::

.. code-block:: console

    cd pkg/debian-build
    make docker-compose.yml
    docker-compose run --rm slavepackage
    docker-compose run --rm slavedistfiles
    docker-compose run --rm slavedebianX

The results will be stored in var/installables.

When it is required to debug the build it may be useful to run directly on the
source tree. The slavedistfiles container still is required as it prepares the
openvpn executables.

.. code-block:: console

    cd pkg/debian-build
    make docker-compose.yml
    docker-compose run --rm slavedistfiles
    docker-compose run --rm --entrypoint /bin/bash slavedebianX
    container$ export USE_GIT_SOURCE=YES
    container$ /usr/local/bin/build.sh

Multiple calls to the build.sh script will not cleanup the files from the
previous builds, but to speed up (skipping the openvpn compiles) it may be
useful to remove the `$(OPENVPN_INSTALL_TARGETS)` dependency from the
sha256sum target in `pkg/Makefile.install`.

Never distribute a package that was build this way, all packages should be
build with the all target of the pkg/Makefile.
