.. _pkg-source:

Source
======

Netsplice comes in multiple packages build for multiple operating systems. The
idea is that this packages just work. While Netsplice is software in
development, it is possible, that the provided packages have certain issues
with the OS that it is used on. We want a 0 issue experience for new users,
especially after the installation, so please report all issues with the
packages. When you are able to research the reason for a problem and possibly
fix the issue because the source is open, the source is where you want to
start. For operating systems that have no package yet, the source is a good
start to create a new generator for a installed version.

It allows different users/identities to use different versions of the
application. In addition some features work 'better' in a python virtualenv.
For example the keyring access may be configured to one of the various methods
of accessing private data. With the source unpacked and self signed on the
machine, some features do not need to be active during execution. This reduces
the attack surface of the system.

To use the source should be possible for everyone that is comfortable with a
shell and can follow the instructions below.


Installation
------------

.. begin-user-installation

* Download the source tar package and verify the signature.
* Unpack
* Install python-dev (Python.h), pyside and pyside-tools using your os package
  manager. There are more packages depending on your platform that need to
  be installed with the include directories (libffi, openssl)
* run:

    .. code-block:: bash

        virtualenv --system-site-packages --python=python2 /prefix/netsplice
        source /prefix/netsplice/bin/activate
        python setup.py install --prefix=/prefix/netsplice
        pip install -r /downloaded_source/netsplice/pkg/requirements.pip

* run `NetspliceGuiApp.py`
* to have independent configurations per installation,

  `export HOME=/prefix/netsplice` and modify the
  `venv/lib64/python2.7/site-packages/Netsplice-VERSION-py2.7.egg/netsplice/config/constants.py` for multiple parallel instances.
* to have independent executables put them in
  /../site-packages/Netsplice-VERSION-py2.7.egg/netsplice/var/build

* to run the code without the *setup.py* rundtrip, use the Makefile (run) or
  execute `python2 src/netsplice/NetspliceGuiApp.py`. This will require
  `PYTHONPATH` and `NAMESPACE_SOURCE` environment variables.

.. end-user-installation

Latest OpenVPN executables
--------------------------

With the installer packages, OpenVPN is shipped with libreSSL and OpenSSL with
multiple versions and patches. The source package does not contain those
binaries, therefore Netsplice falls back to the system provided OpenVPN
version. While this may work for a specific service this has several drawbacks.
One is that security patches provided from upstream may not yet built in, the
other is that the latest stability enhancements are not available and cause
Netsplice to behave unexpected.

Netsplice looks for OpenVPN executables in the site-package that was created
with `python setup.py install`. With the prefix specified above this
directory is: `/prefix/netsplice/lib/python2.7/site-packages/
Netsplice-VERSION-py2.7.egg/netsplice/var/build`

The executable locator then looks for a directory in the build path:
`openvpn/v1.2.3-patch` that contains the sub-structure
`openvpn/sbin/openvpn`.

With that knowledge it is relatively easy to build a new openvpn binary and
install it to this location:

.. code-block:: bash

    cd /tmp
    git clone https://github.com/OpenVPN/openvpn-build
    # (put any patches into openvpn-build/generic/patches)
    cd openvpn-build/generic
    # now edit the build.vars or pass them on the commandline
    VENV_PREFIX=/prefix/netsplice \
    NETSPLICE_VERSION=0.16.1 \
    OPENVPN_VERSION=2.4.1 \
    PKCS11_HELPER_VERSION=1.11 \
    LZO_VERSION=2.10 \
    OPENVPN_URL="https://build.openvpn.net/downloads/releases/openvpn-${OPENVPN_VERSION}.tar.gz" \
    LZO_URL="https://www.oberhumer.com/opensource/lzo/download/lzo-${LZO_VERSION}.tar.gz" \
    PKCS11_HELPER_URL="https://downloads.sourceforge.net/project/opensc/pkcs11-helper/pkcs11-helper-${PKCS11_HELPER_VERSION}.tar.bz2" \
    SPECIAL_BUILD=netsplice-custom \
    INSTALL_ROOT=${VENV_PREFIX}/lib/python2.7/site-packages/ \
    Netsplice-${NETSPLICE_VERSION}-py2.7.egg/netsplice/var/build/openvpn/v${OPENVPN_VERSION} \
    IMAGEROOT=${INSTALL_ROOT} \
    ./build


This now downloads the openvpn dependencies using https, compiles them together
with the openvpn executable and installs everything to the virtualenv prefix.

When the build is complete, test the binary by entering the install directory
and run `./openvpn --version` and `ldd openvpn` to ensure that the build
lzo, libcrypto libraries are used from the prefix, not from the system.

As the source installation does not elevate the privileged backend, run `chmod
+s` and `chown root` on the compiled executable.

To have multiple executables, repeat the build with the other versions.

Depending on your system some dependencies need to be provided with the
includes like libpam-dev. To get the full picture on how we build the netsplice
executables and what needs to be installed refer to the `pkg/debian-
build/Makefile.openvpn` and the Dockerfile in the `pkg/debian-
build/slaves` directories.

Restart netsplice and see that the binary was picked up in the Help->About
dialog and configure the new openvpn version in the Connection Settings.
