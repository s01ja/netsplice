.. _pkg-source:

Documentation
=============

Netsplice comes with a extensive documentation. Thank you for reading it. The
html, pdf and manpages are generated from the doc-tree and with the comments in
the source files.


Installation
------------

.. begin-user-installation

* Download the netsplice-[VERSION].pdf
* Open in your favorite PDF reader.

.. end-user-installation

Package
-------

The documentation package is build using sphinx and latex. The docker
containers prepare a environment where this dependencies are installed without
modifying the developer machine. The PDF part (latex) is very large and
requires the os to hold about 1000 additional packages that, unless required
otherwise, make future updates for the system hard and error-prune.


When the package is build `slaves/sysdocumentation/bin/build.sh` is executed.
This script executes make html, singlehtml, man, text and latexpdf in the
doc directory and copies the result to the installables directory.

To run the package build without all other packages::

.. code-block:: console

    cd pkg/debian-build
    make docker-compose.yml
    docker-compose run --rm slavepackage
    docker-compose run --rm slavedocumentation

The results will be stored in var/installables.

When it is required to debug the build it may be useful to run directly on the
source tree.

.. code-block:: console

    cd pkg/debian-build
    make docker-compose.yml
    docker-compose run --rm --entrypoint /bin/bash slavedocumentation
    container$ export USE_GIT_SOURCE=YES
    container$ /usr/local/bin/build.sh
    # or
    container$ source /usr/local/bin/build.inc.sh
    container$ export USE_GIT_SOURCE=YES
    container$ unpack
    container$ cd /var/tmp/netsplice-VERSION/doc # make cd after every unpack!
    container$ make html
    container$ cp -R ${TMPDIR}/doc/build ${INSTALLABLESDIR}/tmpdoc

Never distribute a package that was build this way, all packages should be
build with the all target of the pkg/Makefile.

Of course when sphinx-build is installed in the developer system, the docker
containers are not required to check that all documentation markup is correct.

Just run `make html` in the `doc` directory and `rm -f doc/build`
afterwards. See the slavedocumentation/Dockerfile for the required packages.

To check the autodoc process::

.. code-block:: console

    export BUILDDIR=$(pwd)
    export PYTHONPATH=${PYTHONPATH}:${BUILDDIR}/src/
    make codedoc > doc/developer/autocode.rst
    cd doc
    make html

Please do not commit the autocode.rst with the generated references. They
change too much (in order) to be usefully maintained.
