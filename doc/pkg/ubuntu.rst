.. _pkg-ubuntu:

Ubuntu
======

Installation
------------

.. begin-user-installation

* Download the ubuntu installer `netsplice_[VERSION].deb`
* Enable community maintained free and opensource package source in `software-properties-gtk`.
* Double-click the deb-file
* Click Install
* or `dpkg -i netsplice_[VERSION].deb; apt-get -f install` in a root-shell

The Application is now installed and added to the Applications Menu in the
Subsection 'System'

.. end-user-installation


Package
-------

The Ubuntu package comes in multiple flavors. Ubuntu LTS and Ubuntu latest.
Both are build in containers that contain only the required packages like
python, pyside, a compiler and Ubuntu packages the pip packages require.

When the package is build `pkg/debian-build/slaves/sysubuntuX/bin/build.sh`
is executed. This script runs the rules to build the openvpn packages and
the pyinstaller commands to produce binaries for the Ubuntu platform.

To run the package build without all other packages::

.. code-block:: console

    cd pkg/debian-build
    make docker-compose.yml
    docker-compose run --rm slavepackage
    docker-compose run --rm slavedistfiles
    docker-compose run --rm slaveubuntuX

The results will be stored in var/installables.

When it is required to debug the build it may be useful to run directly on the
source tree. The slavedistfiles container still is required as it prepares the
openvpn executables.

.. code-block:: console

    cd pkg/debian-build
    make docker-compose.yml
    docker-compose run --rm slavedistfiles
    docker-compose run --rm --entrypoint /bin/bash slaveubuntuX
    container$ export USE_GIT_SOURCE=YES
    container$ /usr/local/bin/build.sh

Multiple calls to the build.sh script will not cleanup the files from the
previous builds, but to speed up (skipping the openvpn compiles) it may be
useful to remove the `$(OPENVPN_INSTALL_TARGETS)` dependency from the
sha256sum target in `pkg/Makefile.install`.

Never distribute a package that was build this way, all packages should be
build with the all target of the pkg/Makefile.
