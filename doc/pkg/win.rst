.. _pkg-win:

Windows
=======


Installation
------------

.. begin-user-installation

* Download the Windows Installer
* Double-click the installer.exe
* Accept the License Agreement
* Choose a Install Location
* Click Install
* Confirm TAP Driver installation

Netsplice is now installed and accessible from the Start Menu

.. end-user-installation


Package
-------

The Windows package is build using mingw and wine. The docker containers
prepare a environment where this dependency is installed without modifying the
developer machine. The containers are split into 3 parts: management,
dependencies, installer.

When the package is build `pkg/debian-build/slaves/syswin32/bin/build.sh` is
executed. This script prepares the environment for the dependencies and
installer containers. When the environment is setup, the dependencies container
is started.

The dependencies container has wine with python, pip and various tools
installed that allow to build openvpn along with the binaries that pip needs
produce for the requirements. When it has finished, the installer container that
has only the the nullsoft installer package, will compile the install.exe.


To run the package build without all other packages::

.. code-block:: console

    cd pkg/debian-build
    make docker-compose.yml
    docker-compose run --rm slavepackage
    docker-compose run --rm slavedistfiles
    docker-compose run --rm slavewin32

The results will be stored in var/installables.

When it is required to debug the build it may be useful to run directly on the
source tree. The slavedistfiles container still is required as it prepares the
openvpn executables.

.. code-block:: console

    cd pkg/debian-build
    make docker-compose.yml
    docker-compose run --rm slavedistfiles
    docker-compose run --rm --entrypoint /bin/bash slavewin32
    container$ export USE_GIT_SOURCE=YES
    container$ /usr/local/bin/build.sh

Multiple calls to the build.sh script will not cleanup the files from the
previous builds, but to speed up (skipping the openvpn compiles) it may be
useful to remove the `$(OPENVPN_INSTALL_TARGETS)` dependency from the
sha256sum target in `pkg/Makefile.install`.

To go even deeper into the dependency or installer builds a container in the container needs to be started::

.. code-block:: console

    container$ cd /var/tmp/netsplice-VERSION_with_underscore/pkg/debian-build/slaves/win32
    container$ make docker-compose.yml
    container$ docker-compose run --rm --entrypoint /bin/bash dependencies
    dependency_container$ /usr/local/bin/dependencies-build.sh
    dependency_container$ exit
    container$ docker-compose run --rm --entrypoint /bin/bash installer
    installer_container$ /usr/local/bin/installer_build.sh

Please be aware that the sub-containers do not update the source from the git
on every step. When updated sources are required, docker-exec in the syswin32
container and run the build.sh again (unpack&cp) without the actual make.

Never distribute a package that was build this way, all packages should be
build with the all target of the pkg/Makefile.
