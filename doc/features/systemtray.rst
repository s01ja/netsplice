.. _`features-systemtray`:

System Tray
===========

.. begin-summary

The System Tray is used to inform the user that the application is running and
which state the application currently has. It provides a Context-menu that
allows to Exit the application and access configured Accounts. A balloon message
can be displayed to notify the user about relevant events. The System Tray is
displayed next to the clock in all systems that provide a notification area.

.. image:: /images/systray.png

.. end-summary


Icons
-----

Various icons signal the state of the application.

* Starting
    The application has been started but needs time to setup its backend
* Started
    The application has been started and is ready to process user input
* Connecting
    The application currently connects a Account VPN connection. The animation
    that displays during the connecting state can be disabled with the
    ``config.gui.SYSTRAY_ANIMATE_ICON`` option.
* Connected
    The application is currently connected to a Account, the VPN connection is
    active

Click Action
------------

When the user clicks on the tray icon, the main window is toggled between visible and invisible. This feature is not available in OSX

Messages
--------

Some messages are displayed using a system tray balloon. Those messages
can be disabled using the ``config.gui.SYSTRAY_SHOW_NOTIFICATIONS`` option.

* Connected

     * Displays when a Account has been successfully connected.
     * -Displays the IP assigned from the VPN server-
     * -Displays the IP visible by other services-
* Connecting

     * Displays when a Account has started to connect.
* Disconnected

     * Displays when the Connection to a VPN server is lost.
     * Displays when the User has chosen to disconnect a Connection.
* Failed

     * Displays when a Connection failed to connect.

Context Menu
------------

Various Actions can be triggered using the context menu

* Quit

    * Quit the Application, shutdown all connections
    * Displays a Confirm message when there are active connections
* Accounts

    * One Entry per configured Account
    * Message
        * Connect (when disconnected)
        * Disconnect (when connected)
        * Connecting (when connecting)
* Show / Hide

    * Show or Hide the main window.
