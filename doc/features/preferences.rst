.. _`features-preferences`:

Preferences
===========

.. begin-summary

Netsplice can be configured to startup when the user logs in, where and how to
store account credentials, what the available providers are and how OpenVPN is
invoked.

.. image:: /images/preferences.png

.. end-summary

The preference dialog will be modeled after the implementation of the features
has completed. Therefore a generic dialog is displayed for now. The following
description applies to the planned preference dialog.

From the Main Menu a Preferences View can be launched that allows to set the
possible options. The options are split into the following sections:

* General
* Display
* Security
* Plugins
* Advanced

The preferences are stored as JSON in
``~/.config/Netsplice/preferences/*.conf`` (linux/OSX) or in
``/Users/[your_name]/AppData/Local/Netsplice/preferences/*.conf``.



The Preferences can be reset to the installation defaults with a button
action.

General
-------

The general preferences control how the application integrates into the
Operating System.

* Status Display
  How/If notifications are displayed (connect/disconnect)
* Notification Sounds
  How/If notification sounds are played (connect/disconnect)
* Autostart behavior
  Control if the Application is launched when the user is logged in. Should it
  automatically connect?

Display
-------

The display preferences the look of the application. This is intended for
better accessibility.

* Font and Font Sizes
* Icon Theme (eg tray icon)
* Colors and Theme

Security
--------

The security preferences control how sensitive information is stored and what
should be done when the application quits / gets no connection.

* Master Password to encrypt all remembered provider passwords (not stored)
* Review/Modify provider passwords
* Shutdown behavior (route to lo)
* When to re-check Provider capabilities (before connection, based on age, never)

Plugins
-------

The plugins preferences control which plugins are active and allows the
installation and uninstallation of plugins.

* List Plugins
* Install Plugin
* Uninstall Plugin
* Enable Plugin
* Disable Plugin

Advanced
--------

The advanced preferences allow configuration of not-so-common features and
options that are not exposed to the user interface. Those options should be
handled with care.

* Alternate backends
* Internal configuration variables
* Updates
* Overrides
* SSL-Certificates for network communication
* Crash Reporter
