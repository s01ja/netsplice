.. _`features-plugins`:

Plugins
=======

.. begin-summary

Netsplice allows installing various plugins. Those extend the core
functionality.

.. end-summary

Unlike the name suggests, plugins are not user-configurable plugins but
require the config/plugins.py to be modified.

The following plugins are part of the default installation:

- :ref:`OpenVPN<plugins-openvpn>`
- :ref:`Process Manager<plugins-process-manager>`
- :ref:`OpenSSH<plugins-openssh>`
- :ref:`Tor<plugins-tor>`
- :ref:`Profiles<plugins-profiles>`
- :ref:`Network Scan<plugins-nmap>` (not implemented yet)
- :ref:`Statistics<plugins-statistics>` (not implemented yet)

Other Plugins must implement the :ref:`Plugin API<plugins-api>` and need to be
installed with the :ref:`Preferences<features-preferences>`. Be careful
trusting the developer of the plugin. Plugins can potentially expose the users
identity even if Netsplice displays everything is alright.
