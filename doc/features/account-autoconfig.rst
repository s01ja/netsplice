.. _`features-account-autoconfig`:

Account Autoconfiguration
=========================

.. begin-summary

Most VPN providers allow the user to customize certain aspects of their
connection. This varies from alternate server ports trough the transport
method (UDP or TCP) to service specific servers like (ipv6.provider.host or
tor.provider.host). At this point the user is usually requested to edit a
ovpn/conf file to customize the parameters. The Account Autoconfiguration
tries to mask this complexity from the user by requesting the Providers
capabilities and presenting them in the user interface.

*This is a planned feature that needs more discussion and evaluation*

.. image:: /images/account-autoconfig.png

.. end-summary


To setup a new account the user can drop a prepared conf file that is used 'as
is' or select a Provider that exposes its capabilities and presents template
configurations that only need username and password or the client certificate
added.

.. _`features-account-autoconfig-add-provider`:

Provider Selection
------------------

Netsplice supports various providers that provide a machine-readable
description of their service.  Providers may request to get listed to the
default list of VPNs and can be added by the user.  Listed providers are
identified by a name, their host and their SSL certificate fingerprint.
Versions of Nersplice that do not allow Provider Selection can be
compiled by a provider that wants to limit the user's choices.  Those
providers can do this as long as they are in compliance with the :ref:`GNU
Public License<gpl3>`

.. image:: /images/account-autoconfig-addprovider.png

Provider Requirements
---------------------

The provider has to expose its capabilities on a HTTPS host in a JSON file.
The details of the JSON file are described in the
:ref:`Provider API<provider-api>`.
Providers that qualify for a default listing in the application can be
simply selected. Unknown providers have to be setup by the user. The user will
enter a hostname or a capabilities-URI that the application will request and
process.

User Interface
--------------

The user will be informed during the capability discovery that the connection
is possible, the connection is secure and the capabilities of the servers can
be processed (version check). If any of those steps fail, the user will be
guided to the manual account setup. Once the capabilities are available the
application can compile user-controls based on the providers definitions. The
user has a Action that resets to the Providers default values.

.. image:: /images/account-autoconfig-configure-provider.png


Update Provider Capabilities
----------------------------

The application will store a cache of the providers capabilities to allow fast
connection and check those capabilities as the user configured (see
:ref:`Preferences<features-preferences>`). When the capabilities change the
user is guided to the :ref:`Account Configuration <features-accounts>` to
review the changes. The provider may choose to update the template
configuration. When no customizations on the template attributes are
available, the application will ?silently? update the template and apply the
users UI choices on it, otherwise the user is prompted to reset the
customizations or review them in the :ref:`Advanced Account Configuration
<features-accounts-config>`.

.. image:: /images/accounts-provider-updated.png


Errors
------

It is possible that in the process errors occur. Those are:

* Remote errors
  (HTTP 400,500) - The provider does not respond as expected. The user needs
  to visit on the providers website for more information and guides how to
  setup an account.
* SSL errors
  The connection cannot be secured.  The stored certificate (tls-pin) does
  not match.  The user needs to visit on the providers website for more
  information and guides how to setup an account.
* Version errors
  The provider capabilities file is invalid or of a incompatible version. The
  user needs to visit on the providers website for more information and guides
  how to setup an account.
