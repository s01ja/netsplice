.. _`features-credential`:

Credentials
===========

.. begin-summary

When a account requires credentials to connect, a credential dialog is shown.
This dialog allows to enter a username and a password and allows the user
to define how this values are handled.


.. image:: /images/credential.png

.. end-summary

Storage
-------

The default is that the application does not store the credentials and forgets
them as soon as they are used. Remember for the session keeps the values in
memory, so they can be reused when the account needs to reconnect but dont
write them to the disk. When the credentials should be stored the OS keystore
is used. The keystore is a operatingsystem provided store that should be used
when the host system is trusted. The preferences option
``config.backend.STORE_PASSWORD_DEFAULT`` configures the preselection of the
dialog.

When the log contains messages about 'OS Keystore not available', then it is
possible that the crypto components of the virtualenv are not built, recreate
the virtualenv and review the output during wheel build. This happens for users
of the source package.
