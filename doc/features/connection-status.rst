.. _`features-connection-status`:

Connection Status
=================

.. begin-summary

When a Account is used to setup and connect, it creates a internal Connection
that is used to monitor the progress and allows Netsplice to handle events.
The major progress is highlighted using colors along with descriptive
messages.

.. image:: /images/connection-status.png

.. end-summary

Status details
--------------

The details are consistently used in various locations in the GUI. The colors
may differ based on the theme set in the preferences.

Colors
~~~~~~

The colors are controlled by the selected theme.

* None: Disconnected
* Red: Failed connection
* Orange: Connecting
* Green: Connected

Details
~~~~~~~

* The current interface velocity
* The assigned IP-address

The details depend on the connection type.

Messages
~~~~~~~~

* Connected
* Connecting
* Disconnected

Account Hierarchy
-----------------

The accounts configured will be configured to a hierarchy. The hierarchy may
be a :ref:`Group<features-accounts-group>` or
a :ref:`Chain<features-accounts-chain>`.

Groups are displayed in a collapsable box, chains as indented boxes.

.. image:: /images/connection-status-groups.png

.. image:: /images/connection-status-chains.png

Read more about :ref:`Groups<features-accounts-group>`
 :ref:`Chains<features-accounts-chain>`.
