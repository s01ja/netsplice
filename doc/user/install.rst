.. _install:

Installation
============

This part of the documentation covers the installation of Netsplice.

.. _download:

Download
========

Get the latest installers, and their matching signatures at `the downloads
page <../downloads>`_.

.. _standalone-bundle:

Standalone bundle
-----------------

The quickest way of running Netsplice is using an installer for the platform.


Signature verification
^^^^^^^^^^^^^^^^^^^^^^

To verify the signature of the downloaded file put both, the download
and its signature in a directory and use ::

    $ gpg --verify netsplice-DOWNLOADED-FILE.asc

Compare sign-key fingerprint with the fingerprint of the public key
of the website.

Checksum verification
^^^^^^^^^^^^^^^^^^^^^

To verify the checksum of the downloaded files create the checksum
with the tool at hand (md5sum, shasum et al) and compare the result
with the contents of the checksum file.

.. code-block:: console

    $ md5sum -c netsplice-DOWNLOADED-FILE.checksum
    $ shasum -c netsplice-DOWNLOADED-FILE.checksum

Debian Installation
-------------------

.. include:: ../pkg/debian.rst
  :start-after: begin-user-installation
  :end-before: end-user-installation

.. include:: ../pkg/debian.rst
  :start-after: begin-hints
  :end-before: end-hints


OSX Installation
----------------

.. include:: ../pkg/osx.rst
  :start-after: begin-user-installation
  :end-before: end-user-installation

Ubuntu Installation
-------------------

.. include:: ../pkg/ubuntu.rst
  :start-after: begin-user-installation
  :end-before: end-user-installation

Windows Installation
--------------------

.. include:: ../pkg/win.rst
  :start-after: begin-user-installation
  :end-before: end-user-installation


Sourcecode
----------

For the users that can find their way through python packages,
the code is in the `source packages`_:

.. _`source packages`: https://www.ipredator.se/netsplice#client_developers

.. include:: ../pkg/source.rst
  :start-after: begin-user-installation
  :end-before: end-user-installation
