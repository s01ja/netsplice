.. _`plugins-api`:

Plugin API
==========

Read the :ref:`Plugin Developer Description<developer-plugins>` to learn how a
plugin is loaded and about the way it should be structured.

.. _plugins-api-interfaces:

Interfaces a Plugin needs to Implement
--------------------------------------

In the plugin root module ``register(app, component)`` needs to be implemented.

Each plugin has to provide a ``register`` function that takes two parameters:

.. code-block:: python

    def register(app, component):
        '''
        Register.

        Register the components of component with the given app instance.
        '''

The app is the initialized ``util.ipc.application``. It is used to allow the
register code of the plugin to extend existing models.

The component is one of the following:

  * backend

    extensions to the NetspliceBackendApp. Use it to add ``backend.connection``
    implementations and ``backend.preferences`` extensions.
  * gui

    extensions to the NetspliceGuiApp. Use it to add ``account_edit`` dialogs
    and to extend the ``gui.preferences``.
  * privileged

    extensions to the NetsplicePrivilegedApp. Use it to add
    :ref:`REST Modules<modules>` with dispatchers and controller endpoints.
  * util

    extensions to the netsplice.util module. Use it to extend the
    ``util.parser`` implementation.


Components a Plugin can access
------------------------------

backend, gui, privileged, util are components that are requested by the
specific apps. The plugin should only register for components it requires.

Interfaces a Plugin can access
------------------------------

Backend Connections
~~~~~~~~~~~~~~~~~~~

Register a connection type that implements the
``backend.connections.connection`` interface and handle the connect/disconnect
requests from the gui in the required manner.


Preferences Model
-----------------

Register a plugin for storing additional user-configurable values. Those values
will show up in the Preferences Dialog. Register a class that implements the
``backend.preferences.model.plugin_item`` interface. To have the preference
accessible in the gui, also implement and register the class in the ``gui``
component.

Preferences may also be implemented for a collection so each instance of that
collection can have a user-configurable value. Collections that have the
``plugins`` subcollection can register a ``plugin_collection_item_model`` using
the ``plugins.register_collection_plugin`` method.

The plugin needs to register request and response models when the values need to
be modified by the gui. The collection and main preferences expose the request
and response with a plugin subcollection to register.

Example to register a general plugin preference that can be modified in the
preference dialog:

.. code-block:: python

    ## ... in example_plugin/backend/preferences/model/plugin.py

    from netsplice.model.plugin_item import (
        plugin_item as plugin_item_model
    )
    from netsplice.util.model.field import field


    class plugin(plugin_item_model):
        def __init__(self, owner):
            plugin_item_model.__init__(self, owner)

            self.plugin_value = field(
                required=False,
                validators=[])

    ## ... in example_plugin/backend/preferences/model/response/plugin.py
    from netsplice.model.plugin_item import (
        plugin_item as plugin_item_model
    )
    from netsplice.util.model.field import field


    class plugin(plugin_item_model):
        def __init__(self):
            plugin_item_model.__init__(self, None)

            self.plugin_value = field(
                required=False
                validators=[])


    ## ... in example_plugin/backend/model/__init__.py.register(app):

    model = app.get_module('preferences').model
    model.plugins.register_plugin(
        PLUGIN_NAME, plugin_model(model))
    model.response.plugins.register_plugin(
        PLUGIN_NAME, response_plugin_model())

    ## To Receive the preferences in the GUI similar models have to be defined
    ## and registered

    ## ... in example_plugin/gui/model/response/plugin.py
    ## ... in example_plugin/gui/model/__init__.py.register(app):

    model = app.get_model().preferences
    model.plugins.register_plugin(
        PLUGIN_NAME, plugin_model(model.plugins))
    model.response.plugins.register_plugin(
        PLUGIN_NAME, response_plugin_model())

This code allows to access the configuration of the plugin with the REST
endpoints for preferences:

.. code-block:: python

    GET /module/preferences/plugin/example_plugin
    {
    "name": "example_plugin",
    "plugin_value": "some-value"
    }
    GET /module/preferences/plugin/example_plugin/attribute/plugin_value
    {
    "value": "some-value"
    }
    PUT /module/preferences/plugin/example_plugin/attribute/plugin_value
    {
    "value": "new-value"
    }


In the gui code of the plugin the gui-dispatcher always reads the preferences
and exposes them in the preferences object:

.. code-block:: python

    application.get_model().preferences.plugins.find_by_name('example_plugin')


To update a preference outside of the preference dialog:

.. code-block:: python

    backend.preferences_plugin_set.emit(
        plugin_name, field_name, attribute_value,
        signal_done, signal_failed)


Example to register a instance plugin preference that can be modified in the
gui:

.. code-block:: python

    ## ... in example_plugin/backend/preferences/model/plugin_collection.py

    from netsplice.model.plugin_collection_item import (
        plugin_collection_item as plugin_collection_item_model
    )
    from netsplice.util.model.field import field


    class plugin_collection(plugin_collection_item_model):
        def __init__(self, owner):
            plugin_collection_item_model.__init__(self, owner)

            self.plugin_value = field(...)

    ## ... in example_plugin/backend/preferences/model/request/plugin_collection.py
    ## ... in example_plugin/backend/preferences/model/response/plugin_collection.py

    ## ... in example_plugin/backend/model/__init__.py.register(app):

    model.plugins.register_collection_plugin(
        PLUGIN_NAME, plugin_collection_model(model))
    model.response.plugins.register_plugin(
        PLUGIN_NAME, response_plugin_collection_model())
    model.request.plugins.register_plugin(
        PLUGIN_NAME, request_plugin_collection_model())

    ## To Receive the preferences in the GUI similar models have to be defined
    ## and registered

    ## ... in example_plugin/gui/model/plugin_collection.py
    ## ... in example_plugin/gui/model/request/plugin_collection.py
    ## ... in example_plugin/gui/model/response/plugin_collection.py

    ## ... in example_plugin/gui/model/__init__.py.register(app):

    model.accounts.plugins.register_collection_plugin(
        PLUGIN_NAME, plugin_collection_model(model.accounts))
    model.accounts.request.plugins.register_plugin(
        PLUGIN_NAME, request_plugin_collection_model())
    model.accounts.response.plugins.register_plugin(
        PLUGIN_NAME, response_plugin_collection_model())


This code allows to access the configuration of the plugin with the REST
endpoints for preferences:

::

    GET /module/preferences/plugin/example_plugin/collection/accounts/instance_id/${account_1_id}
    {
    "name": "example_plugin",
    "plugin_value": "some-value"
    }
    GET /module/preferences/plugin/example_plugin/collection/accounts/instance_id/${account_1_id}/attribute/plugin_value
    {
    "value": "some-value"
    }
    PUT /module/preferences/plugin/example_plugin/collection/accounts/instance_id/${account_1_id}/attribute/plugin_value
    {
    "value": "new-value"
    }

    # for complex fields (lists etc) a valid json of the complete
    # plugin_collection is used:
    PUT /module/preferences/plugin/example_plugin/collection/accounts/instance_id/${account_1_id}
    {
    "name": "example_plugin",
    "plugin_value": "new-value"
    }



Event Loop
----------

Register a plugin for handling events in the desired application. The plugin
module needs to implement ``process_events`` as ``@gen.coroutine``. To register
call ``app.get_event_loop().register_plugin_module``.

Example for a Backend Event loop:

.. code-block:: python

    # ... in plugin/example_plugin/backend/event_plugin_module.py

    from tornado import gen

    import netsplice.backend.event.origins as origins
    import netsplice.backend.event.types as types
    from netsplice.util import get_logger
    from netsplice.util.errors import NotFoundError

    from netsplice.plugins.example_plugin.config.events import EVENT_NAMES

    logger = get_logger()


    class event_plugin_module(object):
        '''
        Event loop for the backend.

        Requires modules registered and waits for model-change on that modules
        to process events.
        '''

        def __init__(self, application):
            '''
            Initialize members.

            Requires the user to set application and add modules.
            '''
            self.application = application

        @gen.coroutine
        def process_event(self, event_model_instance):
            '''
            Process the event in the event_model_instance.

            Evaluate the event-type, name and origin and call handler functions.
            '''
            event_name = event_model_instance.name.get()
            event_type = event_model_instance.type.get()
            event_origin = event_model_instance.origin.get()
            event_data = event_model_instance.data.get()

            if event_type == types.NOTIFY:
                if event_origin == origins.BACKEND:
                    if event_name in EVENT_NAMES:
                        # code that handles the plugin events
                        # yield a sub-co-routine

    # ... in plugin/example_plugin/backend/__init__.py.register(app):

    event_loop = app.get_event_loop()
    if event_loop is not None:
        event_loop.register_plugin_module(event_plugin_module(app))


Gui Account Edit
----------------

Extensions to the Acount Editor widget need a ``account_type`` along with a
``label`` and a ``(QWidget, abstract_account_type_editor)`` implementation that
is displayed when the user selects the label in the account-edit. It is possible
to inherit other account_type_editor's to define fine-tuned presets. To register
call ``netsplice.gui.account_edit.type_editor_factory.register_account_type``
with your implementation.

Main Menu
---------

To add entries to the main menu register a callback with label, shortcut and
desired location. ``netsplice.gui.register_menu_item`` will create a list of
menu items that will be inserted to the main menu when the application starts.
The ``register_menu_item`` functions allows to add items in any of the main
submenus before the last 'separator'. When no separator is available the item
will be added to the end. The developer may set ``separator=True`` and specify
the ``before='objectNameString'``.


GUI Plugins
~~~~~~~~~~~

Gui plugins likely define resources that need to be compiled by the packaging
scripts. By placing qrc and ui files in the plugin, they will be compiled using
the ``setup.py build_gui`` script. ui-files will be loaded by the widgets
of the plugin. Some resources need to be known by the core in order to make
a useful presentation. The plugin needs to load the compiled 'icons_rc' and
call ``netsplice.gui.register_account_type_pixmap`` with the resource-path
provided by the resource definition.

App Plugins
~~~~~~~~~~~

Extensions to the Apps should be done with ``app.add_module`` with their own
endpoints and models. The :ref:`Module Interface <modules>` should be sufficient
for the majority of extensions to the Net, Privileged and Unprivileged Apps.

Example for a App module:

.. code-block:: python

    # ... plugins/example_plugin/unprivileged/example_plugin/__init__.py

    from netsplice.util.ipc.route import get_module_route
    from model import model as module_model

    name = "example_plugin"

    endpoints = get_module_route(
        'netsplice.plugins.example_plugin.unprivileged',
        [
            (r'/module/example_plugin/example/',
                'example'),
        ])

    model = module_model()

    # ... plugins/example_plugin/unprivileged/example_plugin/example_controller.py


    from netsplice.util.ipc.middleware import middleware
    from example import example as example_dispatcher


    class example_controller(middleware):
        '''
        '''
        def __init__(self, request, response):
            middleware.__init__(self, request, response)

        def get(self):
            try:
                example = example_dispatcher()
                response_model = response_example_model()
                response_model.from_json(example.get_something().to_json())
                self.write(response_model.to_json())
                self.set_status(200)
            except ValidationError:
                self.set_status(400)
            except NotFoundError:
                self.set_status(404)
            self.finish()

        def post(self):
            request_model = request_example_model()
            try:
                request_model.from_json(self.request.body.decode('utf-8'))
                example = example_dispatcher()
                example.do_something(request_model.attribute1.get())
                self.set_status(204)
            except ValidationError, errors:
                logger.error(errors)
                self.set_status(400)
            except NotFoundError:
                self.set_status(404)
            except Exception:
                self.set_status(400)
            self.finish()


    # ... plugins/example_plugin/unprivileged/example_plugin/example.py

    class example(object):
        def __init__(self):
            self.some_marshalable = a_model_you_define()

        def do_something(self, validated_attribute):
            pass

        def get_something(self):
            return self.some_marshalable

    # ... plugins/example_plugin/unprivileged/example_plugin/model/*

    model, including request and response.

    # ... plugins/example_plugin/unprivileged/__init__.py.register(app):
    import example_plugin as example_plugin_module

    app.add_module(example_plugin_module)


When this code is active, the unprivileged backend exposes the configured
endpoint ``/module/example_plugin/example`` and passes the requests to the
controller. To use the plugin in the backend, it needs to get registered there
too:

.. code-block:: python

    # ... plugins/example_plugin/backend/dispatcher_endpoints.py

    EXAMPLE_PLUGIN_EXAMPLE = (
        r'module/example_plugin/example')

    # ... plugins/example_plugin/backend/unprivileged_dispatcher.py

    from socket import error as socket_error

    from tornado import gen, httpclient

    import dispatcher_endpoints as endpoints
    # import request & response models
    from netsplice.util import get_logger
    from netsplice.util.model.errors import ValidationError

    logger = get_logger()


    class unprivileged_dispatcher(object):

        def __init__(self, service_instance):
            self.service = service_instance

        @gen.coroutine
        def do_something(self):
            try:
                request_model = request_example_model()
                # fill request_model from parameters
                response = yield self.service.post(
                    endpoints.EXAMPLE_PLUGIN_EXAMPLE,
                    request_model.to_json()
                )

                raise gen.Return('')
            except gen.Return, return_value:
                raise return_value
            except socket_error, errors:
                raise ServerConnectionError(str(errors))
            except ValidationError, errors:
                raise errors
            except ValueError, errors:
                raise ValidationError(str(errors))
            except httpclient.HTTPError, errors:
                raise ServerConnectionError(str(errors))

        @gen.coroutine
        def get_something(self):
            try:
                response = yield self.service.get(
                    endpoints.EXAMPLE_PLUGIN_EXAMPLE, None)

                response_model = response_example_model()
                response_model.from_json(response.body)

                raise gen.Return(response_model)
            except gen.Return, return_value:
                raise return_value
            except socket_error, errors:
                raise ServerConnectionError(str(errors))
            except ValidationError, errors:
                raise errors
            except ValueError, errors:
                raise ValidationError(str(errors))
            except httpclient.HTTPError, errors:
                raise ServerConnectionError(str(errors))

    # ... plugins/example_plugin/backend/__init__.py.register(app):

    unprivileged_dispatcher_instance = app.get_unprivileged()
    unprivileged_dispatcher_instance.process_sniper = unprivileged_dispatcher(
        unprivileged_dispatcher_instance)

Now the plugin may use the registered dispatcher functions:

.. code-block:: python

    valid_response_model = yield self.application.get_unprivileged().example.get_something()
    yield self.application.get_unprivileged().example.do_something()

Profile Plugins
~~~~~~~~~~~~~~~

Profile plugins are used to extend the account with a preset. The most obvious
preset is a configuration that is set in the account when a profile is
selected. Profile plugins register with the
gui.account_edit.type_editor_factory. They can register for a existing
account_type and provide a account_label along with a description and the
initial config.

.. code-block:: python

    from netsplice.gui.account_edit.type_editor_factory import (
        register_account_profile as register_account_profile
    )

    ...

    register_account_profile(
        ACCOUNT_TYPE,
        ACCOUNT_LABEL,
        DEFAULT_CONFIG,
        DESCRIPTION)

There are optional parameters for the profile that allow the plugin to control
if the config will provide a default_route and what launcher and sniper rules
it creates when the profile is selected. The launcher may be one of the
provided templates identified by its executable path or a custom template
provided as a dictionary:

.. code-block:: python

    from netsplice.plugins.process_launcher.config import (
        FIELD_EVENT_NAME, FIELD_ELEVATED, FIELD_EXECUTABLE,
        FIELD_WORKING_DIRECTORY, FIELD_PARAMETERS, FIELD_ID
    )
    from netsplice.config import connection as config_connection

    launcher = [{
        FIELD_EVENT_NAME: config_connection.CONNECTED,
        FIELD_ELEVATED: False,
        FIELD_EXECUTABLE: '/usr/bin/firefox-bin',
        FIELD_PARAMETERS: [],
        FIELD_WORKING_DIRECTORY: '',
        FIELD_ID: uuid_firefox
    }]

When the field_id is provided, it can be referenced by a process_sniper rule:

.. code-block:: python

    sniper = [
        {
            FIELD_EVENT_NAME: config_connection.DISCONNECTED,
            FIELD_SIGNAL: 'SIGTERM',
            FIELD_COMMANDLINE: uuid_firefox
        }
    ]

    ...

    register_account_profile(
        ACCOUNT_TYPE,
        ACCOUNT_LABEL,
        DEFAULT_CONFIG,
        DESCRIPTION,
        default_route=True,
        process_launcher=launcher,
        process_sniper=sniper)

When providing the profile, keep in mind that netsplice is a multiplatform
application, so the launchers should not hardcode a specific binary path as
they differ on the platforms.
