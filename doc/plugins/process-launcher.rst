.. _`plugins-process-launcher`:

Process Launcher
================

.. begin-summary

Launch any command when the connection state changes. Launch as current user
and elevated is supported. The command may have parameters and can be started
in a different working directory.

.. end-summary

VPNs are used to secure activity that a user would not execute in the open
internet.  An IRC chat client will hold as example here.  With the VPN the chat
client connects through a tunnel and the IRC server sees the VPN providers IP
connecting.  When the VPN connection is connected by the user the application
may start.

The Process Launcher automates this behavior: It registers for the events on
certain connection profiles and starts applications when the event occurs.
The user can configure the path of the application with commandline parameters
to be launched when the connection state changes. To shut down the application
when the VPN is disconnected the
:ref:`Process Sniper Plugin<plugins-process-sniper>` may be used.

.. image:: /images/plugins-process-launcher.png

Edit Connection Profile
-----------------------

Edit a Connection Profile reveals the launcher rules. Right-click on a
(disconnected) Connection and select the 'Plugin: Process Manager' tab. The
upper-section of the manager is used to define applications that are launched.

The rules may be ``active`` and ``elevated``. Active rules are executed when the
connection state changes, inactive rules are not. Elevated rules have
limitations, that restrict the user to select from a predefined set of scripts
instead of all available applications.

Each rule has a numeric id that can be referenced in the
:ref:`Process Sniper Plugin<plugins-process-sniper>`.
The id is a integer for convenience, internally a uuid is stored.

Insert
------

Insert a rule for the launcher. Each Rule is associated with a connection event
(connected, disconnected, connecting) a executable with parameters and a working
directory. With a click on the ``Insert`` button a template select dialog opens.
This dialog allows the user to select a general, not prefilled template or a
template that presets the following the rule dialog with defaults for a new
rule. After selecting a executable, the dialog can be closed with the
``Create`` Button that is only available when the configuration is valid..

New rules are active by default and act on the ``connected`` event.

Edit
----

The launcher is able to process multiple rules for a connection. Each rule can
be edited using a double click or by pressing the ``Edit`` button in the toolbar.
A new window appears that allows modifying the rule by choosing from combo boxes
and select the process from the list of currently running processes.

.. image:: /images/plugins-process-launcher-edit.png

.. image:: /images/plugins-process-launcher-edit-elevated.png

Remove
------

Removes the selected Rule.

Test
----

Allows to test the rule by executing the configured application. Tests work only
for normal applications. Elevated applications cannot be tested and need to use
the debug feature of the scripts instead.

For the test no additional environment variables are passed to the application.

.. image:: /images/plugins-process-launcher-test.png

Rule Type
---------

There are two types of processes that can be launched. One is a simple
application that does not care who started it - like firefox or a chat client.
The others are privileged scripts that need to be placed in a specific directory
and are called with relevant information from the connection.


Simple
~~~~~~

A simple application has a executable path, a working directory and a list of
static parameters. Firefox as example has ```firefox-bin``` with the working
directory ```/home/my_user``` and the parameter list
```-Profile news -no-remote``` The application is launched with the privileges
of the NetspliceGuiApp, that is usually the current logged in user.

There are different assumptions for the supported operating systems.

On Linux every file that has the executable bit set can be executed. There is no
support for the various ```.desktop``` or ```.application``` launchers. When the
application that is launched forks or is a launcher-script, then the sniper
module cannot find the correct pid.

On macOS the ```.app``` directories are supported together with the linux/unix
executable bit limitations. Appstore / Native applications can usualy be
launched and killed, but they sometimes need additional configuration to avoid
that they start with the OS (reboots after system updates).

On Windows ```.exe``` files are supported along with the common ```.cmd, .bat,
.ps1, .ps2``` extensions. Powershell scripts need advanced windows
configuration. Applications in windows have the habit to provide launcher-
executables and change their pid and to setup autostart launchers as well.

The ID of a simple rule may be referred in the
:ref:`Process Sniper Plugin<plugins-process-sniper>`. As the assumptions hint,
the method to detect the processes pid is 'not-so-safe' and should be checked
with the configured application.

Privileged
~~~~~~~~~~

A privileged application is a script or application in the Netsplice
installation path. This application is executed with the privileges of the
NetsplicePrivilegedApp. A set of environment variables is defined that the
application may access.

Privileged or elevated scripts do not take parameters. They get executed with a
set of environment variables that are provided by the connection (eg OpenVPN
exposes multiple environment variables that give information about the external
ip-address, the routes and the suggested DNS servers.)

To write a script that acts on the connection, create a dummy first that stores
the environment in a temporary file and look in the existing scripts.

The privileged scripts that are provided with the installation are examples for
common tasks. The content of those scripts should be reviewed by the user as the
commands are executed with root privileges. Some of them should be in the python
application, exposed as api to avoid logic in bash scripts. When there are
use-cases, please expose us so we can integrate them.
Read more in the
ref:`Process Launcher Plugin Developer Documentation<developer-plugin-process-launcher>`

Events
------

The events that are supported by the process launcher are

account_connecting
~~~~~~~~~~~~~~~~~~

This event happens before the connection is established or when it reconnects
from a ping-timeout (eg suspended machine) or network connectivity issue (wifi
network change).

account_connected
~~~~~~~~~~~~~~~~~

This event happens when the connection has been established.

account_disconnected
~~~~~~~~~~~~~~~~~~~~

This event happens when the connection is shut down by the user or by system
events (eg openvpn executable was killed). When Netsplice is shutdown, all
active connections will receive a disconnect event.
