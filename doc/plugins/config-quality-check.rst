.. _`plugins-cqc`:

Config Quality Check
====================

.. begin-summary

Analyze the config for common pitfalls and broken settings. Provides hints
how to enhance stability, performance and security.

.. end-summary


General
-------

Config Quality Check uses the account configuration values to check all
properties with known patterns. Every time the configuration is changed by the
user, the check is re-run. The result of the check is displayed in a formated
report in the account settings.

The plugin is a GUI only extension of Netsplice. The rules, parsing and
formatting is implemented in a separate project to make the knowledge in the
plugin available for other uses.


Features
--------

The plugin checks for common configuration issues in OpenVPN, Tor and OpenSSH
connection configurations. The most comprehensive check is the OpenVPN check.
It contains about 40 checks that rate the config positive, negative or broken.
There are also informative and paranoid hints.


Limitations
-----------

The Config Quality Check does not interfere with the account configuration.
This means that configurations that the check considers broken will still work.
An auto correction of the configuration is not planned.
