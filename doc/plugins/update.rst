.. _`plugins-update`:

Update
======

.. begin-summary

Security software needs regular updates or at least a method to notify the user
that the algorithms they use are out of date. Without a notification the
software has known attack vectors. The many components of a VPN client require
recent components. there are regular updates on the SSL implementation and to
the client components Tor and OpenVPN. When new features are implemented they
might be worth checking out for a better user experience.


.. end-summary

Updates have the problem that they leak information to the provider that

  * a specific software is used
  * a version of that software is running
  * the user is about to download and install a application package

Every operating system has its own method to securely update a component. Linux
distributions have a multitude of yum, deb, portage repositories that can be
configured to download and deploy. On OSX the application replaces itself with
elevated privileges. Windows requires the installer to be run or patch the
binary with the tool chain available.

A user may expect a transparent one-click streamlined process for a update. The
main reason to start the software is to use its functions, uninterrupted.

Taking this into account, the update mechanism for Netsplice is designed to
minimize the effort to keep the crypto- and network components up to date.

A internal counter is maintained that checks its backend store to evaluate the
'age' of the software. With a certain age, the software asks the user to check
for updates (like some os may ask for backups). The update check has to be
triggered manually to prevent a information leak and allow to use a 'real
outdated' (maybe read only) version without letting us know. This may be opted
in to 'always check on start' with the default network configuration.

The automatic or manual check requests the Network backend component to request
and evaluate a configurable update server's response. The response that is
evaluated by the network process may be of any source. Netsplice implements
metalink format parsing that is basically some XML that contains versions and
download URL as well as the signatures. When the backend receives that
response, it decides how to notify the user. The notification may be done with
a toast message, a status bar hint or a descriptive confirm dialog.

As updating the software is a delicate process (review new code or snapshot
configuration information) the update information is not blocking the main
functionality. The 'there is a update' notification results in a changed
application icon and a hyper link to instructions how to update.

No automatic update of installed executables is implemented.


Configuration
-------------

The Preferences dialog has a *Update* tab to change several update related
options:

  * Enable / Disable (Enabled by default)

      - Enabled: The update_check routine is scheduled (last_check +
        maximum_age) that runs at least the minimal local check.
      - Disabled: No routine is scheduled, a existing scheduled routine is
        unscheduled.
  * Enable / Disable remote auto check (Disabled by default)

      - Enabled: When the update_check routine detects a out of date local
        version, the remote is requested with the active routed network.
      - Disabled: No remote request is made. The user only gets a update-
        suggest notification


  * Maximum check age (days)
    The age (in days) until the backend schedules the next update_check

  * Last check (time stamp)
    Date of the last successful request to the update_url.
  * Latest known version String that has been acquired by a successful request
    to the update update_url
  * Update URL that is used to evaluate the version.
    The domain will be resolved using the OS dns, the request may be proxied.
  * Update Signature URL that is used to evaluate the authentic content of the
    update.
    The domain will be resolved using the OS dns.
  * Update Host TLS Fingerprint that is used to check the authentic certificate
    of the remote host.
  * Update signature public key that is used to verify the signatures of remote
    files.


Workflow
--------

With a age of (4h * 3600s), when the last check was at the 2017-01-01T00:00Z
and the local time is 2017-01-01T03:00Z, the check will not have been run. The
check will run when the application is active for at least another hour or if
it is started after that time.

With a age of (4h * 3600s), when the last check was at the 2017-01-01T00:00Z
and the local time is 2017-01-01T04:01Z, the check will have been run once and
from now on at every application start.

With a successful request to the update_url, the date of the last_check will be
updated. This request may be issued from the UI (check now action) or by the
configured auto


UI
--

The main menu has a entry that checks for updates. This opens a dialog that
displays the application version, the application age, the check age and a
'Check Now' button. When a new version exists, the change log is displayed
along with hyper-links where to download the update.

When the application starts and the maximum check age was reached, a gui state
is toggled. The qt-gui receives that toggle change and displays a notification
'a check for updates should be executed'. The notification may be dismissed and
occurs on every application start from then on.

In addition to the notification the version string in the status bar is updated
with a exclamation mark and the application icon is changed.

The frontend will contain a dialog that displays the available information and
allows the user to trigger the update process.


Backend
-------

The backend maintains a interval to check for update ages. This interval is
scheduled from the application start. The interval may be stopped by disabling
and started by enabling the update flag in runtime. In each iteration the
current preferences are read and evaluated with the current time. When a
appropriate age is detected or the last known version is not equal to the
current version, the backend changes the gui-model to indicate that a update
check is suggested.

To check the remote service for a update, the backend instructs the network
backend to fetch and interpret the required data. When data is received and
processed, the backend updates the latest known version preference and returns
the gui-api request with the values.

To request the backend to perform a update check and return the results

**REST Request**::

    GET /module/update/check_now

**REST Response**::

    {
        "version": "1.2.3.4",
        "latest_known_version: "1.2.3.5",
        "changelog": "plain\ntext",
        "last": "utc-timestamp",
        "update_url_list": [
            {
                "os": "os-name",
                "url": "httpsurl",
                "signature": "base64 encoded openssl sha1 signature",
                "sha1": "sha1 checksum",
                "sha256": "sha256 checksum"
            }
        ]
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 403: Not allowed by preference
* 404: Remote URL does not exist
* 502: Remote Request Error (Bad Gateway, Timeouts)


To request the backend to perform a download of a os-specific package into a
selected directory.

**REST Request**::

    POST /module/update/download
    {
        "os": "os-name",
        "destination": "writable local path"
    }

**REST Response**::

    {
        "id": "uuid for GET status",
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 403: Not allowed by preference
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway, Timeouts)


To request the backend about a active download:

**REST Request**::

    GET /module/update/download/uuid

**REST Response**::

    {
        "active": "currently active download",
        "url": "url of download",
        "start_date": "YYYY-MM-DDTHH:MM:SSZ",
        "complete": numeric_value(0-100),
        "bps": numeric_value,
        "errors": true/false,
        "error_message": null/"message from backend",
        "signature_ok": null/true/false
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 403: Not allowed by preference
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway, Timeouts)


To request the backend about the current known updates without a remote
request. It returns the values currently stored in the preferences:

**REST Request**::

    GET /module/update/version_info

**REST Response**::

    {
        "version": "1.2.3.4",
        "latest_known_version: "1.2.3.5",
        "changelog": "plain\ntext",
        "last": "utc-timestamp",
        "update_url_list": [
            {
                "os": "os-name",
                "url": "httpsurl",
                "signature": "base64 encoded openssl sha1 signature",
                "sha1": "sha1 checksum",
                "sha256": "sha256 checksum"
            }
        ]
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 403: Not allowed by preference
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway, Timeouts)


Network
-------

* Http request of the remote URL.
* JSON processing.
* Response structure to api response model transformation.
* To be used by the backend.


To get the latest release from the network, the update/latest request can be
used.

**REST Request**::

    POST /module/update/latest
    {
        "update_url": "url_of_remote_file",
        "update_signature_url": "url_of_remote_file",
        "fingerprint": "tls fingerprint",
        "sign_key": "public key to verify signature"
    }

**REST Response**::

    {
        "version": "v1.2.3.4",
        "date": "YYYY-MM-DDTHH:MM:SSZ",
        "changelog": "plain\ntext",
        "update_url_list": [
            {
                "target_os": "os name for the update",
                "url": "url for the update",
                "signature": "base64encoded signature",
                "sha1sum": "sha1 checksum",
                "sha256sum": "sha256 checksum"
            }
        ]
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway)
* 504: Gateway timeout

To download a specific update from the server and check its signature, the
update/download request can be used. After the download completed and the
signature was verified, a event is sent to the plugin. During the download the
progress can be monitored with the update/download GET method. to cancel the
download the DELETE method may be used at any time.
This is part of the core backend-network functionality.

**REST Request**::

    POST /module/update/download
    {
        "url": "url for the update",
        "fingerprint": "TLS fingerprint for remote server",
        "signature": "expected signature for the download",
        "sign_key": "public key to verify signature",
        "sha1": "sha1 checksum",
        "sha256": "sha256 checksum"
    }

**REST Response**::

    {
        "id": "id of download in network backend"
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway)
* 504: Gateway timeout

**REST Request**::

    DELETE /module/download/{id}

**REST Response**::

    {
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway)
* 504: Gateway timeout


**REST Request**::

    GET /module/download/{id}

**REST Response**::

    {
        "active": "currently active download",
        "url": "url of download",
        "start_date": "YYYY-MM-DDTHH:MM:SSZ",
        "complete": numeric_value(0-100),
        "bps": numeric_value,
        "errors": true/false,
        "error_message": null/"message from backend",
        "checksum_ok": null/true/false,
        "signature_ok": null/true/false
    }

**Errors**::

* 400: Validation Error, Value Error (bad JSON)
* 404: Remote URL does not return
* 502: Remote Request Error (Bad Gateway)
* 504: Gateway timeout

Server
------

On the server side some efford must be taken in order to provide valid
resources that the update plugin can interpret. The first is a *version.json*
that has the expected structure:

**version.json**::

    {
        "version": "1.2.3.4",
        "date": "YYYY-MM-DDTHH:MM:SSZ",
        "changelog": "plain\ntext",
        "update_url_list": [
            {
                "target_os": "os name for the update",
                "url": "url for the update",
                "signature": "base64encoded signature",
                "sha1sum": "sha1 checksum",
                "sha256sum": "sha256 checksum"
            }
        ]
    }

This version needs to be signed and the signature has to be stored in a base64
encoded file with the same name plus a *.sig* extension.

To sign the update json use the following commands::

    $ openssl dgst -sha1 -sign /not/in/webroot/sign_key.pem \
      -out version.json.sigb version.json
    $ base64 -w0 version.json.sigb > version.json.sig
    $ rm version.json.sigb

The pkg/debian-build directory contains a sign_downloads script that can be
used to create the json-file with all required signatures.

Please take caution with the sign_key. It should not be stored in a web-
accessible location (or at the webserver at all) The public-key has to be
configured in the client(s) that have to verify the remote files.

The download library *requires* https and TLS certificate fingerprints. The
fingerprints are SHA256 hashes of the raw base64-decoded public key. This can
be problematic if your https certificate changes on a regular basis (e.g. with
letsencrypt). The new fingerprints have to be configured in the client(s) to
enable them requesting the update files at all.
