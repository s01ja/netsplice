.. _`plugins-statistics`:

Statistics
==========

.. begin-summary

When a VPN connection is active, users tend to question the performance of
the provider based on the services they access.  They visit bandwidth
testers and get unconclusive results that are hard to reproduce (bandwidth
test sites are often excluded from traffic shaping).  The Statistics view
will allow the user to measure the performance of the connection for
providers that support it.

.. end-summary

Ping
----

The simplest measurement is a ping sent to the providers gateway.  It allows
to extract the response time and the network latency.  It is also a very
easy verification that the client-provider connection is intact.

.. image:: /images/plugins-statistics-ping-gateway.png

The next measurement is a ping sent to any internet host that measures the
latency of the client-provider-host connection.

.. image:: /images/plugins-statistics-ping-google.png

The interface allows to enter any ip-address.  When any Account is
connected, Actions are available for each gateway of the connected accounts.

IPerf
-----

When the provider hosts a iperf service the bandwidth between client and
provider can be measured.  This is useful to identify artifical bottlenecks
(so called throtteling) introduced by Internet Service Providers (ISP).

.. image:: /images/plugins-statistics-iperf.png

XXX fix. The user interface allows to enter a host and a port where the iperf
server is located.  When the Provider has a iperf Server it can expose it
through a REST interface. This will be displayed as Actions for connected
Accounts.
XXX fix.
