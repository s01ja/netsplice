.. _testhowto:

Howto for Testers
=================

This document covers a how-to guide to:

#. :ref:`Where and how report bugs for Netsplice <reporting_bugs>`, and
#. :ref:`Quickly fetching latest development code <fetchinglatest>`.

Let's go!

.. _reporting_bugs:

Reporting bugs
--------------

Report all the bugs you can find to us! If something is not quite working yet,
we really want to know. Reporting a bug to us is the best way to get it fixed
quickly, and get our unconditional gratitude.

It is quick, easy, and probably the best way to contribute to Netsplice
development, other than submitting patches.

.. admonition:: Reporting better bugs

   New to bug reporting? Here you have a `great document about this noble art
   <http://www.chiark.greenend.org.uk/~sgtatham/bugs.html>`_.

Where to report bugs
^^^^^^^^^^^^^^^^^^^^

use `Github issues <https://github.com/XXX/XXX/issues>`_.

What to include in your bug report
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* The symptoms of the bug itself: what went wrong? What items appear broken, or
  do not work as expected? Maybe an UI element that appears to freeze?
* The Netsplice version you are running. You can get it by doing `netsplice
  --version`, or you can go to `Help -> About Netsplice` menu.
* The installation method you used: from source code? debian package?
* Your platform version and other details: Ubuntu 12.04? Debian unstable?
  Windows 8? OSX 10.8.4? If relevant, your desktop system also (gnome, kde...)
* When does the bug appear? What actions trigger it? Does it always
  happen, or is it sporadic?
* The exact error message, if any.
* Attachments of the log files, if possible (see section below).

Also, try not to mix several issues in your bug report. If you are finding
several problems, it's better to issue a separate bug report for each one of
them.

Attaching log files
^^^^^^^^^^^^^^^^^^^

If you can spend a little time getting them, please add some logs to the bug
report. They are **really** useful when it comes to debug a problem. To do it:

Launch Netsplice in debug mode. Logs are way more verbose that way::

  netsplice --debug

Get your hand on the logs. You can achieve that either by clicking on the
"Show log" button, and saving to file, or directly by specifying the path to
the logfile in the command line invocation::

  netsplice --debug --logfile /tmp/netsplice-$(date --rfc-3339=seconds).log

Attach the logfile to your bug report.

Need human interaction?
^^^^^^^^^^^^^^^^^^^^^^^

You can also find us in the ``#support`` channel on the our `IRC server`_. If
you do not have a IRC client at hand, you can `enter the channel via web`_.

.. _`IRC server`: irc:irc.ipredator.se:6667
.. _`enter the channel via web`: https://irc.ipredator.se

.. _fetchinglatest:

Fetching latest development code
---------------------------------

Normally, testing the latest :ref:`client bundles <standalone-bundle>` should
be enough. We are engaged in a two-week release cycle with minor releases that
are as stable as possible.

However, if you want to test that some issue has *really* been fixed before the
next release is out (if you are testing a new provider, for instance), you are
encouraged to try out the latest in the development branch. If you do not know
how to do that, or you prefer an automated script, keep reading for a way to
painlessly fetch the latest development code.

We have put together a script to allow rapid testing in different platforms for
the brave souls like you. It more or less does all the steps covered in the
:ref:`Setting up a Work Enviroment <environment>` section, only that in a more
compact way suitable (ahem) also for non developers.

.. note::

   At some point in the near future, we will be using :ref:`standalone bundles
   <standalone-bundle>` with the ability to self-update.

Testing the packages
^^^^^^^^^^^^^^^^^^^^

When we have a release candidate for the supported platforms, we will announce
also the URI where you can download the rc for testing in your system. Stay
tuned!

Testing the status of translations
----------------------------------

We need translators! You can go to `transifex
<https://www.transifex.com/projects/p/netsplice/>`_, get an account
and start contributing.

If you want to check the current status of Netsplice localization in a
language other than the one set in your machine, you can do it with a simple
trick (under linux). For instance, do::

    $ lang=es_ES netsplice

for running Netsplice with the Spanish locales.
